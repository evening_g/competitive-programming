// WA

#include<bits/stdc++.h>

using namespace std;

string dec2bin(long long n, long long size) {
    string result = "";

    while (n > 0) {
        if (n % 2 == 0) {
            result += "0";
        } else {
            result += "1";
        }
        n /= 2;
    }

    while (result.size() < size)
    {
        result += "0";
    }

    return result;
}

int main() {
    cin.tie(nullptr);
    cin.sync_with_stdio(0);

    long long AU;
    cin >> AU;

    vector<char> sieve(AU+1, 0);
    for(long long i=2; i<AU; i++){
        if(sieve[i]) continue;
        for(long long j=i*2; j<=AU; j+=i){
            sieve[j] = 1;
        }
    }

    vector<long long> asteroids;
    for(long long i=2; i<=AU; ++i){
        if(!sieve[i]) asteroids.push_back(i);
    }

    long long total = 1;
    for(int i=0; i<asteroids.size(); i++){
        long long options = 0;
        for(int j = i+1; j<asteroids.size() && asteroids[j] - asteroids[i] < 14; ++j){
            options ++;
        }
        total += options;
    }

    cout << total << '\n';
    

    return 0;
}
