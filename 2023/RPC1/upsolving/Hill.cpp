// AC

#include<iostream>
#include<stack>

using namespace std;

using i64 = long long;

struct Hill
{
    // Altura de la montaña
    int height;
    // Número de montañas a la izquierda que
    // se eliminaron al agregar esta montaña
    i64 removed;
};

int main() {
    cin.tie(nullptr);
    cin.sync_with_stdio(0);

    stack<Hill> hills;
    
    int n, current_height;
    cin >> n;

    i64 total = 0;
    // Agregar una montaña de tamaño 'infinito'
    // porque nunca será eliminada y así no tendremos
    // que checar si la pila está vacía
    hills.push({INT32_MAX, 0});

    // Por cada montaña
    for (int i = 1; i <= n; i++)
    {
        cin >> current_height;

        // Eliminar todas las montañas anteriores que sean
        // más pequeñas
        i64 current_removed = 0;
        while (hills.top().height < current_height) {
            // 1 porque eliminamos esta montaña
            // hills.top().removed porque debemos contar
            // las montañas que eliminó la montaña anterior
            current_removed += hills.top().removed + 1 ;
            hills.pop();
        }

        // Si se puede hacer un puente, lo contamos
        if (hills.top().height == current_height) {
            total += current_removed;
        }

        hills.push({current_height, current_removed});
    }
    
    cout << total << '\n';

    return 0;
}
