/**
 * @file Organ.cpp
 * @brief Problem G Organ Free-man
 * @date 2023-03-11
 * 
 */

#include<bits/stdc++.h>

using namespace std;

using i64 = long long;
using i32 = int;
using i16 = short;
using u64 = unsigned long long;
using u32 = unsigned int;
using u16 = unsigned short;

// Factorials [0, 9]
const u64 fact [] = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};

u64 f(u64 x) {
    if (x <= 9) {
        return fact[x];
    }
    return fact[x%10] + f(x/10);
}

u64 min_diff(u64 y) {
    u64 d = fact[9];

    for (int i = 9; i >= 0; i--)
        if (fact[i] < y)
            return y - fact[i];

    return 0;
}

int main() {
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    u64 y;

    cin >> y;

    u64 d = min_diff(y);

    for (u64 i = 0; i < y; i+=1000)
    {
        cout << i << " = " << f(i) << '\n';
    }
    

    return 0;
}