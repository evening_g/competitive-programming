// WA

#include<bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    cin.sync_with_stdio(0);
    
    int n;
    cin >> n;
    int total = 0;
    
    map<int, int> positions;
    
    for(int i=0; i<n; i++){
        int x;
        cin >> x;
        
        // buscar
        auto pos_x = positions.find(x);
        if(pos_x != end(positions)){
            total += (i-1-positions[x]);
            positions.erase(begin(positions), pos_x);
        } else if (positions.upper_bound(x) == end(positions)){
            positions.clear();
        }

        positions[x] = i;
    }
    


    cout << total << '\n';

    return 0;
}
