// TLE

#include<bits/stdc++.h>

using namespace std;

string dec2bin(long long n, long long size) {
    string result = "";

    while (n > 0) {
        if (n % 2 == 0) {
            result += "0";
        } else {
            result += "1";
        }
        n /= 2;
    }

    while (result.size() < size)
    {
        result += "0";
    }

    return result;
}

int main() {
    cin.tie(nullptr);
    cin.sync_with_stdio(0);

    long long AU;
    cin >> AU;

    vector<char> sieve(AU+1, 0);
    for(long long i=2; i<AU; i++){
        if(sieve[i]) continue;
        for(long long j=i*2; j<=AU; j+=i){
            sieve[j] = 1;
        }
    }

    vector<long long> asteroids;
    for(long long i=2; i<=AU; ++i){
        if(!sieve[i]) asteroids.push_back(i);
    }

    long long size = asteroids.size()-2;
    long long limit = pow(2, size);

    if(asteroids.size() == 1){
        cout << 1 << '\n';
        return 0;
    }

    long long total = 0;

    for (long long i = 0; i < limit; i++)
    {
        string com = dec2bin(i, size);
        com = '1' + com + '1';

        bool valid = true;
        long long prev = 0;
        for(long long j = 0; j < asteroids.size(); j++){
            if(com[j] == '1'){
                if(asteroids[j] - asteroids[prev] > 14){
                    valid = false;
                    break;
                }
                prev = j;
            }
        }

        if(valid){
            total++;
        }
    }

    cout << total << '\n';
    

    return 0;
}
