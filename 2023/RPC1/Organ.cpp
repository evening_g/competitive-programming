// TLE

#include<bits/stdc++.h>

using namespace std;

#define ll long long

ll facts[] = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};

ll fact(int x) {
    return facts[x];
}

ll f(ll x) {
    if (x <= 9) {
        return fact(x);
    }
    return fact(x%10) + f(x/10);
}

ll solve(ll l, ll r, ll y) {
    if (l > r) return -1;

    ll m = (l+r)/2;

    ll x = f(m);

    if (x == y) {
        return x;
    }

    if (x > y) {
        return solve(l, m-1, y);
    } else {
        return solve(m+1, r, y);
    }
}

int main() {
    cin.tie(nullptr);
    cin.sync_with_stdio(0);

    ll y;
    cin >> y;

    ll x;

    ll i = 0;

    do
    {
        x = f(i);
        i++;
        //printf("f(%lld) = %lld\n", i, x);
    } while (x != y);
    

    cout << i-1 << '\n';    

    return 0;
}
