// WA

#include<bits/stdc++.h>

using namespace std;

int total = 0;

int get_distance(vector<char> &explored, vector<int> &terraces, int pos, int n){
    if(n < pos){
        return 0;
    }

    explored[pos] = 1;

    int dist = 1;
    while(terraces[pos] == terraces[pos+1]){
        dist++;
        pos++;
        explored[pos] = 1;
    }

    if(terraces[pos] < terraces[pos+1]) {
        return dist;
    }

    int bridge = get_distance(explored, terraces, pos+1, n);
    
    if(terraces[pos] == terraces[pos+bridge+1]){
        total+=bridge;
    }

    return bridge+2;
}

int main() {
    cin.tie(nullptr);
    cin.sync_with_stdio(0);
    
    int n;
    cin >> n;
    
    vector<int> terraces(n);
    vector<char> explored(n, 0);
    for(int i=0; i<n; i++){
        cin >> terraces[i];
    }
    
    for(int i=0; i<n; ++i){
        if(explored[i])continue;
        get_distance(explored, terraces, i, n);
    }

    cout << total << '\n';

    return 0;
}
