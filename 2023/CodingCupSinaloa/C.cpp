/**
 * It seems this problem does not belong to this contest...
 * */

#include <bits/stdc++.h>
using namespace std;

#define rep(i, a, b) for(int i = a; i < (b); ++i)
#define all(x) begin(x), end(x)
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

vector<string> cave;

struct Miner {
    int state; // 0 piso 1 cayendo 2 saltando
    bool pickaxe;
    bool trampoline;
    bool jumping;
    bool falling;
    int count;
};



string dfs(Miner m, int i, int j, string past) {
    if (cave[i][j] == '*') return past;
    cave[i][j] = '*';

    if (cave[i][j] == 'S') return past;
    if (cave[i][j] == '#') return "";

    if (cave[i][j] == '@') {
        if (m.pickaxe) {
            cave[i][j] == ' ';
            m.pickaxe = false;
            past += '@';
        } else {
            return "";
        }
    }

    if (m.jumping) {
        m.count ++;

        dfs(m, i-1, j, past+'T');

        if (m.count == 4) {
            m.count = 0;
            m.falling = true;
            m.jumping = false;
        }
    }

    if (m.falling) {
        m.count ++;

        string a = dfs(m, i+1, j, past);
        string b = dfs(m, i+1, j+1, past+'r');
        string c = dfs(m, i+1, j-1, past+'l');

        if (a != past) return a;
        if (b != past+'r') return b;
        if (c != past+'l') return c;

        if (m.count == 4) {
            return "";
        }
    }

    if (m.trampoline) {
        m.trampoline = false;
        string a = dfs(m,i-1,j,past+'T');

        if (a != past+'T') return a;
    }

    string b = dfs(m, i, j+1, past+'r');
    string c = dfs(m, i, j-1, past+'l');

    if (b != past+'r') return b;
    if (c != past+'l') return c;

    return past;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);

    for (string line; getline(cin, line);)
        cave.push_back(line);

    int x0, y0;
    for (int i = 0; i < cave.size(); i++)
    {
        for (int j = 0; j < cave.front().size(); j++)
        {
            if (cave[i][j] == 'E') {
                x0 = j;
                y0 = i;
                break;
            }
        }
    }
    
    Miner miner = {0, true, true, false, false, 0};

    cout << dfs(miner, y0, x0, "") << '\n';

    return 0;
}
