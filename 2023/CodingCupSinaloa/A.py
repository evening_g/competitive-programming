import re

n = int(input())

synonyms = dict()

for _ in range(n):
  a, b = input().split()
  synonyms[a] = b
  synonyms[b] = a

sentence = input()
words =  re.findall(r'([a-z]+|[^\w\s]+|\s+)', sentence)
