#include <bits/stdc++.h>
using namespace std;

#define rep(i, a, b) for(int i = a; i < (b); ++i)
#define all(x) begin(x), end(x)
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n;
    cin >> n;

    vector<int> points(n);
    for(int &i : points) {
        cin >> i;
    }

    vector<int> sequence;
    int cont = 0;
    int max_len = 0;
    for(int i = 0; i < n - 1; i++) {
        if(points[i] <= points[i+1]) {
            sequence.push_back(points[i]);
            //cont ++;
        } else {
            if (sequence.empty()) {
                cont = 0;
                continue;
            }

            if(sequence.back() > points[i]) {
                sequence.pop_back();
                cont ++;
            }

            int j = i + 1;

            while (sequence.back() == points[i]) {
                sequence.pop_back(); 
                cont ++;
            }

            while(true) {
                if (sequence.empty() || j == n) {
                    max_len = max(max_len, cont+1);
                    cont = 0;
                    break;
                }

                if (sequence.back() != points[j]) {
                    max_len = max(max_len, cont);
                    cont = 0;
                    //sequence.clear();
                    break;
                }

                sequence.pop_back();
                cont += 2;
                j ++;
            }
            i = j;
        }
    }

    while (!sequence.empty() && sequence.back() == points[n-1]) {
        cont ++;
        sequence.pop_back();    
    }
    
    max_len = max(max_len, cont+1);

    cout << (max_len <= 2 ? "-1" : to_string(max_len)) << '\n';

    return 0;
}