#include<iostream>

using namespace std;

int main() {
    const double perimeter = 0.7853981633974; // m

    double distance;

    cin >> distance;  // km

    distance *= 1000; // m

    int result = (distance/perimeter);

    cout << result << '\n';

    return 0;
}
