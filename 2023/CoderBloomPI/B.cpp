#include<iostream>
#include<math.h>

using namespace std;

double area(int a, int b) {
    return M_PI * a * b;
}

int main() {
    cin.tie(nullptr);
    cin.sync_with_stdio(false); 

    int a1, b1, a2, b2;

    cin >> a1 >> b1 >> a2 >> b2;

    cout << (int)(area(a2, b2)*100/area(a1, b1)) << '\n';

    return 0;
}
