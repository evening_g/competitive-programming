#include<iostream>
#include<math.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    cin.sync_with_stdio(false); 

    const float area = 132.732289614;

    int l;

    cin >> l;

    cout << (int)(area*l) <<'\n';

    return 0;
}
