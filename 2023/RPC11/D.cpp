#include <bits/stdc++.h>

using namespace std;


int main() {
    cin.tie(0)->sync_with_stdio(0);

    double a, b, r;
    cin >> a >> b >> r;

    if (abs(b) <= r) {
        cout << "0\n";
        return 0;
    }

    float dis_ab_to_center = a / sqrt(1+(a/b)*(a/b));
    if (abs(dis_ab_to_center) > r) {
        cout << "-1\n";
        return 0;
    }

    b = abs(b);
    double m = -a / b;
    double A = (1+m*m);
    double B = (2*a*m);
    double C = (a*a - r*r);

    double x1 = (-B + sqrt(B*B - 4*A*C))/(2*A);
    double x2 = (-B - sqrt(B*B - 4*A*C))/(2*A);

    double x = max(x1, x2);

    double y = m*x + a;

    cout<<fixed<<setprecision(7);

    double angle = (atan(y/x)*180)/M_PI;

    cout << angle << '\n';

    return 0;
}
