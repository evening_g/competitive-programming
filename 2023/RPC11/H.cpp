#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vl = vector<ll>;
using vii = vector<pair<int, int>>;
using ii = pair<int, int>;
using mi = vector<vector<int>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (int &i : v) cin >> i
#define printv(v) for (int &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

ll merge(vi &a, vi &b, vi &v) {
    ll swaps = 0;

    int A = a.size();
    int B = b.size();
    int j = 0;
    int k = 0;

    while (j < A and k < B) {
        if (a[j] <= b[k]) {
            v[j+k] = a[j];
            j ++;
        } else {
            v[j+k] = b[k];
            k++;
            swaps ++;
        }
    }

    while (j < A) {
        v[j+k] = a[j];
        j ++;
    }

    while (k < B) {
        v[j+k] = b[k];
        k++;
    }

    return swaps;
}

ll merge_sort(vi &v) {
    if (v.size() <= 1) return 0;

    vi a;
    vi b;

    for (int i = 0; i < v.size() / 2; i++)
        a.push_back(v[i]);

    for (int i = v.size() / 2; i < v.size(); i++)
        b.push_back(v[i]);
    
    ll swaps = 0;
    swaps += merge_sort(a);
    swaps += merge_sort(b);
    
    swaps += merge(a, b, v);

    return swaps;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);

    int n;
    cin >> n;
    vi v = vi(n);

    readv(v);

    ll swaps = merge_sort(v);

    if (swaps%2) {
        cout << "Smokin Joe!\n";
    } else {
        cout << "Oh No!\n";
    }

    return 0;
}
