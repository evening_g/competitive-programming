from math import *


def main():
    a, b, r = map(float, input().split())

    if (abs(b) <= r):
        print("0")
        return
    
    dis_ab_to_center = a / sqrt(1+(a/b)**2)
    if (abs(dis_ab_to_center) >= r):
        print("-1")
        return
    
    b = abs(b)
    m = -a/b
    A = (1+m*m)
    B = (2*m*a)
    C = (a**2 - r**2)

    x1 = (-B + sqrt(B**2 - 4*A*C))/(2*A)
    x2 = (-B - sqrt(B**2 - 4*A*C))/(2*A)

    x = max(x1, x2)
    y = m*x + a

    angle = atan(y/x)
    
    angle_deg = angle * 180 / pi

    print(f"{angle_deg:.7f}")
    

if __name__ == "__main__":
    main()
