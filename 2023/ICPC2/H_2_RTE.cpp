#include <bits/stdc++.h>
using namespace std;

#define all(x) begin(x), end(x)
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

struct setp {
    vector<char> s;
    int size;
};

set<int> explored;

void dfs(const vector<vi> &adj_list, vector<int> &groups, vector<int> &sizes, string base, int id) {
    if (explored.count(id)) return;

    explored.insert(id);

    for (int adj : adj_list[id]) {
        string new_base = base;

        sizes[adj] = sizes[id];
        if(groups[adj]-1 >= new_base.length() || groups[adj]-1 < 0) {
            continue;
        }
        if(new_base[groups[adj]-1] != '1') {
            sizes[adj]++;
            new_base[groups[adj]-1] = '1';
        }
        
        //groups[adj].insert(all(groups[id]));
        dfs(adj_list, groups, sizes, new_base, adj);
    }
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n;
    cin >> n;

    vector<vi> adj_list(n+1, vi());
    vector<int> groups(n+1);
    vector<int> sizes(n+1);

    for (int i = 1; i <= n; i++)
    {
        int parent;
        cin >> parent;

        adj_list[parent].push_back(i);
    }

    for (int i = 1; i <= n; i++)
    {
        int group;
        cin >> group;

        groups[i] = group;
    }    

    string base(n, '0');

    dfs(adj_list, groups, sizes, base, 0);

    for (int i = 1; i <= n; i++)
    {
        cout << sizes[i] << ' ';
    }

    cout << '\n';
    

    return 0;
}