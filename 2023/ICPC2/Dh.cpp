#include <bits/stdc++.h>
using namespace std;

#define all(x) begin(x), end(x)
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

int solve2(vi da, vi db) {
    int total = 0;
    int subcount = 0;

    for (int i = 0; i < db.size(); i++)
    {
        if (db[i] == da[0]) {
            for (int j = 0; j < da.size() && i + j < db.size(); j++)
            {
                if (da[j] == db[i+j]) {
                    subcount ++;
                } else {
                    total += subcount;
                    subcount = 0;
                }
            }

        }
    }
    
    return total;
}

int solve1(vi da, vi db) {
    int total = 0;

    int subcount = 0;
    for (int i = 0; i < db.size(); i++)
    {
        if (db[i] == da[0]) {
            subcount ++;
        } else {
            int d = subcount - da.size() + 1;
            if (d > 0)
                total += d;
            subcount = 0;
        }
    }
    
    return total;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int N, M;
    cin >> N >> M;

    vi a(N);
    vi b(M);

    for(int &i : a) {
        cin >> i;
    }

    for(int &i : b) {
        cin >> i;
    }

    vi da(N-1);

    for (int i = 0; i < N - 1; i++)
    {
        da[i] = a[i+1] - a[i];
    }
    
    vi db(M-1);

    for (int i = 0; i < M - 1; i++)
    {
        db[i] = b[i+1] - b[i];
    }

    bool case1 = true;
    for (int i = 0; i < N - 2; i++)
    {
        if (da[i] != da[i+1]) {
            case1 = false;
            break;
        }
    }
    
    int total;

    if (case1) {
        total = solve1(da, db);
    } else {
        total = solve2(da, db);
    }

    cout << total << '\n';

    return 0;
}