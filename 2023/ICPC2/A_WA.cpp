#include <bits/stdc++.h>
using namespace std;

#define all(x) begin(x), end(x)
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    vector<ll> primes;

    int n;
    cin >> n;

    ll lim = pow(10,(n+1));
    vector<char> sieve(lim, false);

    primes.push_back(1);
    for(ll x = 2; x <= lim; x++) {

        if(x*x <= lim){
            primes.push_back(x*x);
        }
        
        if(sieve[x]) continue;
        primes.push_back(x);
        for(ll u=2*x; u <= lim; u+=x) {
            sieve[u] = true;
        }
    }

    sort(all(primes));

    string num;
    cin >> num;

    int cont = 0;
    for(ll &prime : primes) {
        string primestr = to_string(prime);
        int size = primestr.length();
        if(size > n) break;
        for(int i=0; i < n - size + 1; i++) {
            if(num[i] == 'x') continue;
            string test = num.substr(i, size);
            if(primestr == test) {
                cont++;

                int j;
                for(j=i; j<i+size; j++) {
                    num[j] = 'x';
                }
                i = j;
            }
        }
    }

    cout << cont << "\n";

    return 0;
}