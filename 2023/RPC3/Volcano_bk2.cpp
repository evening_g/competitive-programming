/*
Problem description/source
date: yyyy-mm-dd
*/

#include<bits/stdc++.h>

using namespace std;

using ll = long long;
using ull = unsigned long long;

bool cmp_coord(const pair<int, int> &a, const pair<int, int> &b) {
    if (a.first == b.first) {
        return a.second < b.second;
    }

    return a.first < b.first;
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int n, x, y;

    cin >> n;

    vector<pair<int, int>> coords(n);

    for (int i = 0; i < n; i++)
    {
        cin >> x >> y;
        coords[i] = {x, y};
    }
        
    // Process
    sort(coords.begin(), coords.end(), cmp_coord);

    int total = 0;

    for (int i = 0; i < n - 1; i++)
    {
        total += abs(coords[i+1].first - coords[i].first);
        total += abs(coords[i+1].second - coords[i].second);
    }

    // Print result
    cout << total << '\n';

    return 0;
}
