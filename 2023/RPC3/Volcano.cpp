/*
Problem description/source
date: yyyy-mm-dd
*/

#include<bits/stdc++.h>

using namespace std;

using ll = long long;
using ull = unsigned long long;

bool cmp_coord(const pair<int, int> &a, const pair<int, int> &b) {
    if (a.first == b.first) {
        return a.second < b.second;
    }

    return a.first < b.first;
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int n, x, y;

    cin >> n;

    vector<pair<int, int>> coords(n);

    for (int i = 0; i < n; i++)
    {
        cin >> x >> y;
        coords[i] = {x, y};
    }
        
    // Process
    sort(coords.begin(), coords.end(), cmp_coord);

    int total = coords[n-1].first - coords[0].first;

    int i = 0;
    while (i < n - 1)
    {
        int miny = coords[i+1].second;
        
        int j;
        for (j = i+1; coords[j].first == coords[i+1].first; j++);
        
        int maxy = coords[j-1].second;

        total += min(abs(maxy - coords[i].second), abs(miny - coords[i].second));
        total += maxy - miny;

        if (i + 1 == j)
        {
            i ++;
        } else {
            i = j-1;
        }
    }

    // Print result
    cout << total << '\n';

    return 0;
}
