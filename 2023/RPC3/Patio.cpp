/*
Problem G: AC
*/

#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int solve(int n, string file)
{
    vector<pair<int, int>> sums(n+1);

    for (int i = 1; i <= n; i++)
    {
        if (file[i-1] == 'X')
        {
            sums[i].first = sums[i - 1].first + 1;
            sums[i].second = sums[i - 1].second;
        }
        else
        {
            sums[i].second = sums[i - 1].second + 1;
            sums[i].first = sums[i - 1].first;
        }
    }

    int total = 0;
    for (int i = 0; i <= n; i++)
    {
        // Checar todos los cuadrados a partir de i

        for (int j = 3; i + j*j <= n; j++)
        {
            int x = sums[i + j*j].first - sums[i].first;
            int o = sums[i + j*j].second - sums[i].second;

            // checar si el cuadrado de x o o es (j-2)^2
            if (x == (j - 2)*(j - 2) || o == (j - 2)*(j - 2))
                total ++;
        }        
    }
    
    return total;
}

int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int n;
    string file;

    cin >> n;
    cin >> file;

    cout << solve(n, file) << '\n';

    return 0;
}
