# Chat GPT

import math
from collections import defaultdict

# calculate the Manhattan distance between two points
def manhattan_distance(p1, p2):
    return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])

# find the minimum spanning tree of a graph using Prim's algorithm
def prim_mst(graph, start):
    visited = set()
    edges = defaultdict(list)
    for u in graph:
        for v in graph[u]:
            edges[manhattan_distance(u, v)].append((u, v))
    mst = []
    visited.add(start)
    for _ in range(len(graph) - 1):
        min_dist = min(edges.keys())
        edge = edges[min_dist].pop(0)
        while edge[1] in visited and len(edges[min_dist]) > 0:
            edge = edges[min_dist].pop(0)
        if edge[1] not in visited:
            visited.add(edge[1])
            mst.append(edge)
            for v in graph[edge[1]]:
                if v not in visited:
                    edges[manhattan_distance(edge[1], v)].append((edge[1], v))
    return mst

# read input
n = int(input())
points = []

for _ in range(n):
    points += [tuple(map(int, input().split()))]

# create graph
graph = {}
for i in range(n):
    graph[points[i]] = []
    for j in range(i + 1, n):
        dist = manhattan_distance(points[i], points[j])
        graph[points[i]].append(points[j])
        graph[points[j]].append(points[i])

# find MST and calculate total distance
mst = prim_mst(graph, points[0])
total_dist = sum(manhattan_distance(u, v) for u, v in mst)

# output result
print(total_dist)
