# Problem: C
# No enviado aún

import re

def main():
    n = int(input())

    phone_numbers = []
    for _ in range(n):
        phone_numbers += [str(input())]

    m = int(input())

    damaged_regex = []
    for _ in range(m):
        damaged = str(input())

        damaged = damaged.replace("?", "[0-9]")
        damaged = damaged.replace("*", "[0-9]+")

        damaged_regex += [damaged]

    
    for regex in damaged_regex:
        print(sum(1 for phone_number in phone_numbers if re.match(regex, phone_number)))
    

if __name__ == "__main__":
    main()
