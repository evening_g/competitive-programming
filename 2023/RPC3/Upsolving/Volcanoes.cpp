/**
 * Problem K: Volcanoes
 * 
 * Dynamic Programming Approach
 */

#include<bits/stdc++.h>

using namespace std;

struct Column {
    int max;
    int min;
    int dis;
};

map<int, Column> volcanoes;

int manhattan_distance(int x1, int y1, int x2, int y2) {
    return abs(x2 - x1) + abs(y2 - y1);
}

void add_volcano(int x, int y) {
    if (volcanoes.find(x) == volcanoes.end()) {
        // Add volcano
        volcanoes[x] = {y, y, 0};
    } else {
        if (volcanoes[x].max < y) {
            // If there is only one in this column, just add it to max
            if (volcanoes[x].dis == 0) {
                volcanoes[x].max = y;
                volcanoes[x].dis = abs(y - volcanoes[x].min);
                return;
            }
            volcanoes[x].dis += abs(y - volcanoes[x].max);
            volcanoes[x].max = y;
        } else if ((volcanoes[x].min > y)) {
            // If there is only one in this column, just add it to min
            if (volcanoes[x].dis == 0) {
                volcanoes[x].min = y;
                volcanoes[x].dis = abs(y - volcanoes[x].max);
                return;
            }
            volcanoes[x].dis += abs(y - volcanoes[x].min);
            volcanoes[x].min = y;
        }
    }
}

int solve() {
    // For each column, all that matters is traveling the whole column and moving to the next one in the shortest path
    // Let there be consecutive columns A, B, C
    // What if the shortest path for moving A -> B lets us in a bad position for moving from B -> C
    // We have to adjust these things... But how?

    // Ok so we're forming a DAG taking up->down / down->up in each x coordinate
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int n;

    cin >> n;

    if (n == 0) {
        cout << "0\n";
        return 0;
    }

    for (int i = 0; i < n; i++)
    {
        int x, y;
        cin >> x >> y;

        add_volcano(x, y);
    }

    return 0;
}
