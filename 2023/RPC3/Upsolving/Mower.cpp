/**
 * Problem E: Mover
 */


#include <iostream>

using namespace std;

int main() {
    long long w, h, x, y;

    cin >> w >> h >> x >> y;

    bool can_win = false;

    if (w*h % 2 == 0) {
        // Si hay un número par de casillas puede ganar
        can_win = true;
    }

    if ((x + y) % 2 == 1) {
        // No entiendo esta condición
        can_win = true;
    }

    cout << (can_win ? "Win\n" : "Lose\n");

    return 0;
}