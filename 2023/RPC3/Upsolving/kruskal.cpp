#include<bits/stdc++.h>

using namespace std;

struct Edge {
    int u;
    int v;
    int w;
};

vector<Edge> E;

bool compare_edges_by_weight(const Edge &a, const Edge &b) {
    return a.w >= b.w;
}

bool set_contains(const set<int> &S, int key) {
    return S.find(key) != S.end();
}

/**
 * @brief Returns wether choosing the edge e forms a cycle or not
 * If not, adds the vertex to the corresponding subcycle.
 * 
 * Search each vertex in e, in each subgraph existing
 * if any vertex is not found, create a new subgraph with this two vertex
 * if only one vertex is found, add the other vertex to the subgraph were it was found
 * if both vertices are found in different subgraphs, merge subgraphs
 * if both vertices are found in the same subgraph, return true
 * 
 * @param subgraphs 
 * @param e 
 * @return true 
 * @return false 
 */
bool makes_cycle(map<int, set<int>> &subgraphs, Edge e) {
    int u = -1;  // Subgraph index where e.u is located
    int v = -1;  // Subgraph index where e.v is located

    for (const auto &subgraph : subgraphs) {
        if (set_contains(subgraph.second, e.u)) {}
            u = subgraph.first;

        if (set_contains(subgraph.second, e.v))
            v = subgraph.first;
    }

    if (u == v) {
        if (u == -1) {
            subgraphs[subgraphs.size()] = {e.u, e.v};
            return false;
        } else {
            return true;
        }
    }

    if (v == -1) {
        // Add e.v to subgraph u
        subgraphs[u].insert(e.v);
        return false;
    }

    if (u == -1) {
        subgraphs[v].insert(e.u);
        return false;
    }

    // Merge u and v subgraphs
    subgraphs[u].insert(subgraphs[v].begin(), subgraphs[v].end());

    // Remove subgraph v
    subgraphs.erase(v);

    return false;
}

void kruskal(int n) {
    // Vertices explored
    set<int> explored;
    // Edges path of the MST
    // We take the id from the E vector
    set<int> mst;
    // Subgraphs formed by the selected edges
    map<int, set<int>> subgraphs;

    // Sorts in decreasing order
    // So the last edge is the one with the smallest weight
    sort(E.begin(), E.end(), compare_edges_by_weight);
    int edge_id = E.size() - 1;

    while (mst.size() < n - 1) {
        // Take the smallest edge
        Edge e = E[edge_id];
        edge_id --;

        // Check if it doesn't form a cycle
        if (makes_cycle(subgraphs, e))
            // This doesn't work as it may happen that u and v are already
            // in different graphs and we still have to connect them
            continue;

        explored.insert(e.u);
        explored.insert(e.v);

        mst.insert(edge_id+1);
    }
}
