/**
 * Problem K: Volcanoes
 * 
 * WA
 * 
 * Kruskal doesn't solve the problem
 */

#include<bits/stdc++.h>

using namespace std;

struct Point {
    int id;
    int x;
    int y;

    bool operator ==(const Point &other) const {
        return this->id == other.id;
    }
};

struct Column {
    Point max;
    Point min;
    int dis;
};

struct Edge {
    int u;
    int v;
    int w;
};

map<int, Column> volcanoes;
vector<Edge> E;
map<int, Point> V;

int manhattan_distance(Point a, Point b) {
    return abs(a.x - b.x) + abs(a.y - b.y);
}

void add_volcano(Point v) {
    if (volcanoes.find(v.x) == volcanoes.end()) {
        // Add volcano
        volcanoes[v.x] = {v, v, 0};
        V[v.id] = v;
    } else {
        if (volcanoes[v.x].max.y < v.y) {
            // If there is only one in this column, just add it to max
            if (volcanoes[v.x].dis == 0) {
                volcanoes[v.x].max = v;
                volcanoes[v.x].dis = abs(v.y - volcanoes[v.x].min.y);
                V[v.id] = v;
                return;
            }
            volcanoes[v.x].dis += abs(v.y - volcanoes[v.x].max.y);
            V.erase(volcanoes[v.x].max.id);
            V[v.id] = v;
            volcanoes[v.x].max = v;
        } else if ((volcanoes[v.x].min.y > v.y)) {
            // If there is only one in this column, just add it to min
            if (volcanoes[v.x].dis == 0) {
                volcanoes[v.x].min = v;
                volcanoes[v.x].dis = abs(v.y - volcanoes[v.x].max.y);
                V[v.id] = v;
                return;
            }
            volcanoes[v.x].dis += abs(v.y - volcanoes[v.x].min.y);
            V.erase(volcanoes[v.x].min.id);
            V[v.id] = v;
            volcanoes[v.x].min = v;
        }
    }
}

void create_edges_list() {
    // Parse volcanoes map to edges list with weight
    for (auto it = volcanoes.begin(); next(it) != volcanoes.end(); it++) {
        unordered_set<int> current_points_ids;

        current_points_ids.insert(it->second.max.id);
        current_points_ids.insert(it->second.min.id);

        unordered_set<int> next_points_ids;

        next_points_ids.insert(next(it)->second.max.id);
        next_points_ids.insert(next(it)->second.min.id);

        // Create a edge between min an max for this column
        if (current_points_ids.size() > 1) {
            Edge e;
            e.u = it->second.max.id;
            e.v = it->second.min.id;
            e.w = abs(it->second.max.y - it->second.min.y);
            E.push_back(e);
        }

        // Create edges between columns
        for (int u : current_points_ids) {
            for (int v : next_points_ids) {
                Edge e = {u, v, manhattan_distance(V[u], V[v])};
                E.push_back(e);
            }
        }
    }

    // Add a edge between the last column
    auto last_column = prev(volcanoes.end());
    if (last_column->second.dis != 0) {
        Edge e;
        e.u = last_column->second.max.id;
        e.v = last_column->second.min.id;
        e.w = abs(last_column->second.max.y - last_column->second.min.y);
        E.push_back(e);
    }
}

bool compare_edges_by_weight(const Edge &a, const Edge &b) {
    return a.w >= b.w;
}

bool set_contains(const set<int> &S, int key) {
    return S.count(key) == 1;
}

/**
 * @brief Returns wether choosing the edge e forms a cycle or not
 * If not, adds the vertex to the corresponding subcycle.
 * 
 * Search each vertex in e, in each subgraph existing
 * if any vertex is not found, create a new subgraph with this two vertex
 * if only one vertex is found, add the other vertex to the subgraph were it was found
 * if both vertices are found in different subgraphs, merge subgraphs
 * if both vertices are found in the same subgraph, return true
 * 
 * @param subgraphs 
 * @param e 
 * @return true 
 * @return false 
 */
bool makes_cycle(map<int, set<int>> &subgraphs, Edge e) {
    int u = -1;  // Subgraph index where e.u is located
    int v = -1;  // Subgraph index where e.v is located

    for (const auto &subgraph : subgraphs) {
        if (subgraph.second.count(e.u) == 1)
            u = subgraph.first;

        if (subgraph.second.count(e.v) == 1)
            v = subgraph.first;
    }

    if (u == v) {
        if (u == -1) {
            subgraphs[subgraphs.size()] = {e.u, e.v};
            return false;
        } else {
            return true;
        }
    }

    if (v == -1) {
        // Add e.v to subgraph u
        subgraphs[u].insert(e.v);
        return false;
    }

    if (u == -1) {
        subgraphs[v].insert(e.u);
        return false;
    }

    // Merge u and v subgraphs
    subgraphs[u].insert(subgraphs[v].begin(), subgraphs[v].end());

    // Remove subgraph v
    subgraphs.erase(v);

    return false;
}

int kruskal() {
    // Edges path of the MST
    // We take the id from the E vector
    set<int> mst;
    // Subgraphs formed by the selected edges
    map<int, set<int>> subgraphs;
    // Map to know if we already took an edge from the vertex i
    // to the next column of vertex
    //map<int, bool> is_valid;
    
    set<int> not_valid;

    // for (auto v : V)
    //     is_valid[v.first] = true;

    // Sorts in decreasing order
    // So the last edge is the one with the smallest weight
    sort(E.begin(), E.end(), compare_edges_by_weight);
    int edge_id = E.size() - 1;

    while (mst.size() < V.size() - 1) {
        // Take the smallest edge
        Edge e = E[edge_id];
        edge_id --;

        if (V[e.u].x != V[e.v].x) {
            // If the edges go between columns

            // get the id of the leftmost x
            int leftmost_x = min(V[e.v].x, V[e.u].x);

            // Check if we haven't already made this columns jump
            if (not_valid.count(leftmost_x) == 0) {
                // Mark as invalid
                not_valid.insert(leftmost_x);
            } else {
                // move to the next edge
                continue;
            }
        }

        // Check if it doesn't form a cycle
        if (makes_cycle(subgraphs, e))
            // This doesn't work as it may happen that u and v are already
            // in different graphs and we still have to connect them
            continue;

        mst.insert(edge_id+1);
    }

    long long path_size = 0;

    // These volcanoes won't be counted double as these are at
    // the beginning or the end of the path
    set<int> extreme_volcanoes;

    extreme_volcanoes.insert(volcanoes.begin()->second.max.id);
    extreme_volcanoes.insert(volcanoes.begin()->second.min.id);
    extreme_volcanoes.insert(prev(volcanoes.end())->second.max.id);
    extreme_volcanoes.insert(prev(volcanoes.end())->second.min.id);

    map<int, int> counts;

    for (int i : mst) {
        path_size += E[i].w;
        if (counts.count(E[i].u) == 0) {
            // This is the first time this vertex appears
            counts[E[i].u] = E[i].w; 
        } else {
            // Second > time
            counts[E[i].u] = 0;
        }

        // Same for vertex v
        if (counts.count(E[i].v) == 0) {
            // This is the first time this vertex appears
            counts[E[i].v] = E[i].w; 
        } else {
            // Second > time
            counts[E[i].v] = 0;
        }
    }

    for (auto i : counts) {
        if (extreme_volcanoes.count(i.first) == 0)
            path_size += i.second;
    }
    
    return path_size;
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int n;

    cin >> n;

    if (n == 0) {
        cout << "0\n";
        return 0;
    }

    for (int i = 0; i < n; i++)
    {
        Point volcano;
        volcano.id = i;
        cin >> volcano.x >> volcano.y;

        add_volcano(volcano);
    }

    // Process

    create_edges_list();

    // Apply Kruskal
    cout << kruskal() << '\n';

    return 0;
}
