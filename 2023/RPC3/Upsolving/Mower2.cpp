/**
 * Problem E: Mover
 * Solución del libro
 */


#include <iostream>

using namespace std;

int main() {
    long long w, h, x, y;

    cin >> w >> h >> x >> y;

    // Para que pueda ganar debe haber un número par de casillas
    // o debe estar en una casilla en la que la distancia a cualquier pared sea impar
    cout << ((w%2==0) or (h%2==0) or ((x+y)%2!=0) ? "Win\n" : "Lose\n");

    return 0;
}