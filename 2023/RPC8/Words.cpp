/**
 * Problem B
 * date: 2023-07-15
 * camel_case
*/

#include<stdio.h>
#include<string.h>
#include<set>

int main() {
    char word [30];

    scanf("%s", word);

    std::set<char> vowels;
    vowels.insert({'a', 'e', 'i', 'o', 'u'});

    int easy = 1;
    for (int i = 0; i < strlen(word)-1; i++)
    {
        //printf("%c %c", word[i], word[i+1]);
        if (vowels.count(word[i]) == vowels.count(word[i+1])) {
            easy = 0;
            break;
        }
    }
    
    printf("%d\n", easy);
    
    return 0;
}
