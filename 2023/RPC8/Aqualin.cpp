/**
 * Problem E
 * date: 2023-07-15
 * camel_case
 * status: AC
 * keywords: implicit-graph graph-flood
 * source: https://omegaup.com/arena/problem/Aqualin/
 */

#include <bits/stdc++.h>

using namespace std;

#define all(x) begin(x), end(x)
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef pair<ll, ll> pll;
typedef vector<ll> vl;

bool is_inside_bounds(int i, int j, int n)
{
    return i >= 0 and i < n and j >= 0 and j < n;
}

void dfs(vector<vi> &graph, int i, int j, int &size)
{
    int current = graph[i][j];
    graph[i][j] = -1;

    int dx[] = {1, -1, 0, 0};
    int dy[] = {0, 0, 1, -1};

    for (int k = 0; k < 4; k++)
    {
        if (is_inside_bounds(i + dx[k], j + dy[k], graph.size()) and graph[i + dx[k]][j + dy[k]] == current)
        {
            dfs(graph, i + dx[k], j + dy[k], size);
            size++;
        }
    }
}

int sum(int n)
{
    return (n * (n + 1)) / 2;
}

int solve(vector<vi> &graph)
{
    int total = 0;

    map<int, int> groups;

    for (int i = 0; i < graph.size(); i++)
        for (int j = 0; j < graph.size(); j++) {
            if (graph[i][j] == -1)
                continue;

            int size = 1;
            int group = graph[i][j];
            dfs(graph, i, j, size);

            if (groups.find(group) == groups.end()) {
                groups[group] = size;
            } else if (size > groups[group]) {
                groups[group] = size;
            }
        }

    for (auto group : groups)
        total += sum(group.second - 1);

    return total;
}

int main()
{
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n;
    cin >> n;

    vector<vi> numbers(n, vi(n));
    vector<vi> letters(n, vi(n));

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            char c;
            cin >> c;
            letters[i][j] = (int)c;
            cin >> numbers[i][j];
        }
    }

    int animals = solve(letters);
    int colors = solve(numbers);

    cout << animals << ' ' << colors << '\n';

    return 0;
}
