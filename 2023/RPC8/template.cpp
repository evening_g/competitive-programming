/**
 * Problem description/source
 * date: 2023-07-15
 * camel_case
*/

#include<bits/stdc++.h>

using namespace std;

#define all(x) begin(x), end(x)
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef pair<ll, ll> pll;
typedef vector<ll> vl;

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    cout << "Hello, world!" << '\n';
    
    return 0;
}
