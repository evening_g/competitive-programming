#include<bits/stdc++.h>

using namespace std;

#define all(x) begin(x), end(x)

double get_dir(const string d) {
    if (d.size() == 1) {
        return 
    }
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    /*map<char, double> dirs;
    dirs['N'] = 0;
    dirs['E'] = 90;
    dirs['S'] = 180;
    dirs['W'] = 270;*/

    string x, y;
    cin >> x >> y;

    // invert the strings
    reverse(all(x));
    reverse(all(y));

    double dir_x = get_dir(x);
    double dir_y = get_dir(y);
    
    double diff_dir = min(abs(dir_y - dir_x), 360 - abs(dir_y - dir_x));
    
    cout /*<< fixed*/ << setprecision(7) << diff_dir << '\n';

    return 0;
}
