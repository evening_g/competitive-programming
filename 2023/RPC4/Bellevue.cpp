#include<bits/stdc++.h>

using namespace std;

struct Point {
    int x;
    int y;
};

double angle(const Point &a, const Point &b) {
    int dx = abs(a.x - b.x);
    int dy = abs(a.y - b.y);

    return atan((double)dy / (double)dx);
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int n;

    cin >> n;

    vector<Point> points(n);

    for (Point &p : points) {
        cin >> p.x >> p.y;
    }

    // Process
    double max_angle = 0;
    for (int i = 0; i < n; i++)
    {
        double angle_east = angle(points[0], points[i]);
        double angle_west = angle(points[n-1], points[i]);

        max_angle = max(max_angle, angle_east); 
        max_angle = max(max_angle, angle_west); 

    }

    // Print result
    cout << fixed << setprecision(7) << (max_angle * 180) / M_PI << '\n';

    return 0;
}
