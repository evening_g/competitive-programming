#include <iostream>
#include <algorithm>
#include <vector>
#include <map>

using namespace std;

int lower_bound_binary_search(const vector<int> &v, int x) {
    int l = 0, r = v.size();

    int m = (l+r) / 2;

    while (l < r) {
        // Not found
        if (m == -1 or m == v.size()) {
            return m;
        }

        if (x > v[m]) {
            l = m + 1;
        } else {
            r = m;
        }

        m = (l+r) / 2;
    }

    return m;
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int n, k, b;
    int a, a_prev = 0;
    cin >> n >> k;
    
    vector<int> thickness(5*10e7);

    for (int i = 0; i < n; i++) {
        cin >> a;
        int t = (-a) + a_prev;
        if (t < 0) {
            t = 0;
        }
        thickness[t]++;
        a_prev = t;
    }

    int acum = 0;
    for(int i = 5*10e7-1; i >= 0; i--) {
        acum += thickness[i];
        thickness[i] = acum;
    }

    for (int i = 0; i < k; i++)
    {
        cin >> b;
        b *= 5;
        cout << thickness[b] << ' ';
    }

    

    return 0;
}

