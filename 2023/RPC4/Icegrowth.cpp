#include <bits/stdc++.h>
using namespace std;

#define rep(i, a, b) for(int i = a; i < (b); ++i)
#define all(x) begin(x), end(x)
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

int lower_bound_binary_search(const vector<double> &v, double x) {
    int l = 0, r = v.size();

    int m = (l+r) / 2;

    while (l < r) {
        // Not found
        if (m == -1 or m == v.size()) {
            return m;
        }

        if (x > v[m]) {
            l = m + 1;
        } else {
            r = m;
        }

        m = (l+r) / 2;
    }

    return m;
}

int main() {
	cin.tie(0)->sync_with_stdio(0);
	cin.exceptions(cin.failbit);

    int n, k;
    cin >> n >> k;

    vector<int> temp(n);
    for(int &t : temp){
        cin >> t;
    }

    vector<int> req(k);
    for(int &thickness : req){
        cin >> thickness;
    }

    double size = 0;
    vector<double> ice;
    for(int i = 0; i < n; ++i) {
        double curr_size = -temp[i] / 5.0; 
        size += curr_size;
        if(size < 0) {
            size = 0;
        }
        ice.push_back(size);
    }
    sort(all(ice));

    for(int thickness : req){

        int lesser = lower_bound_binary_search(ice, (double) thickness);

        cout << n - lesser << ' ';
    }

    return 0;
}