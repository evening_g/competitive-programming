#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int lower_bound_binary_search(const vector<int> &v, int x) {
    int l = 0, r = v.size();

    int m = (l+r) / 2;

    while (l < r) {
        // Not found
        if (m == -1 or m == v.size()) {
            return m;
        }

        if (x > v[m]) {
            l = m + 1;
        } else {
            r = m;
        }

        m = (l+r) / 2;
    }

    return m;
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int n, k, b;
    int a, a_prev = 0;
    cin >> n >> k;
    
    vector<int> thickness(n);

    for (int i = 0; i < n; i++) {
        cin >> a;
        int t = (-a) + a_prev;
        if (t < 0) {
            t = 0;
        }
        thickness[i] = t;
        a_prev = thickness[i];
    }

    sort(thickness.begin(), thickness.end());

    while (k--) {
        cin >> b;
        b *= 5; // cuz I don't wanna use float
                
        int p = lower_bound_binary_search(thickness, b);

        int result =  n - p;

        cout << result << ' ';
    }
    cout << '\n';

    return 0;
}

