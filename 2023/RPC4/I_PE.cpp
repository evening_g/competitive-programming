#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

using ll = long long;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int n, k, b;
    ll a, a_prev = 0;
    cin >> n >> k;
    
    vector<ll> thickness(n);

    for (int i = 0; i < n; i++) {
        cin >> a;
        ll t = (-a) + a_prev;
        if (t < 0) {
            t = 0;
        }
        thickness[i] = t;
        a_prev = thickness[i];
    }

    sort(thickness.begin(), thickness.end());

    while (k--) {
        cin >> b;
        b *= 5; // cuz I don't wanna use float
                
        auto p = lower_bound(thickness.begin(), thickness.end(), b);
        int result = thickness.end() - p;

        cout << result << ' ';
    }

    //cout << "\r\n";

    return 0;
}
