#include<bits/stdc++.h>

using namespace std;

#define all(x) begin(x), end(x)

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    map<char, double> dirs;
    dirs['N'] = 0;
    dirs['E'] = 90;
    dirs['S'] = 180;
    dirs['W'] = 270;

    string x, y;
    cin >> x >> y;

    // invert the strings
    reverse(all(x));
    reverse(all(y));

    double dir_x = dirs[x[0]];

    for (int i = 1; i < min((int) x.size(), 30); i++)
    {
        double dir = dirs[x[i]];

        if (dir_x > 180 and x[i] == 'N') {
            dir = 360.0;
        }

        dir_x = (dir + dir_x) / 2;
    }

    double dir_y = dirs[y[0]];

    for (int i = 1; i < min((int) y.size(), 30); i++)
    {
        double dir = dirs[y[i]];

        if (dir_y > 180 and y[i] == 'N') {
            dir = 360.0;
        }

        dir_y = (dir + dir_y) / 2;
    }

    double diff_dir = min(abs(dir_y - dir_x), 360 - abs(dir_y - dir_x));
    
    cout << fixed << setprecision(7) << diff_dir;

    return 0;
}
