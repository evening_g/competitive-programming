n = 27  # 011011

n >>= 1 # 01101
print(n)

n >>= 1 # 0110
print(n)

n >>= 1 # 011
print(n)

