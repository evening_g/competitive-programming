/*

5 4 3
3 4 5

5 3


*/

#include <bits/stdc++.h>

using namespace std;

pair<int, int> positions[] = {
    {3, 1}, // 0
    {0, 0}, // 1
    {0, 1},
    {0, 2},
    {1, 0},
    {1, 1},
    {1, 2},
    {2, 0},
    {2, 1},
    {2, 2}
};

bool is_valid(string n) {
    for (int i = 0; i < n.size() - 1; i++)
    {
        int dx = positions[n[i+1] - '0'].first - positions[n[i] - '0'].first;
        int dy = positions[n[i+1] - '0'].second - positions[n[i] - '0'].second;

        if (dx < 0 or dy < 0)
            return false;
    }
    
    return true;
}

string solve(string n) {
    // Checar con 0 y con los números a partir de el mismo
    int min_diff = 1000;
    int min_value = 1000;

    int n_int = stoi(n);

    for (int i = n_int - 11; i < n_int + 11; i++)
    {
        if (i < 0)
            continue;

        if (is_valid(to_string(i)))
        {
            if (abs(i - n_int) < min_diff) {
                min_diff = abs(i - n_int);
                min_value = i;
            }
        }
    }

    return to_string(min_value);
    
}

int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int t;
    string n;

    cin >> t;

    while (t--) {
        cin >> n;
        cout << solve(n) << '\n';
    }

    return 0;
}
