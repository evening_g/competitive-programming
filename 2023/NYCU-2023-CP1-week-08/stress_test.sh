#!/bin/bash

for a in {1..1000}
do
    for b in {1..99}
    do
        if (($b % 2 == 1))
        then
            echo "$a $b" | python3 D_WA.py > wa.out
            echo "$a $b" | python3 D_TLE.py > tle.out

            diff wa.out tle.out > diff.txt

            if [ -s diff.txt ]
            then
                echo "Test a = $a, b = $b failed"
                break
            else
                rm diff.txt
            fi
        fi
    done
done
