#include <bits/stdc++.h>

using namespace std;

int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int n;

    cin >> n;

    vector<int> a(n);

    for (int &i : a)
        cin >> i;

    // Process
    int total = 0;

    for (int i = 0; i < n - 2; i++)
    {
        for (int j = i + 1; j < n - 1; j++)
        {
            for (int k = j + 1; k < n; k++)
            {
                if (a[k] < a[j] and a[j] < a[i]) {
                    total ++;
                }
            }
        }
    }

    // Print result
    cout << total << '\n';

    return 0;
}
