#include <iostream>

using namespace std;

int main() {
    int A, B, a, b, m, r;

    cin >> A >> B;

    m = A;
    a = ((A * (A+1)) / 2 ) % A;
    r = 1;
    b = B;

    while (b > 0)
    {
        r = r * a % A;
        a = a * a % m;
        b /= 2;
    }
    
    cout << r << '\n';

    return 0;
}