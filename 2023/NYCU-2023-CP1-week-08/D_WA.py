# Powers and Modulus
#
# source: https://open.kattis.com/contests/fwkzf3/problems/powers
# WA
# date: 2023-04-13
#
# Input:
# a b
# Where 1 <= a <= 10^9
#       1 <= b <= 99, b is odd
#
# Output
# (1^b + 2^b + ... + a^b) mod a

a, _ = map(int, input().split())

sumation = (a * (a+1)) // 2
print(sumation % a)
