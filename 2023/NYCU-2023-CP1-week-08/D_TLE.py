a, b = map(int, input().split())

ans = 0
for n in range(1, a):
    x = n**b
    ans += x
    ans %= a

print(ans)
