#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vl = vector<ll>;
using vii = vector<pair<int, int>>;
using ii = pair<int, int>;
using mi = vector<vector<int>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (int &i : v) cin >> i
#define printv(v) for (int &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

#define MAXK 100000

int main() {
    cin.tie(0)->sync_with_stdio(0);

    int n, k;
    cin >> n >> k;

    bitset<MAXK> working(0);
    vi p(n);
    vi c(k);

    readv(p);

    // fill with the firsk k processes
    int next_process;
    for (next_process = 0; next_process < min(k,n); next_process++)
    {
        c[next_process] = p[next_process];
        working[next_process] = true;
    }
    // fill remaining cores with inf
    if (next_process < k) {
        fill(begin(c)+next_process, end(c), INF);
    }
    
    int result = 0;
    // while all cores are not idle
    while(working.count() != 0) {
        int minbt = *min_element(all(c));
        

        result = max(minbt, result);

        for (int i = 0; i < k; i++)
        {
            if (!working[i]) continue;
            c[i] -= minbt;

            if (c[i] == 0) {
                if (next_process < n) {
                    c[i] = p[next_process++];
                } else {
                // core has finished and there are no more proceses
                    working[i] = false;
                    c[i] = INF;
                }
            }
        }        
    }

    cout << result << '\n';

    return 0;
}
