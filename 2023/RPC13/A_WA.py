a, b, c = input().split()

result = max(int(a+b+c), int(a+c+b), int(b+a+c), int(b+c+a), int(c+a+b), int(c+b+a))

print(result)
