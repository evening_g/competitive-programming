t = int(input())

for _ in range(t):
    a, b, c, n = map(int, input().split())

    a += 1
    b += 1
    c += 1

    total = n//a + n//b + n//c - n//(a*b) - n//(a*c) - n//(b*c) + n//(a*b*c)

    print(total)
