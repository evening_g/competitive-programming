#include <bits/stdc++.h>

using namespace std;

using ull = unsigned long long;

#define all(v) v.begin(), v.end()
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    string a, b, c;
    cin >> a >> b >> c;

    vector<ull> v = {stoull(a+b+c), stoull(a+c+b), stoull(b+a+c), stoull(b+c+a), stoull(c+a+b), stoull(c+b+a)};

    ull result = *max_element(all(v));

    cout << result << '\n';

    return 0;
}
