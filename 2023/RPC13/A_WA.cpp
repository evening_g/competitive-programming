#include <bits/stdc++.h>

using namespace std;

#define all(v) v.begin(), v.end()
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i

// a goes before b
bool sort_nums(const string &a, const string &b) {
    for (size_t i = 0; i < min(a.size(), b.size()); i++)
    {
        if (a[i] > b[i]) return true;
        if (a[i] < b[i]) return false;
    }
    
    return a.size() <= b.size();
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    vector<string> v(3);

    readv(v);

    sort(all(v), sort_nums);

    printv(v);

    cout << '\n';

    return 0;
}
