from math import sqrt

def solve(n):
    total = 0
    f = lambda x: ((x+1)*(x+2)*(x+3))//6 - 1

    while n >= 3:
        lower = 1
        upper = int(sqrt(n))

        while lower < upper:
            mid = (lower + upper) // 2

            if f(mid) > n:
                upper = mid - 1
            else:
                lower = mid + 1

        if f(lower) > n:
            lower -= 1
        
        total += lower
        n -= f(lower)

    print(f"{total} {n}")


def main():
    n = int(input())
    solve(n)


if __name__ == "__main__":
    main()
