// TLE

#include<bits/stdc++.h>

using namespace std;

map<int, int> energy;

void get_min_energy(int a) {
    int lim = a / 2;
    energy[a] = INT_MAX;
    for(int i=1; i<=lim; i++){
        energy[a] = min(energy[a], energy[i] + energy[a-i]);
    }
}

int main() {
    cin.tie(nullptr);
    cin.sync_with_stdio(0);

    int n, q;
    cin >> n >> q;

    for(int i=1; i<=n; i++){
        int a;
        cin >> a;
        energy[i] = a;
    }

    vector<int> tests(q);
    for(int &i: tests){
        cin >> i;
    }
    
    int big = *max_element(begin(tests), end(tests));
    for(int i=n+1; i<=big; i++){
        get_min_energy(i);
    }

    for(int i: tests){
        cout << energy[i] << '\n';
    }

    return 0;
}
