n,k = map(int, input().split())
d,s = map(int, input().split())

t_diff = n*d
s_diff = k*s

rem = (t_diff - s_diff) / (n-k)

if rem > 100 or rem < 0:
    print("impossible")
else:
    print(rem)