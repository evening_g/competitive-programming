// WA

#include<bits/stdc++.h>

using namespace std;

map<long long, long long> energy;

void get_min_energy(long long a) {
    long long lim = a / 2;
    energy[a] = INT64_MAX;
    for(long long i=1; i<=lim; i++){
        energy[a] = min(energy[a], energy[i] + energy[a-i]);
    }
}

int main() {
    cin.tie(nullptr);
    cin.sync_with_stdio(0);

    long long n, q;
    cin >> n >> q;

    for(long long i=1; i<=n; i++){
        long long a;
        cin >> a;
        energy[i] = a;
    }

    vector<long long> tests(q);
    for(long long &i: tests){
        cin >> i;
    }
    
    //long long big = *max_element(begin(tests), end(tests));
    long long big = n*2;
    for(long long i=n+1; i<=big; i++){
        get_min_energy(i);
    }

    long long d = energy[n+1] - energy[1];

    for(long long i: tests){

        if(energy.find(i) != energy.end()){
            cout << energy[i] << '\n';
            continue;
        }

        long long nb = i / n - 1;
        if(i%n == 0){
            nb--;
        }

        long long index = (i%n)+n;
        if (index == n) {
            index += n;
        }

        cout << energy[index] + (nb*d) << '\n';
    }

    return 0;
}
