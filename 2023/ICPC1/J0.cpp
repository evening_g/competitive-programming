#include <bits/stdc++.h>
using namespace std;

#define all(x) begin(x), end(x)
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<ll> vi;

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n, q;

    cin >> n >> q;

    vector<vi> dp(n, vi(n, 0));

    vi S(n);

    for (ll &i : S) {
        cin >> i;
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = i+1; j < n; j++)
        {
            dp[i][j] = S[i] * S[j] + dp[i][j-1];

            if (i > 0) {
                dp[i][j] += dp[i-1][j];
            }

            if (j > i+1 && i > 0) {
                dp[i][j] -= dp[i-1][j-1];
            }
        }        
    }

    while (q--)
    {
        int l, r;
        ll ans;
        cin >> l >> r;

        l --; r--;

        if (l == r) {
            ans = S[l]*S[l]; //?
        } else if (l == 0) {
            ans = dp[r-1][r];
        } else {
            ans = dp[r-1][r] - dp[l-1][r];
        }

        cout << ans << '\n';
    }
    

    return 0;
}