#include <bits/stdc++.h>
using namespace std;

#define all(x) begin(x), end(x)
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n;
    cin >> n;

    int lim = 1, c = 1;

    for(int i=1; i<=n; ++i) {
        if(c == lim) {
            lim += 2;
            c = 1;
        } else {
            c++;
        }
    }

    if(c <= lim/2) {
        cout << "Jane\n";
    } else {
        cout << "John\n";
    }

    return 0;
}