n = int(input())
setn = set()
setuq = set()
uq = 0

for _ in range(n):
    line = input().split()
    NAME = ""

    for i in range(1, len(line)):
        NAME += line[i][0].upper()
        
    if NAME not in setn:
        setuq.add(NAME)
    else: 
        setuq.remove(NAME)
    
    setn.add(NAME)
    

print(len(setuq))
