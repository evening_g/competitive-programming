// This is the infamous 7 TB dp

#include <bits/stdc++.h>
#include <iterator>
using namespace std;

#define all(x) begin(x), end(x)
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<ll> vi;

const ll MOD = 10e9 + 7;

int main() {
    cin.tie(0)->sync_with_stdio(0);

    int n, q;

    cin >> n >> q;

    vector<vi> dp(n+1, vi(n+1));

    ll S[n];

    for (ll &i : S) {
        cin >> i;
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = i+1; j < n; j++)
        {

            dp[i][j] = (S[i] * S[j])%MOD;
            
            if(j > i+1) {
                dp[i][j] = (dp[i][j] + dp[i][j-1])%MOD;
            }

            if (i > 0) {
                dp[i][j] = (dp[i][j] + dp[i-1][j])%MOD;
            }

            if (i > 0 && j > i+1) {
                dp[i][j] = (dp[i][j] - dp[i-1][j-1])%MOD;
            }

        }        
    }

    while (q--)
    {
        int l, r;
        ll ans;
        cin >> l >> r;

        l --; r--;

        if (l == r) {
            ans = S[l]; //?
        } else if (l == 0) {
            ans = dp[r-1][r];
        } else {
            ans = dp[r-1][r] - dp[l-1][r];
        }

        cout << ans << '\n';
    }

    return 0;
}
