import java.math.BigInteger;
import java.util.Scanner;

public class J {
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);

        int n = input.nextInt();
        int q = input.nextInt();

        BigInteger [][] dp = new BigInteger[n][n];
        BigInteger [] S = new BigInteger[n];

        for (int i = 0; i < n; i++) {
            S[i] = BigInteger.valueOf(input.nextInt());
        }

        for (int i = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                dp[i][j] = S[i].multiply(S[j]);

                if(j > i+1) {
                    dp[i][j] = dp[i][j].add(dp[i][j-1]);
                }

                if (i > 0) {
                    dp[i][j] = dp[i][j].add(dp[i-1][j]);
                }

                if (i > 0 && j > i+1) {
                    dp[i][j] = dp[i][j].subtract(dp[i-1][j-1]);
                }
            }
        }

        for (int i = 0; i < q; i++) {
            int l = input.nextInt();
            int r = input.nextInt();

            r--;
            l--;

            BigInteger ans;

            if (l == r) {
                ans = S[l]; //?
            } else if (l == 0) {
                ans = dp[r-1][r];
            } else {
                ans = dp[r-1][r].subtract(dp[l-1][r]);
            }

            System.out.println(ans.mod(new BigInteger("10000000007")));
        }

        input.close();
    }
}