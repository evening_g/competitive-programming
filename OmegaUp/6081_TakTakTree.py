n = int(input())

i = 0

while (n-1) % 11 != 0:
    n *= 2
    i += 1

print(f"{i} {n}")

