/**
 * status: AC
 * keywords: math geometry
 * */

#include <bits/stdc++.h>

using namespace std;

struct Rectangle
{
    int left, right, top, bottom;

    int area() {
        return (right-left)*(top-bottom);
    }
};

Rectangle intersection(const Rectangle &a, const Rectangle &b) {
    Rectangle c = {-1, -1, -1, -1};

    if (a.top <= b.top and a.top >= b.bottom) {
        // a.top is inside b
        c.top = a.top;
    }

    if (a.bottom >= b.bottom and a.bottom <= b.top) {
        // a.top is inside b
        c.bottom = a.bottom;
    }

    if (a.right <= b.right and a.right >= b.left) {
        // a.right is inside b
        c.right = a.right;
    }

    if (a.left >= b.left and a.left <= b.right) {
        // a.left is inside b
        c.left = a.left;
    }

    return c;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int x1, y1, x2, y2;
    cin >> x1 >> y1 >> x2 >> y2;

    Rectangle a = {min(x1, x2), max(x1, x2), max(y1, y2), min(y1, y2)};

    cin >> x1 >> y1 >> x2 >> y2;

    Rectangle b = {min(x1, x2), max(x1, x2), max(y1, y2), min(y1, y2)};
    
    Rectangle ab = intersection(a, b);
    Rectangle ba = intersection(b, a);

    // replace -1 with values
    Rectangle intersec = {max(ab.left, ba.left), max(ab.right, ba.right), max(ab.top, ba.top), max(ab.bottom, ba.bottom)};

    int total = a.area() + b.area();
    // check if there are still -1
    // if there are, there's no intersection
    if (intersec.left != -1 and intersec.right != -1 and intersec.top != -1 and intersec.bottom != -1)
        total -= intersec.area();

    cout << total << '\n';

    return 0;
}
