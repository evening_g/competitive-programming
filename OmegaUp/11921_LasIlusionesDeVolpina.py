n = int(input())
v = list(map(int, input().split()))

p = 1
for i in v:
    p *= i

for i in v:
    print(p//i, end=' ')
print()