/**
 * status: AC
 * keywords: graph bfs bfs-depth
 * source: https://omegaup.com/arena/problem/OIEG2013SSB
 *
 * It didn't worked because of fast I/O
 * */

#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;
const int MAX = 101;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int reached = 0;

int n, e, t;
vector<set<int>> adj;

void bfs() {
    queue<int> q;
    q.push(n);
    vi depth(n+1, -1);
    depth[n] = 0;

    while (q.size()) {
        int u = q.front();
        q.pop();

        for (int v : adj[u]) {
            if (depth[v] == -1) {
                q.push(v);
                depth[v] = depth[u] + 1;
                if (depth[v] < t) {
                    reached ++;
                } else {
                    // there are no more nodes
                    // with distance shorter that `t`
                    return;
                }
            }
        }
    }
}

int main() {
    cin >> n >> e >> t;
    adj.resize(n+1);

    while (e --) {
        int u, v;
        cin >> u >> v;
        adj[u].insert(v);
        adj[v].insert(u);
    }

    bfs();

    cout << reached << '\n';

    return 0;
}
