// Rust template for Competitive Programming
use std::collections::HashSet;

#[allow(unused_macros)]
macro_rules! read {
    ($out:ident as $type:ty) => {
        let mut inner = String::new();
        std::io::stdin().read_line(&mut inner).expect("A String");
        let $out = inner.trim().parse::<$type>().expect("Parsable");
    };
}

#[allow(unused_macros)]
macro_rules! read_str {
    ($out:ident) => {
        let mut inner = String::new();
        std::io::stdin().read_line(&mut inner).expect("A String");
        let $out = inner.trim();
    };
}

#[allow(unused_macros)]
macro_rules! read_vec {
    ($out:ident as $type:ty) => {
        let mut inner = String::new();
        std::io::stdin().read_line(&mut inner).unwrap();
        let $out = inner
            .trim()
            .split_whitespace()
            .map(|s| s.parse::<$type>().unwrap())
            .collect::<Vec<$type>>();
    };
}

#[allow(unused_macros)]
macro_rules! next {
    ($type:ty) => {
        {
            let mut inner = String::new();
            std::io::stdin().read_line(&mut inner).expect("Strign was expected");
            let out = inner.trim().parse::<$type>().expect("Parsable");
            out
        }
    }
}

fn main(){
    let t = next!(u32);
    
    for _ in 0..t {
        let mut abc = HashSet::with_capacity(26);
        let s = next!(String);

        for c in s.chars() {
            abc.insert(c);
        }

        if abc.len() == 26 {
            println!("PERFECT");
        } else {
            println!("NO WAY");
        }
    }

    
}
