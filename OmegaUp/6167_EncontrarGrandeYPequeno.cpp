#include <bits/stdc++.h>

using namespace std;

int main() {
    int n;
    cin >> n;

    int v[n];

    int min = INT_MAX;
    int max = INT_MIN;
    int l, u;
    for (int i = 0; i < n; i++) {
        cin >> v[i];
        if (v[i] < min) {
            l = i;
            min = v[i];
        }
        if (v[i] > max) {
            u = i;
            max = v[i];
        }
    }

    swap(v[0], v[u]);
    swap(v[n-1], v[l]);

    for (int i = 0; i < n; i++) {
        cout << v[i] << '\n';
    }
}
