#include <stdio.h>

void print(int n, char *first) {
    if (*first) {
        printf("%d", n);
        *first = 0;
    } (else) {
        printf(" x %d", n);
    }
}

int main(){
    int n, p;
    scanf("%d", &n);

    if (!n) n++;

    char first = 1;

    while (n % 2 == 0) {
        print(n, *first);
        n /= 2;
    }

    return 0;
}
