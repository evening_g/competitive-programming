#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int t;
    string s;

    cin >> t;

    while (t--) {
        cin >> s;
        sort(s.begin(), s.end(), greater<char>());

        string a = s.substr(0, s.length() - 1);
        int b = s[s.length() - 1] - '0';

        long long ans = stoll(a) + b;

        cout << ans << '\n';
    }

    return 0;
}
