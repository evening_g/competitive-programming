/**
 * 
 * 
 * 2023-11-22 
*/

#include <bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    string s;

    cin >> t;
    
    while (t--)
    {
        cin >> s;

        set<char> abc;

        for (char c : s) {
            abc.insert(c);
        }

        if (abc.size() == 26) {
            cout << "PERFECT\n";
        } else {
            cout << "NO WAY\n";
        }
    }
    

    return 0;
}
