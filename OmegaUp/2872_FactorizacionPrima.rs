#[allow(unused_macros)]
macro_rules! next {
    ($type:ty) => {
        {
            let mut inner = String::new();
            std::io::stdin().read_line(&mut inner).expect("Strign was expected");
            let out = inner.trim().parse::<$type>().expect("Parsable");
            out
        }
    }
}

fn print(n: u32, first: &mut bool) {
    if *first {
        print!("{}", n);
        *first = false;
    } else {
        print!(" x {}", n);
    }
}

fn main(){
    let mut n = next!(u32);
    let mut p = 3_u32;
    let mut first = true;

    while n > 0 && n % 2 == 0 {
        print(2, &mut first);
        n /= 2;
    }

    while p*p <= n {
        while n % p == 0 {
            print(p, &mut first);
            n /= p;
        }
        p += 2;
    }

    if n > 1 {
        print(n, &mut first);
    }

    println!();
}
