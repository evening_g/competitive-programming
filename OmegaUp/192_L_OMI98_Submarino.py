INF = int(1e9)

def bfs(root, depth, adj):
    q = list()
    q.append(root)


    while len(q) > 0:
        u = q.pop(0)
        
        for v in adj[u]:
            if depth[v] > depth[u]+1:
                q.append(v)
                depth[v] = depth[u] + 1        


def main():
    n, e, t = map(int, input().split())
    adj = [list() for _ in range(n+1)]
    depth = [INF for _ in range(n+1)]
    depth[n] = 0

    for _ in range(e):
        u, v = map(int, input().split())
        adj[u].append(v)
        adj[v].append(u)

    bfs(n, depth, adj)

    count = 0
    for d in depth[1:n]:
        if d < t:
            count += 1

    print(count)


if __name__ == "__main__":
    main()
