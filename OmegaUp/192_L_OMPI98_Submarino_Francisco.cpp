#include <bits/stdc++.h>
using namespace std;

#define all(x) x.begin(), x.end()

using ll = long long;
using ull = long long;
using vll = vector<ll>;
using pll = pair<long long, long long>;
using vpll = vector<pll>;
using vc = vector<char>;

void solve() { 
  ll n, e, t;
  vector<vll> adj;
  
  cin >> n >> e >> t;
  adj.resize(n + 1);
  
  for (ll i = 0; i < e; i++) {
    ll a, b;
    cin >> a >> b;
    adj[a].push_back(b);
    adj[b].push_back(a);
  }

  queue<pll> dfs;
  vc visited(n + 1, false);
  vll dis(n + 1, 1e5);

  dfs.push({n, 0});

  while (!dfs.empty()) {
    ll d = dfs.front().second;
    ll actual = dfs.front().first;

    dfs.pop();

    if (visited[actual]) continue;
    dis[actual] = d;
    visited[actual] = true;

    for (auto edge: adj[actual]) {
      if (visited[edge]) continue;
      dfs.push({edge, d + 1});
    }
  }

  ll c = 0;
  
  for (ll ds: dis) {
    if (ds < t) c++;
  }

  cout << c - 1 << '\n';
}

int main() {
  cin.tie(0);
  ios_base::sync_with_stdio(0);
  ll t = 1;

  //cin >> t;

  while (t--) {
    solve();
  }
}
