def main():
    n = int(input())

    if n == 0:
        print("1")
        return

    m = (2*n)+1

    v = [0 for _ in range(m)]
    v[1] = 1

    for j in range(1, m):
        for i in range(m-1, 1, -1):
            v[i] += v[i-1]

    print(v[n+1])

if __name__ == "__main__":
    main()
