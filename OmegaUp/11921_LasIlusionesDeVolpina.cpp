#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);
    
    int n;
    cin >> n;
    vll v(n);

    readv(v);

    ll prod = 1;
    int zeros_count = 0;
    for (ll &i : v) {
        if (i == 0) {
            zeros_count ++;
        } else {
            prod *= i;
        }
    }

    for (ll &i : v) {
        if (zeros_count == 0) {
            cout << prod/i << ' ';
        } else if (zeros_count == 1 and i == 0) {
            cout << prod << ' ';
        } else {
            cout << "0 ";
        }
    }

    cout << '\n';

    return 0;
}
