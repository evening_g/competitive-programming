#[allow(unused_macros)]
macro_rules! read {
    ($out:ident as $type:ty) => {
        let mut inner = String::new();
        std::io::stdin().read_line(&mut inner).expect("A String");
        let $out = inner.trim().parse::<$type>().expect("Parsable");
    };
}

#[allow(unused_macros)]
macro_rules! read_str {
    ($out:ident) => {
        let mut inner = String::new();
        std::io::stdin().read_line(&mut inner).expect("A String");
        let $out = inner.trim();
    };
}

#[allow(unused_macros)]
macro_rules! read_vec {
    ($out:ident as $type:ty) => {
        let mut inner = String::new();
        std::io::stdin().read_line(&mut inner).unwrap();
        let $out = inner
            .trim()
            .split_whitespace()
            .map(|s| s.parse::<$type>().unwrap())
            .collect::<Vec<$type>>();
    };
}

#[allow(unused_macros)]
macro_rules! next {
    ($type:ty) => {
        {
            let mut inner = String::new();
            std::io::stdin().read_line(&mut inner).expect("Strign was expected");
            let out = inner.trim().parse::<$type>().expect("Parsable");
            out
        }
    }
}

const MAX: usize = 1_000_000usize;

fn main(){
    let n = next!(u32);

    let mut array: [u32; MAX] = [0; MAX];

    for _ in 0..n {
        let x = next!(i32);
        if x < 0 {
            return;
        }

        array[x as usize] += 1;
    }

    for i in 0..MAX {
        if array[i] > 0 {
            println!("{}", i);
        }
    }
}
