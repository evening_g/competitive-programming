#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    int n, q;
    cin >> n >> q;

    string s;
    cin >> s;

    vi v(n);

    for (int i = 1; i < n; i++) {
        v[i] = s[i-1] == s[i];
    }

    for (int i = 1; i < n; i++) {
        v[i] += v[i-1];
    }

    while (q--) {
        int l, r;
        cin >> l >> r;

        l--;
        r--;

        int x = v[r] - v[l];

        cout << x << '\n';
    }
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    solve();

    return 0;
}
