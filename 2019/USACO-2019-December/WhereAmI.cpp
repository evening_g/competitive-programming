/**
 * http://www.usaco.org/index.php?page=viewproblem2&cpid=964
 * 
 * 2023-11-22 
*/

#include <bits/stdc++.h>

using namespace std;

bool is_valid(const string &farms, const int size) {
    set<string> ranges;

    for (int i = 0; i <= farms.size() - size; i++)
    {
        string range = farms.substr(i, size);

        if (ranges.count(range)) {
            return false;
        }

        ranges.insert(range);
    }
    
    return true;
}

int lower_bound(const string &farms, const int n) {
    int upper = n, lower = 1, mid;

    while (lower < upper) {
        mid = (lower+upper)/2;

        if (is_valid(farms, mid)) {
            // is valid, go down
            upper = mid - 1;
        } else {
            // not valid, go up
            lower = mid + 1;
        }
    }

    if (!is_valid(farms, lower)) {
        lower++;
    }

    return lower;
}

int main() {
    freopen("whereami.in", "r", stdin);
    freopen("whereami.out", "w", stdout);

    int n;
    string farms;
    cin >> n >> farms;

    cout << lower_bound(farms, n) << '\n';

    return 0;
}
