#include<iostream>
#include<algorithm>

using namespace std;

int main() {
    string a, b;

    cin >> a >> b;
    int n = a.length();

    for (size_t i = 0; i < n; i++)
    {
        char ai = tolower(a[i]);
        char bi = tolower(b[i]);

        if (ai != bi) {
            if (ai < bi) {
                cout << "-1\n";
            } else {
                cout << "1\n";
            }

            return 0;
        }
    }

    cout << "0\n";    

    return 0;
}
