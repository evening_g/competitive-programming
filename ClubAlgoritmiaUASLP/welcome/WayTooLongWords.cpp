#include <iostream>

using namespace std;

int main() {
    int n;
    cin >> n;

    for (size_t i = 0; i < n; i++)
    {
        string s;
        cin >> s;

        if (s.length() > 10) {
            cout << s[0] + to_string(s.length()-2) + s.back() + '\n';
        } else {
            cout << s << '\n';
        }
    }
    

    return 0;
}