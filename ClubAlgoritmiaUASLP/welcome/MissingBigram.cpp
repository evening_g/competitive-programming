#include<iostream>

using namespace std;

void solve() {
    int n;
    cin >> n;

    string bg;
    cin >> bg;

    char last = bg[1];

    string result = bg;

    for (int i = 0; i < n-1; i++)
    {
        cin >> bg;
        if (last != bg[0]) {
            result += bg;
        } else {
            last = bg[1];
            result += last;
        }
    }
    
    cout << result << '\n';
}

int main() {
    int t;

    cin >> t;

    while (t--)
        solve();

    return 0;
}