| Nivel | Tema                            | Link                                              |
| ----- | ------------------------------- | ------------------------------------------------- |
| 800   | greedy, strings, implementation | https://codeforces.com/problemset/problem/1925/A  |
| 800   | brute force, strings            | https://codeforces.com/problemset/problem/1775/A1 |
| 900   | greedy, strings                 | https://codeforces.com/problemset/problem/1775/A2 |
| 1100  | game theory                     | https://codeforces.com/problemset/problem/1920/B  |
| 1000  | sorting                         | https://codeforces.com/problemset/problem/1876/A  |
| 1100  | math                            | https://codeforces.com/problemset/problem/1914/C  |
| 1300  | math                            | https://codeforces.com/problemset/problem/1922/C  |