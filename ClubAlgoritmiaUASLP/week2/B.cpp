#include <bits/stdc++.h>

using namespace std;

bool is_valid(const string &s, int &l, int &r) {
    string a = s.substr(0, l);
    string b = s.substr(l, r-l);
    string c = s.substr(r, s.size()-r);

    int left = a.compare(b);
    int right = b.compare(c);

    if (left * right == 0) { // any side is equal
        return true;
    }

    if (left * right <= -1) { // comparisons are different
        return true;
    }

    if (left + right < 0) { // a < b < c
        l = (l+r) / 2;
        //l ++;
    } else {                // a > b > c
        r = (l+r) / 2;
        //r --;
    }

    return false;
}

void solve() {
    string s;
    cin >> s;

    int l = 1, r = s.size()-1;

    while (!is_valid(s, l, r)) {
        if (l >= r) {
            cout << ":(\n";
        }
    }

    string a = s.substr(0, l);
    string b = s.substr(l, r-l);
    string c = s.substr(r, s.size()-r);
    
    cout << a << ' ' << b << ' ' << c << '\n';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;
    while (t--)
        solve();

    return 0;
}
