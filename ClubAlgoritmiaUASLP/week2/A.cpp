#include <bits/stdc++.h>

using namespace std;

void solve() {
    int n, k;

    cin >> n >> k;

    while (n--) {
        for (char letter = 'a'; letter < 'a'+k; letter++) {
            cout << letter;
        }
    }

    cout << '\n';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;
    while (t--)
        solve();

    return 0;
}
