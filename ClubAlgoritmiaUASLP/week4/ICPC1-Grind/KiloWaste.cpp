#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while (true)
#define readv(v)                                                               \
  for (auto& i : v)                                                            \
  cin >> i
#define printv(v)                                                              \
  for (auto& i : v)                                                            \
  cout << i << ' '

#define lsz(x) ~(x) & (x + 1)
#define lso(x) (x) & -(x)

/**
30

7 13 21

7x5 = 35 => 5
13x3 = 39 => 9
21x2 = 42 => 12
21+13 = 34 => 4
13x2 + 7 = 33 => 3

dp[i][j] = min(dp[i-1][j])

*/

mi dp;

void
fill_dp(const vi& values, const int& target)
{
  dp.resize(values.size(), vi(target + 1, 0));

  fill(all(dp[0]), INF);
  dp[0][0] = 0;

  for (int i = 1; i < values.size(); i++) {
    for (int j = 1; j <= target; j++) {
      int remaining;

      if (j - values[i] <= 0) {
        remaining = 0;
      } else {
        remaining = j - values[i];
      }

      dp[i][j] = min(dp[i - 1][j], values[i] + dp[i][remaining]);
    }
  }
}

int
main()
{
  cin.tie(0)->sync_with_stdio(0);
  cin.exceptions(cin.failbit);

  int k, p;

  cin >> k >> p;

  vi rices(p + 1);
  rices[0] = 0;

  for (int i = 1; i <= p; i++) {
    cin >> rices[i];
  }

  sort(all(rices));

  vi dinners(k);
  int max_dinner = 0;
  for (int i = 0; i < k; i++) {
    cin >> dinners[i];
    max_dinner = max(max_dinner, dinners[i]);
  }

  fill_dp(rices, max_dinner);

  for (int& dinner : dinners) {
    cout << dp[rices.size() - 1][dinner] - dinner << '\n';
  }

  return 0;
}
