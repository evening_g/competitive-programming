#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n, m;
    cin >> n >> m;

    vector<int> a(n);
    vector<int> b(m);

    for (int &i : a)
        cin >> i;

    for (int &j : b)
        cin >> j;

    sort(all(a), greater<int>());
    sort(all(b), greater<int>());

    ll total = accumulate(all(a), 0LL) * m;

    int row = 0;
    int min_remaining = m;

    for (int col = 0; col < m; col ++) {
        if (row >= n or a[row] > b[col]) {
            cout << "-1\n";
            return 0;
        }

        if (a[row] == b[col])
            continue;

        if (min_remaining <= 1) {
            min_remaining = m;
            row ++;
        }

        total += (b[col] - a[row]);
        min_remaining --;
    }

    cout << total << '\n';

    return 0;
}
