#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    int n, k;
    cin >> n >> k;

    vi a(n);
    vi b(n);

    readv(a);
    readv(b);

    sort(all(a));
    sort(all(b), greater<int>());

    for (int i = 0; i < k; i++)
        if (a[i] < b[i])
            swap(a[i], b[i]);
        
    cout << accumulate(all(a), 0) << '\n';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}
