#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;

void solve() {
    int n;
    string s;
    cin >> n >> s;

    int total_ballons = 0;
    vi solved(26, 0);

    for (char c : s) {
        int i = c-'A'; 
        if (solved[i]) {
            total_ballons += 1;
        } else {
            solved[i] = 1;
            total_ballons += 2;
        }
    }

    cout << total_ballons << '\n';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
    {
        solve();
    }
    
    return 0;
}
