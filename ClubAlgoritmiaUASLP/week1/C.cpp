#include <bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int k, r;
    cin >> k >> r;

    int total = 1;
    while (total * k % 10 != 0 and total * k % 10 != r) total ++;

    cout << total << '\n';

    return 0;
}
