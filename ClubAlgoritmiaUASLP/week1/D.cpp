#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    int n;
    cin >> n;

    list<int> a;

    for (int i = 0; i < n; i++)
    {
        int x;
        cin >> x;
        a.push_back(x);
    }

    bool is_dense = false;
    int modifications = 0;

    while (!is_dense) {
        is_dense = true;
        for (auto it = a.begin(); it != prev(a.end()); it = next(it)) {
            if (max(*it, *next(it)) > 2*min(*it, *next(it))) {
                is_dense = false;
                a.insert(next(it), ceil((double) max(*it, *next(it))/2.0));
                modifications ++;
            }
        }
    }

    cout << modifications << '\n';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--) {
        solve();
    }

    return 0;
}
