#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

bool cmp(const pi &a, const pi&b) {
    if (a.first == b.first)
        return a.second <= b.second;

    return a.first < b.first;
}

int solve() {
    int n;
    cin >> n;

    vpi a(n);

    for (int i = 0; i < n; i++) {
        a[i] = {0, i};
    }

    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        a[x].first ++;
    }

    sort(all(a), cmp);

    vi c;

    int i = 0;
    bool alice = true;

    while (i < n) {
        if (a[i].first) {
            a[i].first --;

            if (alice) {
                c.push_back(a[i].second);
                i ++;
            }
            
            alice = !alice;
        } else {
            i ++;
        }
    }

    int mex = 0;

    sort(all(c));

    for (int i : c) {
        if (i == mex) {
            mex ++;
        } else if (i > mex) {
            break;
        }
    }
    
    return mex;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        cout << solve() << '\n';


    return 0;
}
