#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    int n, k;
    cin >> n >> k;

    set<int> single_a, single_b, duplicated_a, duplicated_b;

    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;

        if (single_a.count(x)) {
            duplicated_a.insert(x);
            single_a.erase(x);
        } else {
            single_a.insert(x);
        }
    }

    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;

        if (single_b.count(x)) {
            duplicated_b.insert(x);
            single_b.erase(x);
        } else {
            single_b.insert(x);
        }
    }

    vi ka, kb;

    while (ka.size() < 2*k and kb.size() < 2*k) {
        bool added = false;
        if (!duplicated_a.empty() && ka.size() <= 2*k-2) {
            ka.push_back(*duplicated_a.begin());
            ka.push_back(ka.back());
            duplicated_a.erase(ka.back());
            added = true;
        }
        if (!duplicated_b.empty() && kb.size() <= 2*k-2) {
            kb.push_back(*duplicated_b.begin());
            kb.push_back(kb.back());
            duplicated_b.erase(kb.back());
            added = true;
        } 
        
        if (!added) {
            ka.push_back(*single_a.begin());
            kb.push_back(ka.back());
            single_a.erase(ka.back());
            single_b.erase(ka.back());
        }
    }

    printv(ka);
    cout << '\n';

    printv(kb);
    cout << '\n';
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();


    return 0;
}
