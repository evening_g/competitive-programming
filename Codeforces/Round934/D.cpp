#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

string oddify(const string &s) {
    string oddified = "#";

    for (char c : s)
        oddified = oddified + c + '#';

    return oddified;
}

void count_palindromes(const string &s, vi &p, int i) {
    for (; i-p[i] >= 0 and i+p[i] < (int) s.size() and s[i-p[i]]; p[i]++);
    p[i] --;
}

void manacher_odd(vi &p, const string &s) {
    int l = 0, r = 0;
    for (int i = 0; i < (int) s.size(); i++) {
        if (i <= r) {
            int m = l+r-i;
            p[i] = m-l;
            if (m >= 0 and m-p[m] >= l) {
                p[i] = p[m];
            }
        }
        count_palindromes(s, p, i);
        l = i-p[i];
        r = i+p[i];
    }
}

void manacher(vi &odd, vi &even, const string &s) {
    odd.resize(s.size());
    even.resize(2*s.size()+1);
    fill(all(odd), 0);
    fill(all(even), 0);

    string oddified = oddify(s);

    manacher_odd(odd, s);
    manacher_odd(even, oddified);
}

void solve() {
    int n, q;
    cin >> n >> q;

    string s;
    cin >> s;

    // TODO, use instead a map with the position of the bad area start
    // and its lenght
    // use lowerbound to search the nearest bad area

    vi odd, even;
    manacher(odd, even, s);

    while (q--) {
        int l, r;
        cin >> l >> r;
    }
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}
