/**
 * ICPC Mexico Regional 2019, Problem I
 * status: AC
 * keywords: graph
 * source: https://codeforces.com/group/fz1bXzUa6c/contest/474363/problem/I
*/

#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vl = vector<ll>;
using vii = vector<pair<int, int>>;
using ii = pair<int, int>;
using mi = vector<vector<int>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (int &i : v) cin >> i
#define printv(v) for (int &i : v) cout << i << ' '

struct Node {
    ll id;
    vector<Node*> adj;

    void add(Node *v) {
        adj.push_back(v);
    }

    bool is_email() {
        return adj.empty();
    }
};

set<ll> emailed;
vl emails_sent; // emails sent if start on node i
vector<Node> graph;

ll dfs(Node *u) {
    if (emails_sent[u->id] != -1) {
        // If we already visited this node
        // just retrieve the value
        return emails_sent[u->id];
    }

    if (u->is_email()) {
        // is an email
        emailed.insert(u->id);

        // 1 more email sent
        emails_sent[u->id] = 1;
        return 1;
    }

    ll result = 0;
    for (Node* v : u->adj) {
        result = (result%MOD + dfs(v)%MOD) % MOD;
    }

    emails_sent[u->id] = result;
    return result;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);

    int n, l;
    cin >> n >> l;

    graph.resize(n+1);
    emails_sent.resize(n+1);
    fill(all(emails_sent), -1);

    for (int u = 1; u <= n; u++)
        graph[u].id = u;

    for (int u = 1; u <= l; u++)
    {
        int k;
        cin >> k;

        while (k--)
        {
            int v;
            cin >> v;

            graph[u].add(&graph[v]);
        }
    }

    ll before = dfs(&graph[1]) % MOD;
    ll after = emailed.size() % MOD;

    cout << before << ' ' << after << '\n';

    return 0;
}
