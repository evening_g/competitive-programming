/**
 * ICPC Mexico Regional 2019, Problem I
 * Status: AC
 * */

#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vii = vector<pair<int, int>>;
using ii = pair<int, int>;
using mi = vector<vector<int>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
             tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
             tree_order_statistics_node_update>;

const int MOD = (int) 10e9+7;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (int &i : v) cin >> i
#define printv(v) for (int &i : v) cout << i << ' '

int main() {
    cin.tie(0)->sync_with_stdio(0);

    int n, x;
    cin >> n >> x;

    vi a(n);
    readv(a);
    a.push_back(INF);

    int max_viewpoints = 1;
    int current_count = 1;

    for (int i = 1; i <= n; i++)
        if (a[i]-a[i-1] <= x)
            current_count ++;
        else
            if (current_count > max_viewpoints)
                max_viewpoints = current_count;
            current_count = 1;
    
    cout << max_viewpoints << '\n';

    return 0;
}
