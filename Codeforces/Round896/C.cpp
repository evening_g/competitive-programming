#include <bits/stdc++.h>

using namespace std;

void solve()
{
    // rows, cols
    int n, m;
    cin >> n >> m;

    vector<vector<int>> M(n, vector<int>(m));

    // m = 1 is a special case, because i%(m-1)
    // results in division by 0
    if (m == 1)
    {
        for (int i = 0; i < n; i++)
            M[i][0] = 0;
    }
    else
    {
        for (int i = 0; i < n; i++)
            M[i][0] = i % (m - 1) + 1;
    }

    for (int j = 1; j < m; j++)
        for (int i = 0; i < n; i++)
            M[i][j] = (M[i][j - 1] + 1) % m;

    int ans = n + 1;
    if (m <= n)
        ans = m;
    if (m == 1) // Again 0 special case
        ans = 0;

    cout << ans << '\n';
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
            cout << M[i][j] << ' ';
        cout << '\n';
    }
}

int main()
{
    int t;

    cin >> t;

    while (t--)
        solve();

    return 0;
}