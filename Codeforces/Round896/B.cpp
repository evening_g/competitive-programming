#include <bits/stdc++.h>

using ll = long long;

using namespace std;

struct Point {
    ll x;
    ll y;
};

ll manhattan_distance(const Point &a, const Point &b) {
    return abs(a.x - b.x) + abs(a.y - b.y);
}

void solve()
{
    int n, k, a, b;
    cin >> n >> k >> a >> b;

    vector<Point> cities(n);

    for (Point &city : cities)
        cin >> city.x >> city.y;

    Point A = cities[a-1];
    Point B = cities[b-1];

    Point nearest_to_A = B;
    Point nearest_to_B = A;

    for (int i = 0; i < k; i++)
    {
        // Find nearest major city to A and B
        if (manhattan_distance(A, cities[i]) < manhattan_distance(A, nearest_to_A))
            nearest_to_A = cities[i];

        if (manhattan_distance(B, cities[i]) < manhattan_distance(B, nearest_to_B))
            nearest_to_B = cities[i];
    }
    
    ll ans = 0;

    if (a > k)
        ans += manhattan_distance(A, nearest_to_A);

    if (b > k)
        ans += manhattan_distance(B, nearest_to_B);

    if (manhattan_distance(A, B) < ans)
        ans = manhattan_distance(A, B);

    cout << ans << '\n';
}

int main()
{
    int t;

    cin >> t;

    while (t--)
        solve();

    return 0;
}