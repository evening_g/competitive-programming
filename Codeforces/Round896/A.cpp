#include<bits/stdc++.h>

using namespace std;

void solve() {
    int n;
    cin >> n;
    
    for (int i = 0; i < n; i++)
    {
        int x;
        cin >> x; 
    }
    
    if (n % 2 == 0) {
        // even

        cout << "2\n";
        cout << "1 " << n << '\n';
        cout << "1 " << n << '\n';
    } else {
        // odd

        cout << "4\n";
        cout << "1 " << (n-1) << '\n';
        cout << "1 " << (n-1) << '\n';
        cout << (n-1) << ' ' << n << '\n'; 
        cout << (n-1) << ' ' << n << '\n'; 
    }
}

int main() {
    int t;

    cin >> t;

    while (t--)
        solve();

    return 0;
}