#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vl = vector<ll>;
using vii = vector<pair<int, int>>;
using ii = pair<int, int>;
using mi = vector<vector<int>>;

using indexed_multiset = tree<int, null_type, less_equal<int>, rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int, null_type, less<int>, rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while (true)
#define readv(v)      \
    for (auto &i : v) \
    cin >> i
#define printv(v)     \
    for (auto &i : v) \
    cout << i << ' '

#define lsz(x) ~(x) & (x + 1)
#define lso(x) (x) & -(x)

void solve()
{
    int n;
    cin >> n;

    vi a(n);
    vi b(n);

    readv(a);
    readv(b);

    sort(all(a));
    sort(all(b));

    ll r = 0;

    for (int i = 0; i < n; i++)
        r += a[i]+b[i];

    cout << r << '\n';
}

int main()
{
    cin.tie(0)->sync_with_stdio(0);

    int t;

    cin >> t;

    while (t--)
    {
        solve();
    }

    return 0;
}
