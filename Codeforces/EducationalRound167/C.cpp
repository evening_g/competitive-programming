#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

#define addA A += (int) a[i]
#define addB B += (int) b[i]

void solve() {
    int n;
    cin >> n;
    vector<short> a(n), b(n);
    readv(a);
    readv(b);

    int A = 0, B = 0;

    for (int i = 0; i < n; i++) {
        if (a[i] == b[i]) {
            if (a[i] >= 0) {
                // will increment
                if (A > B) {
                    addB;
                } else {
                    addA;
                }
            } else {
                // dec
                if (A > B) {
                    addA;
                } else {
                    addB;
                }
            }
        } else if (a[i] > b[i]) {
            addA;
        } else {
            addB;
        }
    }
    
    cout << min(A, B) << '\n';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}
