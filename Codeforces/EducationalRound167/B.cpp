#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    string a , b;
    cin >> b >> a;
    int A = a.size();
    int B = b.size();
    a = "^"+a;
    b = "^"+b;

    mi M(A+1, vi(B+1, 0));
    int best = 0;

    for (int i = 1; i <= A; i++) {
        for (int j = 1; j <= B; j++) {
            if (a[i] == b[j]) {
                M[i][j] = M[i-1][j-1] + 1;
                best = max(best, M[i][j]);
            } else {
                M[i][j] = M[i][j-1];
            }
        }
    }

    cout << A + B - best << '\n';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}
