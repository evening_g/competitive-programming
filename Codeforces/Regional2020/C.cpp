#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vl = vector<ll>;
using vii = vector<pair<int, int>>;
using ii = pair<int, int>;
using mi = vector<vector<int>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (int &i : v) cin >> i
#define printv(v) for (int &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
    cin.tie(0)->sync_with_stdio(0);

    int n;
    cin >> n;

    vi v(n);

    readv(v);

    int m = accumulate(all(v),0) / n;

    // table with more surplus glasses
    int s = 0;
    vi e(n);

    for (int i = 0; i < n; i++) {
        e[i] = v[i]-m;
        if (e[i]>e[s]) s=i;
    }

    int sumr = e[s];
    int prevr = e[s];
    for (int i = (s+1)%n; i!=s ; ++i%=n)
    {
        sumr += prevr+e[i];
        prevr += e[i];
    }

    reverse(all(e));
          
    s = n-s-1;
    int suml = e[s];
    int prevl = e[s];
    for (int i = (s+1)%n; i!=s ; ++i%=n)
    {
        suml += prevl+e[i];
        prevl += e[i];
    }

    cout << min(sumr, suml) << '\n';

    return 0;
}
