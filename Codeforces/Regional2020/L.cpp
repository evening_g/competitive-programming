// WA

#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vl = vector<ll>;
using vii = vector<pair<int, int>>;
using ii = pair<int, int>;
using mi = vector<vector<int>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (int &i : v) cin >> i
#define printv(v) for (int &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
    cin.tie(0)->sync_with_stdio(0);

    int n, x;
    cin >> n >> x;

    vii times(n);

    for (int i = 0; i < n; i++)
    {
        int s, d;
        cin >> s >>d;

        times[i].first = s;
        times[i].second = s+d;
    }

    sort(all(times));

    int mincount = INF;
    int starttime;

    for (int i = 0; i <= 480; i++)
    {
        int count = 0;

        for (auto [ start, end] : times) {
            if (start < i and end < i)
                continue;

            int time_in;
            if (start%x == i)
                time_in = start;
            else
                time_in = start - (start%x) + x + i;

            
            int time_out;
            if (end%x == i)
                time_out = end;
            else
                time_out = end - (end%x) + i;

            count += (time_out-time_in)/x + 1;
        }
        
        if (count < mincount) {
            mincount = count;
            starttime = i;
        }
    }
    
    cout << starttime << ' ' << mincount << '\n';

    return 0;
}
