# TLE

n = int(input())
A = list(map(int, input().split()))

s = 0

for a in A:
    s += 1<<a

count_ones = 0

count_ones = bin(s).count('1')

if count_ones in [1,2] and n > 1:
    print('Y')
else:
    print('N')
