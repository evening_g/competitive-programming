#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 100000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

vi coins = {1, 3, 6, 10, 15};
int MAX = 30;
vi dp(MAX, INF);

void build_dp() {
    dp[0] = 0;
    for (int coin : coins) {
        for (int i = 1; i < MAX; i++) {
            int new_value = INF;
            if (i >= coin) {
                new_value = dp[i-coin] + 1;
            }
            dp[i] = min(new_value, dp[i]);
        }
    }
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    build_dp();

    while(t--) {
        int n;
        cin >> n;

        int coins15 = max((n/15)-1, 0);
        // 15 <= remaining <= 30
        int result = coins15 + dp[n-coins15*15];

        cout << result << '\n';
    }

    return 0;
}
