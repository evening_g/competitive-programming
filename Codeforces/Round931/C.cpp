#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 100000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    int n, m;
    cin >> n >> m;

    int d, a, b;
    cout << "? 1 1" << endl;
    cout.flush();
    cin >> d;

    n --;
    m --;

    int ax, ay, bx, by, guess;
    if (d <= m) {
        cout << "? " << "1 " << d+1 << endl;
        cout.flush();
        cin >> a;
        ax = a/2;
    } else {
        cout << "? " << d-m+1 << ' ' << m+1 << endl;
        cout.flush();
        cin >> a;
        ax = d-m+a/2;
    }

    ay = -ax + d;

    if (d <= n) {
        cout << "? " << d+1 << " 1" << endl;
        cout.flush();
        cin >> b;
        bx = d-b/2;
    } else {
        cout << "? " << n+1 << ' ' << d-n+1 << endl;
        cout.flush();
        cin >> b;
        bx = n-b/2;
    }

    by = -bx + d;

    cout << "? " << ax+1 << ' ' << ay+1 << endl;
    cout.flush();
    cin >> guess;
    if (guess) {
        cout << "! " << bx+1 << ' ' << by+1 << endl;
    } else {
        cout << "! " << ax+1 << ' ' << ay+1 << endl;
    }
    cout.flush();
    return;
}

int main() {
    int t;
    cin >> t;
    
    while(t--)
        solve();

    return 0;
}
