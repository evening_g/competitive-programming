// keywords: tree dfs dp

#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using ld = long double;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

mi adj;
vector<float> ans;

ld dfs(int u, int p) {
    ld sum = 0;
    ld num = 0;

    for (const int &v : adj[u]) {
        if (v != p) {
            sum += dfs(v, u) + 1;
            num += 1;
        }
    }

    if (num == 0)
        return 0;
    
    return sum/num;
} 

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n;
    cin >> n;

    adj.resize(n+1);
    ans.resize(n+1);
    fill(all(ans), -1);


    for (int i = 1; i < n; i++)
    {
        int u, v;
        cin >> u >> v;

        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    ld mean = dfs(1, -1);

    cout << fixed << setprecision(7) << mean << '\n';

    return 0;
}
