#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    int n, m;
    cin >> n >> m;
    vector<vi> M(n+2, vi(m+2, -1));

    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            cin >> M[i][j];
        }
    }

    int di[] = {0, 0, 1, -1};
    int dj[] = {-1, 1, 0, 0};
    
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            int upp = 0;

            for (int k = 0; k < 4; k++) {
                if (M[i+di[k]][j+dj[k]] != -1) {
                    upp = max(upp, M[i+di[k]][j+dj[k]]);
                }
            }
            
            if (upp < M[i][j]) {
                M[i][j] = upp;
            }
        }
    }

    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            cout << M[i][j] << ' ';
        }
        cout << '\n';
    }
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}
