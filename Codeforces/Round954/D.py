"""
brute force
WA on test 2
"""

def main():
        t = int(input())
        for _ in range(t):
                solve()

def test(s: str, index: int) -> int:
        p = int(s[index]+s[index+1])

        v = list()
        for i in range(len(s)):
                if i == index+1:
                        continue
                if i == index:
                        v.append(p)
                else:
                        v.append(int(s[i]))

        if len(v) == 1:
                return v[0]

        res = 0
        ones = 0

        for i in v:
                if i == 0:
                        return 0
                if i == 1:
                        ones += 1
                else:
                        res += i

        if ones == len(v):
                return 1

        return res


def solve():
        n = int(input())
        s = input()

        best = int(1e9)
        for i in range(len(s)-1):
               best = min(best, test(s, i))

        print(best)


if __name__ == "__main__":
        main()
