// Road to savings AC
// https://codeforces.com/gym/104614/problem/I
// keywords: graph dijsktra

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;

const int INF = 1000000000;

int N;
mi W;

void dijkstra(int s, vi &dist, vi &path) {
    dist.assign(N+1, INF);
    path.assign(N+1, -1);

    dist[s] = 0;
    priority_queue<pi, vpi, greater<pi>> q;
    q.push({0, s});

    while (!q.empty()) {
        int u = q.top().second;
        int dist_u = q.top().first;

        q.pop();

        if (dist_u != dist[u])
            continue;

        for (int v = 1; v <= N; v++) {
            if (W[u][v] == INF)
                continue;

            if (dist[u] + W[u][v] < dist[v]) {
                dist[v] = dist[u] + W[u][v];
                path[v] = u;
                q.push({dist[v], v});
            }
        }
    }
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int m, a, b;
    cin >> N >> m >> a >> b;

    W.assign(N+1, vi(N+1, INF));

    for (int i = 0; i < m; i++) {
        int u, v, w;
        cin >> u >> v >> w;
        W[u][v] = W[v][u] = w;
    }

    // dijkstra a->b
    vi dist_a, path_a, dist_b, path_b;
    dijkstra(a, dist_a, path_a);
    dijkstra(b, dist_b, path_b);
    
    int total = 0;
    for (int u = 1; u <= N; u++) {
        for (int v = u+1; v <= N; v++) {
            // For each edge, checks if edge is in the shortest path
            // if the path from a->u + u->v + v->b == a->b, then u->v is in the shortest path
            if (    dist_a[u] + W[u][v] + dist_b[v] != dist_a[b]
                and dist_b[u] + W[u][v] + dist_a[v] != dist_b[a]
                and W[u][v] != INF) {
                    total += W[u][v];
                }
        }
    }
    
    cout << total << '\n';

    return 0;
}
