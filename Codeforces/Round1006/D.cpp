#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    int n;
    cin >> n;
    vi v(n);
    readv(v);
    int invs = 0;
    
    pi bestp = make_pair(0, 0);
    int bestv = invs;
    for (int l = 0; l < n-1; l++) {
        int curinvs = invs;
        for (int r = l+1; r < n; r++) {
            if (v[l] < v[r])
                curinvs ++;
            else if (v[l] > v[r])
                curinvs --;
                
            if (curinvs < bestv) {
                bestv = curinvs;
                bestp = make_pair(l, r);
            }
        }
    }
    
    cout << bestp.first + 1 << ' ' << bestp.second + 1 << '\n';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}
