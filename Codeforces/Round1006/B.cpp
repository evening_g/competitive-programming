// tags: combinatory easy

#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    // val is the number of subsequences equal to -_-
    // maximize this
    // out max subseq
    // _-_-_-_-_-
    // ---___--
    int n;
    cin >> n;
    string s;
    cin >> s;
    ull eye = 0;
    ull mouth = 0;
    for (char c : s) {
        if (c == '-') eye ++;
        if (c == '_') mouth ++;
    }
    ull eye_left = eye / 2;
    ull eye_right = eye / 2 + eye % 2;
    ull result = eye_left * mouth * eye_right;
    cout << result << '\n';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}