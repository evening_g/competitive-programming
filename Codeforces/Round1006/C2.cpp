// tags: bitwise

#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
        int n, x;
        cin >> n >> x;
        vi v(n, 0);
        int remaining = x;
        for (int i = 0; i < n; i++) {
                if ((i | x) == x) {
                        v[i] = i;
                        remaining &= ~i;
                } else {
                        break;
                }
        }
        v[n-1] |= remaining;
        printv(v);
        cout << '\n';
}

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int t;
        cin >> t;

        while (t--)
            solve();

        return 0;
}
