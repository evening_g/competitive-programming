// https://codeforces.com/contest/1398/problem/B

#include <bits/stdc++.h>

using namespace std;

#define all(v) v.begin(), v.end()

struct Substring {
    char character;
    int size;
};

bool compare(const Substring &a, const Substring &b) {
    // return true if `a` goes before `b`

    return a.size > b.size;
}

int solve(const string &binstr) {
    bool turn = true; // Alice's turn
    int alice = 0; // Alice's points

    // Fill an array counting the number consecutive
    // equal numbers
    vector<Substring> substrings;


    substrings.push_back({binstr[0], 1});

    for (size_t i = 1; i < binstr.size(); i++)
    {
        if (binstr[i] == substrings.back().character) {
            substrings.back().size ++;
        } else {
            substrings.push_back({binstr[i], 1});
        }
    }
    
    sort(all(substrings), compare);

    for (const Substring &substring : substrings) {
        if (substring.character == '1') {
            if (turn) {
                alice += substring.size;
            }
            turn = !turn;
        }
    }

    return alice;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while(t--) {
        string binstr;
        cin >> binstr;

        cout << solve(binstr) << '\n';
    }


    return 0;
}
