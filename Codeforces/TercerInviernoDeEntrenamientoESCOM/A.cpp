#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int n;
        cin >> n;
        vi v(n);
        readv(v);

        int sum = 0;
        int i = 0;
        while (i < n) {
                int maxnum = -INF;
                while (i < n and v[i] >= 0) {
                        maxnum = max(maxnum, v[i]);
                        i++;
                }
                if (maxnum != -INF) {
                        sum += maxnum;
                        maxnum = -INF;
                        while (i < n and v[i] < 0) {
                                maxnum = max(maxnum, v[i]);
                                i++;
                        }
                        if (i < n - 1)
                                sum += maxnum != -INF ? maxnum : 0;
                } else {
                        i++;
                }
        }

        cout << sum << '\n';

        return 0;
}
