#include <bits/stdc++.h>

using namespace std;

int main() {
	int g, l;

	cin >> g >> l;

	string known(l, '?');

	map<char, int> should_use;
	set<char> should_not;
	vector<set<char>> available(l);

	for (int i = 0; i < l; i++) {
		for (char c = 'a'; c < 'z'; c++) {
			available[i].insert(c);
		}
	}

	g --;
	while (g--) {
		string w, r;
		cin >> w >>r;
		for (int i = 0; i < l; i++) {
			if (r[i] == 'G') {
				known[i] = w[i];
				should_use[w[i]] = 0;
			} else if(r[i] == 'Y') {
				available[i].erase(w[i]);
				if (should_use.count(w[i])) {
					should_use[w[i]] ++;
				} else {
					should_use[w[i]] = 1;
				}
			} else {
				should_not.insert(w[i]);
				available[i].erase(w[i]);
			}
		}	
	}

	for (int i = 0; i < l; i++) {
		if (known[i] != '?') continue;

		bool found = false;
		for (auto &[letter, count] : should_use) {
			if (available[i].count(letter) == 0) continue;

			if (count > 0) {
				known[i] = letter;
				found = true;
				should_use[letter] --;
			}

            if (should_not.count(letter) == 0) {
				known[i] = letter;
				found = true;
				should_use[letter] --;
                continue;
			}
		}

		if (!found) {
			for (char letter : available[i]) {
				if (should_not.count(letter) == 0) {
					known[i] = letter;
				}
			}
		}
	}

	cout << known << '\n';

	return 0;
}
