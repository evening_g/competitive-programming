#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    string b;
    cin >> b;
    int r = 1;

    int zc = 0;
    for (size_t i = 1; i < b.size(); i++) {
        if (b[i-1] == '1' and b[i] == '0') {
            r ++;
        } else if (b[i-1] == '0' and b[i] == '1') {
            zc ++;
            if (zc > 1) {
                r ++;
            }
        }
    }
    
    cout << r << '\n';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}
