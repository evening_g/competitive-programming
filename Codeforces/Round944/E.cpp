#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    int n, k, q;
    cin >> n >> k >> q;

    vector<double> points(k+1);
    vector<double> times(k+1);

    points[0] = times[0] = 0;

    for (int i = 1; i <= k; i++)
        cin >> points[i];

    for (int i = 1; i <= k; i++)
        cin >> times[i];
    
    while (q--) {
        double d;
        cin >> d;

        int i = upper_bound(all(points), d) - points.begin() - 1;

        if (points[i] == d) {
            cout << times[i] << ' ';
        } else {
            ll t = (ll) times[i] + (int) ((times[i+1] - times[i]) / (points[i+1] - points[i])*(d-points[i]));
            cout << t << ' ';
        }
    }
    cout << '\n';
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}
