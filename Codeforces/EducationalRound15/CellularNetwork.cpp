#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vull = vector<ull>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

bool covers_all_towers(const vll &cities, const vll towers, ll range)
{
    int city = 0;
    int tower = 0;

    while (city < cities.size() and tower < towers.size()) {
        if (cities[city] < towers[tower] - range) {
            return false;
        }

        if (cities[city] > towers[tower] + range) {
            tower ++;
            continue;
        }

        city ++;
    }

    return city == cities.size();
}

ll lower_bound(const vll &cities, const vll &towers)
{
    ll lower = 0, upper = (ll) 10e10;
    ll mid;

    while (lower < upper) {
        mid = (lower+upper) / 2;

        if (covers_all_towers(cities, towers, mid)) {
            // if true, we can still go left
            upper = mid - 1;
        } else {
            lower = mid + 1;
        }
    }

    if (!covers_all_towers(cities, towers, lower)) {
        lower ++;
    }

    return lower;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n, m;
    cin >> n >> m;

    vll cities(n);
    vll towers(m);

    readv(cities);
    readv(towers);

    cout << lower_bound(cities, towers) << '\n';

    return 0;
}
