#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    int n, m;
    cin >> n >> m;

    vi a(n+1), b(m);
    a[0] = -INF;
    for (int i = 1; i <= n; i++)
        cin >> a[i];
    
    readv(b);

    for (int i = 1; i <= n; i++) {
        if (    (a[i] > b[0] - a[i] && b[0] - a[i] >= a[i-1])
            ||  (a[i-1] > a[i] && b[0] - a[i] >= a[i-1]))
        {
            a[i] = b[0] - a[i];
        } else if(a[i] < a[i-1]) {
            cout << "nO\n";
            return;
        }
    }
    
    cout << "yEs\n";
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}

