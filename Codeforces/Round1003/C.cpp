/**
 * Binary search
 * Greedy
 * */

#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int find(const vi &b, const int a, const int x) {
    int l = 0, r = b.size();

    int m = (l + r) / 2;

    while (l < r) {
        if (m == -1 or m == b.size())
            return m;
        if (x > b[m] - a)
            l = m + 1;
        else
            r = m;
        m = (l + r) / 2;
    }
    return m;
}

void solve() {
    int n, m;
    cin >> n >> m;
    vi a(n+1), b(m), c(n+1);
    a[0] = c[0] = -INF;
    for (int i = 1; i <= n; i++) {
        int x;
        cin >> x;
        a[i] = c[i] = x;
    }
    readv(b);

    sort(all(b));

    for (int i = 1; i <= n; i++) {
        int j = find(b, a[i], c[i-1]);

        if (j >= 0 && j < m && (b[j] - a[i] < c[i] || c[i-1] > c[i]))
            c[i] = b[j] - a[i];

        if (c[i-1] > c[i]) {
            cout << "NO\n";
            return;
        }
    }
    
    cout << "YES\n";
}

int main() {
#ifdef DEBUG
    cout << "DEBUG\n";
#else
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);
#endif

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}

// vim: set sw=4: set ts=4
