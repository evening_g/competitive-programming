from math import sqrt

def solve(n: int) -> bool:
    powers = list()

    p = 2
    while p <= int(sqrt(n)):
        count = 0

        while n % p == 0:
            n //= p
            count += 1

        if count > 0:
            powers.append(count)

        p += 1
    
    if n > 1:
        powers.append(1)

    D = 1
    for p in powers:
        D *= (p+1)

    return (len(powers) == 1 and D % 2 == 0) or (D == 2 or D == 4)
        

def main():
    N = int(input())

    if solve(N):
        print('Y')
    else:
        print('N')


if __name__ == "__main__":
    main()
