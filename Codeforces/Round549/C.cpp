/**
 * source: https://codeforces.com/contest/1143/problem/C
 * status: unfinished
 * keywords: tree
 * */

#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n;
    cin >> n;

    vector<char> respect(n, 0);
    mi adj = vector(n+1);

    for (int i = 1; i <= n; i++) {
        int p, r;
        cin >> p >> r;

        if (p != -1)
            adj[p].pushback(i);

        respect[i] = (char) r;
    }

    // TODO

    return 0;
}
