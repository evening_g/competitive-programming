#include<bits/stdc++.h>

using namespace std;

void solve() {
    int n;
    cin >> n;
    
    vector<pair<int,int>> a(n);
    vector<int> result(n);

    for (int i = 0; i < n; i++)
    {
        cin >> a[i].first;
        a[i].second = i;
    }
    
    sort(a.begin(), a.end());
    
    for (int i = 0; i < n; i++)
    {
        result[a[i].second] = n-i; 
    }
    
    
    for (int i : result)
        cout << i << ' ';
    
    cout << '\n';
}

int main() {
    int t;

    cin >> t;

    while (t--)
        solve();

    return 0;
}