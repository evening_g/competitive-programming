#include<bits/stdc++.h>

using namespace std;

void solve() {
    int n;
    cin >> n;

    string s;
    cin >> s;

    int changes = 0;
    int equals = 0;
    int dummy = n%2;

    for (int i = 0; i < n/2; i++)
    {
        if (s[i] == s[n-i-1])
        {
            equals ++;
        }
        else
        {
            changes ++;
        }
    }
    
    for (int i = 0; i <= n; i++)
    {
        if (i < changes)
        {
            cout << '0';
            continue;
        }

        int res = i - changes;

        if (res/2 > equals)
        {
            cout << '0';
            continue;
        }

        if (res%2 == 1 && dummy == 0)
        {
            cout << '0';
            continue;
        }

        cout << '1';
    }
    
    cout << '\n';
}

int main() {
    int t;

    cin >> t;

    while (t--)
        solve();

    return 0;
}