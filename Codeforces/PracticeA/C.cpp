/**
 * @author me
 * Status: WA on test 15
*/

#include <bits/stdc++.h>

using namespace std;

void solve()
{
    int n;

    cin >> n;

    set<int> unused;

    for (int i = 1; i <= n; i++)
    {
        unused.insert(i);
    }
    

    for (int i = n; i >= 1; i--)
    {
        if (unused.count(i)) {
            int c = i;
            while(c > 0) {
                if(unused.count(c)){
                    cout << c << ' ';
                    unused.erase(c);
                }
                if(c%2){
                    break;
                }
                c /= 2;
            }
        }
    }

    for (int i : unused) {
        cout << i << ' ';
    }

    cout << '\n';
    
}

int main()
{
    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}
