/**
 * @author camel_case
 * status: WA
*/

#include <bits/stdc++.h>

using namespace std;

// Cookies eaten from a to b
int cookies_eaten(int a, int b, int d, const vector<int> &v)
{
    return (int)(ceil(float(v[b]-v[a])/float(d)));
}

void solve()
{
    int n, m, d;
    cin >> n >> m >> d;

    vector<int> v;

    v.push_back(1);
    int x;
    for (int i = 0; i < m; i++)
    {
        cin >> x;
        v.push_back(x);
    }
    m += 1;
    
    // Find the amount of cookies without eating anything
    int res = 1;
    for (int i = 1; i < m; i++)
    {
        res += cookies_eaten(i-1, i, d, v);
    }
    res += (n-v[m-1])/d;

    // Remove each vendor and find the amount of cookies
    // eaten without it
    int removed = -1;
    int aux, minr = (int) 10e9;
    for (int i = 1; i < m; i++)
    {
        // Remove the cookies in (i-1, i]
        aux = res - cookies_eaten(i-1, i, d, v);

        // Remove the cookies after i
        if (i == m-1) {
            // If i is last cookie
            // remove the cookies in (i, n]
            aux -= (n-v[m-1])/d;
        } else {
            // If there are cookies after i
            // remove the cookies in (i, i+1)
            aux -= cookies_eaten(i, i+1, d, v);
        }

        // Add the cookies in (i-1, i+1]
        if (i+1 >= m) {
            // If i+1 doesn't exist
            // as we are removing i
            // add the cookies in (i-1, n]
            aux += (n-v[i-1])/d;
        } else {
            // If i+1 exists,
            // add the cookies in [i-1, i+1)
            aux += cookies_eaten(i-1, i+1, d, v);
        }

        if (aux < minr) {
            minr = aux;
            removed = i;
        }
    }

    cout << minr << ' ' << removed << '\n';
}

int main()
{
    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}
