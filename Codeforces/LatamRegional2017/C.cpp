/**
 * @author me
 * Status: WA on test 15
*/

#include <bits/stdc++.h>

using namespace std;

int main()
{
    int k, n;
    cin >> k >> n;

    vector<pair<int,int>> v(k+1);

    for (int i = 0; i < k+1; i++)
    {
        v[i].first = 0;
        v[i].second = i;
    }
    
    for (int i = 0; i < n; i++)
    {
        int x;
        cin >> x;
        v[x].first ++;
    }
    
    sort(v.begin(), v.end());

    int diff = v[k].first - v[1].first;

    if (diff > 2) {
        cout << "*\n";
        return 0;
    }

    auto aux = v;
    aux[k].first --;

    sort(aux.begin(), aux.end());

    diff = aux[k].first - aux[1].first;

    if (diff == 0) {
        cout << "-" + to_string(v[k].second) + '\n';
        return 0;
    }

    aux = v;
    aux[1].first ++;

    sort(aux.begin(), aux.end());

    diff = aux[k].first - aux[1].first;

    if (diff == 0) {
        cout << "+" + to_string(v[1].second) + '\n';
        return 0;
    }

    aux = v;
    aux[1].first ++;
    aux[k].first --;

    sort(aux.begin(), aux.end());

    diff = aux[k].first - aux[1].first;

    if (diff == 0) {
        cout << "-" + to_string(v[k].second) +  " +" + to_string(v[1].second) + '\n';
        return 0;
    }

    cout << "*\n";

    return 0;
}
