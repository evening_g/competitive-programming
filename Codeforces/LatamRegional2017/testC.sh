#!/bin/bash

for i in {0..9}
do
    ./C.out < tests/in$i.txt > out.txt
    diff -Z -B out.txt tests/out$i.txt > diff.txt

    if [ -s diff.txt ]
    then
        echo -e "Test $i failed"
    fi
done

rm out.txt diff.txt
