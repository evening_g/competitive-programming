/**
 * @author me
 * status: AC
*/

#include <bits/stdc++.h>

using namespace std;

int main()
{
    int c1, p1, b1, c2, p2, b2;
    cin >> c1 >> p1 >> b1 >> c2 >> p2 >> b2;

    int cr = c2 - c1;
    if (cr < 0) cr = 0;
    int pr = p2 - p1;
    if (pr < 0) pr = 0;
    int br = b2 - b1;
    if (br < 0) br = 0;

    int r = cr+ pr + br;

    cout << r << '\n';

    return 0;
}
