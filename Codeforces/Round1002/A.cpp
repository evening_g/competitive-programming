#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
        int n;
        cin >> n;
        vi a(n), b(n);
        readv(a);
        readv(b);
        sort(all(a));
        sort(all(b));
        int diffa = 1, diffb = 1;
        for (int i = 1; i < n; i++) {
                if (a[i] != a[i-1])
                        diffa ++;
                if (b[i] != b[i-1])
                        diffb ++;
        }

        if (diffa + diffb >= 4)
                cout << "YeS\n";
        else
                cout << "nO\n";
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}
