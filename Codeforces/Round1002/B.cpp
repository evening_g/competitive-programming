#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
        int n, k, o = 1;

        cin >> n >> k;

        int res;
        vi a(n);
        readv(a);

        if (n == k) {
                a.push_back(0);
                a.push_back(0);

                for (int i = 1; i < a.size(); i+=2) {
                        if (a[i] != (i+1)/2) {
                                res = (i+1)/2;
                                goto exit;
                        }
                }
        }

        for (int i = 1; i < n; i++, o++)
                if (a[i] != 1)
                        break;

        if (n - k + 1 >= o)
                res = 1;
        else
                res = 2;

exit:
        cout << res << '\n';
}

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int t;
        cin >> t;
        while (t--)
                solve();

        return 0;
}
