t = int(input())

for _ in range(t):
    _ = int(input())
    a = set(map(int, input().split()))
    b = set(map(int, input().split()))
    unord_cartesian_prod_len = len(a) * len(b) - len(a.intersection(b)) // 2
    print("YES" if unord_cartesian_prod_len >= 3 else "NO")

