#include<iostream>
#include<vector>
#include<string>

using namespace std;

int calculate(vector<string> grid, int right, int down) {
    int total = 0, col = 0;
    
    int width = grid[0].length();

    for (int i = 0; i < grid.size(); i += down)
    {
        if (grid[i][col] == '#') {
            total ++;
        }

        col = (col + right) % width;
    }
    
     return total;
}

int main() {
    vector<string> grid;
    string row;

    freopen("data.in", "r", stdin);

    while (cin >> row) {
        grid.push_back(row);
    }

    long long total = calculate(grid,1,1);  
    total *= calculate(grid, 3, 1);  
    total *= calculate(grid, 5, 1);  
    total *= calculate(grid, 7, 1);  
    total *= calculate(grid, 1, 2); 

    cout<<total<<endl;

    return 0;
}