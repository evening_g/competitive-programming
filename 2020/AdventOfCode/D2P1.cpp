#include<iostream>
#include<string>
using namespace std;

int main() {
    char hyphen, colon;
    int lowest, highest;
    char letter;
    string password;

    int counter, total = 0;

    freopen("data.in", "r", stdin);

    while (cin>>lowest>>hyphen>>highest>>letter>>colon>>password) {
        //cout<<lowest<<hyphen<<highest<<letter<<colon<<password;
        counter = 0;

        for (char c : password) {
            if (c == letter) {
                counter ++;
            }
        }

        if (lowest <= counter && highest >= counter) {
            total ++;
        }
    }
    
    cout<<total<<'\n';

    return 0;
}