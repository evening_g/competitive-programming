#include <bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    float n,a,b, r = 0;

    cin>>n>>a>>b;

    for (float i = a; i <= b; i += 0.00001)
    {
        r += (n/i);
    }
    
    r /= (b-a) * 100000;

    cout<<r<<'\n';

    return 0;
}