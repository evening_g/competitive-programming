#include <bits/stdc++.h>

using namespace std;

int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
    
    float n,a,b;

    cin>>n>>a>>b;

    cout.precision(7);
    cout<<(n/((a+b)/2.0))<<'\n';

    return 0;
}