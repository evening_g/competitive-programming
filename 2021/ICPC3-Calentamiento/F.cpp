#include <bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int credits = 100, max = 100, c, v;
    cin>>c;

    for (int i = 0; i < c; i++)
    {
        cin>>v;
        credits += v;
        if (credits > max) {
            max = credits;
        }
    }
    cout << max << '\n';

    return 0;
}