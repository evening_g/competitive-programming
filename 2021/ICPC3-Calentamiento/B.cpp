#include <bits/stdc++.h>

using namespace std;

/*
SOLUTION
Calculate the biggest factorial number that fits in n
*/

int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int n, fac_n = 1, r = 0;

    cin>>n;
    
    // This calculates the first factorial number that does not fit in n
    int i = 0;
    while (fac_n < n) {
        i++;
        fac_n *= i;
    }
    // So we return 1
    fac_n /= i;
    i --;

    int aux;
    while (n > 0) {
        aux = trunc(n / fac_n);
        n = n % fac_n;
        r += aux;

        fac_n /= i;
        i --;
    }

    cout<<r<<'\n';
}