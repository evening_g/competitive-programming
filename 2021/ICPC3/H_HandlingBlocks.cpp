/*
https://codeforces.com/gym/103388/problem/H

Solution
1. Read the data
2. Save the data in a pair (number, color)
3. Save the colors order in an array
4. Order the numbers in the pair
5. Check if the order of the colors is the same than the original
*/

#include<bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int n,k;
    bool has_solution = true;

    cin>>n>>k;

    pair<int, int> blocks_colors[n];
    int ordered_colors[n];

    for (int i = 0; i < n; i++) {
        cin>>blocks_colors[i].first;
        cin>>blocks_colors[i].second;
        ordered_colors[i] = blocks_colors[i].second;
    }

    // Process

    sort(blocks_colors, blocks_colors + n);

    for (int i = 0; i < n; i++) {
        if (ordered_colors[i] != blocks_colors[i].second) {
            has_solution = false;
            break;
        }        
    }

    // Print result

    cout<<(has_solution ? "Y" : "N")<<'\n';

    return EXIT_SUCCESS;
}