/*
K. Kathmandu

time limit per test: 0.25 seconds
memory limit per test: 1024 megabytes
input: standard input
output: standard output

The pandemic is getting better and you can finally do the thing you've been dreaming of for the past few years: eat at your favorite restaurant! The restaurant happens to be in Kathmandu, but that's fine, you can always take a plane.

The problem is that planes almost always leave you restless. You consider yourself properly rested if you can sleep for T uninterrupted minutes, which means you are never awake from a certain moment t to t+T. Also, you're a very easy sleeper: you can fall asleep at the start of any minute and wake up at the end of any minute.

Of course, if you sleep too much you will miss all the airplane meals! That is unacceptable, as no opportunity for free food should go to waste.

Luckily, the airplane company sent you the whole flight schedule: the duration of the flight, D minutes, the number of meals that are going to be served, M, and the exact time they will serve the meals, yi. You need to be awake at the time the meal is being served to be able to eat it, otherwise, the steward will not serve you. Since you're always hungry, you will devour the meal instantly.

Now you are wondering, for the optimal plane traveling experience, can you get properly rested and still eat all meals during the flight?

Input
The first line of input contains three integers, T, D, M (1≤T,D≤105, 0≤M≤1000), representing, respectively, the number of minutes you need to sleep without interruption to be properly rested, the duration of the flight and the number of meals that are going to be served during the flight.

Each of the next M lines contains an integer yi (0≤yi≤D). These integers represent the times at which each meal is going to be served, and are given in chronological order.

Output
Output a line containing one character. If you can get properly rested and still eat all meals during the flight, write the upper case letter 'Y'; otherwise write the uppercase letter 'N'.
*/

#include<bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Definir variables
    int t, d, m, yi, init = 0;
    bool result = false;

    // Leer datos
    cin>>t>>d>>m;

    // Se repite esto para cada comida
    for (int i = 0; i < m; i++)
    {
        // Se lee a qué hora es la comida
        cin>>yi;

        /*init se inicializó en 0
        lo que se hace es calcular el tiempo entre init y la comida
        por eso init inicia en 0
        */
        if (yi - init >= t) {
            /*
            si el tiempo entre init y la comida es mayor o igual al tiempo que quiere dormir,
                entonces el resultado es verdadero*/
            result = true;
            break;  // y se detiene el ciclo porque ya no tiene sentido comprobar para las siguientes comidas
        } else {
            /*
            si no, entonces init va ser igual a la hora de esta comida
            para que en la siguiente comida, se calcule el tiempo entre la comida anterior y la actual*/
            init = yi;
        }
    }

    /*
    Cuando terminemos de revisar todas las comidas, entonces sólo queda revisar el tiempo entre la hora de la última comida (guardada en init) y el tiempo para que el avión aterrize (d)*/
    if (d - init >= t) {
        result = true;
    }

    // Imprimir resultado
    cout<<(result ? "Y" : "N")<<'\n';
    
    return 0;
}