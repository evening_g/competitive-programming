/*
https://codeforces.com/gym/103388/problem/N
date: 2021-11-06

INPUT
The first line contains two integers Y and N (1≤Y,N≤3×105), representing the number of years of competition and the number of former contestants that Vini had talked to, respectively. (Yes, painting cars is a millenary tradition, also a very popular one!).

The next line contains Y integers x1, x2, ..., xY (0≤xi≤105), representing
the how many slots to the ICPC for their region there were for each year.

Each of the next N lines contains three integers ai, pi and fi (1≤ai≤Y, 1≤pi≤105, 0≤fi≤Y−ai), representing
the year that the i-th former competitor had their last participation,
the position the i-th former competitor's team ranked
for how many years after competing the i-th former competitor followed the results after retiring, respectively.

OUTPUT
Output N lines, the i-th line should contain how many years the i-th former competitor felt unlucky.

EXAMPLE
input
5 3
1 2 3 4 5
1 3 4
2 6 3
3 4 1

output
3
0
1

SOLUTION
For each participant
    find how many numbers >= pi there are in [ai + 1, ai+fi]
*/

#include<bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int y, n, ai, pi, fi, count;

    cin>>y>>n;

    int slots [y];

    for (int &i : slots)
        cin>>i;

    // Process

    for (int i = 0; i < n; i++)
    {
        // Reads the data for this contestant
        cin>>ai>>pi>>fi;

        count = 0;
        if (pi > slots[0]) { // if the contestant won then they wouldn't feel unlucky

            for (int j = ai; j <= ai + fi - 1; j++)
                if (slots[j] >= pi) count++;
        }   

        cout<<count<<'\n';     
    }

    return EXIT_SUCCESS;
}