/*
Problem description/source
date: 2021-12-18
*/

#include<bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int r, c, e, s, l;
    bool ok;
    // position
    pair<int, int> p;
    pair<int, int> loop_start;

    while(cin>>r>>c>>e) {
        char grid[r][c];
        bool mark[r][c];
        p.first = 0;
        p.second = e-1;
        ok = true;
        s = 0;
        l = 0;

        // Reading the grid
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                cin>>grid[i][j];
                mark[i][j] = false;
            }            
        }
        
        // solve
        while (ok) {
            // marks the current position
            mark[p.first][p.second] = true;

            // moves the ball
            switch (grid[p.first][p.second])
            {
            case 'N':
                p.first --;
                break;
            
            case 'S':
                p.first ++;
                break;
            
            case 'E':
                p.second ++;
                break;
            
            case 'W':
                p.second --;
                break;
            
            default:
                break;
            }
            s++;

            // checks if the ball is inside grid
            ok = p.first >= 0 & p.first < r & p.second >= 0 & p.second < c;

            // if the ball keeps inside the grid then checks if it
            // has fallen into a loop

            if (ok) {
                if (mark[p.first][p.second]) {
                    // if enters here, so the ball is in a loop
                    ok = false;

                    // saves the start point
                    loop_start = p;                    

                    // to find out how many steps the loop is, we will travel it otherwise
                    do
                    {
                        switch (grid[p.first][p.second])
                        {
                        case 'N':
                            p.first ++;
                            break;
                        
                        case 'S':
                            p.first --;
                            break;
                        
                        case 'E':
                            p.second --;
                            break;
                        
                        case 'W':
                            p.second ++;
                            break;
                        
                        default:
                            break;
                        }
                        l++;
                    } while (p != loop_start);
                }
            }
        }

        if (l == 0) {
            // if there is no loop
            cout<<s<<" step(s) to exit"<<'\n';
        } else {
            cout<<s<<" step(s) before a loop of "<<l<<" step(s)"<<'\n';
        }
    }
    
    return EXIT_SUCCESS;
}