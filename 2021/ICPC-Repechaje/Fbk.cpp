/*
Problem description/source
date: 2021-12-18

12:58

Only checks if the ball has gone out of the grid
but not if it has entered into a loop
*/

#include<bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int r, c, e, s;
    bool ok;
    // position
    pair<int, int> p;

    while(cin>>r>>c>>e) {
        char grid[r][c];
        bool mark[r][c];
        p.first = 0;
        p.second = e-1;
        ok = true;
        s = 0;

        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                cin>>grid[i][j];
                mark[i][j] = false;
            }            
        }
        
        while (ok) {
            // marks the current position
            mark[p.first][p.second] = true;

            // moves the ball
            switch (grid[p.first][p.second])
            {
            case 'N':
                p.first --;
                break;
            
            case 'S':
                p.first ++;
                break;
            
            case 'E':
                p.second ++;
                break;
            
            case 'W':
                p.second --;
                break;
            
            default:
                break;
            }
            s++;

            // checks if the ball has gone out of the grid

            if (p.first < 0 | p.first >= r | p.second < 0 | p.second >= c) {
                // The ball has come out of the grid
                ok = false;
            }
        }

        cout<<s<<" step(s) to exit"<<'\n';
    }
    
    return EXIT_SUCCESS;
}