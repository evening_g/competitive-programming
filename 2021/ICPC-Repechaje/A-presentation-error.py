def main():
    # Define variables and read data
    
    t = int(input())

    dm = ""

    for i in range(t):
        em = input()

        for c in em:
            if c == '0':
                dm += 'O'

            elif c == '1':
                dm += 'I'
            
            elif c == '2':
                dm += 'Z'
            
            elif c == '3':
                dm += 'E'

            elif c == '4':
                dm += 'A'

            elif c == '5':
                dm += 'S'

            elif c == '6':
                dm += 'G'

            elif c == '7':
                dm += 'T'

            elif c == '8':
                dm += 'B'

            elif c == '9':
                dm += 'P'

            else:
                dm += c

        dm += '\n'

    print(dm)
    
if __name__ == "__main__":
    main()