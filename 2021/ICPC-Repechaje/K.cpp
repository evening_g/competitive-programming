/*
Problem description/source
date: yyyy-mm-dd
*/

#include<bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int t, pm, m;
    
    bool asc, ordered;
    
    cin>>t;

    for (int i = 0; i < t; i++) {
        ordered = true;

        cin>>pm>>m;
        
        asc = pm - m < 0;
        pm = m;

        for (int j = 0; j < 8; j++) {
            cin>>m;

            if (pm - m < 0 != asc) {
                ordered = false;
            }
            pm = m;
        }
        
        if (ordered) {
            cout<<"Ordered"<<'\n';
        } else {
            cout<<"Unordered"<<'\n';
        }
    }
    
    return EXIT_SUCCESS;
}