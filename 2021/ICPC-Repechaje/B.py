from math import pow

def to_dec(data):
    dec = 0;

    for i in range(len(data)):
        if data[i] == '*':
            dec += pow(2, len(data) - i - 1)

    return str(int(dec))


def main():
    t = int(input())

    for i in range(t):
        bc = input()
        
        data = bc[1:len(bc)-1].split(')(')
        
        h1 = data[0:2]
        h2 = data[2:6]
        m1 = data[6:9]
        m2 = data[9:13]
        s1 = data[13:16]
        s2 = data[16:20]

        print(f"Case {i+1}: {to_dec(h1)}{to_dec(h2)}:{to_dec(m1)}{to_dec(m2)}:{to_dec(s1)}{to_dec(s2)}")

    
if __name__ == "__main__":
    main()