/*
Problem description/source
date: yyyy-mm-dd
*/

#include<bits/stdc++.h>

using namespace std;

int main() {
    // cin.tie(nullptr);
    // ios_base::sync_with_stdio(false);

    // Define variables and read data
    long long n, n2, m, i, c = 1;
    char order;
    string o = "";

    vector<long long> b;

    while (cin>>n>>m>>order) {
        o = "";
        n2 = n;

        b.clear();

        // Parse n to binary
        i = 0;
        while (pow(2, i) < n)
            i ++;
            
        while (n > 0) {
            if (pow(2, i) <= n) {
                n -= pow(2, i);
                b.push_back(pow(2, i)*m);
            }

            i --;
        }

        //cout<<"Case "<<c<<": "<<n<<" x "<<m<<" = ";
        o = "Case " + to_string(c) + ": " + to_string(n2) + " x " + to_string(m) + " = ";
        if (n2 == 0 | m == 0) {
            o += "0";
        } else {
            if (order == 'b') {
                o += to_string(b[0]);
                for (long long j = 1; j < b.size(); j++)
                    o += " + " + to_string(b[j]);
            } else {
                o += to_string(b[b.size()-1]);
                for (long long j = b.size() - 2; j >= 0; j--)
                    o += " + " + to_string(b[j]);
            }
        }
        cout<<o<<'\n';

        c++;
    }
    
    // Process
    
    // Print result
    
    return EXIT_SUCCESS;
}