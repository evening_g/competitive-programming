/*
DESCRIPTION

The Commission for the Regional deveLopment of Fastminton (CRLF) organizes annual tournaments
of the novel and unusual game of Fastminton, a Badminton derivative. CRLF is proud to be organizing
the third big tournament, that will be held this year.
The commission’s former programmer has developed a computerized system to capture and store
the results from the matches, point by point, to individual files. He left before being able to complete
a parser to the files, so the CRLF requires your help to guarantee that all the records can be read
from the written files, to avoid loosing years of thrilling game results.
A summary of the Fastminton rules was given to you to help in your tasks. It is, in essence, a
shorter (or faster) version of Badminton:

• Fastminton matches are always played with two opposing players in a court that is divided by a
net;
• Players are identified by their relative positions on the score (left and right player);
• A match is composed of three games. The player that reaches 2 games is the winner;
• A game is won when a player reaches at least 5 points with a at least a 2 point difference from
his opponent, or by reaching 10 point (whichever comes first);
• The left player begins serving on the first game. For all other games, the serving player is the
one that won the last game;
• Every round results in a point for the server or the receiver player. The player who scores serves
the next round.

INPUTS

The input is comprised of a single line containing a sequence of characters. This line represents a
complete match event sequence, and may contain the characters S (server point), R (receiver point) or
Q (score announcement). The input does not contains consecutive score announcements.

OUTPUT

The program must print a line containing the current score for each score announcement (Q) found
on the input.
If the game is underway, the announcement will be in the format “GL (PL) - GR (PR)”, where
GL and GR are the number of games won by the left and right players, and PL and PR are the current
points of the left and right players. An asterisk (*) must be appended to the point marked of the
player that will serve in the next round.
If the game has finished, the announcement will follow “GL - GR” with the string “(winner)”
added at the end of the game marked of the winner player.

DESCRIPTION
At the start of the game, left is server
The output is comprised by 3 letters
S = Server got a point
R = Receiver got a point
Q = Announce score
At each S add a point to the server
At each R add a pont to the receiver and make Receiver the Server

If the highest score is bigger or equal than 5 and the difference with the smaller score is bigger or equal 2 then the game finishes

If the highest score is 10 then the game finishes

When a team wines two out of three games then the match finishes

How to change the games and finish the matches

A team wins if scores 10 or more points:
    Else if the difference of points is 2 or more

A team wins the match if wins 2 games
*/

// Import the reader from console
import java.util.Scanner;

public class F
{
    public static void main(String[] args) {
        // Declare the reader
        Scanner sc = new Scanner(System.in);

        // At the start of the game left is server
        boolean isRightServer = false;
        
        // Here we save the scores, at the start they're both 0
        int points [] = {0,0};
        int gamesWinned [] = {0,0};

        // Here we are going to save the scores to print
        String actualScore = "";

        // Reading the record
        String gameRecord = sc.nextLine();
        sc.close();

        // For each char at game_record
        for (int i = 0; i < gameRecord.length(); i++) {
            // We do different things for each char
            switch (gameRecord.charAt(i)) {
                case 'R':
                    // Adds a point to the receiver team
                    // if not is_right_server then it adds 1 in position 1, else in position 0
                    points[!isRightServer ? 0 : 1] += 1;

                    //!is_right_server ? 0 : 1 is a conditional, if the statement before ? is true
                    // then it returns the value before : else the one after :

                    // If the receiver gets the point then the server changes
                    isRightServer = !isRightServer;
                    break;
            
                case 'S':
                    // Adds a point to the server team
                    // if is_right_server then it adds 1 in position 1, else in position 0
                    points[isRightServer ? 0 : 1] += 1;
                    break;
            
                case 'Q':
                    // First proofs if the match has finished
                    if (gamesWinned[0] >= 2) {
                        // Left wins the match
                        actualScore = gamesWinned[1] + " - " + gamesWinned[0] + " (winner)";
                    } else if (gamesWinned[1] >= 2) {
                        // Right wins the match
                        actualScore = gamesWinned[1] + " (winner) - " + gamesWinned[0];
                    } else {
                        // if the game hasn't finished then prints the actual score
                        actualScore = gamesWinned[1] + " (" + points[1] + (!isRightServer ? "*" : "") + ") - " + gamesWinned[0] + " (" + points[0] + (!isRightServer ? "" : "*") + ")";
                    }
                    System.out.println(actualScore);
                    break;
            
                default:
                    break;
            }

            // At the end of every record, it proofs if the game has finished
            // if 0 has winned the game
            if (points[0] >= 10 || (points[0] >= 5 && points[0] - points[1] >= 2)) {
                // adds 1 to games_winned in 0
                gamesWinned[0] += 1;
                // Resets the points
                points[0] = 0;
                points[1] = 0;
            } else if (points[1] >= 10 || (points[1] >= 5 && points[1] - points[0] >= 2)) {
                // Same with the other player
                gamesWinned [1] += 1;
                points[0] = 0;
                points[1] = 0;
            }
        }
    }
}