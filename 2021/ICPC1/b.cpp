/*
DESCRIPCIÓN

Batalla naval es un juego clásico de estrategia de para dos jugadores.
Cada jugador pone su serie de barcos en una rejilla de 10 × 10 y luego
el juego consiste en adivinar las posiciones de las naves enemigas. Las
reglas del juego no son necesarias para este problema, y existen muchas
variaciones, pero aquí estamos interesados en un problema mucho más
básico. Dada una lista de barcos y sus posiciones, tu tarea es checar
si la posición inicial es válida.

Las filas y columnas de la rejilla están numeradas del 1 al 10, y los
barcos están posicionados vertical u horizontalmente, ocupando una
secuencia de cuadrados continuos en el tablero. Para este el
posicionamiento es válido si:

- No hay posiciones ocupadas por más de un barco.
- Todos los barcos están totalmente dentro del tablero.

ENTRADA

La primera línea de la entrada contiene un entero N, 1 <= N <= 100 que
representa la cantidad de barcos.
Cada una de las siguientes N líneas contiene 4 enteros D, L, R y C
Con D ∈ {0,1}
1 <= L <= 5
1 <= R, C <= 10
Los cuales describen al barco.
Si D = 0 entonces el bote está en posición horizontal y ocupa las
posiciones (R, C)...(R, C+L-1)
De otra manera entonces el bote está en posición vertical y ocupa
las posiciones (R, C)...(R+L-1, C)

SALIDA

Imprimir una sola línea conteniendo un solo carácter. Si las posiciones
iniciales de los barcos es válido entonces escribir una letra mayúscula
"Y"; de otra manera escribir la letra mayúscula "N".
*/

#include<iostream>

bool is_inside_the_grid(int r, int c) {    return r >= 0 & r <= 9 & c >= 0 & c <= 9;}

int main()
{
    bool grid [10] [10];
    bool positions_are_valid = true;
    int n, d, l, r, c;

    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            grid[i][j] = false;
        }
        
    }
    
    std::cin>>n;

    // Repeats this for each boat
    for (int i = 0; i < n; i++)
    {
        // Reading boat's description
        std::cin>>d>>l>>r>>c;

        // Decreases in 1 because here the grid starts in 0 and not in 1
        r --;
        c --;

        if (positions_are_valid) {
            // Executes only if the positions of the previous boats are valid
            
            if (d == 0) {
                // The boat is horizontal

                // Verifies every each position of the boat
                for (int j = 0; j < l; j++)
                {
                    if (is_inside_the_grid(r, c+j)) {
                        // If the position is inside the grid
                        if (grid[r][c+j]) {
                            // If any of the positions of this boat is occupied
                            // Then the position is invalid
                            positions_are_valid = false;
                        } else {
                            // Marks this position as occupied
                            grid[r][c+j] = true;
                        }
                    } else {
                        // Else the position is invalid
                        positions_are_valid = false;
                    }
                    
                }
                
            } else {
                // The boat is vertical

                // Verifies every each position of the boat
                for (int j = 0; j < l; j++)
                {
                    if (is_inside_the_grid(r+j, c)) {
                        // If the position is inside the grid
                        if (grid[r+j][c]) {
                            // If any of the positions of this boat is occupied
                            // Then the position is invalid
                            positions_are_valid = false;
                        } else {
                            // Marks this position as occupied
                            grid[r+j][c] = true;
                        }
                    } else {
                        // Else the position is invalid
                        positions_are_valid = false;
                    }
                }
            }
        }
    }

    if (positions_are_valid) {
        std::cout<<"Y"<<std::endl;
    } else {
        std::cout<<"N"<<std::endl;
    }

    return 0;
}