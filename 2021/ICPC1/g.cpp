/*
You start with 100 coins before entering the
game, you must open all the boxes one by one
each box has an amount of coins, it can be
positive or negative you can choose when to
stop.
This alhorithm must expresswhich is the biggest
sum you can finish with

INPUTS
A whole number n representating the amount
of boxes
n whole numbers x representating the values
of the boxes

OUTPUT
The biggest value when summing all the boxes
one by one
*/

#include<iostream>

using namespace std;

int main()
{
    int n, x, sum = 100, max_value = 100;

    cin>>n;

    for (int i = 0; i < n; i++)
    {
        cin>>x;
        sum += x;
        if (sum > max_value) {
            max_value = sum;
        }
    }
    
    cout<<max_value<<endl;

    return 0;
}