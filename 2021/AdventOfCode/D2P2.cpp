/*
Source:
https://adventofcode.com/2021/day/2#part2

Date: 2021-12-13
*/

#include<iostream>
#include<map>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    freopen("short_data.in","r", stdin);
    freopen("data.out","w", stdout);

    // Define variables and read data
    string direction;
    int amount;

    /*
    "depth": Depth of the submarine
    "horizontal_position": Horizontal position of the submarine
    "aim": Aim of the submarine
    */
    map<string, int> position;

    /*
    Posible direction values
    "down"
    "forward"
    "up"
    */
    while (cin>>direction>>amount) {
        if (direction == "forward") {
            position["horizontal_position"] += amount;
            position["depth"] += position["aim"] * amount;
        } else if (direction == "down") {
            position["aim"] += amount;
        } else if (direction == "up") {
            position["aim"] -= amount;
        }
    }
   
    // Process

    // Print result
    
    cout << (position["depth"] * position["horizontal_position"]) << '\n';
     
    return EXIT_SUCCESS;
}