"""
Source:
https://adventofcode.com/2021/day/3

Date: 2021-12-14
"""

from math import pow

def main():
    data_in_file = open('data.in', 'r')
    data_out_file = open('data.out', 'w')

    data_in_file.seek(0)
    data_in = data_in_file.read()

    report = data_in.split('\n')

    # Sum 1 if the bit is 1, 0 otherwise rest 1
    most_common_bit = [0] * len(report[0])

    # Searchs for the most common bit in each position
    for binary in report:
        for i in range(len(binary)):
            if binary[i] == '1':
                most_common_bit[i] += 1
            else:
                most_common_bit[i] -= 1

    # At the end of this. The data in most common bit contains
    #   a positive number if the most common bit in that position is 1
    #   else the most common bit is 0

    # Now we have to get the binary gamma rate and the epsilon rate

    gamma_rate = 0
    epsilon_rate = 0
    
    #for i in range(len(most_common_bit)-1, -1, -1):
    for i in range(len(most_common_bit)):
        if most_common_bit[i] > 0:
            gamma_rate += pow(2, len(most_common_bit) - i - 1)
        else:
            epsilon_rate += pow(2,  len(most_common_bit) - i - 1)

    data_out_file.write(str(int(gamma_rate * epsilon_rate)))    

    data_in_file.close()
    data_out_file.close()


if __name__ == '__main__':
    main()
