/*
Source:
https://adventofcode.com/2021/day/2#part2

Date: 2021-12-13
*/

import java.util.*;
import java.io.*;

public class D2P2 {
    public static void main(String[] args) throws IOException {
        // This is supposed to save the file short_data.in in dataInput
        File dataInput = new File("short_data.in");
        // This is supposed to make bf to read the data from the file
        BufferedReader br = new BufferedReader(new FileReader(dataInput));

        String direction;
        int amount;

        Map<String, Integer> position = new HashMap<>();
        position.put("depth", 0);
        position.put("horizontal_position", 0);
        position.put("aim", 0);

        // Reading the data

        /*
        I've done this on two ways
         1. I save all the content of the file in a String variable
            then split the String in lines
            and analyze each line
         2. Make the reader to read from the file instead that
            from console
        */

        for (String instruction = br.readLine() ; instruction != null ; instruction = br.readLine()) {
            direction = instruction.substring(0, instruction.indexOf(' '));
            amount = Integer.parseInt(instruction.substring(instruction.indexOf(' ') + 1, instruction.length()));

            switch (direction) {
                case "forward":
                    position.put("horizontal_position", position.get("horizontal_position") + amount);
                    position.put("depth", position.get("depth") + position.get("aim") * amount);
                    break;
            
                case "down":
                    position.put("aim", position.get("aim") + amount);
                    break;
            
                case "up":
                    position.put("aim", position.get("aim") - amount);
                    break;
            
                default:
                    break;
            }
        }

        System.out.println(position.get("depth") * position.get("horizontal_position"));

        br.close();
    }
}