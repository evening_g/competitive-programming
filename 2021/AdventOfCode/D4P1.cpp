/*
Source:
https://adventofcode.com/2021/day/4

Date: 2021-12-16

This doesn't work, reading the data is hard, maybe it'd
be better to do it in Python
*/

#include<bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    freopen("short_data.in","r", stdin);
    freopen("data.out","w", stdout);

    // Define variables

    string instructions;

    int number;
    char comma;
    vector<int> numbers;

    array<array<int, 5>, 5> board;
    vector<array<array<int, 5>, 5>> boards;

    // Read data

    // Reads the numbers

    cin>>instructions;

    /*while (cin>>number>>comma) {
        numbers.push_back(number);
        if (comma != ',') {
            break;
        }
    }*/

    int i = 0, j = 0;
    while (cin>>number) {
        board[i][j] = number;
        
        j++;

        if (i == 5 & j == 5) {
            boards.push_back(board);

            i = 0;
            j = 0;
        } else if (j == 5) {
            j = 0;
            i ++;
        }
    }
        
    
    // Process
    
    // Print result

    /*cout<<"NUMBERS:\n";
    for (int &i : numbers)
        cout<<i<<'\n';*/

    cout<<instructions<<'\n';

    cout<<"BOARDS:\n";
    for (auto &i : boards) {
        for (int j = 0; j < 5; j++)
        {
            for (int k = 0; k < 5; k++)
            {
                cout<<i[j][k]<<'\t';
            }
            cout<<'\n';
        }
        cout<<'\n';
    }
        
    
    return EXIT_SUCCESS;
}