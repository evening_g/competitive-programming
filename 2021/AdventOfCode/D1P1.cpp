/* Source https://adventofcode.com/2021/day/1
 * 2021-12-12
 * */

#include <iostream>

using namespace std;

int main() {
    int previous_measurement, actual_measurement;
    int measurements_larger_than_previous = 0;

    freopen("data.in","r", stdin);
    freopen("output.txt","w", stdout);

    cin>>previous_measurement;

    while (cin>>actual_measurement) {
        if (actual_measurement > previous_measurement)
            measurements_larger_than_previous++;

        previous_measurement = actual_measurement;
    }

    cout<<measurements_larger_than_previous;

    return 0;
}
