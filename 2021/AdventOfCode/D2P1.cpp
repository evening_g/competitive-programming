/*
Source:
https://adventofcode.com/2021/day/2

Date: 2021-12-13
*/

#include<bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    freopen("data.in","r", stdin);
    freopen("output.txt","w", stdout);

    // Define variables and read data
    string direction;
    int amount;
    map<string, int> positions;

    while (cin>>direction>>amount)
        positions[direction] += amount;
    
    // Process

    // Print result
    
    cout<<(positions["forward"] * (positions["down"] - positions["up"]))<<'\n';
    
    return EXIT_SUCCESS;
}