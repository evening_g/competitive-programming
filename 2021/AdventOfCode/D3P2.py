"""
Source:
https://adventofcode.com/2021/day/3#part2

Date: 2021-12-14
"""

from math import pow

# Most common bit
# Returns a positive integer if the most common bit is 1
# Returns a negative integer if the most common bit is 0
# Returns 0 if there is no most common bit
def most_common_bit(report, position):
    result = 0;

    for binary in report:
        if binary[position] == '1':
            result += 1
        else:
            result -= 1

    return result;

# Returns True if the char of string at position equals character
# otherwise returns False
def char_at_equals(string, position, character):
    return string[position] == character;


# Parses a binary number into a decimal number
def binary_to_decimal(binary):
    decimal = 0

    for i in range(len(binary)):
        if binary[i] == '1' or binary[i] == 1:
            decimal += pow(2,len(binary) -i -1 )

    return decimal


def main():
    data_in_file = open('data.in', 'r')
    data_out_file = open('data.out', 'w')

    # READING DATA

    data_in_file.seek(0)
    data_in = data_in_file.read()

    report = data_in.split('\n')    

    # PROCESSING DATA

    """
    At the end of this. The data in most common bit contains
      a positive number if the most common bit in that position is 1
      else the most common bit is 0

    Now we have to get the binary gamma rate and the epsilon rate
    
    -----------------------------------------------------------------------------

    The bit criteria depends on which type of rating value you want to find:

    - To find oxygen generator rating, determine the most common value (0 or 1)
        in the current bit position, and keep only numbers with that bit in that
        position. If 0 and 1 are equally common, keep values with a 1 in the
        position being considered.

    - To find CO2 scrubber rating, determine the least common value (0 or 1) in
        the current bit position, and keep only numbers with that bit in that
        position. If 0 and 1 are equally common, keep values with a 0 in the
        position being considered.

    -----------------------------------------------------------------------------

    Finding the oxigen generator rating

    First approximation

    For each i position
        Determine the most common bit in i -> most common bit

        If most common bit is smaller than 0
            most common bit is 0
        else if most common bit is equal or greater than 0
            most common bit is 1

        For each record
            Delete records with the bit at i not equal to the most common bit

    Finding the CO2 scrubber rating

    for each i position
        determine the most common bit in i -> most common bit

        most common bit returns             | less common bit
        < 0 if 0 is the less common bit     | 0
        > 0 if 1 is the less common bit     | 1
        = 0 if there is no less common bit  | 0

        if most common bit is smaller than 0
            less common bit is 1
        else if less common bit is equal or greater than 0
            less common bit is 0

        For each record
            Delete records with the bit at i not equal to the less common bit
    """
    
    oxigen_generator_rating = []
    co2_scrubber_rating = []

    oxigen_generator_rating[:] = report
    co2_scrubber_rating[:] = report

    del report

    # Finding the oxigen generator rating

    for i in range(len(oxigen_generator_rating[0])):
        bit = most_common_bit(oxigen_generator_rating, i)

        if bit < 0:
            bit = 0
        else:
            bit = 1

        j = 0
        while j < len(oxigen_generator_rating) and len(oxigen_generator_rating) > 1:
            if not char_at_equals(oxigen_generator_rating[j], i, str(bit)):
                del oxigen_generator_rating[j]
            else:
                j += 1
                
    # Finding the CO2 scrubber rating

    for i in range(len(co2_scrubber_rating[0])):
        bit = most_common_bit(co2_scrubber_rating, i)

        if bit < 0:
            bit = 1
        else:
            bit = 0

        j = 0
        while j < len(co2_scrubber_rating) and len(co2_scrubber_rating) > 1:
            if not char_at_equals(co2_scrubber_rating[j], i, str(bit)):
                del co2_scrubber_rating[j]
            else:
                j += 1
        

    """
    # For testing purposes

    print(most_common_bit)
    print(oxigen_generator_rating)
    print(co2_scrubber_rating)    
    print(gamma_rate)
    print(epsilon_rate)
    print(oxigen_generator_rating)
    print(binary_to_decimal(oxigen_generator_rating[0]))
    print(co2_scrubber_rating)
    print(binary_to_decimal(co2_scrubber_rating[0]))  
    """

    data_out_file.write(str(binary_to_decimal(oxigen_generator_rating[0]) * binary_to_decimal(co2_scrubber_rating[0])))    

    data_in_file.close()
    data_out_file.close()


if __name__ == '__main__':
    main()
