/* Source https://adventofcode.com/2021/day/1#part2
 * 2021-12-12
 *
 * Solution:
 * 
 * */

#include <iostream>
#include<vector>

using namespace std;

int main() {
    vector<int> sums;

    int i, n;
    int measurement;
    int measurements_larger_than_previous = 0;

    freopen("data.in","r", stdin);
    freopen("output.txt","w", stdout);
    
    i = 0;
    while (cin>>measurement) {                     // reads all the measurements
        n = i;

        while (n >= i - 2 & n >= 0) {               // sums the current measurement to the last 3 positions of the sums vector
            if (sums.size() <= n) {
                sums.push_back(measurement);        // if there's no sum for the current measurement, then creates one
            } else {
                sums[n] += measurement;             // else, just sums it
            }

            n --;
        }

        i ++;
    }

    for (i = 1; i < sums.size() - 2; i++)           // reviews how many sum increments are, the -2 is because the last 2 items are not sums
        if (sums[i] > sums[i - 1])
            measurements_larger_than_previous ++;

    cout<<measurements_larger_than_previous;

    return 0;
}
