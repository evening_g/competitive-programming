"""
Source:
https://adventofcode.com/2021/day/2#part2

Date: 2021-12-13
"""

def main():
    # Define variables and read data
    # r, n = input().split(' ')

    position = {"depth" : 0, "horizontal_position" : 0, "aim" : 0}

    data_in_file = open('data.in', 'r')
    data_out_file = open('data.out', 'w')

    data_in_file.seek(0)
    data_in = data_in_file.read()

    instructions = data_in.split('\n')
    
    # Process

    for instruction in instructions:
        direction, amount = instruction.split(' ')
        amount = int(amount)

        if direction == "forward":
            position["horizontal_position"] += amount
            position["depth"] += position["aim"] * amount
        elif direction == "down":
            position["aim"] += amount
        elif direction == "up":
            position["aim"] -= amount

    
    # Print results
    data_out_file.write(str(position["depth"] * position["horizontal_position"]))

    data_in_file.close()
    data_out_file.close()

    
if __name__ == "__main__":
    main()
