// source: https://omegaup.com/arena/calentamiento-ing2021/#problems/Pares-e-impares

#include<iostream>

int main() {
    int n, m, even = 0, odd = 0;

    std::cin>>n;
    for (int i = 0; i < n; i++)
    {
        std::cin>>m;

        if (m % 2 == 0) {
            even ++;
        } else {
            odd ++;
        }
    }
    
    std::cout<<even<<" "<<odd<<'\n';

    return 0;
}