// source https://omegaup.com/arena/calentamiento-ing2021/#problems/Digitos-Inteligentes--

#include<iostream>

int sum_digits(int num) {
    int sum = 0;

    while (num > 0) {
        sum += num % 10;
        num /= 10;
    }

    return sum;
}

int main() {
    std::cin.tie(nullptr);
    std::ios_base::sync_with_stdio(false);

    int n, l , r, sum;
    std::cin>>n;

    for (int i = 0; i < n; i++)
    {
        sum = 0;
        std::cin>>l>>r;
        for (int i = l; i <= r; i++)
            sum += sum_digits(i);
        
        std::cout<<sum<<'\n';
    }

    return 0;
}