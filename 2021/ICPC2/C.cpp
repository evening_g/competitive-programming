#include<iostream>
#include<cstring>
#include <vector>

using namespace std;

int f = 0;

void proof(string s) {
  int r = 0, b = 0;

    int i = 0;
    while (i < s.size() && r <= b) {
        if (s[i] == 'R') {
            r ++;
        } else {
            b ++;
        }

        if (r > b) {
            s = s.substr(1,s.length()-1) + s.front();
            f ++;
            proof(s);                
        }

        i ++;
    }
}

int main()
{
  int t;
  string stack;

  cin>>t;

  vector <int> answers;

  for (int i = 0; i < t; i++) {
      f = 0;
      cin>>stack;
      proof(stack);
      answers.push_back(f);
  }

  for (int i = 0; i < answers.size(); i++) {
      cout<<answers[i]<<endl;
  }

  return 0;
}