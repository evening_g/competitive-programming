#include<iostream>

using namespace std;

int f = 0;

void product_of_digits(int n)
{
    int m = 1;
    
    string s = to_string(n);
    
    if (s.length() != 1) {
        for (int i = 0; i < s.length(); i++)
        {
            m *= s[i] - '0';
        }

        f ++;
        product_of_digits(m);   
    }
}

int main()
{
    int n, m;

    cin>>n;

    int answers [n];

    for (int i = 0; i < n; i++)
    {
        cin>>m;
        f = 0;
        product_of_digits(m);
        answers[i] = f;
    }

    for (int i = 0; i < n; i++)
    {
        cout<<answers[i]<<endl;
    }
    
    

    return 0;
}