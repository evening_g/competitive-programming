# This program is supposed to move the first char of a string to the end of it
# Example:
# input = Hola
# output = olaH

string = input()

string += string[0]
string = string[1:]

print(string)