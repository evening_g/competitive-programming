import java.util.Scanner;

public class I
{
    public static int f = 0;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n;
        n = sc.nextInt();
        sc.close();
        
        System.out.println(product_of_digits(n));
    }

    public static int product_of_digits(int n) {
        String sn = n + "";
        n = 1;

        f ++;

        if (sn.length() == 1) {
            return f;
        } else {
            for (int i = 0; i < sn.length(); i++) {
                n *= Integer.parseInt(sn.charAt(i)+"");
            }
                        
            product_of_digits(n);
            return n;
        }        
    }
}