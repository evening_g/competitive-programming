f = 0

def product_of_digits(n):
    m = 1
    global f
    if len(str(n)) == 1:
        return f
    else:
        for i in str(n):
            m = m * int(i)

        f += 1
        product_of_digits(m)

def main():
    answers = []
    n = int(input())
    global f

    for i in range(n):
        m = input()
        f = 0
        product_of_digits(m)
        answers += [f]

    for i in answers:
        print(i)

main()