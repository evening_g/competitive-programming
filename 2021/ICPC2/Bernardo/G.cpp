 // Searches the longest string for each element of the grid

#include<iostream>
#include<vector>
#include<math.h>

using namespace std;

void preprocessing(){
    return;
}

// Return '#' if out of bounds, '#' < 'A'
inline char getChar(const vector<vector<char>>& grid, int i, int j) {
    if (i < 0 || i >= grid.size() || j < 0 || j >= grid.size()) return '#';

    return grid[i][j];
}

int solve_square(
    const vector<vector<char>>& grid,
    vector<vector<int>>& dp,
    int i, int j
) {
    if (dp[i][j] == 0) {
        dp[i][j] = 1;
        for (int x = i - 1; x <= i + 1; x++)
        {
            for (int y = j - 1; y <= j + 1; y++)
            {
                if (getChar(grid, x, y) == grid[i][j] + 1)
                    dp[i][j] = max(dp[i][j], solve_square(grid, dp, x, y));
            }    
        }        
    }

    return dp[i][j];
}

void solve() {
    int n, m;
    vector<vector<char>> grid;
    vector<vector<int>> dp;

    cin>>n>>m;

    grid = vector<vector<char>>(n,vector<char>(m));
    dp = vector<vector<int>>(n,vector<int>(m,0));

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin>>grid[i][j];
        }
    }

    int ans = 1; // It's the same than initializing with 1

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            // solve_square(grid, dp, i, j) = max len starting in grid[i][j]
            ans = max(ans, solve_square(grid, dp, i, j));
        }
    }
    
    cout<<ans<<'\n';
}

int main(){
    cin.tie(nullptr)->sync_with_stdio(false);
}