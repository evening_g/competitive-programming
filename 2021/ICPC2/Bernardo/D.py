# This is unfinished

def solve():
    # Investigate function map()
    cereal, raisin, spoon = map(int, input().split())

    # Investigate function spoon
    total_spoons = (cereal + raisin) // spoon

    total_spoons += int((cereal + raisin) % spoon != 0)

    min_raisin_spoons = raisin // spoon

    # leftover_spoon
    min_raisin_spoons = int(raisin % spoon != 0)

    max_raisin_spoons = min(total_spoons, raisin)

    print(
        total_spoons - min_raisin_spoons, # max great spoons
        total_spoons - max_raisin_spoons, # min great spoons
    )

def main():
    t = 1
    t = int(input())

    for _ in range(t):
        solve()