import java.util.Scanner;

public class C {
    public static int f = 0;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t;        
        String stack;
        

        t = sc.nextInt();
        int answers [] = new int[t];   

        // Repeats this t times
        for (int i = 0; i < t; i++) {
            f = 0;
            stack = sc.next();
            proof_stack(stack);
            answers[i] = f;
        }
        sc.close();

        for (int i = 0; i < answers.length; i++) {
            System.out.println(answers[i]);
        }
    }

    public static void proof_stack(String stack) {
        StringBuffer s = new StringBuffer();
        s = s.append(stack);
        int r = 0, b = 0;

        int i = 0;
        while (i < s.length() && r <= b) {
            if (s.charAt(i) == 'R') {
                r ++;
            } else {
                b ++;
            }

            if (r > b) {
                s.append(s.charAt(0));
                s.deleteCharAt(0);
                f ++;
                proof_stack(s.toString());                
            }

            i ++;
        }
    }
}
