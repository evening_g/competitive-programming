f = 0

# This function is gonna calculate how many
# cards bob has to move to win
def proof_stack(stack):
    r = 0
    b = 0
    i = 0
    global f

    while(i < len(str(stack)) and r <= b):
        if (stack[i] == 'R'):
            r += 1
        else:
            b += 1
        
        if (r > b):
            f += 1
            stack += stack[0]
            stack = stack[1:]
            proof_stack(stack)
        
        i += 1

    return f

def main():
    t = int(input())
    answers = []
    global f

    for i in range(t):
        f = 0
        answers += [proof_stack(input())]

    for i in answers:
        print(i)


if __name__ == '__main__':
    main()