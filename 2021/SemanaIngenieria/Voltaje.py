"""
Source:
https://omegaup.com/arena/ING_UASLP_2021/practice/#problems/Voltajes

DESCRIPCIÓN
En el Laboratorio de Electrónica y Sistemas Digitales (LESD) del Área de
Ciencias de la Computación, la maestra Marcela está realizando la práctica
de medición de voltajes para la materia de Fundamentos de Circuitos
Eléctricos. La práctica consiste en medir el voltaje en un circuito de
mallas de acuerdo con un orden específico.

Durante la práctica Yoguel se despistó en algunos momentos pensando en la
siguiente conferencia de la Semana de Ingeniería, y no puso atención a las
indicaciones omitiendo algunas de las mediciones de voltaje o haciendo
algunas mediciones extra. Al momento de elaborar el reporte de la práctica
Yoguel tiene que anotar las mediciones que realizó, pero no está seguro si
hizo las mediciones correctas y en el orden indicado por la práctica.

Dada una secuencia de mediciones de voltaje de acuerdo al orden
establecido por la maestra, ¿podrías ayudar a Yoguel a determinar si sus
mediciones son adecuadas para reportalas?

Recuerda que Yoguel pudo haber omitido algunas mediciones (tal vez
ninguna), lo cual es aceptable para la maestra siempre y cuando todas sean
correctas, estén en el orden requerido por la práctica y no haya realizado
mediciones extra.

Entrada
La primer línea de entrada contiene un número entero N (1<=N<=10), la
cantidad de mediciones realizadas durante la práctica. La segunda línea
contiene una secuencia de N números enteros, las mediciones de voltaje
indicados por la maestra. La tercer línea contiene un número entero
M (1<=M<=10), la cantidad de mediciones realizadas por Yoguel. La última
línea contiene una secuencia de  números enteros, las mediciones de
voltaje realizadas por Yoguel. Todos los valores de voltaje están en un
rango [1, 12] volts.

Salida
Imprime "SI" (sin las comillas) en caso de que todas las mediciones de
Yoguel sean correctas y estén en el mismo orden requerido por la práctica,
o imprime "NO" (sin las comillas) en caso contrario.
"""

def main():
    # Define variables and read data
    ok = 1

    n = input()
    arrn = input().split(' ')

    m = input()
    arrm = input().split(' ')

    # We are going to have two arrays
    # We need to know if deleting some elements of arrn, it is equal to arrm and keeps the same order

    # we are going to have two pointers
    # one for arrm and one for arrn
    # for each element of arrm we are going to find it in arrn
    # if it does not exist, then we finish the program and the output is "NO"
    # else we save its index
    # then for the next element of arrn, we find if it exists after the index

    previous_index = -1
    for m in arrm:  # For each element of arrm
        index = -1  # We are going to find its index in arrn
        for i in range(previous_index + 1, int(n)):
            if arrn[i] == m:
                index = i
                previous_index = i
                break
        
        # if it does not exist, then the index is -1
        if index == -1:
            ok = 0
            break

    if ok == 1:
        print("SI")
    else:
        print("NO")


if __name__ == "__main__":
    main()