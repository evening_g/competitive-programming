/**
 * @file rarity_and_new_dress.cpp
 * @brief 
 * @version 0.1
 * @date 2022-05-26
 * 
 * @copyright
 * 
 * Source: https://codeforces.com/contest/1393/problem/D
 * 
 */

#include <bits/stdc++.h>

using namespace std;

bool is_valid_index(const vector<vector<char>> &pattern, int x, int y) {
    return (
            x >= 0 and x <= pattern.size()
        and y >= 0 and y <= pattern.front().size()
    );
}

/**
 * @brief BFS pattern to find a star
 * 
 * Check if all neighbors are equal to this
 * if so, explore all the neighbors
 * Else, return 1
 * 
 * @param pattern 
 * @param x 
 * @param y 
 * @return int size of the start
 */
int dfs(vector<vector<char>> pattern, int x, int y) {
    int dx [] = {0, 1, 0, -1};
    int dy [] = {1, 0, -1, 0};

    for (size_t i = 0; i < 4; i++)
    {
        if (
                !is_valid_index(pattern, x + dx[i], y + dy[i])
            or  !pattern[x + dx[i]][y + dy[i]] != pattern[x][y]
        ) { return 1; }
    }
    
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int n, m, x, sum;

    cin>>n>>m;

    vector<vector<char>> pattern (n, vector<char> (m));

    // Read pattern
    for (size_t i = 0; i < n; i++)
        for (size_t j = 0; j < m; j++)
            cin>>pattern[i][j];
        
    // Process
    sum = 0;
    for (size_t i = 0; i < n; i++)
        for (size_t j = 0; j < m; j++) {            
            x = dfs(pattern, i, j);
            sum += (2 * x * x) - (2 * x) + 1;            
        }

    cout<<sum<<'\n';

    return 0;
}

