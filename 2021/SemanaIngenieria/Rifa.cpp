/*
Source:
https://omegaup.com/arena/ING_UASLP_2021/practice/#problems/rifa-uaslp-2021

DESCRIPCIÓN
Para la Semana de Ingeniería se realizara un concurso de programación. Se
planea rifar un premio sorpresa entre los participantes.

Los organizadores del Concurso preferirían realizar esta rifa de una forma
original y divertida.

Una de las ideas que se plantearon fue la siguiente:

Crear una cantidad de N boletos con números aleatorios a_i, tal que
(1<=a_i<=N)

Repartir los boletos entre los participantes, de tal manera que a cada uno
le toque un boleto distinto.

N boletos con valores aleatorios entre 1 y N, se colocan en una urna y se
extraen secuencialmente, el ganador de la rifa será el participante que
posea el primer numero que se repita.

Si ningun numero aparece dos veces se repite el paso anterior.

ENTRADA
La primera línea de la entrada contiene un entero N, la cantidad de numeros
en la secuencia.

La segunda línea contiene N números enteros  tal que (1<=a_i<=N).

SALIDA
Imprime el primer numero que se repite en la secuencia, si ningun numero
se repite, imprime -1.

SOLUCIÓN
Vamos a guardar los números en un unordered_set, al agregar un elemento al
unordered_set se agrega sólo si ese elemento no se encuentra ya en el
contenedor.

Entonces para cada elemento que se agrega, vamos a comparar si el
contenedor creció, si no, entonces ese boleto ya se repitió
*/

#include <bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int n, ni, prevlen = 0, rep = -1;

    unordered_set<int> arr;

    cin>>n;
    for (int i = 0; i < n; i++)
    {
        cin>>ni;

        arr.insert(ni);
        
        if (arr.size() == prevlen) {
            rep = ni;
            break;
        } else {
            prevlen ++;
        }
    }
    
    cout<<rep<<'\n';

    return 0;
}