/*
DESCRIPCION
Todas las ciudades de Alumina están localizadas sobre una línea enumerada.
Por lo tanto, cada ciudad tiene asociada un número de acuerdo a su
posición en la línea. No hay dos ciudades en un mismo punto.

Los habitantes de Alumina adoran enviarse cartas entre ellos. Cada persona
puede enviar una carta solo a habitantes de otra ciudad (Ya que si viven
en la misma ciudad, entonces es fácil entregarla).

Soñará extraño, pero es cierto... el costo de enviar una carta es
equivalente a la distancia entre las ciudades del destinatario y el
remitente.

Para cada ciudad debes calcular dos valores min_i y max_i, donde min_i es
el mínimo costo de enviar una carta desde la -ésima ciudad a cualquier
otra ciudad, y max_i es el máximo costo de enviar una carta desde la
i-ésima ciudad a cualquier otra.

ENTRADAS

SALIDAS

EJEMPLOS


*/

#include <bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int n;
    cin>>n;
    int positions [n];
    int max [n];
    int min [n];
    
    for (int &i : positions) {
        cin>>i;
    }

    for (int i = 0; i < n; i++)
    {
        // Calculates max
        if ( abs(positions[i] - positions[0]) > abs(positions[i] - positions[n-1]) ) {
            max[i] = abs(positions[i] - positions[0]);
        } else {
            max[i] = abs(positions[i] - positions[n-1]);
        }

        // Calculates min;
        if (i == 0) {
            min[0] = abs(positions[0] - positions[1]);
        } else if (i == n - 1)
        {
            min[n - 1] = abs(positions[n - 1] - positions[n - 2]);
        } else if (abs(positions[i] - positions[i+1]) > abs(positions[i] - positions[i-1]))
        {
            min[i] = abs(positions[i] - positions[i-1]);
        } else {
            min[i] = abs(positions[i] - positions[i+1]);
        }       
        
    } 
    
    for (int i = 0; i < n; i++)
    {
        cout<<min[i]<<" "<<max[i]<<'\n';
    } 

    return 0;
}