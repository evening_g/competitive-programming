/*
DESCRIPCIÓN
La maestra Silvia planteó el siguiente reto a sus alumnos de Estructuras
de Datos que participarán en el Concurso de Programación de la Semana de
Ingeniería: se otorgará un punto extra en el siguiente parcial al alumno
que obtenga la mayor puntuación en el concurso, pero si hay un empate el
punto se tendrá que dividir.

Entre todos los participantes del concurso solamente 3 pertenecen al grupo
de la maestra, conociendo el nombre y el puntaje obtenido de cada uno de
ellos, ¿podrías ayudar a la maestra a determinar el nombre del alumno que
obtiene el punto extra?, o si hubiera un empate indicarlo con un mensaje
diciendo "Empate".

ENTRADA
La entrada consta de 3 líneas consecutivas, cada línea tiene una cadena
con el nombre del alumno y un número flotante con su puntuación, ambos
separados por un espacio. Se garantiza que en todos los casos los alumnos
tienen nombres diferentes.

SALIDA
La salida contiene únicamente el nombre del alumno que obtiene el punto
extra, o de la palabra "Empate" (sin las comillas) en caso de que 2 o más
alumnos obtengan la misma puntuación máxima.

EJEMPLOS

Entrada

Raúl 155
Joana 322.1
Pedro 232.5

Salida

Joana

Entrada

Eduardo 255.5
Octavio 128.2
Héctor 255.5

Salida

Empate
*/

#include <bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    string name;
    float score;
    float max = 0;
    string winner;

    for (int i = 0; i < 3; i ++){
        cin>>name;
        cin>>score;
        
        if (score == max) {
            winner = "Empate";
        }

        if (score > max) {
            max = score;
            winner = name;
        }
    }

    cout<<winner<<'\n';

    return 0;
}