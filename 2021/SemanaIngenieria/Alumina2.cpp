/*
Esta es una mejora de Alumina.cpp

DESCRIPCION
Todas las ciudades de Alumina están localizadas sobre una línea enumerada.
Por lo tanto, cada ciudad tiene asociada un número de acuerdo a su
posición en la línea. No hay dos ciudades en un mismo punto.

Los habitantes de Alumina adoran enviarse cartas entre ellos. Cada persona
puede enviar una carta solo a habitantes de otra ciudad (Ya que si viven
en la misma ciudad, entonces es fácil entregarla).

Soñará extraño, pero es cierto... el costo de enviar una carta es
equivalente a la distancia entre las ciudades del destinatario y el
remitente.

Para cada ciudad debes calcular dos valores min_i y max_i, donde min_i es
el mínimo costo de enviar una carta desde la -ésima ciudad a cualquier
otra ciudad, y max_i es el máximo costo de enviar una carta desde la
i-ésima ciudad a cualquier otra.

ENTRADAS

SALIDAS

EJEMPLOS


*/

#include <bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Cantidad de ciudades
    int n;

    // Distancia a la ciudad a la izquierda y a la derecha respectivamente
    int dleft, dright;

    // Leer cantidad de ciudades
    cin>>n;

    // Posiciones de las ciudades
    int positions [n];

    /* Distancias mínimas y máximas de cada ciudad
    first para mínimo
    second para el máximo*/
    pair<int, int> min_max [n];
    
    // Leer las posiciones
    for (int &i : positions)
        cin>>i;

    for (int i = 0; i < n; i++)
    {
        // Calculates min
        dleft = abs(positions[i] - positions[i-1 == -1 ? n-1 : i-1]);
        dright = abs(positions[i] - positions[i+1 == n ? 0 : i+1]);

        min_max[i].first = dleft < dright ? dleft : dright;

        // Calculates max        
        dleft = abs(positions[i] - positions[0]);
        dright = abs(positions[i] - positions[n-1]);      

        min_max[i].second = dleft > dright ? dleft : dright;
    }

    // Imprimir las distancias
    for (auto &i : min_max)
        cout<<i.first<<" "<<i.second<<'\n';

    return 0;
}