# Competitive programming

## C++ Template

````c++
/*
Problem description/source
date: yyyy-mm-dd
*/

#include<bits/stdc++.h>

using namespace std;

#define all(x) begin(x), end(x)
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef pair<ll, ll> pll;
typedef vector<ll> vl;

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    // Define variables and read data
    
    // Process
    
    // Print result
    
    return 0;
}
````

## Java Template

````java
/*
Problem description/source
date: yyyy-mm-dd
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
        
        // Define variables and read data
        // short n = Short.parseShort(sc.readLine());
        
        // Process
        
        // Print result
    }
}
````

## Python Template

````python
"""
Problem description/source
date: yyyy-mm-dd
"""

def main():
    # Define variables and read data
    # r, n = input().split(' ')
    
    # Process
    
    # Print results
    
if __name__ == "__main__":
    main()
````

## Numeric limits of data types

You need the `numeric_limits` class template in the `<limits>` header. Example:

````c++
#include <iostream>
#include <limits>

using namespace std;

template<typename T>
void showMinMax() {
   cout << "min: " << numeric_limits<T>::min() << endl;
   cout << "max: " << numeric_limits<T>::max() << endl;
   cout << endl;
}

int main() {
    // This prints the min and max value of the datatype <short>
    // Replace <short> for other datatypes
   	showMinMax<short>();
}
````

## Time complexity

| Time complexity | Description example                                          |
| --------------- | ------------------------------------------------------------ |
| 1               | Input output - applying a formula                            |
| log n           | Bubble algorithm                                             |
| n               | One dimension loop                                           |
| √n              | Grover's algorithm, find number of divisors                  |
| n log n         | Divide and conquer algorithm, quick sort                     |
| n^2^            | Two dimensions loop                                          |
| n^3^            | Three dimensions loop                                        |
| 2^n^            | An algorithm that iterates through all the subsets of a set  |
| n!              | An algorithm that iterates through all the permutations of a set |

| Input size | Required time complexity |
| ---------- | ------------------------ |
| n <= 10    | n!                       |
| n <= 20    | 2^n^                     |
| n <= 500   | n^3^                     |
| n <= 5000  | n^2^                     |
| n <= 10^6^ | n log n or n             |
| n > 10^6^  | log n or 1               |

