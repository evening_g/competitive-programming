/**
 * @file poi.cpp
 * @brief https://oj.uz/problem/view/IOI09_poi
 * @version 0.1
 * @date 2022-06-24
 * 
 * @bug Score 55%
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int find_rank(const vector<int> &v, const int &target) {
    int index = 0;
    while(v[index] != target) index++;
    return index + 1;
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // DEFINE VARIABLES

    int n, t, p;

    cin >> n >> t >> p;

    vector< vector<int> > contestants(n, vector<int>(t)); // tasks solved by each contestant
    vector<int> tasks(t, 0);    // worth of each task
    vector<int> score(n, 0);    // score of each contestant

    // READ DATA

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < t; j++) {
            cin >> contestants[i][j];
            // if this user solved this task
            if (contestants[i][j] == 0) {
                // the worth of the task decreases with each contestant that solves it
                tasks[j] ++;
            }
        }        
    }
    
    // PROCESS
    //calculate score of each contestant
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < t; j++) {
            if (contestants[i][j] == 1) {
                score[i] += tasks[j];
            } 
        }
    }
    
    // get philip schore
    int philip_score = score[p - 1];

    // sort by score
    sort(score.begin(), score.end(), greater<int>());

    // as there can't be contestats with the same score
    // we use philip's score to find its rank

    cout << philip_score << " " << find_rank(score, philip_score) << '\n';
       
    return 0;
}
