/**
 * @file substring_removal_game.cpp
 * @brief https://codeforces.com/contest/1398/problem/B
 * @version 0.2
 * @date 2022-06-23
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

bool compare(const pair<int, char> &a, const pair<int, char> &b) {
    return a.first > b.first;
}

int solve(string &binstr) {
    int alice = 0;   // Alice points
    bool turn = true;// Alice's turn
    vector<pair<int, char>> repeated;

    // COUNT REPEATED
    /*
    This section counts the number of repeated elements in the string
    For example, if the string is 011011110111
    Creates a vector with {1, 2, 1, 4, 1, 3}

    */
    char prev = binstr[0];
    repeated.push_back({1, prev});
    int pos = 0;

    for (int i = 1; i < binstr.size(); i++) {
        if (binstr[i] == prev) {
            repeated[pos].first ++;
        } else {
            repeated.push_back({1, binstr[i]});
            pos++;
        }
        prev = binstr[i];
    }
    
    sort(repeated.begin(), repeated.end(), compare);
    
    for (int i = 0; i < repeated.size(); i++) {
        if (repeated[i].second == '1') {
            if (turn) {
                alice += repeated[i].first;
            }
            turn = !turn;
        }
    }
    
    return alice;
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int t;
    string binstr;

    cin >> t;
    while (t--) {
        cin >> binstr;
        cout << solve(binstr) << '\n';
    }


    return 0;
}