/**
 * @file two_arrays_and_swaps.cpp
 * @brief https://codeforces.com/problemset/problem/1353/B
 * @version 0.2
 * @date 2022-06-22
 * 
 */

#include<bits/stdc++.h>

using namespace std;

int sum(vector<int> &v) {
    int result = 0;

    for (int &i : v)
        result += i;

    return result;
}

void solve() {
    int n, k;

    cin >> n >> k;

    vector<int> a(n);
    vector<int> b(n);
       
    for (int &i : a)
        cin>>i;

    for (int &i : b)
        cin>>i;

    sort(a.begin(), a.end());                   // sort a in ascending order
    sort(b.begin(), b.end(), greater<int>());   // sort b in descending order

    // swap `k` elements of a and b
    for (int i = 0; i < n and i < k; i++) {
        if (a[i] < b[i]) {
            swap(a[i], b[i]);
        } else {
            k++;
        }
    }
    cout << sum(a) << "\n";
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int t;

    cin >> t;

    while (t--)
        solve();

    return 0;
}