/**
 * @file stove.cpp
 * @brief https://oj.uz/problem/view/JOI18_stove
 * @version 0.1
 * @date 2022-06-27
 * 
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

using i16 = short;
using i32 = int;
using i64 = long long;

using u16 = unsigned short;
using u32 = unsigned int;
using u64 = unsigned long long;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    u32 guests, matches, operation_time;

    cin >> guests >> matches;

    vector<u32> time(guests);
    vector<u32> time_spaces(guests-1);

    // READ DATA
    cin >> time[0];
    for (u32 i = 1; i < guests; i++) {
        cin >> time[i];
        // Calculate the space between each guest
        time_spaces[i-1] = time[i] - time[i-1];
    }
    
    // PROCESS
    sort(time_spaces.begin(), time_spaces.end(), greater<int>());

    // Turn on the stove by first time
    operation_time = 1;
    matches -= 1;

    // For each space in decreasing order
    for (u32 i = 0; i < time_spaces.size(); i++) {
        if (matches > 0) {
            // if there are more matches
            // use one to turn the stove
            operation_time ++;
            matches --;
        } else {
            // else, leave it one since the last guess
            operation_time += time_spaces[i];
        }
    }
    
    cout << operation_time << '\n';
       
    return 0;
}