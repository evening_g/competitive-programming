/**
 * @file buggy_sorting.cpp
 * @brief https://codeforces.com/problemset/problem/246/A
 * @version 0.1
 * @date 2022-06-23
 * 
 * 
 */


#include<bits/stdc++.h>

using namespace std;

/**
 * @brief Buggy algorithm
 * 
 * @param v 
 */
/*void bubble_sort(vector<int> &v) {
    for (int i = 0; i < v.size() - 1; i++)
        for (int j = i; j < v.size() - 1; j++)
            if (v[j] > v[j + 1])
                swap(v[j], v[j + 1]);
}*/

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int n;

    cin >> n;

    //vector<int> v = {2, 2};

    if (n <= 2) {
        cout << "-1\n";
    } else {
        cout << "2 2 ";
        for (size_t i = 0; i < n - 2; i++) {
            cout << "1 ";    
            //v.push_back(1);
        }
    }

    /*cout << "Buggy sort:\n";

    bubble_sort(v);

    for (int &i : v) {
        cout << i << " ";
    }*/
       
    return 0;
}
