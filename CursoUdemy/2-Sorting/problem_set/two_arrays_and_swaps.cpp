/**
 * @file two_arrays_and_swaps.cpp
 * @brief https://codeforces.com/problemset/problem/1353/B
 * @version 0.1
 * @date 2022-06-22
 * @bug It is not obligatory to always swap the first k elements
 */

#include<bits/stdc++.h>

using namespace std;

int sum(vector<int> &v) {
    int result = 0;

    for (int &i : v)
        result += i;

    return result;
}

void solve() {
    int n, k;

    cin >> n >> k;

    vector<int> a(n);
    vector<int> b(n);
       
    for (int &i : a)
        cin>>i;

    for (int &i : b)
        cin>>i;

    sort(a.begin(), a.end());                   // sort a in ascending order
    sort(b.begin(), b.end(), greater<int>());   // sort b in descending order

    // swap first `k` elements of a and b
    /*for (size_t i = 0; i < k; i++)
        swap(a[i], b[i]);*/

    // same as

    swap_ranges(a.begin(), a.begin() + k, b.begin());
    
    cout << sum(a) << "\n";
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int t;

    cin >> t;

    while (t--)
        solve();

    return 0;
}