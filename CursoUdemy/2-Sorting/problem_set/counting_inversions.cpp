/**
 * @file counting_inversions.cpp
 * @brief https://www.spoj.com/problems/INVCNT/
 * @version 0.1
 * @date 2022-06-27
 *  
 */

#include <iostream>
#include <vector>

using namespace std;

using i16 = short;
using i32 = int;
using i64 = long long;

using u16 = unsigned short;
using u32 = unsigned int;
using u64 = unsigned long long;

u64 invertions = 0;

void merge(vector<u32> &v, u32 l, u32 r) {
    u32 m = (l + r) / 2;    // find the middle

    // copy each half to different vectors
    vector<u32> left(m - l + 1);
    vector<u32> right(r - m);   // +1 because the second will catch m

    for (u32 i = l; i <= m; i++)
        left[i - l] = v[i];
    
    for (u32 i = m + 1; i <= r; i++)
        right[i - m - 1] = v[i];

    // merge
    u32 i = 0, j = 0;
    while (i < left.size() and j < right.size()) {
        // if the left is the smaller
        // we do not have to do any "invertion"
        if (left[i] <= right[j]) {
            v[l+i+j] = left[i];
            i ++;
        } else {
            // else if the smaller is in the right
            // we must invert it
            v[l+i+j] = right[j];
            j ++;
            // if this number is smaller, then all the remaining
            // elements in `left` are also smaller
            invertions += left.size() - i;
        }
    }

    // when one of the list is exhausted
    // fill with the remaining list

    // left
    while (i < left.size()) {
        v[l+i+j] = left[i];
        i ++;
    }

    // right
    while (j < right.size()) {
        v[l+i+j] = right[j];
        j ++;
    }
}

void merge_sort(vector<u32> &v, u32 l, u32 r) {
    if (l < r) {
        u32 m = (l + r) / 2;    // find the middle
        
        merge_sort(v, l, m);    // sort left
        merge_sort(v, m+1, r);  // sort right

        merge(v, l, r);         // merge both sides
    }
}

void solve() {
    // DECLARE VARIABLES
    u32 n;

    cin >> n;

    vector<u32> a(n);

    // READ DATA
    for (u32 &i : a)
        cin >> i;

    // SORT
    invertions = 0;
    merge_sort(a, 0, n-1);

    // PRINT
    cout << invertions << '\n';
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    u32 t;

    cin >> t;

    while (t--)
        solve();
    
    return 0;
}
