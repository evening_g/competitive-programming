/**
 * @file substring_removal_game.cpp
 * @brief https://codeforces.com/contest/1398/problem/B
 * @version 0.1
 * @date 2022-06-23
 * @bug Nobody would choose a 0 string even if it's the largest
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int find_greatest(const vector<int> &v) {
    int greatest = v[0];
    int position = 0;

    for (int i = 1; i < v.size(); i++) {
        if (v[i] > greatest) {
            greatest = v[i];
            position = i;
        }
    }
    
    return position;
}

void remove_greatest(vector<int> &v, vector<char> &chars, int index) {
    v[index] = 0;
    chars[index] == chars[index] == '0' ? '1' : '0';

    if (index - 1 >= 0) {
        v[index] += v[index - 1];
        v.erase(v.begin() + index - 1);
        chars.erase(chars.begin() + index - 1);
        index --;   // as the previous element was removed
                    // all indexes decrease in one
    }

    if (index + 1 < v.size()) {
        v[index] += v[index + 1];
        v.erase(v.begin() + index + 1);
        chars.erase(chars.begin() + index + 1);
    }
}

int solve(string &binstr) {
    int alice = 0;   // Alice points
    bool turn = true;// true for Alice turn
    vector<int> repeated;
    vector<char> chars;

    // COUNT REPEATED
    /*
    This section counts the number of repeated elements in the string
    For example, if the string is 011011110111
    Creates a vector with {1, 2, 1, 4, 1, 3}

    */
    char prev = binstr[0];
    repeated.push_back(1);
    chars.push_back(prev);
    int pos = 0;

    for (int i = 1; i < binstr.size(); i++) {
        if (binstr[i] == prev) {
            repeated[pos] ++;
        } else {
            repeated.push_back(1);
            chars.push_back(binstr[i]);
            pos++;
        }
        prev = binstr[i];
    }
    
    // CALCULATE
    while (repeated.size() > 1) {
        // turn for alice
        pos = find_greatest(repeated);

        if (turn and chars[pos] == '1') {
            alice += repeated[pos];
        }   
        turn = !turn;     

        remove_greatest(repeated, chars, pos);
    }

    if (turn and chars[pos] == '1') {
        alice += repeated[0];
    }

    return alice;
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int t;
    string binstr;

    cin >> t;
    while (t--) {
        cin >> binstr;
        cout << solve(binstr) << '\n';
    }


    return 0;
}