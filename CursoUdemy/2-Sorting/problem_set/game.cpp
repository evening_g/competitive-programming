/**
 * @file game.cpp
 * @brief https://codeforces.com/problemset/problem/984/A
 * @version 0.1
 * @date 2022-06-24
 * 
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int n;

    cin >> n;

    vector<int> v(n);

    for (int &i : v)
        cin >> i;

    sort(v.begin(), v.end());

    cout << v[ceil((n - 1) / 2)] << "\n";

    return 0;
}