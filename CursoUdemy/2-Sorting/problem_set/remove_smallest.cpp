/**
 * @file remove_smallest.cpp
 * @brief https://codeforces.com/contest/1399/problem/A
 * @version 0.1
 * @date 2022-06-22
 * 
 */

#include<bits/stdc++.h>

using namespace std;

bool solve() {
    int n;

    cin >> n;

    vector<int> v(n);

    for (int &i : v)
        cin>>i;

    sort(v.begin(), v.end());

    int i = 0, j = 1;

    while (j < n)
        if (v[j++] - v[i++] > 1)
            return false;

    return true;
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int t;    

    cin >> t;

    while (t--) {
        if (solve()) {
            cout << "YES\n";
        } else {
            cout << "NO\n";
        }
    }
    
    return 0;
}
