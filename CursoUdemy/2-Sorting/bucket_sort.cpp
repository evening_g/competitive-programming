/**
 * @file bucket_sort.cpp
 * @author your name (you@domain.com)
 * @brief Bucket sort is a special algorithm as Count sort,
 * it is useful when we have an array that is distributed uniformely
 * In this, we imagine that we have certain amount of buckets, and for each element
 * we put it in the corresponding bucket
 * @version 0.1
 * @date 2022-06-20
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <iostream>
#include <vector>
#include <map>

using namespace std;

struct player {
    float height;
    bool is_football;

    player(float _height, bool _is_football) {
        height = _height;
        is_football = _is_football;
    }
};

void bucket_sort(vector<player> &players) {
    map< int, vector<player> > football_players;
    map< int, vector<player> > non_football_players;

    // Sort players according to their height and sport
    for (player &p : players) {
        if (p.is_football) {
            football_players[(int) (p.height * 5)].push_back(p);    // stores in intervals of 0.2
        } else {
            non_football_players[(int) (p.height * 5)].push_back(p);
        }
    }

    // clear the players
    players.clear();

    // refill the players in order, first football and then other players
    for (auto &fp : football_players)
        for (player &p : fp.second)
            players.push_back(p);
    
    for (auto &fp : non_football_players)
        for (player &p : fp.second)
            players.push_back(p);
}

int main() {
    freopen("Resources/footballData.txt", "r", stdin);
    freopen("Resources/output.txt", "w", stdout);

    vector<player> players;

    float height;
    bool is_football;

    while (cin>>height>>is_football)
        players.push_back(player(height, is_football));

    bucket_sort(players);

    for (player &p : players)
        cout << p.height << " " << p.is_football << "\n";

    return 0;
}