/**
 * @file count_sort.cpp
 * @brief Count sort is a sorting algorithm useful when many elements are repeated
 * and there are only a few types.
 * For example, if you wanna sort the sizes of shoes of 100 people, then Count sort
 * is a good option as there are only a few shoe sizes
 * 
 * It has a time complexity of O(n)
 * 
 * @version 0.1
 * @date 2022-06-20
 * 
 */

#include <iostream>
#include <map>
#include <vector>

using namespace std;

/**
 * @brief Count sort implementation for a string
 * 
 * @param str 
 */
void count_sort(string &str) {
    // If we are saving custom structs
    // we have to create our custom operators
    // or introduce a comparison function as third argument
    map<char, int> occurencies;

    // count occurrencies
    for (char &c : str)
        occurencies[c] ++;
    
    // restore the string
    str = "";

    // the map saves the keys in alphanumeric order
    // rewrite the string
    // add each type, n times
    for (auto &n : occurencies)
        for (size_t i = 0; i < n.second; i++)
            str += n.first;
}

int main() {
    freopen("tests/input.txt", "r", stdin);
    freopen("tests/output.txt", "w", stdout);

    string dna;
    cin >> dna;

    count_sort(dna);

    cout << dna << "\n";

    return 0;
}