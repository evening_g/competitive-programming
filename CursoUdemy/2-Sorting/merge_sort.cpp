#include <iostream>
#include <vector>

using namespace std;

/**
 * @brief Splits the subset list[l:r] at half and merges them
 * 
 * @param list 
 * @param l 
 * @param r 
 */
void merge(vector<int> &list, size_t l, size_t r) {
    size_t m = (l + r) / 2;
    
    vector<int> left (m - l + 1);
    vector<int> right (r - m);

    // copy the left half
    for (size_t i = l; i <= m; i++)
        left[i-l] = list[i];
    
    // copy the right half
    for (size_t i = m+1; i <= r; i++)
        right[i-m-1] = list[i];
    
    size_t l_ite = 0, r_ite = 0;

    // while both sublists aren't empty
    while (l_ite < left.size() && r_ite < right.size()) {
        if (left[l_ite] < right[r_ite]) {
            list[l + l_ite + r_ite] = left[l_ite];  // add element from left
            l_ite ++;
        } else {
            list[l + l_ite + r_ite] = right[r_ite]; // and element from right
            r_ite ++;
        }
    }
    
    // when one list is empty, fill with the other list

    // add remaining elements from left
    while (l_ite < left.size()) {
        list[l + l_ite + r_ite] = left[l_ite];
        l_ite ++;
    }

    // add remaining elements from right
    while (r_ite < right.size()) {
        list[l + l_ite + r_ite] = right[r_ite];
        r_ite ++;
    }
}

/**
 * @brief Merge Sort. Time complexity: n log(n)
 * 
 * @param list
 * @param l 
 * @param r 
 */
void merge_sort(vector<int> &list, size_t l, size_t r) {
    if (l < r) {                    // stop when sublist is null
        size_t m = (l + r) / 2;

        merge_sort(list, l, m);     // merge sort left
        merge_sort(list, m+1, r);   // merge sort right

        merge(list, l, r);          // merge both sides
    }
}

int main() {
    int n;

    cin >> n;

    vector<int> list(n);

    for (int &i : list)
        cin >> i;

    merge_sort(list, 0, n - 1);

    for (int &i : list)
        cout << i << ' ';

    return 0;
}
