import random

from numpy import sort

def main():
    n = int(input())

    list = random.sample(range(n), n)

    #  with open(f"../Ficheros/{nombre}", mode="a", encoding="UTF-8") as fichero1:
    with open("input.txt", mode="w", encoding="UTF-8") as input_file:
        print(n, file=input_file)
        for i in list:
            print(i, end=' ', file=input_file)

    list.sort()

    with open("expected_output.txt", mode="w", encoding="UTF-8") as expected_output:
        for i in list:
            print(i, end=' ', file=expected_output)
            

if __name__ == '__main__':
    main()
