/**
 * @file quicksort.cpp
 * @brief Quick sort implementation, with random index selection
 * @version 0.1
 * @date 2022-06-19
 * 
 * Algorithm analysis
 * https://www.cs.swan.ac.uk/~csfm/Courses/CS_332/quicksort.pdf
 * 
 * Notes:
 * Do not use size_t or any other unsigned data type to manage indexes in quicksort
 * Some of them may be negative, but they are properly managed
 * If they become negative with an unsigned data type, an overflow will occur
 * 
 */

#include <iostream>
#include <vector>

using namespace std;

/**
 * @brief Returns a random number in [l, r]
 * 
 * @param l 
 * @param r 
 * @return int 
 */
int get_pivot(int l, int r) {
    return l + rand() % (r - l);
}

/**
 * @brief Moves all the elements of `list` to the left side
 * if they're smaller than `pivot`
 * 
 * @param list 
 * @param pivot 
 * @param l 
 * @param r 
 * @return int 
 */
int partition(vector<int> &list, int l, int r) {
    // initialize the final pivot index
    int pivot_index = l;

    // chose a random pivot and swap it to the last element
    swap(list[get_pivot(l, r)], list[r]);

    // move all elements smaller to the pivot to the left
    for (int i = l; i <= r; i++) {
        // if the i-th element is smaller than the pivot
        if (list[i] <= list[r]) {
            swap(list[i], list[pivot_index]);
            pivot_index ++; // move left the pivot index
        }
    }
    
    // we move the pivot at the end, so the index is one before
    return pivot_index - 1;
}

/**
 * @brief quick sort implementation with random pivot
 * 
 * @param list 
 * @param l 
 * @param r 
 */
void quick_sort(vector<int> &list, int l, int r) {
    if (l < r) {
        int pivot_index = partition(list, l, r);

        quick_sort(list, l, pivot_index - 1);   // sort left side
        quick_sort(list, pivot_index + 1, r);   // sort right side
    }
}

int main() {
    int n;

    cin >> n;

    vector<int> list(n);

    for (int &i : list)
        cin >> i;

    quick_sort(list, 0, n - 1);

    for (int &i : list)
        cout << i << ' ';

    return 0;
}
