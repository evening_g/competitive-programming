/**
 * @file trust_groups.cpp
 * @brief https://onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=2756
 * @version 0.2
 * @date 2022-07-29
 * 
 * Checking for my algorithm
 */

#include <iostream>
#include <vector>
#include <stack>
#include <map>
#include <algorithm>

using namespace std;

#define loop while(true)

using i16 = short;
using i32 = int;
using i64 = long long;

using u16 = unsigned short;
using u32 = unsigned int;
using u64 = unsigned long long;

using f32 = float;
using f64 = double;

const u64 MOD = 10e9 + 7;


// Trust relationships graph
map < string, vector<string> > graph;
// Transposed graph
map < string, vector<string> > tgraph;
// Explored nodes
map < string, bool > explored;
// Roots found of first DFS
stack <string> roots;

void dfs1(string v) {
    explored[v] = true;

    for (string u : graph[v])
        if (!explored[u])
            dfs1(u);
}

void dfs2(string v) {
    explored[v] = true;

    for (string u : tgraph[v])
        if (!explored[u])
            dfs2(u);
}

int kosaraju() {
    u32 scc = 0;

    // For each unexplored vertex, save it and, run DFS on it    
    for (pair<string, bool> v : explored) {
        if (!v.second) {
            dfs1(v.first);
            roots.push(v.first);
        }
    }

    // Restart the explored
    for (auto &e : explored) e.second = false;

    // Run DFS on each of the found roots, on the transposed graph
    while (!roots.empty()) {
        const string v = roots.top();
        roots.pop();

        if (!explored[v]) {
            dfs2(v);
            scc ++;
        }
    }

    for (pair<string, bool> v : explored) {
        if (!v.second) {
            dfs2(v.first);
            scc ++;
        }
    }

    return scc;
}

int main() {
    u32 p, t;
    string name1, surname1, name2, surname2;

    loop {
        // Reset all data for each test case
        graph.clear();
        tgraph.clear();
        explored.clear();
        while (!roots.empty()) roots.pop();

        // Read data
        cin >> p >> t;

        // Break on "phantom" test case
        if (p == 0 and t == 0) break;

        // Read names
        while (p--) {
            cin >> name1 >> surname1;
            graph[name1+surname1] = {};
            explored[name1+surname1] = false;
        }

        // Read relationships
        while (t--) {
            cin >> name1 >> surname1 >> name2 >> surname2;
            graph[name1+surname1].push_back(name2+surname2);
            tgraph[name2+surname2].push_back(name1+surname1);
        }

        cout << kosaraju() << '\n';
    }
       
    return 0;
}
