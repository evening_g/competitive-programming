#include <iostream>
#include <vector>
#include <stack>
#include <map>
#include <sstream>

using namespace std;

vector <int> SCC;
vector <vector<int>> adjList, invAdjList;
vector<bool> visited;
map<string, int> namesDict;
int N;

// Step 1
void dfsSCC(int node, stack<int> &S) {
    visited[node] = true;
    for (int neighbour : adjList[node]) {
        if (!visited[neighbour])
            dfsSCC(neighbour, S);
    }

    S.push(node);
}

stack <int> getStack() {
    stack<int> S;
    visited.assign(N, false);
    for (int i =0; i < N; i++) {
        if (!visited[i])
            dfsSCC(i, S);
    }
    return S;
}

// Step 2
void invertGraph() {
    invAdjList.assign(N, vector<int>());

    for (int i = 0; i < N; i++) {
        for (int u : adjList[i]) {
            invAdjList[u].push_back(i);
        }
    }
}

// Step 3
void dfsCC(int node, int cnt) {
    SCC[node] = cnt;
    for (int neighbour : invAdjList[node]) {
        if (SCC[neighbour] == -1)
            dfsCC(neighbour, cnt);
    }
}

int findSCCs(stack<int> &S) {
    SCC.assign(N, -1);
    int numSCCs = 0, curNode;
    while (!S.empty()) {
        curNode = S.top();
        S.pop();
        if (SCC[curNode] == -1) {
            dfsCC(curNode, numSCCs);
            numSCCs ++;
        }
    }
    return numSCCs;
}

int SCCs() {
    stack<int> S = getStack();
    invertGraph();
    return findSCCs(S);
}

void solve(int M) {
    adjList = vector<vector<int>>(N);
    namesDict.clear();
    string line;

    for (int i = 0; i < N; i++) {
        getline(cin, line);
        if (namesDict.find(line) == namesDict.end())
            namesDict[line] = namesDict.size();
    }

    while (M--) {
        getline(cin, line);
        int node1 = namesDict[line];
        getline(cin, line);
        adjList[node1].push_back(namesDict[line]);
    }

    cout << SCCs() << '\n';
}

int main() {
    int M;
    string line;
    while(true) {
        getline(cin, line);
        if (line[0] == '0') break;
        stringstream S(line);

        S >> N >> M;
        solve(M);
    }

    return 0;
}

