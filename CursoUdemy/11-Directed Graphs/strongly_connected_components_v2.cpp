/**
 * @file strongly_connected_components_v2.cpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.2
 * @date 2022-08-01
 * 
 * Graph:
 * 
 * 0 <- 1 <- 2 <- 7
 * ↓    ↑    ↓    ↑
 * 3 -> 4 <- 5 -> 6
 * 
 * {{3}, {0}, {1, 5}, {4}, {1}, {4, 6}, {7}, {2}};
 * 
 * This is my own build so it may be buggy
 */

#include <iostream>
#include <vector>
#include <stack>

using namespace std;

using i16 = short;
using i32 = int;
using i64 = long long;

using u16 = unsigned short;
using u32 = unsigned int;
using u64 = unsigned long long;

// Graph size
const u16 N = 9;

vector < vector<u16> > adj = 
{
    {1},        // 0
    {2, 3},     // 1
    {0},        // 2
    {4},        // 3
    {5},        // 4
    {3},        // 5
    {2, 8},     // 6
    {6},        // 7
    {7},        // 8
};

vector < vector<u16> > rev(N, vector<u16>());
// vector <bool> explored(N, false);
// SCC resulting of DFS 1
vector <i16> sccs1(N, -1);
vector <i16> sccs2(N, -1);
vector <u16> roots;

void dfs1(u16 v, i16 scc) {
    sccs1[v] = scc;

    for (const u16 &u : adj[v])
        if (sccs1[u] == -1)
            dfs1(u, scc);
}

void transpose_graph() {
    for (u16 v = 0; v < N; v++)
        for (const u16 &u : adj[v])
            rev[u].push_back(v);
}

void dfs2(u16 v, i16 scc) {
    sccs2[v] = scc;

    for (const u16 &u : rev[v])
        if (sccs2[u] == -1)
            dfs2(u, scc);
}

void kosaraju() {
    // Step 1: Run DFS on the graph to find the roots
    i16 scc = 0;
    stack <u16> roots;

    for (u16 v = 0; v < N; v++)
        if (sccs1[v] == -1) {
            dfs1(v, scc);
            scc ++;
            roots.push(v);
        }

    // Step 2: Transposing the graph
    transpose_graph();

    // Step 3: Run DFS on the transposed graph
    scc = 0;

    // Start exploring the roots we find previously
    // This to discard SCCs that may had been counted together
    while (!roots.empty()) {
        const u16 v = roots.top();
        roots.pop();
        
        if (sccs2[v] == -1) {
            dfs2(v, scc);
            scc ++;
        }
    }

    // Explore the rest of the unexplored nodes
    for (u16 v = 0; v < N; v++)
        if (sccs2[v] == -1) {
            dfs2(v, scc);
            scc ++;
        }

    // sccs2 contains the real SCCs
}
 
int main() {
    kosaraju();

    for (u16 i = 0; i < N; i++) {
        cout << (i) << " " /*<< sccs1[i] << " "*/ << sccs2[i] << "\n";
    }
    
    return 0;
}
