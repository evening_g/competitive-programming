/**
 * @file strongly_connected_components.cpp
 * @author your name (you@domain.com)
 * @brief Algorythm to find Strongly Connected Components (SCC)
 * @version 0.1
 * @date 2022-07-31
 * 
 * In a directed graph, SCCs can be thought of as self-contained cycles,
 * where every vertex in a given cuvle can reach every other vertex in
 * the same cycle.
 * 
 * SCCs must have the biggest size possible.
 * 
 * Graph:
 * 
 * 0 <- 1 <- 2 <- 7
 * ↓    ↑    ↓    ↑
 * 3 -> 4 <- 5 -> 6
 * 
 */

#include <iostream>
#include <vector>
#include <stack>

using namespace std;

// Graph size
const unsigned int N = 8;

// stores the scc to which each node belongs
vector <int> scc(N, -1);
vector < vector<int> > adj_list(N, vector<int>()), inv_adj_list(N, vector<int>());
vector <bool> explored(N, false);
// Stores each node after exploring its connections with DFS
stack <int> nodes;

void dfs_scc(int node) {
    explored[node] = true;

    for (const int &neighbour : adj_list[node])
        if (!explored[neighbour])
            dfs_scc(neighbour);
    
    // After exploring all the connections (and the connections
    // of the connections) of this node, add it to the stack
    nodes.push(node);
}

/**
 * @brief Fills the stack `nodes` with DFS
 * 
 * Travels all the nodes in the graph
 * if a node has not been explored,
 * explores it, and all its connections
 * then pushes the first node to a stack
 * 
 * Example: 
 * 
 * For the graph:
 * 
 * 0 <- 1 <- 2 <- 7
 * ↓    ↑    ↓    ↑
 * 3 -> 4 <- 5 -> 6
 * 
 * It will stack:
 * 
 */
void fill_stack() {
    for (int node = 0; node < N; node++)
        if (!explored[node])
            dfs_scc(node);
}

/**
 * @brief Inverts the vertexes in the graph,
 * If there exists a connection from a -> b
 * makes it b -> a
 * 
 */
void invert_graph() {
    // For each node, find all its connections
    // And set a connection from the neighbour
    // to the node
    for (int node = 0; node < N; node++)
        for (int &connection : adj_list[node])
            inv_adj_list[connection].push_back(node);
}

void dfs_set_cc(int node, int component) {
    scc[node] = component;

    for (const int &neighbour : inv_adj_list[node])
        if (scc[neighbour] == -1)
            dfs_set_cc(neighbour, component);
}

/**
 * @brief Finds the SCC to which each node belongs
 * Returns the number of components that there exists
 * 
 * @return int 
 */
int find_sccs() {
    int component = 0, node;

    while (!nodes.empty()) {
        node = nodes.top();
        nodes.pop();

        if (scc[node] == -1) {
            dfs_set_cc(node, component);
            component++;
        }
    }

    return component;
}

/**
 * @brief Solve function
 * 
 */
int sccs() {
    fill_stack();
    invert_graph();
    const int components = find_sccs();

    return components;
}

int main() {
    adj_list = {{3}, {0}, {1, 5}, {4}, {1}, {4, 6}, {7}, {2}};
    
    sccs();

    for (const int &component : scc)
        cout << component << " ";

    return 0;

}
