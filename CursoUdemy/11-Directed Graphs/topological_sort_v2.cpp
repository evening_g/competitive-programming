/**
 * @file topological_sort.cpp
 * @brief Topological sort algorythm for Directed Acyclic Graphs (DAG)
 * @version 0.2 I just want to know what happens if I use a stack instead a queue
 * @date 2022-07-29
 * 
 * Graph:
 * 0 --> 1 --> 2 --> 7
 * ^     ^     ^
 * |     |     |
 * 3     4 --> 5 --> 6
 * 
 * The topological sort algorythm is used to find a way to travel
 * all the nodes in a DAG without repeating any.
 * 
 * In general ways, it starts finding the sources (nodes which aren't connected by 
 * any other node), then destroying the sources and finding new ones, until the full
 * graph is traveled.
 * 
 * If a stack is used instead a queue
 * it gives a order as a DFS would do
 * I'm not sure yet if it is right or not
 * 
 * @todo Find bugs in this thing
 */

#include <iostream>
#include <vector>
#include <queue>
#include <stack>

using namespace std;

/**
 * @brief Count how many nodes are connected to each node
 * 
 * @param adj_list 
 * @param incomming 
 */
void get_incomming
(
    const vector < vector<int> > &adj_list,
    vector <int> &incomming
)
{
    incomming.assign(adj_list.size(), 0);
    
    // Travel along the adjacecy list
    for (const auto &node : adj_list)
        for (const int &neighbour : node)
            // this node is connected to this neighbour
            // then the neighbour has one more incomming connection
            incomming[neighbour] += 1;
}

/**
 * @brief Finds the sources of the graph
 * Requires `incomming` vector, which contains the incomming
 * connections of each node
 * 
 * @param incomming 
 * @param sources 
 */
void get_sources
(
    const vector <int> &incomming,
    stack <int> &sources
)
{
    for (int i = 0; i < incomming.size(); i++)
        if (incomming[i] == 0)
            sources.push(i);
}

/**
 * @brief Topological sort algorythm
 * Sorts the graph in `adj_list` and puts the result in `sorted`
 * 
 * Algorythm explanation
 * 
 * @param adj_list 
 * @param sorted 
 */
void topological_sort
(
    const vector < vector<int> > &adj_list,
    vector <int> &sorted
)
{
    vector <int> incomming;
    stack <int> sources;
    int node;

    // Get sources
    get_incomming(adj_list, incomming);
    get_sources(incomming, sources);

    // Each node will eventually turn into a source.
    // When there are no sources left
    // then there are no nodes left
    while (!sources.empty()) {
        // Get a source
        node = sources.top();
        sources.pop();


        // Add this source to the sorted vector
        sorted.push_back(node);

        // Foe each neighbour of this node
        for (const int &neighbour : adj_list[node]) {
            // We don't need to check if we already explored
            // this node, because the graph is a DAG

            // We are removing this source
            // So the neighbour will have 1 incomming less
            incomming[neighbour] -= 1;

            // if the neighbour now has 0 incomming nodes
            // then it has become a source
            if (incomming[neighbour] == 0)
                sources.push(neighbour);
        }            
    }
}

int main() {
    vector < vector<int> > adj_list =
    {
        {1},
        {2},
        { },
        {0, 4},
        {1, 5},
        {2},
        {3, 7},
        {4, 8},
        {5}
    };

    vector <int> topological_order;

    topological_sort(adj_list, topological_order);

    for (int &node : topological_order)
        cout << node << " ";

    return 0;
}