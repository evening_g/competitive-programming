# Strongly connected components and Condensation graphs

## Definitions

$G$ Graph.
$V$ Vertices.
$n$ Number of vertices.
$m$ Number of edges

## Strongly connected component (SCC)

A strongly connected component is a maximal subset of vertices which form a cycle, where every vertex in the subset, can reach any other vertex in the same subset.

They can also be thought of as subsets of vertices $$C$$ such that any two vertices of this subset are reachable from each other, i.e. for any $$u,v\in C$$:
$$
u\mapsto v, v\mapsto u
$$
Where $$\mapsto$$ means, is reachable from, to.

![](SCC1.png)

## Condensation graph

For a given graph $$G$$, its condensation graph is a graph $$G^{SCC}$$ that contains in each vertex a SCC of $$G$$.

Condensation graphs are acyclical, if during the construction of the graph we result with a cycle, then the SCC involved in the cycle belong to a single bigger SCC. There is a oriented edge between two vertices $$C_i$$ and $$C_j$$ of the condensation graph, if and only if there are two vertices $$u\in C_i$$, and $$v\in C_j$$ such that there is an edge in the initial graph, $$(u,v)\in E$$.

![`](condensed_graph.png)

## Algorithm to find SCC - Kosaraju's Algorithm

### Algorithm explanation

This is an easy algorithm based in a series of two DFS that works in $$O(n+m)$$ time.

#### First step

We start at each vertex of the graph and run a DFS from every non-visited vertex, until all the graph is explored. We must keep track of the **exit time** $$tout[v]$$ because it will be used.

**Notations**

Exit time $$tout[C]$$ of $$C$$ is the maximum of values $$tout[v]$$ by all $$v\in C$$.

Entry time $$tin[V]$$ of $$C$$ is the minimum of values $$tin[v]$$ by all $$v\in C$$.

**Theorem**

Let $$C$$ and $$C'$$ are two different SCC and there is an edge $$(C, C')$$, in a condensation graph between these two vertices. Then $$tout[C] > tout[C']$$.

In other words, if $$C$$ and $$C'$$ are two different SCC, and there is a vertex from $$C$$ to $$C'$$, then the exit time of $$C$$ will be bigger than the one of $$C'$$.

There are two main different cases depending on which SCC will be visited first in the DFS, i.e. depending on the difference between $$tin[C]$$ and $$tin[C']$$.

- **If $$C$$ was reached first,** it means that at some point, DFS felt at some vertex $$v$$, of component $$C$$, and all other vertices of $$C$$ and $$C'$$ have not been visited yet, so DFS will fall through all vertices $$u$$ of $$C$$ and $$C'$$, let $$u\in C\cup C', u\neq v$$, we have that $$tout[v] > tout[u]$$.
- **If $$C'$$ was reached first,** it means that DFS felt at some vertex $$v$$, of component $$C'$$, and all other vertices of $$C$$ and $$C'$$ have not been visited yet. By condition, there is an edge $$(C. C')$$, but $$C$$ is **not reachable** from $$C'$$, so DFS will not reach vertices of $$C$$, and $$C$$ components will be visited later. So $$tout[C]$$ > $$tout[C']$$.

With this, the proof is complete. **In resume,** any edge $$(C, C')$$ in a condensation graph, comes from a component with a larger value of $$tout$$ to a component with a smaller value. :call_me_hand:

If we sort all vertices $$v\in V$$ in decreasing order of their $$tout[v]$$, then the first vertex $$u$$ is going to do as *root* for a SCC, then we run DFS from $$u$$ to visit all the vertices in this and only this SCC. With this, we can select all SCC, by finding the root of each SCC, exploring it with DFS and then removing it to pass to the next SCC.

#### Second step

Let's consider a transposed graph $$G^T$$ a graph resulting by reversing the direction of each edge in $$G$$. This graph will exactly the same SCC than the original, but the edges in its condensation graph will be reversed. Now there are no edges from our original *root* vertices to other components. We have turned our *roots* into *leaves* and otherwise.

Now we can visit the whole *root* SCC, containing vertex $$v$$ by running a DFS on $$v\in G^T$$. This search will visit all vertices in this and only this SCC, same as before, we then remove this SCC and find the next vertex $$v$$ with the maximal $$tout[v]$$ and so on until exploring the full graph.

### Implementation

1. Run DFS on graph $$G$$ until exploring all of the vertices. Every set of vertices explored in each search is a SCC.
2. Get $$G^T$$. Run DFS on $$G^T$$ until exploring all of the vertices.  Every set of vertices explored in each search is a SCC.

Step one represents a **reversed topological sort** of $$G$$.

The algorithm's scheme generates SCC by decreasing order of their exit times, this it **generates components - vertices of condensation** graph in topological sort order.

