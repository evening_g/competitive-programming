/**
 * @file bfs_single_source_shortest_paths.cpp
 * @brief 
 * @version 0.1
 * @date 2022-08-08
 * 
 * This algorithm is used to find out the smallest amount
 * of nodes we have to travel from `root` to `destination`.
 * 
 * Graph:
 * 
 * 0 <- 1 <- 2 <- 7
 * ↓    ↑    ↓    ↑
 * 3 -> 4 -> 5 -> 6
 */

#include <iostream>
#include <queue>
#include <vector>

const int INF = INT32_MAX;
const int N = 8;

using namespace std;

/**
 * @brief BFS implementation to find the shortes path
 * to each node from `root`.
 * 
 */
void distances_bfs(
    const int root,
    const vector < vector<int> > &adj,
    vector < vector<int> > &distances
) {
    // Variable definitions

    queue<int> to_explore;
    int node;

    // The distance from `root` to itself is 0
    distances[root][root] = 0;
    to_explore.push(root);

    while (!to_explore.empty()) {
        node = to_explore.front();
        to_explore.pop();

        // For each of the neighbours of `node`
        for (const int &neighbour : adj[node]) {
            // If the distance from `root` to `neighbour` has not
            // been updated, then we have to explore this node
            if (distances[root][neighbour] == INF) {
                to_explore.push(neighbour);
                // The distance from `root` to `neighbour`
                // is equal to the distance from `root` to `node` + 1
                distances[root][neighbour] = distances[root][node] + 1;
            }
        }
    }
    
}

int main() {
    // Grid of distances
    // Where the i-th row contains all the distances
    // from the node `i` to all of the others
    vector < vector<int> > distances(N, vector<int>(N, INF));
    // Adjacency list
    vector < vector<int> > adj = {{3}, {0}, {1, 5}, {4}, {1, 5}, {6}, {7}, {2}};

    // For each node, explore the distances from this to all of the others
    for (int node = 0; node < N; node++)
        distances_bfs(node, adj, distances);

    // Print the distances
    for (const auto &i : distances) {
        for (const auto &j : i)
            cout << j << ' ';

        cout << '\n';
    }

    return 0;
}
