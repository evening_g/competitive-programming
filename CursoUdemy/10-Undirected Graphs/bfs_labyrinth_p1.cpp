/**
 * @file bfs_labyrinth_p1.cpp
 * @author your name (you@domain.com)
 * @brief BFS implementation to solve labyrinth problem (single source)
 * @version 0.1
 * @date 2022-07-29
 * 
 * Imagine that you are inside a labyrinth, that has many exits
 * And you want to know which is the nearest exit.
 * 
 * The labyrinth is represented by a graph, where each node
 * is a point where you can take different paths
 * 
 * Graph:
 * 
 *  0 -- 1 -- 2 -- (7)
 *  |    |    |
 * (3)   4 -- 5 -- (6) 
 */

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

/**
 * @brief Compare function for std::bsearch
 * 
 * It must read to void pointers x, y
 * Return -1 if x goes before y
 * Return 0 if x and y are equal
 * Return 1 if x goes after y
 * 
 * @param x 
 * @param y 
 * @return int 
 */
int compare(const void *x, const void *y) {
    const int *a = (int*)x;
    const int *b = (int*)y;
    return *a < *b ? -1
        :  *a > *b ? +1
        : 0;
}

/**
 * @brief Returns wether if std::bsearch found this node in the exits array or not
 * 
 * @param node 
 * @param exits 
 * @return true 
 * @return false 
 */
bool is_exit(int node, const vector <int> &exits) {
    int* index = (int*) bsearch(&node, exits.data(), exits.size(), sizeof(int), compare);
    return (index);
}

/**
 * @brief BFS implementation to find the nearest exit of the labyrinth
 * 
 * Returns the first exit it finds
 * If it finds no exit, then returns -1
 * 
 * @param start 
 * @param adj_list 
 * @param exits 
 * @param explored 
 * @return int 
 */
int nearest_exit
(
    int start,
    const vector < vector <int> > &adj_list,
    const vector <int> &exits,
    vector <bool> &explored
)
{
    queue <int> to_explore;
    to_explore.push(start);
    int node;

    while (!to_explore.empty()) {
        node = to_explore.front();
        to_explore.pop();
        explored[node] = true;

        if (is_exit(node, exits))
            return node;
        
        for (int neighbour : adj_list[node])
            if (!explored[neighbour])
                to_explore.push(neighbour);
    }

    // Returns -1 if there is no exit
    return -1;
}

int main() {
    const int N = 8;
    vector < vector <int> > adj_list =
    {
        {1, 3},
        {0, 2, 4},
        {1, 5, 7},
        {0},
        {1, 5},
        {2, 4, 6},
        {5},
        {2}
    };

    vector <int> exits = {3, 6, 7};
    vector <bool> explored (N, false);
    int start;

    cout << "Start node: ";
    cin >> start;

    int exit = nearest_exit(start, adj_list, exits, explored);
    cout << "Nearest exit: " << exit << '\n';

    return 0;
}