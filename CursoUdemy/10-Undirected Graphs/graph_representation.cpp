/**
 * @file graph_representation.cpp
 * @date 2022-07-27
 * @brief Graph Representation in C++
 * 
 * In competitive programming, it is easier to adapt the C++ data types 
 * to represent a graph, than creating a graph class.
 * 
 * There are 3 main ways of representing a graph, each with its pros and cons
 * 
 * The 3 ways are explained in this file.
 * 
 * Graph examples
 * 
 * Graph 1:
 * 
 * 0 --> 1
 *      ^
 *      |
 * 3 <-> 2
 *  
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    
    // Graph 1

    /**
     * @brief Adjacency list
     * 
     * This representation uses a two dimensional vector
     * 
     * In the i-th position, in stores a list which contains
     * the nodes to which the i node is connected
     * 
     * It makes BFS and DFS more efficient
     * 
     * You can use binary search to check if one node is connected to another
     * But only if the nodes are sorted
     */
    vector< vector<uint> > adj_list =
    {
        {1},    // node 0 is connected to 1
        { },    // node 1 is connected to none
        {1, 3}, // node 2 is connected to 1 and 3
        {2}     // node 3 is connected to 2
    };
    
    /**
     * @brief Adjacency matrix
     * 
     * This representation uses a matrix to represent with
     * 0 and 1 wether if the j-th node is connected to the i-th node
     * where j is the row and i the column
     * 
     * It makes BFS and DFS slower
     * 
     * You can check in constant time wether if two nodes are connected
     * (just checking the value in [i][j])
     * 
     */
    vector< vector<bool> > adj_matrix =
    {
        {0, 1, 0, 0},   // node 0 is connected to 1
        {0, 0, 0, 0},   // node 1 is connected to none
        {0, 1, 0, 1},   // node 2 is connected to 1 and 3
        {0, 0, 1, 0}    // node 3 is connected to 2
    };

    /**
     * @brief 
     * 
     * This representation uses a list of pairs to represent
     * each pair of nodes that are connected.
     * 
     * In directed graphs, usualy the first node of the pair
     * is connected to the second
     * 
     * It makes Kruskal's algorithm more efficient
     * 
     */
    vector< pair<uint, uint> > edges =
    {
        {0, 1},
        {2, 1},
        {2, 3},
        {3, 2}
    };

    return 0;
}
