// Program to find the house with the biggest number of potatos
// In the graph, each connected group is a house and each node a room
// Each room contains a number of potatoes
// Graph (undirected):
// 0      1 ---- 2
// |	  |	     |
// 3	  4 ---- 5
// Values: 6, 7, 2, 8, 14, 15

#include <iostream>
#include <vector>

using namespace std;

/**
 * Room definition
 * */
struct Room
{
    // Number of potatoes in the room
    int potatoes;
    // Has the room been visited
    bool visited;
    // Adjacent rooms to this
    vector<Room*> adjacent;

    // Constructor
    Room(int _potatoes, vector<Room*> _adjacent) {
        potatoes = _potatoes;
        visited = false;
        adjacent = _adjacent;
    }
};

/**
 * DFS implementation to count potatoes
 */
int count_potatos(Room* root)
{
    // Mark this node as visited
    root->visited = true;
    int total = root->potatoes;

    // For each of its neighbours
    for (auto neighbour : root->adjacent) {
        if (!neighbour->visited) {
            // The total of potatoes in this room
            // is going to be equal to the potatoes in this room
            // plus the potatoes in each neighbour
            // Cannot return here because then it will only count
            // the potatoes of one neighbour
            total += count_potatos(neighbour);
        }
    }

    return total;
}

int main()
{
    Room r0(6, {});
    Room r1(7, {});
    Room r2(2, {});
    Room r3(8, {});
    Room r4(14, {});
    Room r5(15, {});

    r0.adjacent = { &r3 };
    r1.adjacent = { &r2, &r4 };
    r2.adjacent = { &r1, &r5 };
    r3.adjacent = { &r0 };
    r4.adjacent = { &r1, &r5 };
    r5.adjacent = { &r2, &r4 };

    vector<Room*> rooms = { &r0, &r1, &r2, &r3, &r4, &r5 };
    int start;

    cout << "Start room : ";
    cin >> start;
    cout << "Potatoes: " << count_potatos(rooms[start]);

    return 0;
}
