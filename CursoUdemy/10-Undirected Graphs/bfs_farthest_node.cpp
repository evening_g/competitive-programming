// Graph (undirected):
// 0 ---- 1 ---- 2
// |	  |	     |
// 3	  4 ---- 5

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

int farthest_node(
    const int root,
    vector <bool> &explored,
    const  vector < vector<int> > &adj_list)
{
    queue<int> to_explore;
    int node, last_node;

    to_explore.push(root);
    explored[root] = true;
    last_node = root;

    while (!to_explore.empty()) {
        node = to_explore.front();
        to_explore.pop();
        last_node = node;

        for (int neighbour : adj_list[node]) {
            if (!explored[neighbour]) {
                to_explore.push(neighbour);
                explored[neighbour] = true;
            }
        }
    }

    return last_node;
}

int main()
{
    const int N = 6;
    int root;

    vector <bool> explored (6, false);
    vector < vector<int> > adj_list =
    {
        {1, 3},
        {0, 2, 4},
        {1, 5},
        {0},
        {1, 5},
        {2, 4}
    };


    cout << "Node as root: ";
    cin >> root;

    cout << "Farthest node = " << farthest_node(root, explored, adj_list) << '\n';
}
