/**
 * @file conected_components.cpp
 * @brief DFS implementation to count connected components and state
 * in which group is located each node.
 * 
 * @version 0.1
 * @date 2022-07-28
 * 
 * Graph (undirected):
 *	0 ---- 1 ---- 2
 *	|	   |	  |
 *	3	   4 ---- 5		6 ---- 7	8 ---- 9 ---- 10
 */

#include <iostream>
#include <vector>

using namespace std;

void dfs
(
    const vector< vector<int> > &adj_list,
    vector<int> &groups,
    int group,
    int node
)
{
    groups[node] = group;

    for (int neighbour : adj_list[node])
        if (groups[neighbour] == -1)
            dfs(adj_list, groups, group, neighbour);
}

int main() {
    const int N = 11;

    vector< vector<int> > adj_list =
    {
        {1, 3},
        {0, 2, 4},
        {1, 5},
        {0},
        {1, 5},
        {2, 4},
        {7},
        {6},
        {9},
        {8, 10},
        {9}
    };

    vector<int> groups(N, -1);
    int group = 0;

    for (int i = 0; i < N; i++)
        if (groups[i] == -1)
            dfs(adj_list, groups, group++, i);
            
    cout << "There are " << group << " groups\n";

    for (int i : groups)
        cout << i << " ";
       
    return 0;
}
