/**
 * @file dfs.cpp
 * @date 2022-07-27
 * @brief DFS and BFS implementation with adjacency list
 *
 * Graph:
 *
 * 0 -- 1 -- 2
 * |    |    |
 * 3    4 -- 5
 */

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

void bfs(
    const int root,
    const vector<vector<int>> &graph,
    vector<bool> &explored)
{
    queue <int> to_explore;
    int node;

    to_explore.push(root);
    explored[root] = true;

    while (!to_explore.empty()) {
        node = to_explore.front();
        to_explore.pop();

        cout << node << '\n';

        for (int neighbour : graph[node])
            if (!explored[neighbour]) {
                explored[neighbour] = true;
                to_explore.push(neighbour);
            }
    }
}

void dfs(
    const int node,
    const vector<vector<int>> &graph,
    vector<bool> &explored)
{
    // Mark this node as explored
    explored[node] = true;

    cout << node << '\n';

    // For each neighbour of this graph
    for (int neighbour : graph[node])
        if (!explored[neighbour])
            dfs(neighbour, graph, explored);
}

int main()
{
    const int N = 6;

    // Adjacency list graph
    vector<vector<int>> graph =
    {
        {1, 3},
        {0, 2, 4},
        {1, 5},
        {0},
        {1, 5},
        {4, 2}
    };

    vector<bool> explored(N, false);

    cout << "DFS:\n";

    for (int i = 0; i < N; i++)
        if (!explored[i])
            dfs(i, graph, explored);

    cout << "BFS:\n";

    explored.assign(N, false);

    for (int i = 0; i < N; i++)
        if (!explored[i])
            bfs(i, graph, explored);
}
