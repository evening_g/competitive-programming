/**
 * @file quick_select.cpp
 * @author your name (you@domain.com)
 * @brief Quick select is a variation of Quick sort used when you need the n-th
 * smallest element
 * It only sorts the part of the array where the n-th element we are looking for is located
 * 
 * @version 0.1
 * @date 2022-07-10 
 * 
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int get_pivot(int l, int r) {
    return l + rand() % (r - l);
}

int partition(vector<int> &list, int l, int r) {
    int pivot_index = l;

    // choose a random pivot and swap it with the last element
    swap(list[get_pivot(l, r)], list[r]);

    for (int i = l; i <= r; i++) {
        if (list[i] <= list[r]) {
            swap(list[i], list[pivot_index]);
            pivot_index ++;
        }
    }

    return pivot_index - 1;
}

/**
 * @brief Finds the n-th element in the list
 * 
 * @param list 
 * @param l 
 * @param r 
 * @param n 
 * @return int 
 */
int quick_select(vector<int> &list, int l, int r, int n) {
    // base case
    if (l >= r)
        // should not enter here
        return list[l];

    // recursive cases
    int pivot_index = partition(list, l, r);

    // if the pivot is the n-th element
    if (pivot_index == n)
        return list[pivot_index];
    
    // if the pivot is at the left of the n-th element
    // then to look on the right
    else if (pivot_index < n)
        return quick_select(list, pivot_index + 1, r, n);

    // if the pivot is at the right of the n-th element
    // then look to the left
    else 
        return quick_select(list, l, pivot_index - 1, n);
}

int main() {
    int n, k;
    
    cin >> n;

    vector<int> list(n);

    for (int &i : list)
        cin >> i;

    cin >> k;
    
    cout << quick_select(list, 0, n-1, k) << '\n';
}