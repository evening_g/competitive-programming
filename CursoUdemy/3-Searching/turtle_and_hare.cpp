/**
 * @file turtle_and_hare.cpp
 * @brief Robert W. Floyd's Turtle and Hare cycle detection algorithm
 * 
 * The Turtle and Hare algorithm employees two pointers, 
 * one that moves fast (the hare) and one that moves slow (the tortoise)
 * The purpouse is that if any cycle exists, the hare will eventually reach
 * the tortoise by the back
 * 
 * @version 0.1
 * @date 2022-07-21
 *  
 */

#include <iostream>
#include <vector>

/**
 * @brief Linked list implementation
 * 
 * A linked list is a set of nodes,
 * each one attached to another.
 * 
 * A node that points to NULL is called tail
 * A node that there exist no other node pointing to this
 * is called head
 * 
 */

using lltype = unsigned int;

struct node
{
  lltype data;
  struct node *next;
};

class linked_list
{
  private:
    node *head, *tail;

  public:
    // Constructor
    linked_list()
      {
        head = NULL;
        tail = NULL;
      }

    // Methods

    /**
     * @brief Adds node `n`
     * 
     * @param n 
     */
    void
    add_node(lltype n)
      {
        // Save `n` in a new node
        node *tmp = new node;

        tmp -> data = n;
        tmp -> next = NULL;

        // if the linked list is empty
        if (head == NULL)
          {
            // the head and the tail are the same node
            // `tmp`
            head = tmp;
            tail = tmp;
          }
        else
          {
            // puts the node at the end of the list
            // the nre node goes after the tail
            tail -> next = tmp;
            // now the tail is the new node
            tail = tail -> next;
          }
      }

    /**
     * @brief Retrieves all the nodes in the linked list
     * and returns a vector
     * 
     * @return std::vector<lltype> 
     */
    std::vector<lltype>
    retrieve_list()
      {
        std::vector<lltype> list;

        // if the list has a cycle
        // return the vector empty
        // else it'll get into an infinite loop
        if (has_cycle())
          return list;

        node *n = head;

        do
          {
            list.push_back(n->data);
            n = n -> next;
          }
        while (n != NULL);

        return list;
      }

    /**
     * @brief Returns wether if the linked list has a cycle or not
     * 
     * Tortoise and Hare algorithm
     * 
     * @return true 
     * @return false 
     */
    bool
    has_cycle()
      {
        node *tortoise = head, *hare = head -> next;

        while (hare != NULL)
          {
            if (tortoise == hare)
              return true;

            tortoise = tortoise -> next;
            hare = hare -> next;

            if (hare != NULL)
              hare = hare -> next;
          }
        
        return false;
      }
};

int main()
{
  linked_list ll;

  lltype n;

  while (true)
    {
      std::cin >> n;

      if (n == 0)
        break;
      else
        ll.add_node(n);
    }

  std::vector<lltype> list = ll.retrieve_list();

  for (auto &i : list)
    std::cout << i << " ";

  return 0;
}
