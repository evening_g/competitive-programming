/**
 * @file ksmall.cpp
 * @brief https://www.spoj.com/problems/KSMALL/
 * @version 0.1
 * @date 2022-07-10
 * 
 */

#include <iostream>
#include <algorithm>

using namespace std;

using u32 = unsigned int;
using i32 = int;

u32 list[5000000];

void randomize(unsigned a,unsigned b,unsigned mod)
{
	for( u32 i=0 ; i<5000000 ; i++ )
	{
		a = 31014 * (a & 65535) + (a >> 16);
		b = 17508 * (b & 65535) + (b >> 16);
		list[i] = ((a << 16) + b) % mod;
	}
}

u32 get_pivot(u32 l, u32 r) {
    return l + rand() % (r - l);
}

u32 partition(u32 l, u32 r) {
    u32 p = l;

    swap(list[p], list[r]);

    for (u32 i = 0; i <= r; i++) {
        if (list[i] <= list[r]) {
            swap(list[p], list[i]);
            p++;
        }
    }

    return p --;
}

u32 quick_select(u32 l, u32 r, u32 k) {
    if (l >= r)
        return list[l];

    u32 p = partition(l, r);

    if (p == k)
        return list[k];

    else if (p < k)
        return quick_select(p + 1, l, k);

    else 
        return quick_select(l, p - 1, k);
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    u32 a, b, mod, k;

    cin >> a >> b >> mod >> k;

    randomize(a, b, mod);

    cout << quick_select(0, 5000000-1, k) << '\n';

    return 0;
}

