// !nullptr solution AC

#include <bits/stdc++.h>

using namespace std;

#define ll long long
using vll = vector<long long>;
const ll INF = 1e5 + 2;


void solve() {
    ll n, c;

    cin >> n >> c;

    vll arr(n);
    vector<ll> wait (n);
    ll acum = 0;

    for (int i=0; i<n; i++) {
        cin >> arr[i];
        acum += arr[i];
       
        wait[i] = acum / c;

        if (wait[i] >= n-i) {
            wait[i] = INF;
        }

        acum -= c;
        if (acum < 0) acum = 0;
    }

    ll m = 0;
    ll val = wait[0];
    for (int i=1; i<n; i++) {
        if (wait[i] < val) {
            m = i;
            val = wait[i];
        }
    }

    if (val == INF) {
        cout << "impossible";
    } else {
        cout << m;
    }
    cout << "\n"; 
}

int main() {
    solve();
}
