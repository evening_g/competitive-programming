// Still WA

#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n, c;
    cin >> n >> c;

    vll a(n);
    readv(a);

    ll best = INF;
    ll index = -1;

    ll queue_length = 0;

    for (int i = 0; i < n; i++) {
        queue_length+=a[i];
        ll time = queue_length / c;

        if (time < best and i+time < (ll) n) {
            best = time;
            index = i;
        }
        
        queue_length = max(0ll, queue_length-c);
    }

    if (index >= 0) {
        cout << index << '\n';
    } else {
        cout << "impossible\n";
    }

    return 0;
}
