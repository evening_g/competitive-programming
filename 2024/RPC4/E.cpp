#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using vpll = vector<pair<ll, ll>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

void solve() {
    
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n, k;
    cin >> n >> k;

    vll v(n+1);
    vll sp1(n+1);
    vll sp2(n+1);
    vpll r(n-k+2);
    sp1[0] = sp2[0] = v[0] = 0;

    for (int i = 1; i <= n; i++)
        cin >> v[i];

    for (int i = 1; i <= n; i++)
    {
        sp1[i] = v[i] + sp1[i-1];
        sp2[i] = i*v[i] + sp2[i-1];
    }

    for (int i = 1; i <= n-k+1; i++)
    {
        int j = i+k-1;
        r[i].second = i;
        r[i].first = (sp2[j]-sp2[i-1]) - (i-1)*(sp1[j]-sp1[i-1]);
    }
    
    stable_sort(all(r));

    for (int i = 1; i < n-k+2; i++) {
        cout << r[i].second << ' ' << r[i].first << '\n';
    }

    return 0;
}
