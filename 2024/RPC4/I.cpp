#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

struct abc {
    ll a;
    ll b;
    ll c;
};

vector<vector<vector<abc>>> m;

abc solve(abc x) {
    if (m[x.a][x.b][x.c].a != -1)
        return m[x.a][x.b][x.c];

    int not_valid = 0;
    if (x.a == 0) not_valid ++;
    if (x.b == 0) not_valid ++;
    if (x.c == 0) not_valid ++;

    long mx = max(max(x.a, x.b), x.c);
    if (mx > 2*(x.a+x.b+x.c-mx))
        not_valid += 2;

    if (not_valid > 1 or x.a < 0 or x.b < 0 or x.c < 0) {
        m[x.a][x.b][x.c] = {0, 0, 0};
        return m[x.a][x.b][x.c];
    }

    abc a, b, c;
    if (x.a > 0)
        a = solve({x.a-1, x.b, x.c});
    else
        a = {0,0,0};
    if (x.b > 0)
        b = solve({x.a, x.b-1, x.c});
    else
        b = {0,0,0};
    if (x.c > 0)
        c = solve({x.a, x.b, x.c-1});
    else
        c = {0,0,0};

    abc value;
    value.a = (a.b + a.c) % MOD;
    value.b = (b.a + b.c) % MOD;
    value.c = (c.b + c.a) % MOD;
    m[x.a][x.b][x.c] = value;

    return m[x.a][x.b][x.c];
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    abc x;
    cin >> x.a >> x.b >> x.c;

    m.resize(x.a+1);
    for (auto &b : m) {
        b.resize(x.b+1);
        for (auto &c : b) {
            c.resize(x.c+1);
            for (auto &y : c)
                y = {-1,-1,-1};
        }
    }

    m[0][0][1] = {0,0,1};
    m[0][1][0] = {0,1,0};
    m[1][0][0] = {1,0,0};

    abc r = solve(x);
    cout << ((r.a + r.b) % MOD + r.c) % MOD << '\n';

    return 0;
}
