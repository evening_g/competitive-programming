#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
    int x, y, z;
    cin >> x >> y >> z;

    for (int i = 0; i < x; i++)
    {
        string as, bs;
        cin >> as >> bs;

        // can turn on
        bool grid = true;
        for (char &a : as) {
            if (a == '-') {
                grid = false;
                break;
            }
        }
        
        // should turn on
        bool pattern = false;
        for (char &b : bs) {
            if (b == '*') {
                pattern = true;
                break;
            }
        }

        if (!grid && pattern) {
            cout << "N\n";
            return 0;
        }
    }
    
    cout << "Y\n";

    return 0;
}
