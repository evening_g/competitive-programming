#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

map<char, ll> values = {
    {'I', 1},
    {'V', 5},
    {'X', 10},
    {'L', 50},
    {'C', 100},
    {'D', 500},
    {'M', 1000}
};

int i;

ll f(string s, int j, char stop) {
    i --;
    if (j == 0 or values[s[j-1]] > values[s[j]]) {
        return values[s[j]];
    } else if (values[s[j-1]] == values[s[j]]) {
        return values[s[j]] + f(s, j-1, stop);
    } else {
        return values[s[j]] - f(s, j-1, stop);
    }
}

ll solve(string s) {
    ll total = 0;
    
    i = s.size();
    int j;

    loop {
        j = i - 1;

        if (j < 0) break;

        total += f(s, j, s[j]);
    }

    return total;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while(t--) {
        string s;
        cin >> s;

        cout << solve(s) << '\n';
    }

    return 0;
}

