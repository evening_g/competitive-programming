#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while (true)
#define readv(v)                                                               \
  for (auto &i : v)                                                            \
  cin >> i
#define printv(v)                                                              \
  for (auto &i : v)                                                            \
  cout << i << ' '

#define lsz(x) ~(x) & (x + 1)
#define lso(x) (x) & -(x)

int find_pos(stack<int> p, int index) {
  int i;
  loop {
    if (p.empty()) {
      i = -INF;
      break;
    }

    i = p.top();
    p.pop();

    if (i <= index)
      break;
  }
  return i;
}

int main() {
  cin.tie(0)->sync_with_stdio(0);
  cin.exceptions(cin.failbit);

  int n, k;
  cin >> n >> k;

  vi l(n * k);
  vi r(n * k);

  readv(l);
  readv(r);

  vector<stack<int>> pl(n + 1);
  vector<stack<int>> pr(n + 1);

  reverse(all(l));
  reverse(all(r));

  for (int i = 0; i < n * k; i++) {
    pl[l[i]].push(i);
    pr[r[i]].push(i);
  }

  int i, j;
  i = j = n * k - 1;
  int found = 0;
  while (i >= 0 and j >= 0) {
    if (l[i] == r[j]) {
      found++;
      i--;
      j--;
      continue;
    }

    int ir = find_pos(pr[l[i]], j);
    int dl = abs(i - ir);
    int il = find_pos(pl[r[j]], i);
    int dr = abs(j - il);

    if (dl < dr) {
      j = ir;
    } else {
      i = il;
    }
  }

  cout << found << '\n';

  return 0;
}
