# Descripción

Ladybug está de visita en la UASLP, lo que no sabe, es que Hawk Moth, también está de visita, buscando el Miraculous del Mapache, que según la leyenda, está oculto en el edificio I. Hawk Moth ha akumatizado a Ikari Gozen y está causando destrucción en la explanada. Ladybug se ha dado cuenta que Ikari Gozen sólo se puede mover como un caballo de ajedrez. Ladybug necesita llegar hasta donde el Miraculous antes que Ikari Gozen, para eso, necesita saber cuántos saltos necesita realizar Ikari Gozen para llegar.

Recuerda que en el ajedrez el caballo se mueve en forma de L. A continuación se muestra una imagen de ejemplo.

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fchessdelights.com%2Fwp-content%2Fuploads%2F2021%2F08%2FScreen-Shot-2021-08-07-at-10.29.42-AM.png&f=1&nofb=1&ipt=547e0ceb1569f50b2cf7a31abee9e2d74ea4bc9ee54a6edc44ff1be1d5fa1066&ipo=images)

En el ejemplo, la posición del caballo, en notación algebraica es e4. Las puntas de las flechas representan los lugares a donde el caballo se puede mover.

# Entrada

La casilla de Ikari Gozen y del Miraculous del Mapache, separadas por un espacio, en notación algebraica de ajedrez.

# Salida

Un único entero que representa el número de saltos que Ikari Gozen debe realizar para llegar hasta el Miraculous.

# Ejemplo

||input
d1 f2
||output
1
||description
Ikari Gozen sólo necesita realizar un salto.
||end

# Límites

* Puedes asumir que las posiciones de Ikari Gozen y del Miraculous siempre son válidas.

