# Ikari Gozen

Ladybug está de visita en la UASLP, lo que no sabe, es que Hawk Moth, también está de visita, buscando el Miraculous del Mapache, que según la leyenda, está oculto en el edificio I. Hawk Moth ha akumatizado a Ikari Gozen y está causando destrucción en la explanada. Ladybug se ha dado cuenta que Ikari Gozen sólo se puede mover como un caballo de ajedrez. Ladybug necesita llegar hasta donde el Miraculous antes que Ikari Gozen, para eso, necesita saber cuántos saltos necesita realizar Ikari Gozen para llegar.

Recuerda que en el ajedrez el caballo se mueve en forma de L. A continuación se muestra una imagen de ejemplo.

Poner imagen aquí.

En el ejemplo, la posición del caballo blanco, en notación algebraica es f3. Y la del caballo negro es b8. Los puntos representan los posibles movimientos del caballo, según su color.

**Entradas**

La casilla de Ikari Gozen y del Miraculous del Mapache, separadas por un espacio, en notación algebraica de ajedrez.

*Poner imagen del tablero de ajedrez con un ejemplo*

**Salidas**

Un único entero que representa el número de saltos que Ikari Gozen debe
realizar para llegar hasta el Miraculous.

# Ejemplo

Entrada

```
d1 f2
```

Salida

```
1
```

**
