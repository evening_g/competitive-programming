#include <iostream>
#include <queue>

using namespace std;

const int INF = 1000000000;
const vector<int> moves_i = { 1,  2, 2, 1, -1, -2, -2, -1};
const vector<int> moves_j = {-2, -1, 1, 2,  2,  1, -1, -2};

int a, b, c, d;
vector<vector<int>> board(8, vector<int>(8, INF));

bool is_inside(int i, int j) {
        return i >= 0 and i <= 7 and j >= 0 and j <= 7; 
}

void bfs() {
        int d = 0;
        queue<pair<int, int>> q;
        q.push({a, b});
        board[a][b] = 0;
        while (!q.empty()) {
                auto [i, j] = q.front(); q.pop();
                
                for (int k = 0; k < moves_j.size(); k++) {
                        int x = i+moves_i[k];
                        int y = j+moves_j[k];

                        if (!is_inside(x, y))
                                continue;

                        if (board[i][j] + 1 > board[x][y])
                                continue;

                        q.push({x, y});
                        board[x][y] = board[i][j] + 1;
                }
        }
}

int main() {
        string input;
        getline(cin, input);
        a = input[0] - 'a';
        b = input[1] - '1';
        c = input[3] - 'a';
        d = input[4] - '1';

        bfs();

        cout << board[c][d] << '\n';

        return 0;
}
