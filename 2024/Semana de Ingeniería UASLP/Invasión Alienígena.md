# Invasión alienígena

La UASLP está siendo invadida por aliens

Se ha descubierto que todas las naves están formadas por cubos del mismo
tamaño, cada cubo pesa 1 tonelada. Así mismo, se ha descubierto que las naves
están organizadas por categorias. Las naves de categoría 1 están formadas por
1 cubo. Las naves de categoría 2 están formadas por 7 cubos. Y la de categoría
3 por 25 cubos.

A continuación se muestra una imagen donde se puede observar la forma de las
naves.

*Poner imagen*

Tu trabajo es encontrar el peso de cada una de las $N$ naves que se están
aproximando.

# Entrada

Un entero $Q$, seguido por $Q$ lineas. Cada línea contiene un único entero
$N$, que representa la categoría de la nave $i$.

# Salida

$Q$ líneas con un único entero, representando el peso de la nave $i$.

# Límites

$1 <= Q <= 10e9$

$1 <= N <= 10e6$

