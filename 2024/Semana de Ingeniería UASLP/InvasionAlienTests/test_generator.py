from random import randint

for i in range(1, 10):
    x = randint(2, 201)
    y = randint(2, 201)
    z = randint(2, 201)
    t = randint(2, 201)

    with open(f"{i}.in", 'w+') as inp:
        inp.write(f"{x} {y} {z} {t}\n")

    with open(f"{i}.out", 'w+') as out:
        out.write(f"{(x-2)*(y-2)*(z-2)*(t-2)}\n")

