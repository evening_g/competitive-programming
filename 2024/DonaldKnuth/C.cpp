#include <bits/stdc++.h>
using namespace std;

#define rep(i, a, b) for(int i = a; i < (b); ++i)
#define all(x) begin(x), end(x)
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

int main() {
	cin.tie(0)->sync_with_stdio(0);
	cin.exceptions(cin.failbit);

    int T = 1;
    cin >> T;

    while(T--) {
        int n, m, k;
        cin >> n >> m >> k;

        if (m == 1 && n >= k) {
            cout << "N\n";
            continue;
        }

        if (m == 2 && k % 2 == 0) {
            cout << "N\n";
            continue;
        }

        int a = m;
        int b;

        for (b = m; b > 1; b--) {
            if ((a+b) % k != 0 and (a+b) % k != a and (a+b) % k != b) break;
        }

        if (b == 0) {
            cout << "N\n";
            continue;
        }

        cout << "S ";

        for (int i = 0; i < n-2; i+=2) {
            cout << a << ' ' << b << ' ';
        }
        if (n%2) {
            cout << a;
        }

        cout << '\n';
    }

    return 0;
}
