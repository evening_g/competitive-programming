#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int n;
        cin >> n;

        vector<pair<string, string>> patterns(n);

        readv(patterns).first >> i.second;
        
        int m;
        cin >> m;

        while (m--) {
                string word;
                cin >> word;

                bool yes = false;
                for (auto &i : patterns) {
                        if (i.first.size() != word.size())
                                continue;

                        bool matches = true;
                        for (int j = 0; j < i.first.size(); j++) {
                                if (i.first[j] == 'l' and (word[j] < 'a' or word[j] > 'z')) {
                                        matches = false;
                                        break;
                                }
                                if (i.first[j] == 'u' and (word[j] < 'A' or word[j] > 'Z')) {
                                        matches = false;
                                        break;
                                }
                                if (i.first[j] == 'd' and (word[j] < '0' or word[j] > '9')) {
                                        matches = false;
                                        break;
                                }
                        }

                        if (matches) {
                                yes = true;
                                cout << i.second << '\n';
                                break;
                        }
                }

                if (!yes) {
                        cout << "Liar!\n";
                }
        }

        return 0;
}
