#include <bits/stdc++.h>
using namespace std;
#define all(v) v.begin(), v.end()
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        set<char> u;
        string s;
        cin >> s;
        u.insert(all(s));
        cout << s.size() - u.size() << '\n';
        return 0;
}

