#include <bits/stdc++.h>
using namespace std;
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        string s;
        cin >> s;
        char prev = 'Z' + 1;
        int count = 0;
        for (int i = s.size() - 1; i >= 0; i--) {
                if (s[i] >= prev) break;
                count ++;
                prev = s[i];
        }
        cout << s.size() - count << '\n';
        return 0;
}

