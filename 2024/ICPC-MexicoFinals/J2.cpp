#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;
#define M_PI 3.14159265358979323846

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        double a, t;
        cin >> a >> t;

        vector<pair<double, double>> v(t);

        for (auto &i : v) {
                cin >> i.first >> i.second;
                i.second = i.first + (i.second / tan((a * M_PI / 180.0)));
        }

        sort(all(v));

        double total = 0;
        double end = v[0].second;
        double dist;
        for (int i = 1; i < t; i++) {
                dist = min(end - v[i-1].first, v[i].first - v[i-1].first);
                total += dist;
                end = max(v[i].second, end);
        }

        total += end - v[v.size() - 1].first;
        
        cout << fixed << setprecision(5) << total << '\n';

        return 0;
}

