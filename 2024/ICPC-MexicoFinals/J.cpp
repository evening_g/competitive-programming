#include <bits/stdc++.h>
#define M_PI 3.14159265358979323846
#define all(v) v.begin(), v.end()
#define db double
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int a, t;
        cin >> a >> t;

        vector<pair<db, db>> v(t);

        for (auto &i : v) {
                int h;
                cin >> i.first >> h;
                i.second = i.first + ((db)h / tan(((db) a * M_PI / 180.0)));
        }

        sort(all(v));

        db total = 0;
        db end = v[0].second;
        db dist;
        for (int i = 1; i < t; i++) {
                dist = min(end - v[i-1].first, v[i].first - v[i-1].first);
                total += dist;
                end = max(v[i].second, end);
        }

        total += end - v[v.size() - 1].first;
        
        cout << fixed << setprecision(5) << total << '\n';

        return 0;
}
