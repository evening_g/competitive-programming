from collections import Counter

def main():
    n, m = input().split()

    visited = set()
    visited.add(n)

    k = 1
    while n != m:
        if k > 100:
            print("I'm bored")
            return
        
        numbers = Counter(n)
        
        new_n = ""
        for i in range(10):
            if str(i) in numbers:
                new_n += str(numbers[str(i)]) + str(i)

        if new_n in visited:
            print("Does not appear")
            return

        n = new_n
        visited.add(n)
        k += 1

    if n == m:
        print(k)

if __name__ == "__main__":
    main()
