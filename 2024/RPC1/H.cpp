/**
 * status: WA
 *
 * the solution of this one is doing a dijkstra starting in a and other starting in b
 * then for each edge u, v, if a->u + u->v + v-> b == a->b then the edge is in the path
 * the solution is in Competitive Programming/Codeforces/ECNA2022/I.cpp
 *
 * keywords: graph dijkstra
 * */

#include <bits/stdc++.h>
using namespace std;

#define rep(i, a, b) for(int i = a; i < (b); ++i)
#define all(x) begin(x), end(x)
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

const int INF = 1000000000;

int n;

vector<vector<pair<int, int>>> adj;

vi bfs(int a) {
    queue<int> q;
    vector<int> dist(n, INF);
    dist[a] = 0;
    
    vector<char> visited(n, false);

    q.push(a);

    while(!q.empty()) {

        int v = q.front();
        q.pop();

        if(visited[v]) continue;

        for(auto w : adj[v]) {
            q.push(w.first);
            dist[w.first] = min(dist[w.first], dist[v] + w.second);
        }

        visited[v] = true;
    }

    return dist;
}

int get_shortest_paths(int a, int b, vi dist) {
    queue<int> q;
    q.push(b);

    int total = 0;
    set<pair<int, int>> visited;

    while (!q.empty()) {
        int v = q.front();
        q.pop();

        for (auto w : adj[v]) {
            pii edge = {min(v, w.first), max(v, w.first)};
            if (dist[w.first] == dist[v] - w.second and visited.count(edge) == 0) {
                total += w.second;
                q.push(w.first);
                visited.insert(edge);
            }
        }
    }

    return total;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int m, a, b;
    cin >> n >> m >> a >> b;

    int total = 0;

    adj.resize(n);
    for(int i = 0; i < m; i++){
        int u, v, w;
        cin >> u >> v >> w;
        adj[u - 1].push_back({v - 1, w});
        adj[v - 1].push_back({u - 1, w});
        total += w;
    }

    a--;
    b--;

    vi dist = bfs(a);
    int to_pave = get_shortest_paths(a, b, dist);
    int result = total - to_pave;

    cout << result << '\n';

    return 0;
}
