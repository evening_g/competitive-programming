#include <bits/stdc++.h>
using namespace std;

int main() {
    double x, y;
    cin >> x >> y;

    double r = 100.0 - x;
    double p = 100.0 - y;

    double t = y*r/p;

    double result = x / t;

    cout << fixed << setprecision(7) << result << '\n';

    return 0;
}

