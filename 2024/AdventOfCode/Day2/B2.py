safe = 0
while True:
    line = input()
    if line == "":
        break
    v = list(map(int, line.split()))

    for i in range(len(v)):
        line = v.copy()
        del line[i]

        inc = sorted(line)
        dec = sorted(line, reverse=True)

        if line != inc and line != dec:
            continue

        valid = True
        for i in range(len(line)-1):
            valid &= 1 <= abs(inc[i] - inc[i+1]) <= 3

        if valid:
            safe += 1
            break

print(safe)

