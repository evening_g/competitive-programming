safe = 0

def check(v, dec) -> bool:
    v = [0] + v + [int(10e9)]
    if dec:
        v = list(reversed(v))

    d = True
    for i in range(1, len(line)-1):
        if v[i] > v[i+1] or not 1 <= abs(v[i] - v[i+1]) <= 3:
            if v[i] < v[i+2] and 1 <= abs(v[i] - v[i+2]) <= 3 and d:
                d = False
            else:
                return False

    return True


while True:
    line = input()
    if line == "":
        break
    line = list(map(int, line.split()))

    valid = check(line, True) or check(line, False)

    safe += int(valid)

print(safe)
