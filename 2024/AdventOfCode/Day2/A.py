safe = 0
while True:
    line = input()
    if line == "":
        break
    line = list(map(int, line.split()))
    inc = sorted(line)
    dec = sorted(line, reverse=True)

    if line != inc and line != dec:
        continue

    valid = True
    for i in range(len(line)-1):
        valid &= 1 <= abs(inc[i] - inc[i+1]) <= 3

    safe += int(valid)

print(safe)
