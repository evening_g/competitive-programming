#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

bool possible(const vi &v, vi &s, int target) {
        s.resize(target+1);
        fill(all(s), -1);
        s[0] = 0;
        vi u(v.size());

        for (const int &c : v)
                for (int i = target; i > 0; i--)
                        if (i - c >= 0 and s[i-c] != -1)
                                s[i] = c;

        return s[target] != -1;
}

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        
        int n;
        cin >> n;
        vi v(n);
        vi s;
        readv(v);

        sort(all(v), greater<int>());
        int sum = accumulate(all(v), 0);
        int target = sum / 2;
        
        if (sum%2)
                goto no;


        if (!possible(v, s, target))
                goto no;
no:
        cout << "-1";

yes:
        return 0;
}
