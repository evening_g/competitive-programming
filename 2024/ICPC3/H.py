"""
Status: WA
Wa on test 3. I don't know why.

Examples:

101011110000001110000000

11001*10**1*00**
1*1***01

1*111*111*
*11*111*11*1

111111101111111111111111
1000000001011111111111111111111
1****************11111111111111
111111111011111100111110111110110100010000000000000001

1110001110001110001110001110001110001100011100011100011100011100011100011100100
1010101010101010101010101010101010101010

1*10*011*000*110*0111000*1100011100*110001*100011100011100011100011100011100100
10101**0*01*10101*10101*101010*01010101*

"""

def main():
    n = list(input())
    m = list(input())

    ln = get_asterisks(n)
    lm = get_asterisks(m)

    n = int("".join(n), base=2)
    m = int("".join(m), base=2)

    N = 0
    M = 0
    for i in range(2<<len(ln)+1):
        for j in range(2<<len(lm)+1):
            if n % m == 0:
                print(f"n={n:b}")
                print(f"m={m:b}")
                return

            M += 1
            m = mutate(m, lm, M)
        N += 1
        n = mutate(n, ln, N)

    print("No solution found")
    

def get_asterisks(n) -> list:
    l = list()
    ln = len(n)
    for i in range(ln):
        if n[i] == '*':
            n[i] = '0'
            l.append(ln - 1 - i)

    return l[::-1]


def mutate(n, ln, N):
    for i in range(len(ln)):
        if N & (1<<i):
            n |= (1<<ln[i])
        else:
            n &= ~(1<<ln[i])

    return n


if __name__ == "__main__":
    main()

