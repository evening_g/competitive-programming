#include <iostream>
#include <vector>

using namespace std;

int uniquePaths(int n, int m) {
    vector<int> dp(n, 1);

    for (size_t row = 1; row < m; row++)
    {
        for (size_t col = 1; col < n; col++)
        {
            dp[col] = dp[col-1]+dp[col];
        }
    }
    
    return dp[n-1];
}

int main() {
    int n, m;

    cin >> n >> m;

    cout << uniquePaths(n, m) << '\n';

    return 0;
}