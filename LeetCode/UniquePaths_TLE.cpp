#include <iostream>

using namespace std;

int uniquePaths(int n, int m) {
    if (n == 1 || m == 1) {
        return 1;
    }

    return uniquePaths(n-1, m) + uniquePaths(n, m-1);
}

int main() {
    int n, m;

    cin >> m;

    cout << uniquePaths(n, m) << '\n';

    return 0;
}