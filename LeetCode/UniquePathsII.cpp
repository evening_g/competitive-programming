#include <iostream>
#include <vector>

using namespace std;

int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
    int n = obstacleGrid.size();
    int m = obstacleGrid.front().size();

    vector<int> dp(n+1, 0);
    dp[1] = 1;

    for (size_t row = 1; row <= m; row++)
    {
        for (size_t col = 1; col <= n; col++)
        {
            if (obstacleGrid[col-1][row-1] == 1) {
                dp[col] = 0;
            } else {
                dp[col] += dp[col-1];
            }
        }
    }
    
    return dp[n];
}

int main() {
    vector<vector<int>> obstacleGrid = {
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {1, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 1},
        {0, 0, 0, 0},
        {0, 0, 0, 0}
    };

    cout << uniquePathsWithObstacles(obstacleGrid) << '\n';

    return 0;
}