use std::cmp;

fn min_sub_array_len(target: i32, nums: Vec<i32>) -> i32 {
    let mut min_length = (1e9) as i32;

    let mut i = 0_usize;
    let mut j = 0_usize;
    let mut current_sum = 0;

    while i < nums.len() && i <= j {
        while j < nums.len() && current_sum < target {
            current_sum += nums[j];
            j += 1;
        }

        while i <= j && current_sum >= target {
            current_sum -= nums[i];
            i += 1;
        }

        min_length = cmp::min(min_length, (j-i+1) as i32);
    }
           
    min_length
}

fn main() {
    let nums = vec![1, 2, 3, 4, 5];
    let target = 15;

    println!("{}", min_sub_array_len(target, nums));
}