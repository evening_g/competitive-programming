#include <map>
#include <vector>
#include <iostream>

const int INF = (int) 10e9;

using namespace std;

int minSubArrayLen(int target, vector<int>& nums) {
    int min_length = INF;
    int i = 0, j = 0, current_sum = 0;

    while(j < nums.size() and i <= j)
    {

        while (j < nums.size() and current_sum < target)
        {
            current_sum += nums[j];
            j ++;
        }

        while (i <= j and current_sum >= target)
        {
            // move i to the right until this is false
            current_sum -= nums[i];
            i ++;
        }

        min_length = min(min_length, j-i+1);
    }

    if (min_length <= nums.size())
    {
        return min_length;
    }
    else
    {
        return 0;
    }
}

int main()
{
    vector<int> nums = {1, 2, 3, 4, 5};
    int target = 15;

    cout << minSubArrayLen(target, nums) << '\n';

    return 0;
}
