#include <iostream>
#include <vector>

using namespace std;

int uniquePaths(int n, int m) {
    vector<int> dp(n, 1);

    for (size_t row = 1; row < m; row++)
    {
        for (size_t col = 1; col < n; col++)
        {
            dp[col] = dp[col-1]+dp[col];
        }
    }
    
    return dp[n-1];
}

int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
    int n = obstacleGrid.size();
    int m = obstacleGrid.front().size();

    // Find obstacle
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            if (obstacleGrid[i][j] == 1) {
                return uniquePaths(n, m) - uniquePaths(i+1, j+1) * uniquePaths(n-i, m-j);
            }
        }
    }
    
    // obstacle not found
    return -1;
}

int main() {
    vector<vector<int>> obstacleGrid = {{0, 1}, {0, 0}};

    cout << uniquePathsWithObstacles(obstacleGrid) << '\n';

    return 0;
}