#include <bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int d, w;
    cin >> d >> w;

    map<int, int> dict;

    while (d--) {
        int n, e;
        cin >> n >> e;

        dict[n] = e;
    }

    while (w--) {
        int w;
        cin >> w;

        if (dict.count(w)) {
            cout << dict[w] << '\n';
        } else {
            cout << "C?\n";
        }
    }

    return 0;
}