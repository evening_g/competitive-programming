#include <bits/stdc++.h>

using namespace std;

struct Rectangle {
    int left, right, top, bottom;

    int area() {
        return (right-left)*(top-bottom);
    }
};

Rectangle intersection(Rectangle a, Rectangle b) {
    Rectangle c = {-1, -1, -1, -1};

    if (a.top >= b.bottom and a.top <= b.top) {
        c.top = a.top;
    }

    if (a.bottom >= b.bottom and a.bottom <= b.top) {
        c.bottom = a.bottom;
    }
    
    if (a.left >= b.left and a.left <= b.right) {
        c.left = a.left;
    }

    if (a.right >= b.left and a.right <= b.right) {
        c.right = a.right;
    }

    return c;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int x1, y1, x2, y2;
    cin >> x1 >> y1 >> x2 >> y2;
    Rectangle a = {min(x1, x2), max(x1, x2), max(y1, y2), min(y1, y2)};

    cin >> x1 >> y1 >> x2 >> y2;
    Rectangle b;
    b.left = min(x1, x2);
    b.right = max(x1, x2);
    b.top = max(y1, y2);
    b.bottom = min(y1, y2);

    Rectangle ab = intersection(a, b);
    Rectangle ba = intersection(b, a);

    Rectangle intersec = {max(ab.left, ba.left), max(ab.right, ba.right),
                            max(ab.top, ba.top), max(ab.bottom, ba.bottom)};

    int total;
    if (intersec.bottom == -1 or intersec.top == -1 or intersec.left == -1 or intersec.right == -1) {
        // no hay interseccion
        total = a.area() + b.area();
    } else {
        // si hay interseccion
        total = a.area() + b.area() - intersec.area();
    }

    cout << total << '\n';

    return 0;
}
