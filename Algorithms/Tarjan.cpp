#include<iostream>
#include<vector>
#include<stack>
#include<set>
#include<algorithm>

using namespace std;

#define loop while(1)

int N = 7;
/**
 * 1 - 0 -- 2
 * |   |  /  \
 * 3 - 4 5 - 6
 * 
*/
vector<vector<int>> adj = {
    {1, 2, 4},
    {0, 3},
    {0, 5, 6},
    {1, 4},
    {0, 3},
    {2, 6},
    {2, 5},
};

stack<int> exploring_stack;
vector<char> exploring(N, false);
vector<char> visited(N, false);
vector<int> low_link(N, 0);
int countSCC = 0;

void dfs(int u) {
    low_link[u] = u;
    visited[u] = true;
    exploring_stack.push(u);
    exploring[u] = true;

    for (int v : adj[u]) {
        if (!visited[v]) {
            dfs(v);
        }
        
        if (exploring[v]) {
            low_link[u] = min(low_link[u], low_link[v]);
        }
    }

    if (low_link[u] == u) {
        cout << "Vertice cut: " << u << '\n';
        countSCC ++;
        while (exploring_stack.top() != u) {
            exploring[exploring_stack.top()] = false;
            exploring_stack.pop();
        }
    }
}

void tarjan() {
    for (int u = 0; u < N; u++) {
        if (!visited[u]) {
            dfs(u);
        }
    }
}

int main() {
    tarjan();

    return 0;
}