**Bit operations**

 `<<`  Carry to the left.  
 `>>`  Carry to the right. 
 `~`   Negation.           
 `-`   2 complement        
 `|`   or                  
 `&`   and                 
 `^`   xor                 

**Check if bit $i=0$**

```c
n ^ (1<<i)
```

**Check if bit $i=1$**

```c
n & (1<<i)
```

**Make bit $i=0$**

```c
n &= ~(1<<i)
```

**Make bit $i = 1$**

```c
n |= (i<<i)
```

**Toggle bit $i$**

```c
n ^= (1<<i)
```

**Less significative $1$**

```c
lso = n & -n
```

**Less significative $0$**

```c
lsz = ~n & (k+1)
```

**Make all bits in $[0..i] = 1$**

```c
n = (1<<i)-1
```

**Count number of $1$'s**

```cpp
__builtin_popcount(n)
```

**Count number of trailing $0$'s**

```cpp
__builtin_ctz(n)
```


