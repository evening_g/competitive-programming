# Longest incresing subsequence problem using a dynamic approach
# Source: https://youtu.be/aPQY__2H3tE

def print_lis(v: list, p: list, i: int):
    if i == 0:
        return
    
    print_lis(v, p, p[i])
    print(f"{v[i]} ", end="")


def lis(v: list) -> int:
    dp = [0] * len(v)   # lis at position i
                        # the result is max(dp)

    p = [0] * len(v)

    # For each element i at the list
    # the LIS will be dp[i] = 1 + max(dp[k] where k < i and v[k] < v[i])
    # dp[0] = 0
    # v[0] = 0

    result = 0
    lis_index = 0
    for i in range(1, len(v)):
        # idk if this is the best implementation?
        for k in range(i):
            if v[k] < v[i]:
                if dp[k] + 1 > dp[i]:
                    dp[i] = dp[k] + 1
                    p[i] = k

        if dp[i] > result:
            result = dp[i]
            lis_index = i

    print("LIS: ", end="")
    print_lis(v, p, lis_index)
    print()

    return result


def main():
    assert(lis([0, 5, 2, 8, 6, 3, 6, 9, 5]) == 4)
    assert(lis([0, 1, 2, 3, 4, 5, 6, 7]) == 7)
    assert(lis([0, 3, 2, 1]) == 1)

    print("Ok")


if __name__ == "__main__":
    main()
