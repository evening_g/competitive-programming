/**
 * Manacher's Algorithm
 * for finding all substrings that are palindromes, in a string in O(n)
 * 
 * keywords: palindrome dp
 * source: https://cp-algorithms.com/string/manacher.html
*/

#include <iostream>
#include <vector>

using namespace std;

string oddify(const string &s) {
    string oddified = "#";

    for (char c : s)
        oddified = oddified + c + "#";

    return oddified;
}

void count_palindromes(const string &s, vector<int> &p, int i) {
    for (; i-p[i] >= 0 and i+p[i] < (int) s.size() and s[i-p[i]] == s[i+p[i]]; p[i]++);
    p[i] --; // remove the last one that causes the loop to break 
}

void manacher_odd(vector<int> &p, const string &s) {
    int l = 0, r = 0;
    for (int i = 0; i < (int) s.size(); i++) {
        if (i <= r) {
            int mirror = l + r - i;

            if (mirror >= 0 and mirror-p[mirror] >= l) {
                p[i] = p[mirror];
            } else {
                p[i] = mirror - l;
            }
        }

        count_palindromes(s, p, i);
        
        l = i - p[i];
        r = i + p[i];
    }
}

void manacher(vector<int> &odd, vector<int> &even, string &s) {
    odd.resize(s.size());
    even.resize(2 * s.size() + 1);
    fill(odd.begin(), odd.end(), 0);
    fill(odd.begin(), odd.end(), 0);

    string oddified = oddify(s);

    manacher_odd(odd, s);
    manacher_odd(even, oddified);
}

int main() {
    string str;
    cin >> str;

    vector<int> odd;
    vector<int> even;

    manacher(odd, even, str);

    for (int i : odd) {
        cout << i << ' ';
    }
    cout << '\n';

    // this prints the following positions
    //  a a b b
    // ^ ^ ^ ^ ^
    for (size_t i = 0; i < even.size(); i += 2)
    {
        cout << even[i]/2 << ' ';
    }
    cout << '\n';

    return 0;
}