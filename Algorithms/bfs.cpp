/**
 * @file bfs.cpp
 * @author Bernardo
 * @brief BFS impletentation to Calculate the distances to the nodes in a map
 * @version 1.0
 * @date 2022-05-21
 * 
 * With the same cases than this problem
 * https://cses.fi/problemset/task/1192/
 * 
 * Create a program that prints the map, but with the distance from start to each node instead of '.'
 * 
 * 
 */

#include <vector>
#include <queue>
#include <iostream>

using namespace std;

const int MAX = 1e6;
const int MOD = 1e9 + 7;

bool is_valid_index(
    // graph map
    const vector<vector<char>> &floor_map,
    //position
    int i,
    int j
) {
    return (
        i >= 0                              // left bound
        and i < floor_map.size()            // right bound
        and j >= 0                          // upper bound
        and j < floor_map.front().size()    // lower bound
    );
}

struct coord {
    int i;
    int j;

    coord(){};
    coord(int _i, int _j) {
        i = _i;
        j = _j;
    }
};

int main() {
    // floor size
    int n, m;

    cin>>n>>m;

    // Initializes the vector with the floor size
    vector<vector<char>> floor_map(n, vector<char> (m));

    // Initializes the vector of distances and fills it with -1
    vector<vector<int>> dist(n, vector<int> (m, -1));

    coord start, end;

    // Reads the map and finds the start and end nodes
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin>>floor_map[i][j];

            if (floor_map[i][j] == 'A') {
                start.i = i;
                start.j = j;
            }

            if (floor_map[i][j] == 'B') {
                end.i = i;
                end.j = j;
            }
        }
    }

    // The distance from the start point to itself is 0
    dist[start.i][start.j] = 0;

    // BFS
    queue<coord> q;
    // sets the start point
    q.push(start);

    // while the queue is not empty -> there are still unexplored nodes
    while (q.size()) {
        coord current = q.front();
        q.pop();

        // create all posible directions
        int dx[] = {0, 1, 0, -1};
        int dy[] = {1, 0, -1, 0};

        // for each direction
        for (int i = 0; i < 4; ++i) {
            if  (
                // if this is a valid position
                is_valid_index(floor_map, current.i + dx[i], current.j + dy[i])
                // and it is not a wall
                and floor_map[current.i + dx[i]][current.j + dy[i]] != '#'
            ) {
                // if the distance to this node is unexplored
                if (dist[current.i + dx[i]][current.j + dy[i]] == -1) {
                    // initialize it with the current distance
                    dist[current.i + dx[i]][current.j + dy[i]] = dist[current.i][current.j] + 1;

                    // adds this node to the queue to explore it in the next iteration
                    //q.emplace(current.i + dx[i], current.j + dy[i]);
                    q.push(coord({current.i + dx[i], current.j + dy[i]}));
                }
            }
        }
    }

    for (auto row : dist) {
        for (auto col : row) {
            if (col == -1) {
                cout<<"####";
            } else {
                printf("%3.0d ", col);
                //cout<<col<<"\t";
            }
        }
        cout<<'\n';
    }

    return 0;
}

/*
21 21
#####################
#.........#...#.....A
#.#.#.###.#.###.#.###
#.#.#...#.#.#...#...#
#######.###.#.###.#.#
#.....#.....#.#...#.#
#####.#####.###.#.#.#
#.#...#.......#.#.#.#
#.#.#####.#####.#.###
#...#.........#.#...#
#.###.#.#####.###.#.#
#.....#.#.......#.#.#
###########.#####.#.#
#...#.#.....#.....#.#
###.#.###.#.###.#####
#.#.....#.#.#...#...#
#.#.###.#.#.#.#.#.###
#...#.#...#.#.#.....#
#.###.#.###.#######.#
B.#.................#
#####################
*/