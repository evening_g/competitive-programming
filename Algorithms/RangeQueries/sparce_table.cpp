/**
 * Sparce table.
 * Implementation of minimum queries for a static array.
 * 
 * Competitive Programmer's Handbook, Page 85
 * 
 * Sparse table for array [1, 3, 4, 8, 6, 1, 4, 2]
 * 
 * | Size | Index | 0    | 1    | 2    | 3    | 4    | 5    | 6    | 7    |
 * | ---- | ----- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
 * | 1    | 0     | 1    | 3    | 4    | 8    | 6    | 1    | 4    | 2    |
 * | 2    | 1     | 1    | 3    | 4    | 6    | 1    | 1    | 2    |      |
 * | 4    | 2     | 1    | 3    | 1    | 1    | 1    |      |      |      |
 * | 8    | 3     | 1    |      |      |      |      |      |      |      |
 */

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

vector<int> lg;

/**
 * Minimum Queries
 * ====================================================================================
 * Sparse Table Method
 * Precalculate all values of minq(a, b) where b - a + 1 is a power of 2.
 * 
 * Recursive formula:
 * minq(a, b) = min(minq(a, a+w-1), minq(a+b, b))
 * Where
 * b-a+1 is a power of 2
 * w = (b-a+1) / 2
 * 
 * Now any value minq(a, b) can be calculated:
 * minq(a, b) = min(minq(a, a+k), minq(b-k+1, b))
 * Where
 * k is the largest power of two that does not exceed b-a+1
 * Therefore, the range [a, b] is represented as the union of [a, a+k-1] and [b-k+1, b]
 * 
 * Watch "Sparce Table.png"
 */

void build_sparce_table(const vector<int> &v, vector<vector<int>> &st) {
    int N = v.size();
    int K = lg[N];

    st.resize(K+1, vector<int>(N));
    copy(v.begin(), v.end(), st[0].begin());

    for (int i = 1; i <= K; i++)
        for (int j = 0; j < N-(1<<i)+1; j++)
            st[i][j] = min(st[i-1][j], st[i-1][j+(1<<(i-1))]);
}

void print_sparce_table(const vector<vector<int>> &st) {
    cout << '\t';
    for (int i = 0; i < st.front().size(); i++)
        cout << i << '\t';
    cout << '\n';
    
    for (int i = 0; i < st.size(); i++)
    {
        cout << i << '\t';
        for (int j = 0; j < st.front().size()-(1<<i)+1; j++)
        {
            cout << st[i][j] << '\t';
        }
        cout << '\n';
    }
}

void find_lgs(int N) {
    lg.resize(N+1);
    lg[1] = 0; // lg 1 -> 0
    for (int i = 2; i <= N; i++)
        lg[i] = lg[i/2] + 1;
}

int minq(const vector<vector<int>> &st, int a, int b) {
    int i = lg[b-a+1];
    int result = min(st[i][a], st[i][b-(1<<i)+1]);
    return result;
}

int main()
{
    int n; cin >> n;
    vector<int> array(n);

    for (int &i : array) cin >> i;

    find_lgs(n);
    vector<vector<int>> sparce_table;
    build_sparce_table(array, sparce_table);
    print_sparce_table(sparce_table);

    int t; cin >> t;

    while (t--)
    {
        int a, b;
        cin >> a >> b;

        cout << minq(sparce_table, a, b);
    }    

    return 0;
}