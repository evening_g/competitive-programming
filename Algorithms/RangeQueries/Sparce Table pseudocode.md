| Size | Index | 0    | 1    | 2    | 3    | 4    | 5    | 6    | 7    |
| ---- | ----- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| 1    | 0     | 1    | 3    | 4    | 8    | 6    | 1    | 4    | 2    |
| 2    | 1     | 1    | 3    | 4    | 6    | 1    | 1    | 2    |      |
| 4    | 2     | 1    | 3    | 1    | 1    | 1    |      |      |      |
| 8    | 3     | 1    |      |      |      |      |      |      |      |

```pseudocode
build_sparce_table(v) -> matrix {
	N = v.size()
	K = lg(N)
	
	sparce_table = matrix[K+1][N]
	sparce_table[0] = v
	
	for i in 1..=K {
		for j in 0..N-2^i+1 {
			sparce_table[i][j] = min(sparce_table[i-1][j], sparce_table[i-1][j+2^(i-1)])
		}
	}
	
	return sparce_table
}
```

| Power | Decimal | Binary |
| ----- | ------- | ------ |
| 2^0   | 1       | 0001   |
| 2^1   | 2       | 0010   |
| 2^2   | 4       | 0100   |
| 2^3   | 8       | 1000   |

