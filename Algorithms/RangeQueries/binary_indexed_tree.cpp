/**
 * Binary Indexed Tree or Fenwick tree
 *
 * It allows to efficiently update array values and make sum queries
 *
 * Binary indexed tree is often represented as an array.
 * Asume that the arrays are 1-indexed
 *
 * Let p(k) be the largest power of 2 that divides k.
 *
 * tree[k] = sumq(k - p(k) + 1, k)
 *
 * Each position k contains the sum of values in a range of the
 * original array whose lenght is p(k) and that ends at position k
 *
 * p(k) can be easily calculated with p(k) = k & -k
 * 
 * For some reason this throws "illegal expression" when
 * using `size_t` instead of `int`.
 */

#include <cstddef>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Builds the Fenwick Tree `t` from the array `v`
 * */
void build_fenwick_tree(const vector<int> &v, vector<int> &t)
{
    copy(v.begin(), v.end(), t.begin());

    for (int k = 1; k < v.size(); k++)
    {
        int pk = k & -k;

        for (int i = 1; i < pk; i <<= 1)
        {
            t[k] += t[k - i];
        }
    }
}

/**
 * Returns the sum of the range [0, k] from the Fenwick Tree `t`
 * */
int sumq(size_t k, const vector<int> &t)
{
    int s = 0;

    while (k)
    {
        s += t[k];
        k -= k & -k;
    }

    return s;
}

/**
 * Adds `x` in the position `k` to the Fenwick Tree `t`
 * */
void add(size_t k, int x, vector<int> &t)
{
    while (k < t.size()) {
        t[k] += x;
        k += k&-k;
    }
}

/**
 * Returns the sum of the range [a, b] from the Fenwick tree `t`
 * */
int sum_range(size_t a, size_t b, const vector<int> &t) {
    int s = sumq(b, t);

    if (a-1) {
        s -= sumq(a-1, t);
    }

    return s;
}

int main()
{
    int n;
    cin >> n;

    vector<int> v(n);
    vector<int> t(n);

    for (int &i : v)
        cin >> i;

    build_fenwick_tree(v, t);

    for (int i = 1; i < n; i++)
    {
        printf("Sum 0..%d = %d\n", i, sumq(i, t));
    }

    add(5, 1, t);
    
    for (int i = 1; i < n; i++)
    {
        printf("Sum 0..%d = %d\n", i, sumq(i, t));
    }

    int a, b;

    cin >> a >> b;

    cout << sum_range(a, b, t) << '\n';

    return 0;
}
