# Range Queries

The following programs are based on the *Competitive Programmer's Handbook, Page 83*.

Typical queries are:

- Sum of range[a, b]
- Min of range[a, b]
- Max of range[a, b]

