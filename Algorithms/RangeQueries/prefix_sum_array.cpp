/**
 * Prefix sum array.
 * Implementation of sum queries for a static array.
 * 
 * Competitive Programmer's Handbook, Page 85
 */

#include <iostream>
#include <vector>

using namespace std;

/**
 * Sum Queries
 * ===========================================================================
 * Let v be an array 1-indexed of size n
 * The v prefix array is an array 0-indexed of size n+1
 * The position i of the array represents the sum of the first i elements of v
 * Therefore, to find the sum of [a, b] we calculate prefix[b] - prefix[a]
 */
vector<int> build_prefix_sum_array(const vector<int> &v) {
    vector<int> prefix;

    prefix.push_back(0);

    for (const int &i : v)
        prefix.push_back(prefix.back() + i);

    return prefix;
}

int sum(const vector<int> &prefix, int a, int b) {
    return prefix[b] - prefix[a-1];
}

int main()
{
    int n; cin >> n;
    vector<int> array(n);

    for (int &i : array) cin >> i;

    vector<int> prefix = build_prefix_sum_array(array);

    for (int &i : prefix) cout << i << ' ';
    cout << '\n';

    int t; cin >> t;

    while (t--)
    {
        int a, b;
        cin >> a >> b;

        cout << sum(prefix, a, b) << '\n';
    }
    

    return 0;
}