# Binary indexed tree

![Binary Indexed Tree illustration](Binary Indexed Tree.png)

| k    | p(k) | Array |
| ---- | ---- | ----- |
| 1    | 1    | 1..1  |
| 2    | 2    | 1..2  |
| 3    | 1    | 3..3  |
| 4    | 4    | 1..4  |
| 5    | 1    | 5..5  |
| 6    | 2    | 4..6  |
| 7    | 1    | 7..7  |
| 8    | 1    | 1..8  |

## Pseudocode

```cpp
int sum(int k) {
    int s = 0;
    while (k >= 1) {
        s += tree[k];
        k -= k&-k;
    }
    return s;
}
```

```cpp
void add(int k, int x) {
    while (k <= n) {
        tree[k] += x;
        k += k&-k;
    }
}
```

```pseudocode
```



## p(k)

```pseudocode
p(k) = k & -k
```

`-k` is `k` 2 complement

| k (dec) | k (bin) | -k (bin) | p(k) |
| ------- | ------- | -------- | ---- |
| 10      | 1010    | 0110     | 0010 |
| 4       | 100     | 100      | 100  |
| 12      | 1100    | 0100     | 100  |
| 5       | 101     | 011      | 001  |

## Representation

| Index     | 1    | 2    | 3    | 4    | 5    | 6    | 7    | 8    |
| --------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| Array     | 8    | 1    | 3    | 5    | 7    | 9    | 5    | 2    |
| Tree      | 8    | 9    | 3    | 17   | 7    | 16   | 5    | 40   |
| Sum Range | 1..1 | 1..2 | 3..3 | 1..4 | 5..5 | 5..6 | 7..7 | 1..8 |

## Example

**Input**

```
9
0 1 2 3 4 5 6 7 8
```
