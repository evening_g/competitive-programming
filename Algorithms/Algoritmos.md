# Algoritmos

## Count sort

1. ````c
   /**
    * @file count_sort.cpp
    * @brief Count sort is a sorting algorithm useful when many elements are repeated
    * and there are only a few types.
    * For example, if you wanna sort the sizes of shoes of 100 people, then Count sort
    * is a good option as there are only a few shoe sizes
    * 
    * It has a time complexity of O(n)
    * 
    * @version 0.1
    * @date 2022-06-20
    * 
    */
   
   #include <iostream>
   #include <map>
   #include <vector>
   
   using namespace std;
   
   /**
    * @brief Count sort implementation for a string
    * 
    * @param str 
    */
   void count_sort(string &str) {
       // If we are saving custom structs
       // we have to create our custom operators
       // or introduce a comparison function as third argument
       map<char, int> occurencies;
   
       // count occurrencies
       for (char &c : str)
           occurencies[c] ++;
       
       // restore the string
       str = "";
   
       // the map saves the keys in alphanumeric order
       // rewrite the string
       // add each type, n times
       for (auto &n : occurencies)
           for (size_t i = 0; i < n.second; i++)
               str += n.first;
   }
   
   int main() {
       freopen("tests/input.txt", "r", stdin);
       freopen("tests/output.txt", "w", stdout);
   
       string dna;
       cin >> dna;
   
       count_sort(dna);
   
       cout << dna << "\n";
   
       return 0;
   }
   ````

## Bucket sort

````c
/**
 * @file bucket_sort.cpp
 * @brief Bucket sort is a special algorithm as Count sort,
 * it is useful when we have an array that is distributed uniformely
 * In this, we imagine that we have certain amount of buckets, and for each element
 * we put it in the corresponding bucket
 * @version 0.1
 * @date 2022-06-20
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <iostream>
#include <vector>
#include <map>

using namespace std;

struct player {
    float height;
    bool is_football;

    player(float _height, bool _is_football) {
        height = _height;
        is_football = _is_football;
    }
};

void bucket_sort(vector<player> &players) {
    map< int, vector<player> > football_players;
    map< int, vector<player> > non_football_players;

    // Sort players according to their height and sport
    for (player &p : players) {
        if (p.is_football) {
            football_players[(int) (p.height * 5)].push_back(p);    // stores in intervals of 0.2
        } else {
            non_football_players[(int) (p.height * 5)].push_back(p);
        }
    }

    // clear the players
    players.clear();

    // refill the players in order, first football and then other players
    for (auto &fp : football_players)
        for (player &p : fp.second)
            players.push_back(p);
    
    for (auto &fp : non_football_players)
        for (player &p : fp.second)
            players.push_back(p);
}

int main() {
    freopen("Resources/footballData.txt", "r", stdin);
    freopen("Resources/output.txt", "w", stdout);

    vector<player> players;

    float height;
    bool is_football;

    while (cin>>height>>is_football)
        players.push_back(player(height, is_football));

    bucket_sort(players);

    for (player &p : players)
        cout << p.height << " " << p.is_football << "\n";

    return 0;
}
````

## Quick select

````c
/**
 * @file quick_select.cpp
 * @author your name (you@domain.com)
 * @brief Quick select is a variation of Quick sort used when you need the first n
 * smallest element
 * It only sorts the part of the array where the n-th element we are looking for is located
 * 
 * @version 0.1
 * @date 2022-07-10 
 * 
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int get_pivot(int l, int r) {
    return l + rand() % (r - l);
}

int partition(vector<int> &list, int l, int r) {
    int pivot_index = l;

    // choose a random pivot and swap it with the last element
    swap(list[get_pivot(l, r)], list[r]);

    for (int i = l; i <= r; i++) {
        if (list[i] <= list[r]) {
            swap(list[i], list[pivot_index]);
            pivot_index ++;
        }
    }

    return pivot_index - 1;
}

/**
 * @brief Finds the n-th element in the list
 * 
 * @param list 
 * @param l 
 * @param r 
 * @param n 
 * @return int 
 */
int quick_select(vector<int> &list, int l, int r, int n) {
    // base case
    if (l >= r)
        // should not enter here
        return list[l];

    // recursive cases
    int pivot_index = partition(list, l, r);

    // if the pivot is the n-th element
    if (pivot_index == n)
        return list[pivot_index];
    
    // if the pivot is at the left of the n-th element
    // then to look on the right
    else if (pivot_index < n)
        return quick_select(list, pivot_index + 1, r, n);

    // if the pivot is at the right of the n-th element
    // then look to the left
    else 
        return quick_select(list, l, pivot_index - 1, n);
}

int main() {
    int n, k;
    
    cin >> n;

    vector<int> list(n);

    for (int &i : list)
        cin >> i;

    cin >> k;
    
    cout << quick_select(list, 0, n-1, k) << '\n';
}
````

## Turtle and hare

````c
/**
 * @file turtle_and_hare.cpp
 * @brief Robert W. Floyd's Turtle and Hare cycle detection algorithm
 * 
 * The Turtle and Hare algorithm employees two pointers, 
 * one that moves fast (the hare) and one that moves slow (the tortoise)
 * The purpouse is that if any cycle exists, the hare will eventually reach
 * the tortoise by the back
 * 
 * @version 0.1
 * @date 2022-07-21
 *  
 */

#include <iostream>
#include <vector>

/**
 * @brief Linked list implementation
 * 
 * A linked list is a set of nodes,
 * each one attached to another.
 * 
 * A node that points to NULL is called tail
 * A node that there exist no other node pointing to this
 * is called head
 * 
 */

using lltype = unsigned int;

struct node
{
  lltype data;
  struct node *next;
};

class linked_list
{
  private:
    node *head, *tail;

  public:
    // Constructor
    linked_list()
      {
        head = NULL;
        tail = NULL;
      }

    // Methods

    /**
     * @brief Adds node `n`
     * 
     * @param n 
     */
    void
    add_node(lltype n)
      {
        // Save `n` in a new node
        node *tmp = new node;

        tmp -> data = n;
        tmp -> next = NULL;

        // if the linked list is empty
        if (head == NULL)
          {
            // the head and the tail are the same node
            // `tmp`
            head = tmp;
            tail = tmp;
          }
        else
          {
            // puts the node at the end of the list
            // the nre node goes after the tail
            tail -> next = tmp;
            // now the tail is the new node
            tail = tail -> next;
          }
      }

    /**
     * @brief Retrieves all the nodes in the linked list
     * and returns a vector
     * 
     * @return std::vector<lltype> 
     */
    std::vector<lltype>
    retrieve_list()
      {
        std::vector<lltype> list;

        // if the list has a cycle
        // return the vector empty
        // else it'll get into an infinite loop
        if (has_cycle())
          return list;

        node *n = head;

        do
          {
            list.push_back(n->data);
            n = n -> next;
          }
        while (n != NULL);

        return list;
      }

    /**
     * @brief Returns wether if the linked list has a cycle or not
     * 
     * Tortoise and Hare algorithm
     * 
     * @return true 
     * @return false 
     */
    bool
    has_cycle()
      {
        node *tortoise = head, *hare = head -> next;

        while (hare != NULL)
          {
            if (tortoise == hare)
              return true;

            tortoise = tortoise -> next;
            hare = hare -> next;

            if (hare != NULL)
              hare = hare -> next;
          }
        
        return false;
      }
};
````

## Multiroot BFS

````c
/**
 * @brief BFS implementation to find the nearest exit of the labyrinth
 * 
 * Returns the first exit it finds
 * If it finds no exit, then returns -1
 * 
 * @param start 
 * @param adj_list 
 * @param exits 
 * @param explored 
 * @return int 
 */
int nearest_exit
(
    const vector <int> &roots,
    const vector < vector <int> > &adj_list,
    const vector <int> &exits,
    vector <bool> &explored
)
{
    queue <int> to_explore;
    int node;

    for (int root : roots)
        to_explore.push(root);    

    while (!to_explore.empty()) {
        node = to_explore.front();
        to_explore.pop();
        explored[node] = true;

        if (is_exit(node, exits))
            return node;
        
        for (int neighbour : adj_list[node])
            if (!explored[neighbour])
                to_explore.push(neighbour);
    }

    // Returns -1 if there is no exit
    return -1;
}
````

## Binary search

````c++
/**
 * @brief Binary search algorithm
 * 
 * @param arr 
 * @param target 
 * @return int 
 */
int search(vector<int> arr, int target) {
    int lower = 0;
    int upper = arr.size() - 1;
    int index = -1;

    while (lower <= upper) {                    // While the limits are not together
        index = lower + (upper - lower) / 2;    // We find the index between the limits

        if (arr[index] == target) {             // If it is equal to the target
            return index;                       // Then this is the index we are looking for
        } else if (arr[index] > target) {       // Else, if this element is bigger than the target
            upper = index - 1;                  // Then our target must be in (lower, index) that's why index -1
        } else {
            lower = index + 1;                  // Otherwhise it must be in (index, upper) that's why index + 1
        }
    }
    
    return -1;  // If it reaches here, then it didn't find the target, and we return -1
}
````

## Límites numéricos

````c++
## Numeric limits of data types

You need the `numeric_limits` class template in the `<limits>` header. Example:

````c++
#include <iostream>
#include <limits>

using namespace std;

template<typename T>
void showMinMax() {
   cout << "min: " << numeric_limits<T>::min() << endl;
   cout << "max: " << numeric_limits<T>::max() << endl;
   cout << endl;
}

int main() {
    // This prints the min and max value of the datatype <short>
    // Replace <short> for other datatypes
   	showMinMax<short>();
}
````

# Teoría de números

## Algorítmo de euclides

````python
# 2022-02-02
# This program finds the greatest common divisor (GCD) of two numbers using the Euclidean algorithm
# https://en.wikipedia.org/wiki/Euclidean_algorithm#Procedure
# ABSTRACT
# Given two natural numbers a and b, a < b
# While b mod a != 0
# Find the residue of b / a = c
# b = a, a = c

def find_gcd(a, b):
    while True:
        c = b % a
        b = a
        a = c

        if c == 0:
            break

    return b


def main():
    a = int(input())
    b = int(input())

    if b < a:
        # if b < a, swaps a and b to make a < b
        c = a
        a = b
        b = c

    gcd = find_gcd(a, b)

    print(gcd)


if __name__ == "__main__":
    main()
````

## Decimal a base n

````python
from math import trunc

# This function parses a number base 10 to base n
# only for n <= 10
def to_base(dec, base):
    dec = int(dec)
    base = int(base)
    result = ""

    while (dec >= base):
        result = str(dec % base) + result
        dec = trunc(dec / base)
    result = str(dec) + result
    
    return result

print(to_base(input(), input()))
````

## Conjuntos potencia

````python
"""
DESCRIPCION
Este programa debe mostrar todos los conjuntos potencia de s

ENTRADAS
Se debe modificar el la lista s

SALIDAS
Todos los conjuntos potencia y la cantidad de estos

NOTA
No es necesario que los elementos de s esten en orden
La cantidad de conjuntos potencia siempre va ser 2^n(s)

EJEMPLO
Entrada
s = [a,b,c]

Salida

a,
b
c
a b
a c
b c
a b c
8
(No importa el orden de los conjuntos potencia)

PROCESO
Se va hacer un ciclo que obtenga todos los números en sistema binario
desde 0 hasta N(P(S)) - 1

Ejemplo
Si S = {1, 2, 3}
N(P(S)) = 8
Entonces num_binario sera : obtendra
0 0 0   0 0 0   Conjunto vacío
0 0 1   3
0 1 0   2
0 1 1   2 3
1 0 0   1
1 0 1   1 3
1 1 0   1 2
1 1 1   1 2 3

Como se puede observar, al reemplazar todos los 1 en cada número
por el elemento de s en la posición del 1 se obtienen todos los
conjuntos potencia en desorden

Ahora solo hay que guardar esto en una lista e imprimirlos
de menor a mayor
"""

from math import trunc

# Este método pasa un número de decimal a binario
def dec_to_binary(dec):
    num_binario = []

    if (dec != 0):
        while (dec > 1):        
            if (dec % 2 > 0):
                num_binario += [1]
            else:
                num_binario += [0]
            dec = trunc(dec/2)
        num_binario += [1]
    else:
        num_binario += [0]

    return num_binario


# Es el conjunto del cual obtendremos los subconjuntos
s = ["a", "b", "c", "d", "e"]

# Se calcula el total de conjuntos potencia que habrá
total_conjuntos_potencia = pow(2,len(s))

# Se inicializa la variable num_binario
# Aquí se guardará cada número decimal en binario
# para posteriormente analizarlo y hacer el proceso
num_binario = []

# Aquí se guardarán todos los conjuntos potencia
conjuntos_potencia = []

# Aquí se guardará cada conjunto potencia
# para posteriormente agregarlo a la lista conjuntos_potencia
conjunto_potencia = ""

# Se recorren todos los números desde el 0 hasta el total de
# conjuntos potencia - 1
for i in range(total_conjuntos_potencia):
    # Se reestablece conjunto_potencia para cada paso
    conjunto_potencia = ""
    # Se pasa i a binario y se guarda en la lista num_binario
    num_binario = dec_to_binary(i)

    # Se recorre la lista num_binario
    for j in range(len(num_binario)):
        # Si hay un 1 en la posición j
        if (num_binario[j] == 1):
            # Se concatena a conjunto potencia el elemento de s en j
            conjunto_potencia += s[j]
    
    # Después de analizar todo el número binario
    # se agrega conjunto_potencia a la lista conjuntos potencia
    conjuntos_potencia += [conjunto_potencia]

# Se imprimen todos los conjuntos potencia en desorden
for i in conjuntos_potencia:
    print(i)
print(total_conjuntos_potencia)
````

## Principios

## Tipos de redondeo

![image-20220616132250254](/run/media/huerta/KINGSTON/Documentos/Teoría de números/images/redondeo.png)

## Números poligonales

Son números enteros que pueden ser representados por patrones geométricos.

### Números cuadrados

$$
s_n=1+3+5+\dots+(2n+1)=n^2
$$

![image-20220616135753769](/run/media/huerta/KINGSTON/Documentos/Teoría de números/images/ej-1.4.1.png)

### Números triangulares

$$
t_n=1+2+3+\dots+n=\frac{n(n+1)}{2}
$$

![image-20220616151000116](/run/media/huerta/KINGSTON/Documentos/Teoría de números/images/ej-1.4.2.png)
$$
t_n+t_{n-1}=s_n
$$

### Número poligonal

![image-20220616151207033](/run/media/huerta/KINGSTON/Documentos/Teoría de números/images/ej-1.4.3.png)

#### Fórmula general de los números poligonales

$$
\frac{rn(n-1)}{2}+sn
$$

**Donde:**

$$n$$ = La posición del número que queremos calcular (comenzando con 1)
$$r$$ = El aumento en cada figura
$$s$$ = El número en que comenzamos

### Números tetraédricos

Son los análogos de los números triangulares en 3D.
$$
T_n=t_1+t_2+\dots+t_n\frac{n(n+1)(n+2)}{6}
$$

### Números piramidales de base cuadrada

$$
P_n=\frac{n(n+1)(2n+1)}{6}
$$

## Sumatorias

Siendo *c* una constante.
$$
\sum^n_{k=1}c=cn
$$

$$
\sum^n_{k=1}ca_k=c\sum^n_{k=1}a_k
$$

$$
\sum^n_{k=1}[a_k+b_k]=\sum^{n}_{k=1}a_k+\sum^{n}_{k=1}b_k
$$

$$
\sum^b_{k=a}F(k)=\sum^{b+c}_{k=a+c}F(k-c)
$$

$$
\sum^b_{k=a}F(k)=\sum^{b-c}_{k=a-c}F(k+c)
$$

$$
\sum^n_{k=1}[F(k)-F(k-1)]=F(n)-F(0)
$$

Si $$n\in\Z^+$$, entonces
$$
\sum^n_{k=1}k=\frac{n(n+1)}{2}
$$

$$
\sum^n_{k=1}k^2=\frac{n(n+1)(2n+1)}{6}
$$

$$
\sum^n_{k=1}k^3=\frac{n^2(n-1^2)}{4}
$$

$$
\sum^n_{k=1}k^4=\frac{n(n+1)(6n^3+9n^2+n-1)}{30}
$$

## Productorias

Siendo $$c$$ una constante.
$$
\prod^n_{i=1}{i}=n!
$$

1. Productoría de una constante:
   $$
   \prod^n_{i=1}c=c^n
   $$
   Si $$i=a;\Rightarrow n > a > 1 \Rightarrow$$
   $$
   \prod^n_{i=a}c^{n-a+1}
   $$

2. Productoria de una constante por una variable
   $$
   \prod^n_{i=1}(k\cdot x_i)=k^n\left[\prod^n_{i=1}x_i\right]
   $$

3. Productoria de dos variables
   $$
   \prod^n_{i=1}x_iy_i=\left[\prod^n_{i=1}x_1\right]\left[\prod^n_{i=1}y_1\right]
   $$

4. Productoria doble
   $$
   \prod^n_{i=1}\left[\prod^m_{j=1}x_{ij}\right]=\prod^m_{j=1}\left[\prod^n_{i=1}x_{ij}\right]
   $$

## 