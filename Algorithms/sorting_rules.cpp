/*
source: https://www.hackerrank.com/contests/udemy-welcome-contest/challenges/
date: 2022-06-08
*/

#include<bits/stdc++.h>

using namespace std;

int a [] = {0, 1, 5, 4, 3, 2, 6};
int b [] = {0, 6, 4, 2, 5, 3, 1};

bool sort_rules(const int &i, const int &j) {
    return a[i] <= a[j];
}

int main() {
    // sort b in base a
    sort(b, b+7, sort_rules);

    for (int i = 0; i < 7; i++)
        cout << b[i] << " ";

    return 0;
}