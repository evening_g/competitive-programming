# Mapas

Los mapas son como los sets, pero además tienen asignado un valor.

Por ejemplo, aquí tenemos un conjunto con frutas.

![image-20231122113325165](/home/huerta/.config/Typora/typora-user-images/image-20231122113325165.png)

¿Qué pasa si además le "vinculamos" un valor a cada fruta?

![image-20231122113448146](/home/huerta/.config/Typora/typora-user-images/image-20231122113448146.png)

Ahora tenemos esta estructura donde los nombres de las frutas son la llave y tienen un valor.

Esto es un mapa.

## Declaración.

```cpp
map<Llave, Tipo> mapa;
```

## Agregar valores

¿Qué pasa si a nuestro mapa de frutas le hago lo siguiente?

```cpp
frutas["Zapote"] = 3;
```

![image-20231122113749554](/home/huerta/.config/Typora/typora-user-images/image-20231122113749554.png)

## Obtener valores

Pero ¿qué pasa si hago esto?

```cpp
cout << frutas["Pera"];
```

El programa va crashear porque estamos intentando acceder a un valor que no existe. Entonces primero hay que verificar que exista.

```cpp
if (frutas.count("Pera")) {
	cout << frutas["Pera"];
}
```

## Eliminar valores

Como la llave en realidad es un conjunto, tiene la mayoría de las operaciones de conjuntos.

```cpp
frutas.erase("Piña");
```

## Recorrer el mapa

Necesitamos un for-range.

```cpp
for (auto &fruta: frutas) {
    fruta.first; // Llave
	fruta.second; // Valor
}
```

## Diferencia entre map y unordered_map

En el mapa normal, las llaves se guardan ordenadamente. Internamente es un árbol

En el unordered_map, las llaves no están en orden. Internamente es una tabla de hash.

Como explicó Mikel la clase pasada, aunque se supone que el unordered_map tiene operaciones de complejidad $O(c)$ a veces resulta más lento que el mapa ordenado, con complejidad de $O(\log{n})$.

---

## Hacer funciones para ordenar estructuras propias

Debemos hacer siempre la siguiente sintaxis:

```cpp
bool compare(const myStruct &a, const myStruct &b) {
    if (a /*va antes que */ b) {
        return true;
    } else {
        return false;
    }
}
```



## Problemas

https://omegaup.com/course/UASLP2021/assignment/set1#problems/COMI-Traductor

https://cses.fi/problemset/task/1640/

https://codeforces.com/contest/1398/problem/B





