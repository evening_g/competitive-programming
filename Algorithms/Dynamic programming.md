# Dynamic programming

Simple steps for solving problems using a dynamic approach.

Source: [5 Simple Steps for Solving Dynamic Programming Problems](https://youtu.be/aPQY__2H3tE).

1. **Visualization**

Visualize examples to find patterns. Sometimes trying to represent all the possible solutions with a DAG may help.

2. **Find an appropriate subproblem**

A simpler version of our overall problem.

3. **Find relationships among subproblems**

4. Generalize the relationship

5. **Implement**

Solving subproblems in order. In order to solve a problem, all its predecessors must be solved.