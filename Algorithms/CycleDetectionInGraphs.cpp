#include<iostream>
#include<vector>

using namespace std;

int N = 7;
/**
 * 1 - 0 -- 2
 * |   |  /  \
 * 3 - 4 5 - 6
 * 
*/
vector<vector<int>> adj = {
    {1, 2, 4},
    {0, 3},
    {0, 5, 6},
    {1, 4},
    {0, 3},
    {2, 6},
    {2, 5},
};

enum states {UNVISITED, EXPLORED, VISITED};
vector<states> state(N, UNVISITED);
vector<int> ancestor(N, -1); // previously visited node
vector<int> depth(N, 0); // Depth from the root

void cycle_check(int u) {
    state[u] = EXPLORED;

    for (int v : adj[u]) {
        if (state[v] == UNVISITED) {
            cout << "Tree edge (" << u << ',' << v << ")\n";
            ancestor[v] = u;
            depth[v] = depth[u]+1;
            cycle_check(v);
        } else if (state[v] == EXPLORED) {
            if (ancestor[u] == v) {
                // cout << "Bidirectional edge (trivial) (" << u << ',' << v << ")\n";
            } else {
                cout << "Back edge (" << u << ',' << v << ")\n";
                cout << "\twith length " << depth[u] - depth[v] << '\n';
            }
        } else if (state[v] == VISITED) {
            // If visited an already visited edge, we went frontwards
            // cout << "Forward/Cross edge (" << u << ',' << v << ")\n";
        }
    }

    state[u] = VISITED;
}

int main() {
    for (int u = 0; u < N; u++) {
        if (state[u] == UNVISITED) {
            cycle_check(u);
        }
    }
    
    for (int i : depth) {
        cout << i << ' ';
    }
    cout << '\n';

    return 0;
}