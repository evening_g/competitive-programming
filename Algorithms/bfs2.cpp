#include <bits/stdc++.h>
using namespace std;
 
struct coord{
    int i;
    int j;
    coord(){};
    coord(int _i, int _j){
        i = _i;
        j = _j;
    }
};
 
bool isValidIndex(const vector<vector<char>> &map, int i, int j){
    return (j<map.front().size() && j>=0 && i<map.size() && i>=0);
}
 
int main(){
    ios::sync_with_stdio(0); 
    cin.tie(0);
 
    int m, n;
    cin >> n >> m;
 
    //vector char es mejor en estos casos
    vector<vector<char>> map(n, vector<char>(m));
    vector<vector<int>> dist(n, vector<int>(m,-1));
    
    coord start, end;
 
    for(int i=0; i<n; ++i){
        for(int j=0; j<m; ++j){
            cin >> map[i][j];
            
            if(map[i][j] == 'A'){
                start.i = i;
                start.j = j;
            }
            
            if(map[i][j] == 'B'){
                end.i = i;
                end.j = j;
            }
        }
    }
 
    dist[start.i][start.j] = 0;
    
    queue<coord> q;
    q.push(start);
    
    while(!q.empty()){
        coord current = q.front();
        q.pop();
        
        int dx[] = {0,1,0,-1};
        int dy[] = {1,0,-1,0};
        
        for(int i=0; i<4; ++i){
            if(isValidIndex(map, current.i + dx[i], current.j + dy[i]) 
                && map[current.i + dx[i]][current.j + dy[i]] != '#'){
                
                if(dist[current.i + dx[i]][current.j + dy[i]] == -1){
                    dist[current.i + dx[i]][current.j + dy[i]] = dist[current.i][current.j] + 1;
                    q.push(coord({current.i + dx[i], current.j + dy[i]}));
                    //cout << current.i + dx[i] << ' '  << current.j + dy[i] << '\n';
                    //q.emplace(current.i + dx[i], current.j + dy[i]);
                }
                
            }
        }
    }
    
    for(auto row : dist){
        for(auto col : row){
            if(col == -1){
                cout << '#';
            } else {
                cout << col;
            }
        }
        cout << '\n';
    }
    
    return 0;
}
 
/*
5 8
########
#A.....#
####.#.#
#B.....#
########
*/