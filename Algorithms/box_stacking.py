# Box Stacking using dynamic programming
# Source: https://youtu.be/aPQY__2H3tE
# Box (Li, Wi, Hi) can be can be on top of Box (Lj, Wj, Hj) if
# Li < Lj and Wi < Wj
# Find the tallest boxes tower!

def tallest_stack(boxes: list[(int, int, int)]) -> int:
    # Sort boxes by top area so we ensure a good order
    boxes.sort(key=lambda x: x[0] * x[1])

    # Map boxes to each own height
    heights = {box:box[2] for box in boxes}

    # Find the boxes that can be stacked above the i-th box
    for i in range(0, len(boxes)):
        box = boxes[i]

        S = list()
        for j in range(i):
            if is_stackable(boxes[j], box):
                S.append(boxes[j])
        
        heights[box] = box[2] + max([heights[box] for box in S], default=0)

    return max(heights.values(), default=0)


def is_stackable(top: tuple[int, int, int], bottom: tuple[int, int, int]) -> bool:
    return top[0] < bottom[0] and top[1] < bottom[1]


def main():
    boxes = [(1, 2, 2),
             (4, 5, 3),
             (2, 4, 1),
             (3, 6, 2),
             (2, 3, 2),
             (1, 5, 4)]
    
    # The tallest stack is
    # (1, 2, 2), (2, 3, 2), (4, 5, 3) => 7
    
    assert(tallest_stack(boxes) == 7)

    print("Ok")


if __name__ == "__main__":
    main()
