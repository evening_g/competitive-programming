def kruskal(n: int, edges: list[(int, int, int)]) -> list:
    # Declare variables
    mst = []
    subgraphs = []

    # Sort edges by weight
    edges.sort(key = lambda e: e[2])
    
    # There must be n-1 edges for the mst to connect all the vertices
    while (len(mst) < n - 1):
        edge = edges.pop(0)

        if merge(subgraphs, edge):
            mst += [edge]

    return mst


def merge(subgraphs: list[set], edge: tuple(int, int, int)) -> bool:
    u = edge[0]
    v = edge[1]
    index_u = index_v = -1

    for i, subgraph in enumerate(subgraphs):
        if u in subgraph:
            index_u = i

        if v in subgraph:
            index_v = i

    # If none of the vertices in the edge were found
    # Create a new subgraph with both vertices
    if index_u == index_v == -1:
        subgraphs += [{u, v}]
        return True
        
    # If both vertices were found in the same subgraph
    # This edge shall not be used as it'd form a cycle
    if index_u == index_v:
        return False
    
    # If only the vertex u was found
    # Add v to the subgraph were it was found
    if index_u != -1:
        subgraphs[index_u].add(v)
        return True

    # Same if only vertex v was found...
    if index_v != -1:
        subgraphs[index_v].add(u)
        return True
    
    # Both vertices exist in different subgraphs
    # Merge the subgraphs
    subgraphs[index_u].update(subgraphs[index_v])
    del subgraphs[index_v]
    return True


def main():
    # Number of vertices
    n = int(input())

    # Number of edges
    m = int(input())

    edges = []
    for _ in range(m):
        # Read edges data
        u, v, w = map(int, input().split())
        edges += [(u, v, int(w))]

    mst = kruskal(n, edges)
    
    print("MST:")
    [print(edge) for edge in mst]
        


if __name__ == "__main__":
    main()
