```cpp
include <ext/pb_ds/assoc_container.hpp>
include <ext/pb_ds/tree_policy.hpp>

using namespace __gnu_pbds;

tree <int, null_type, less, rb_tree_tag, tree_order_statistics_node_update>
```

| Argument      | Description                                                 |
| ------------- | ----------------------------------------------------------- |
| `int`         | Data type                                                   |
| `null_type`   | If `null_type` use as set.<br />If `mapped_type` use as map |
| `less`        | Comparison function. `less_equal` turns it into a multiset. |
| `rb_less_tag` | `rb_less_tag` `splay_tree_tag` `ov_tree_tag`                |
| `tree_order`  | To keep track of metadata                                   |

| Method                  | Description                                 |
| ----------------------- | ------------------------------------------- |
| `find_by_order(k)`      | Iterator to k-th element                    |
| `order_of_key(n)`       | Number of items smaller to n                |
| `copy_from_range(a, b)` | Copies the elements in the iterators [a, b) |

