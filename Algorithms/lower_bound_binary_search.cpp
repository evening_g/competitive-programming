#include <iostream>
#include <vector>
#include <assert.h>

using namespace std;

int lower_bound_binary_search(const vector<int> &v, int x) {
    int l = 0, r = v.size();

    int m = (l+r) / 2;

    while (l < r) {
        // Not found
        if (m == -1 or m == v.size()) {
            return m;
        }

        if (x > v[m]) {
            l = m + 1;
        } else {
            r = m;
        }

        m = (l+r) / 2;
    }

    return m;
}

int main() {
               //-1  0  1  2  3  4  5  6  7  8  9
    vector<int> v = {1, 2, 2, 2, 3, 3, 5, 6, 6};

    assert(lower_bound_binary_search(v, 0) == 0);
    assert(lower_bound_binary_search(v, 1) == 0);
    assert(lower_bound_binary_search(v, 2) == 1);
    assert(lower_bound_binary_search(v, 3) == 4);
    assert(lower_bound_binary_search(v, 4) == 6);
    assert(lower_bound_binary_search(v, 5) == 6);
    assert(lower_bound_binary_search(v, 6) == 7);
    assert(lower_bound_binary_search(v, 7) == 9);

    cout << "OK\n";

    return 0;
}
