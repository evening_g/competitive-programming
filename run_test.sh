# !/bin/bash

if [ $# -lt 2 ]
then
    echo "Usage: ./run_test.sh executable test_files"
    echo "test files must have extensions .in and .ans for input and answer respectively"
    exit
fi

for file in $2*.in
do
    $1 < $file > output.txt

    ans=${file%.*}.ans

    diff output.txt $ans > diff.txt

    if [ -s diff.txt ]
    then
        echo "Error in $file"
        break
    else
        echo "$file ok"
    fi

    rm diff.txt output.txt
done
