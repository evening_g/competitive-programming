/**
 * WA
 *
 * Say if a list is in increasing, decreasing or neither order.
 *
 * */

#include <bits/stdc++.h>
using namespace std;
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        int n, s = 0;
        cin >> n;
        string prev, curr;
        cin >> prev;
        n --;
        while (n--) {
                cin >> curr;
                if (prev < curr) {
                        if (s == -1) {
                                s = 0;
                                break;
                        } else {
                                s = 1;
                        }
                } else if (prev > curr) {
                        if (s == 1) {
                                s = 0;
                                break;
                        } else {
                                s = -1;
                        }
                }
        }

        if (s == 1)
                cout << "INCREASING\n";
        else if (s)
                cout << "DECREASING\n";
        else
                cout << "NEITHER\n";

        return 0;
}
