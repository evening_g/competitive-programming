#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int a, b;
    cin >> a >> b;
    int r = b - a;

    string piece = " piece";
    if (r*r != 1)
        piece += "s";
    if (r > 0)
        cout << "Dr. Chaz will have " << r << piece << " of chicken left over!\n";
    else
        cout << "Dr. Chaz needs " << -r << " more" << piece << " of chicken!\n";


    return 0;
}
