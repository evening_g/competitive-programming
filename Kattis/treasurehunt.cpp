#include <bits/stdc++.h>
using namespace std;

#define readv(v) for (auto &i : v) cin >> i
#define loop for(;;)

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int R, C;
        cin >> R >> C;

        vector<string> M(R);
        readv(M);

        int steps = 0;
        int i = 0, j = 0;
        string exit_msg;
        for (int steps = 0; true; steps ++) {
                if (i < 0 || j < 0 || i >= R || j >= C) {
                        exit_msg = "Out";
                        goto exit;
                }
                char d = M[i][j];
                M[i][j] = '#';
                switch(d) {
                        case 'N':
                                i --;
                                break;
                        case 'S':
                                i ++;
                                break;
                        case 'E':
                                j ++;
                                break;
                        case 'W':
                                j --;
                                break;
                        case 'T':
                                exit_msg = to_string(steps);
                                goto exit;
                        case '#':
                                exit_msg = "Lost";
                                goto exit;
                        default:
                                break;
                }
        }

exit:
        cout << exit_msg << '\n';

        return 0;
}
