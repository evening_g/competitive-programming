#include <bits/stdc++.h>
#define loop for(;;)
using namespace std;
using ll = long long;
const int INF = 1000000000;
int score(int a, int b) {
    int l = 0;
    if (a < b)
        swap(a, b);

    if (a == 2 and b == 1)
        l = INF;
    else if (a == b)
        l = a * 100 + b * 10;
    else
        l = a*10 + b;

    return l;
}
int main() {
    int a, b, c, d;
    int l, r;
    loop {
        cin >> a >> b >> c >> d;

        if (!a)
            break;

        l = score(a, b);
        r = score(c, d);

        if (l > r)
            cout << "Player 1 wins.\n";
        else if (r > l)
            cout << "Player 2 wins.\n";
        else
            cout << "Tie.\n";
    }

    return 0;
}
