#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int n;
    cin >> n;

    while (n--) {
        char name[51];
        int y1, m1, d1, y2, m2, d2, c;

        scanf("%s %d/%d/%d %d/%d/%d %d", name, &y1, &m1, &d1, &y2, &m2, &d2, &c);

        if (y1 >= 2010 or y2 >= 1991)
            cout << name << " eligible\n";
        else if (c > 40)
            cout << name << " ineligible\n";
        else
            cout << name << " coach petitions\n";
    }

    return 0;
}

