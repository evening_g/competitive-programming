/**
 * tags: array
 * status: TLE
 */
#include <bits/stdc++.h>
using namespace std;
int main()
{
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        string x, y;
        cin >> x >> y;

        // Find p, where y = M^p
        int p = y.size() - 1;
        int x_length = x.size();

        // Remove leading zeroes in x
        int i = 0;
        while (x[i++] == '0');
        if (i)
                x = x.substr(i-1, x.size()-i);

        cout << x << '\n';

        return 0;
}
