#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int n,t=0;
    cin >> n;
    while (cin >> n)
        if (n < 0)
            t -= n;

    cout << t << '\n';

    return 0;
}
