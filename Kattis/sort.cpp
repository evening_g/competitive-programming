/**
 * tags: map dictionary memory
 * */
#include <bits/stdc++.h>
using namespace std;
#define all(v) v.begin(), v.end()
struct Item {
    int num;
    int cnt;
    int idx;
};
bool Item_sort(const Item &a, const Item &b) {
    if (a.cnt == b.cnt)
        return a.idx > b.idx;
    else
        return a.cnt > b.cnt;
}
int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);
    int n, c, x;
    cin >> n >> c;
    map<int, Item> m;
    while (n--) {
        cin >> x;
        if (!m.count(x))
            m[x] = {x, 0, n};
        m[x].cnt++;
    }
    vector<Item> v;
    for (auto& [num, item] : m)
        v.push_back(item);
    sort(all(v), Item_sort);
    for (Item i : v)
        while (i.cnt--)
            cout << i.num << ' ';
    cout << '\n';
    return 0;
}
