/**
 * tags: input interesting
 */

#include <bits/stdc++.h>
using namespace std;

bool solve(int t) {
    int x, y, diff;
    bool jolly = true;
    vector<char> s(t, 1);
    s[0] = 0; // won't count diff = 0
    cin >> x;
    for (int i = 1; i < t; i++) {
        cin >> y;
        diff = abs(x - y);
        if (diff < t and s[diff])
            s[diff] = 0;
        else
            jolly = false;
        x = y;
    }
    return jolly;
}

int main() {
    int t;
    
    while (cin >> t)
        if (solve(t))
            cout << "Jolly\n";
        else
            cout << "Not jolly\n";

    return 0;
}
