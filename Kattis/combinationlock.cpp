#include <bits/stdc++.h>
using namespace std;


int dist(int to, int from) {
        if (to < from)
                to += 360;
        return to - from;
}

bool solve() {
        int p, a, b, c;
        cin >> p >> a >> b >> c;

        if ((p|a|b|c) == 0)
                return false;

        p *= 9;
        a *= 9;
        b *= 9;
        c *= 9;
        int total = 360 * 3;

        total += dist(p, a);
        total += dist(b, a);
        total += dist(b, c);

        cout << total << '\n';

        return true;
}

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        while (solve());

        return 0;
}
