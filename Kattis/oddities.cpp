#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int n, x;
    cin >> n;

    while (n--) {
        cin >> x;
        if (x % 2)
            cout << x << " is odd\n";
        else
            cout << x << " is even\n";

    }

    return 0;
}

