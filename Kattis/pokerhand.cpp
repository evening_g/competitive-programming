#include <bits/stdc++.h>
using namespace std;
int main() {
        map<char, int> m;
        string c;
        while (cin >> c) m[c[0]] ++;
        int r = 0;
        for (auto &i : m)
                if (i.second > r)
                        r = i.second;
        cout << r << '\n';
        return 0;
}

