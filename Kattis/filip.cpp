#include <bits/stdc++.h>

using namespace std;

#define all(v) v.begin(), v.end()

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        string a, b;
        cin >> a >> b;

        reverse(all(a));
        reverse(all(b));

        if (a > b)
                cout << a << '\n';
        else
                cout << b << '\n';

        return 0;
}
