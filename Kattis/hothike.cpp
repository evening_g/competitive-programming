#include <bits/stdc++.h>
using namespace std;
#define readv(v) for (auto &i : v) cin >> i
#define INF 1000000000
#define maxt(i) max(v[i], v[i+2])
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        int n; cin >> n;
        deque<int> v(n);
        readv(v);
        v.push_front(INF);
        int m = 0;
        for(int i = 1; i <= n - 2; i++)
                if (maxt(i) < maxt(m))
                        m = i;
        cout << m << ' ' << maxt(m) << '\n';
        return 0;
}
