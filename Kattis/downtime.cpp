#include <bits/stdc++.h>
using namespace std;

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int n, k, max_requests = 0, count = 0;
        cin >> n >> k;

        map<int, int> v;
        
        for (int i = 0; i < n; i++) {
                int t; cin >> t;
                if (!v.count(t))
                        v[t] = 0;
                v[t] += 1;
                v[t+1000] -= 1;
        }

        for (auto &[t, c] : v) {
                count += c;
                max_requests = max(max_requests, count);
        }

        cout << (max_requests / k) + (max_requests % k != 0) << '\n';

        return 0;
}
