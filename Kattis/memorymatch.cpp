#include <bits/stdc++.h>
#define loop for(;;)
using namespace std;
using ll = long long;
int main() {
    int n, k;
    cin >> n >> k;
    map<string, set<int>> found;
    set<int> known;
    while (k--) {
        int a, b;
        string x, y;
        cin >> a >> b >> x >> y;
        known.insert(a);
        known.insert(b);
        if (x != y) {
            found[x].insert(a);
            found[y].insert(b);
        } else {
            found.erase(x);
        }
    }

    int t = 0, single = 0;
    for (const auto &i : found) {
        if (i.second.size() == 2) {
            t++;
        } else {
            single ++;
        }
    }

    if (n - known.size() == single)
        t += single;
    else if (n - known.size() == 2)
        t ++;

    cout << t << '\n';
    return 0;
}
