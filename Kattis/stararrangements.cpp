// math interesting

#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int n;
    cin >> n;

    cout << n << ":\n";
    for (int x = 2; x <= n/2+n%2; x++)
        for (int y = x-1; y <= x; y++) {
            int mod = n % (x + y);
            if (mod == 0 or mod == x)
                cout << x << ',' << y << '\n';
        }

    return 0;
}
