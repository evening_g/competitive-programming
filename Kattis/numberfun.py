# TLE

def main():
    n = int(input())

    for _ in range(n):
        solve()

def solve():
    a, b, c = map(int, input().split())
    n = [a, b]
    op = "+-*/"

    for i in range(2):
        for o in op:
            if eval(f"{n[i]}{o}{n[(i+1)%2]}") == c:
                print("Possible")
                return
                

    print("Impossible")

main()
