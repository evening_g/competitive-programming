#include <stdio.h>

int main() {
    int n;
    scanf("%d", &n);
    int x = 1 << n;

    int result =  x*x + 2*x + 1;

    printf("%d\n", result);

    return 0;
}
