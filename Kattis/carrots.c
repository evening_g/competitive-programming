#include <stdio.h>

int main() {
    int a, solution;
    scanf("%d %d", &a, &solution);

    printf("%d\n", solution);

    return 0;
}
