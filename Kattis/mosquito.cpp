#include <bits/stdc++.h>
using namespace std;

int main() {
        cin.tie(0)->sync_with_stdio(0);

        int m, p, l, e, r, s, n;
        while (cin >> m >> p >> l >> e >> r >> s >> n) {
                while (n--) {
                        int mi = p / s;
                        int li = m * e;
                        int pi = l / r;
                        
                        m = mi;
                        l = li;
                        p = pi;
                }
                cout << m << '\n';
        }

        return 0;
}
