n = int(input())
d = input()
print('\n\n'.join([f"{i} bottle{'s' if i != 1 else ''} of {d} on the wall, {i} bottle{'s' if i != 1 else ''} of {d}.\nTake {'it' if i == 1 else 'one'} down, pass it around, {i-1 if i-1 != 0 else 'no more'} bottle{'s' if i-1 != 1 else ''} of {d}{' on the wall' if i-1 != 0 else ''}." for i in range(n, 0, -1)]))
