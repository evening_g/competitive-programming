// input sorting

#include <bits/stdc++.h>
#define loop for(;;)
using namespace std;
using ll = long long;
int main() {
    char board [8][8];
    char tmp[4*8+2];

    for (int i = 0; i < 8*2 + 1; i++) {
        if (i%2) {
            // read 8 positions
            for (int j = 0; j < 8; j++) {
                scanf("|[.:]%c[.:]", &board[i][j]);
            }
            scanf("|\n");
        } else {
            scanf("%s", tmp);
        }
    }

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++)
            printf("%c", board[i][j]);
        printf("\n");
    }
    
    return 0;
}
