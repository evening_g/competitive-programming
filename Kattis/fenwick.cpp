#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vl = vector<ll>;
using vii = vector<pair<int, int>>;
using ii = pair<int, int>;
using mi = vector<vector<int>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (int &i : v) cin >> i
#define printv(v) for (int &i : v) cout << i << ' '

#define lsz(x) (~(x) & (x+1))
#define lso(x) ((x) & -(x))

class FenwickTree {
    private:
    vl ft;

    public:
    FenwickTree(int m) { ft.assign(m+1,0); }

    void build(const vl &v) {
        int m = (int) v.size()-1;
        ft.assign(m+1, 0);

        for (int i = 1; i <= m; ++i) {
            ft[i] += v[i];
            if (i+lso(i) <= m)
                ft[i+lso(i)] += ft[i];
        }
        
    }

    FenwickTree(const vl &v) { build(v); }

    FenwickTree(int m, const vi &v) {
        vl f(m+1, 0);

        for (int i = 0; i < (int) v.size(); ++i)
            ++f[v[i]];
        build(f);
    }

    // range sum query [1, j]
    ll rsq(int j) {
        ll sum = 0;
        // go to parent
        // until reaching the root
        for (; j; j -= lso(j))
            sum += ft[j];

        return sum;
    }

    // range sum query [i, j]
    ll rsq(int i, int j) {
        return rsq(j) - rsq(i);
    }

    void update(int i, ll x) {
        // go to child until reaching the end
        for (; i < (int) ft.size(); i+= lso(i))
            ft[i] += x;
    }

    // find smallest index with value k
    int select(ll k) {
        int l = 0, r = (int) ft.size()-1;
        for (int i = 0; i < 30; ++i) {
            int m = (l+r)/2;
            if (rsq(1,m) < k) {
                l = m;
            } else {
                r = m;
            }
        }

        return r;
    }
};

int main() {
    cin.tie(0)->sync_with_stdio(0);

    int n, q;
    cin >> n >> q;
    FenwickTree ft(n);

    while (q--) {
        char o;
        int a;
        ll b;

        cin >> o >> a;

        if (o == '+') {
            cin >> b;
            ft.update(a+1,b);
        } else {
            cout << ft.rsq(a) << '\n';
        }
    }

    return 0;
}
