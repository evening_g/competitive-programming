#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

class UnionFind {
    public:
    vi p, rank;

    UnionFind(int N) {
        p.resize(N);

        for (int i = 0; i < N; i++)
            p[i] = i;
       
        rank.assign(N, 0);
    }

    int find_set(int i) {
        if (p[i] == i) 
            return i;

         return (p[i] = find_set(p[i]));
    }

    void join_sets(int i, int j) {
        int x = find_set(i);
        int y = find_set(j);

        if (x == y)
            return;

        if (rank[x] > rank[y])
            swap(x, y);

        p[x] = y;

        if (rank[x] == rank[y])
            rank[y] ++;
    }
};

struct Edge {
    int u, v, w;
};

bool cmpEdge(const Edge &a, const Edge &b) {
    return a.w < b.w;
};

void solve() {
    int m, c;
    cin >> m >> c;

    vector<Edge> g(c * (c-1) / 2);

    for (Edge &e : g)
        cin >> e.u >> e.v >> e.w;

    sort(all(g), cmpEdge);

    int dist = 0;
    UnionFind s = UnionFind(c);
    
    for (Edge &e : g) {
        int x = s.find_set(e.u);
        int y = s.find_set(e.v);

        if (x == y)
            continue;

        dist += e.w;
        s.join_sets(x, y);
    }

    if (m >= dist + c)
        cout << "yes\n";
    else
        cout << "no\n";
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int t;
    cin >> t;

    while (t--)
        solve();

    return 0;
}

