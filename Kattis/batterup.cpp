#include <bits/stdc++.h>
using namespace std;
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        int sum = 0, cnt = 0, x, n;
        cin >> n;
        while (n --) {
                cin >> x;
                if (x != -1) {
                        cnt ++;
                        sum += x;
                }
        }
        cout << fixed << setprecision(4) << (double) sum / (double) cnt << '\n';
        return 0;
}
