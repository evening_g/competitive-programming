#include <bits/stdc++.h>
using namespace std;

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int k, n, t;
        char r;
        cin >> k >> n;

        int time = 3*60 + 30;

        while (n--) {
                cin >> t >> r;
                time -= t;
                if (time <= 0)
                        break;
                if (r == 'T')
                        k ++;
        }

        cout << (k - 1) % 8 + 1 << '\n';

        return 0;
}
