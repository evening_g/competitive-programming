"""
Status: WA
"""

def main():
    x = int(input())
    y = int(input())
    r = x / y

    print(f"{r:g}")


if __name__ == "__main__":
    main()

