#include <bits/stdc++.h>
using namespace std;
using vi = vector<int>;
#define all(v) v.begin(), v.end()
#define readv(v) for (auto &i : v) cin >> i
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        vi v(3);
        readv(v);
        string k;
        cin >> k;
        sort(all(v));
        for (char &c : k)
                cout << v[c - 'A'] << ' ';
        cout << '\n';
        return 0;
}
