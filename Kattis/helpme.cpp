// input sorting

/**
 * Actual
 * White: Ke1,Qd1,Ra1,Rh1,Bc1,Bf1,Nb1,a2,c2,d2,f2,g2,h2,a3,e4
 * Black: Ke8,Qd8,Ra8,Rh8,Bc8,Ng8,Nc6,a7,b7,c7,d7,e7,f7,h7,h6,
 * 
 * Expected
 * White: Ke1,Qd1,Ra1,Rh1,Bc1,Bf1,Nb1,a2,c2,d2,f2,g2,h2,a3,e4
 * Black: Ke8,Qd8,Ra8,Rh8,Bc8,Ng8,Nc6,a7,b7,c7,d7,e7,f7,h7,h6
 */
#include <bits/stdc++.h>
#define loop for(;;)
using namespace std;
using ll = long long;
struct Piece {
    char type;
    char row;
    char col;
    char clr;
};

map<char, int> order;
void print(const vector<Piece> v) {
    for (int i = 0; i < v.size(); i++) {
        Piece p = v[i];
        if (p.type != 'P')
            printf("%c", p.type);
        printf("%c%c", p.col, p.row);
        if (i != v.size()-1)
            printf(",");
    }
    printf("\n");
}
bool cmp(const Piece &a, const Piece &b) {
    if (order[a.type] == order[b.type]) {
        if (a.row == b.row)
            return a.col < b.col;
        if (a.clr == 'w')
            return a.row < b.row;
        else
            return a.row > b.row;
    }

    return order[a.type] < order[b.type];
}
int main() {
    order['K'] = 0;
    order['Q'] = 1;
    order['R'] = 2;
    order['B'] = 3;
    order['N'] = 4;
    order['P'] = 5;
    char board [8][9];
    char tmp;

    for (int i = 0; i < 8*2 + 1; i++) {
        if (i%2) {
            // read 8 positions
            scanf("\n");
            for (int j = 0; j < 8; j++) {
                scanf("|%c%c%*[^|]", &tmp, &board[i/2][j]);
            }
            scanf("|\n");
            board[i/2][8] = '\0';
        } else {
            scanf("%*[^\n]");
        }
    }

    vector<Piece> white, black;

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (board[i][j] == '.' or board[i][j] == ':')
                continue;
            if (board[i][j] >= 'a' and board[i][j] <= 'z')
                black.push_back({board[i][j] + ('A' - 'a'), '8' - i, 'a' + j, 'b'});
            else
                white.push_back({board[i][j], '8' - i, 'a' + j, 'w'});
        }
    }

    sort(white.begin(), white.end(), cmp);
    sort(black.begin(), black.end(), cmp);

    printf("White: ");
    print(white);
    
    printf("Black: ");
    print(black);

    return 0;
}
