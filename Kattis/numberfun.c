#include <stdio.h>

int solve() {
    int a, b, c;
    scanf("%d %d %d", &a, &b, &c);

    int p = 0;
    p |= a + b == c;
    p |= a - b == c;
    p |= b - a == c;
    p |= a * b == c;
    p |= a * c == b;
    p |= b * c == a;

    return p;
}

int main() {
    int n;
    scanf("%d", &n);

    while (n--)
        if (solve())
            printf("Possible\n");
        else
            printf("Impossible\n");

    return 0;
}
