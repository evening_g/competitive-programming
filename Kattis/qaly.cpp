#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int n;
    cin >> n;

    double total = 0, q, y;

    while (n--) {
        cin >> q >> y;
        total += (q*y);
    }

    cout << fixed << setprecision(3) << total << '\n';

    return 0;
}
