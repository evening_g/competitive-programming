#include <bits/stdc++.h>
using namespace std;
using vi = vector<int>;
#define all(v) v.begin(), v.end()
#define readv(v) for (auto &i : v) cin >> i
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        vi v(3);
        readv(v);
        sort(all(v));
        int d0 = v[1] - v[0];
        int d1 = v[2] - v[1];
        if (d0 == d1)
                cout << v[2] + d1;
        else if (d0 == 2 * d1)
                cout << v[0] + d1;
        else
                cout << v[1] + d0;
        cout << '\n';
        return 0;
}
