#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int g, s, c;
    cin >> g >> s >> c;

    int m = 3*g + 2*s + 1*c;

    if (m >= 8)
        cout << "Province or ";
    else if (m >= 5)
        cout << "Duchy or ";
    else if (m >= 2)
        cout << "Estate or ";

    if (m >= 6)
        cout << "Gold\n";
    else if (m >= 3)
        cout << "Silver\n";
    else
        cout << "Copper\n";

    return 0;
}
