#include <bits/stdc++.h>
using namespace std;

using ll = long long;

void solve() {
    char a[5], b[5];
    scanf("\n%[^+=]%[^\n]", a, b);
    if (a[0] == 'P') {
        cout << "skipped\n";
        return;
    }
    int A = atoi(a), B = atoi(b);
    cout << A + B << '\n';
}

int main() {
    int n;
    cin >> n;

    while (n--)
        solve();

    return 0;
}

