#include <bits/stdc++.h>
using namespace std;
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        string s;
        cin >> s;
        int a = 0, b = 0, c = 0;

        for (char &x : s) {
                if (x == 'T') a++;
                if (x == 'C') b++;
                if (x == 'G') c++;
        }

        cout << a*a + b*b + c*c + 7 * min(a, min(b, c)) << '\n';

        return 0;
}
