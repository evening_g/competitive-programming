#include <bits/stdc++.h>
using namespace std;
using vi = vector<int>;
#define loop while(true)
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        vector<char> solved(26, 0);
        int solved_count = 0, time = 0, m;
        vi attempts(26, 0);
        loop {
                char p;
                string s;
                cin >> m;
                if (m == -1) break;
                cin >> p >> s;
                p -= 'A';
                if (s == "right") {
                        solved[p] = 1;
                        solved_count ++;
                        time += m;
                } else
                        attempts[p] ++;
        }
        for (int p = 0; p < 26; p++)
                if (solved[p]) time += (20 * attempts[p]);
        cout << solved_count << ' ' << time << '\n';
        return 0;
}
