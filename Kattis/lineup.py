n = int(input())
l = []
for _ in range(n):
    l.append(input())

inc = sorted(l)
dec = sorted(l)[::-1]

if l == inc:
    print("INCREASING")
elif l == dec:
    print("DECREASING")
else:
    print("NEITHER")
