#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int x, y;
    cin >> x >> y;

    if (x == 0 and y == 1) {
        cout << "ALL GOOD\n";
        return 0;
    }

    if (y == 1) {
        cout << "IMPOSSIBLE\n";
        return 0;
    }
    
    double a = (double) x / (1.0 - (double) y);
    cout << fixed << setprecision(6) << a << '\n';

    return 0;
}

