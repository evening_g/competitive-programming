#include <stdio.h>

int main() {
    int a, i;
    scanf("%d %d", &a, &i);

    int result = (i-1)*a+1;

    printf("%d\n", result);

    return 0;
}
