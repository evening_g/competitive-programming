#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int l, r;
    cin >> l >> r;

    if (!(l|r))
        cout << "Not a moose\n";
    else
        cout << ((l^r) ? "Odd " : "Even ") << max(l,r)*2 << '\n';

    return 0;
}
