#include <bits/stdc++.h>
#define loop for(;;)
using namespace std;
using ll = long long;
int main() {
    int n;
    cin >> n;
    int current = 20, chkpnt = 20;
    int chance = 1;
    int yards;
    while (n--) {
        cin >> yards;
        
        current += yards;

        if (current <= 0) {
            cout << "Safety\n";
            return 0;
        }

        if (current >= 100) {
            cout << "Touchdown\n";
            return 0;
        }

        if (current - chkpnt >= 10) {
            chance = 0;
            chkpnt = current;
        } else if (chance == 4) {
            break;
        }

        chance ++;
    }
    cout << "Nothing\n";
    return 0;
}
