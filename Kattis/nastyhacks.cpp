#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int n, a, b, c;
    cin >> n;
    while (n--) {
        cin >> a >> b >> c;
        if (a+c > b)
            cout << "do not advertise\n";
        else if (a+c < b)
            cout << "advertise\n";
        else
            cout << "does not matter\n";
    }

    return 0;
}


