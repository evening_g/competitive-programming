#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int N;
int LIMIT;

ll simulate(mi &graph, int source) {
    mll squawks(LIMIT+1, vll(N, 0LL));
    squawks[0][source] = 1;

    for (int t = 0; t < LIMIT; t++) {
        for (int i = 0; i < N; i++) {
            if (squawks[t][i]) {
                for (int u :graph[i]) {
                    squawks[t+1][u] += squawks[t][i];
                }
                squawks[t][i] = 0;
            }
        }
    }
    
    return accumulate(all(squawks[LIMIT]), 0LL);
}


int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int m, s;
    cin >> N >> m >> s >> LIMIT;

    mi graph(N, vi());

    for (int i = 0; i < m; i++)
    {
        int u, v;
        cin >> u >> v;
        graph[u].push_back(v);
        graph[v].push_back(u);
    }

    cout << simulate(graph, s) << '\n';

    return 0;
}
