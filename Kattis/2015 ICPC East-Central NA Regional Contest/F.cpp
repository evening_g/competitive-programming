// Unfinished

#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int s, r, f, t;
    cin >> s >> r >> f >> t;

    set<string> raw_materials;
    for (int i = 0; i < f; i++)
    {
        string state;
        cin >> state;
        raw_materials.insert(state);
    }
    
    vector<set<string>> transportations(t, set<string>());
    for (int i = 0; i < t; i++)
    {
        int n;
        for (int j = 0; j < n; j++)
        {
            string state;
            cin >> state;
            transportations[i].insert(state);
        }
        
    }

    return 0;
}
