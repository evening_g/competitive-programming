#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int N;

struct Point {
    int i; 
    int j;

    Point rotate_right() {
        int _j = N - i - 1;
        int _i = j;

        return {_i, _j};
    }
};

bool compare_points(const Point &a, const Point &b) {
    if (a.i == b.i)
        return a.j <= b.j;

    return a.i <= b.i;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    cin >> N;

    vector<Point> points;

    for (int i = 0; i < N; i++)
    {
        string line;
        cin >> line;

        for (int j = 0; j < N; j++)
            if (line[j] == '.')
                points.push_back({i, j});        
    }

    string encrypted;
    cin >> encrypted;
    queue<char> encrypted_queue;
    for (char c : encrypted) encrypted_queue.push(c);
    string unencrypted(encrypted.size(), '#');

    int remaining = N*N;
    //vector<vector<char>> grid(N, vector<char>(N, '#'));
    int k = 1;
    
    loop
    {
        for (const Point &point : points) {
            if (unencrypted[point.i*N+point.j] == '#') {
                remaining --;
                unencrypted[point.i*N+point.j] = encrypted_queue.front();
            } else if (unencrypted[point.i*N+point.j] != encrypted_queue.front()) {
                // letter doesn't match
                cout << "invalid grille\n";
                return 0;
            }
            encrypted_queue.pop();
        }

        if (k == 4) {
            break;
        }

        k++;
        for (Point &point : points) point = point.rotate_right();
        sort(all(points), compare_points);
    }
    
    if (remaining != 0) {
        cout << "invalid grille\n";
    } else {
        cout << unencrypted << '\n';
    }

    return 0;
}
