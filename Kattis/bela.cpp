#include <bits/stdc++.h>
#define loop for(;;)
using namespace std;
using ll = long long;
int main() {
    int n;
    char b;
    cin >> n >> b;
    n *= 4;

    map<char, int> dom;
    map<char, int> ntd;

    dom['A'] = ntd['A'] = 11;
    dom['K'] = ntd['K'] = 4;
    dom['Q'] = ntd['Q'] = 3;
    dom['J'] = 20;
    ntd['J'] = 2;
    dom['T'] = ntd['T'] = 10;
    dom['9'] = 14;
    ntd['9'] = 0;
    dom['8'] = ntd['8'] = 0;
    dom['7'] = ntd['7'] = 0;

    char v, s;
    int val = 0;
    while (n--) {
        cin >> v >> s;

        if (s == b)
            val += dom[v];
        else
            val += ntd[v];
    }

    cout << val << '\n';
}
