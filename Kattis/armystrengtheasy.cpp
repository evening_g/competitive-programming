#include <bits/stdc++.h>
using namespace std;
using vi = vector<int>;
#define all(v) v.begin(), v.end()
#define readv(v) for (auto &i : v) cin >> i
void solve() {
        int G, M;
        cin >> G >> M;
        vi g(G);
        vi m(M);
        readv(g);
        readv(m);
        sort(all(g), greater<int>());
        sort(all(m), greater<int>());
        while (g.size() and m.size())
                if (g.back() >= m.back())
                        m.pop_back();
                else
                        g.pop_back();
        if (g.size())
                cout << "Godzilla\n";
        else
                cout << "MechaGodzilla\n";
}

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        int t;
        cin >> t;
        while (t--)
                solve();
        return 0;
}
