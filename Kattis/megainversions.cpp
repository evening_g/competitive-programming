#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

ll invs = 0;

void merge_sort3(vi &v, int l, int r) {
        int count = (r - l + 1) / 3;
        int count_last = count + (r - l + 1) % 3;
        vi vl(count), vm(count), vr(count_last);

        copy(v.begin(), v.begin() + count, vl.begin());
        copy(v.begin() + count, v.begin() + (2 * count), vm.begin());
        copy(v.begin() + (2 * count), v.end(), vr.begin());

        int i, j, k;
        i = j = k = 0;

        while (i + j + k <= r - l) {
                int a = vl[i];
                int b = vl[j];
                int c = vl[k];
        }
}

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int n;
        cin >> n;

        vi v(n);
        readv(v);

        merge_sort3(v, 0, n-1);

        cout << invs << '\n';

        return 0;
}
