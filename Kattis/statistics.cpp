#include <iostream>
#include <algorithm>
#define INF 1000000000
using namespace std;
int main() {
    int n, x, i = 1, m, M;
    while (cin >> n) {
        m = INF, M = -INF;
        while (n--) {
            cin >> x;
            m = min(m, x);
            M = max(M, x);
        }

        cout << "Case " << i++ << ": " << m << ' ' << M << ' ' << M-m << '\n';
    }

    return 0;
}
