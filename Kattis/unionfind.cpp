#include <bits/stdc++.h>

using namespace std;

class UnionFind {
    public:
    vector<int> p, rank;

    UnionFind(int N) {
        p.resize(N);

        for (int i = 0; i < N; i++)
            p[i] = i;
       
        rank.assign(N, 0);
    }

    int find_set(int i) {
        if (p[i] == i) 
            return i;

         return (p[i] = find_set(p[i]));
    }

    void join_sets(int i, int j) {
        int x = find_set(i);
        int y = find_set(j);

        if (x == y)
            return;

        if (rank[x] > rank[y])
            swap(x, y);

        p[x] = y;

        if (rank[x] == rank[y])
            rank[y] ++;
    }
};

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int n, q, a, b;
        cin >> n >> q;
        UnionFind s = UnionFind(n);
        char o;
        while (q--) {
                cin >> o >> a >> b;
                if (o == '=')
                        s.join_sets(a, b);
                else
                        if (s.find_set(a) == s.find_set(b))
                                cout << "yes\n";
                        else
                                cout << "no\n";
        }

        return 0;
}

