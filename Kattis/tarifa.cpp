#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int x, n;
    cin >> x >> n;

    int s = x, p;

    while (n--) {
        cin >> p;
        s -= p;
        s += x;
    }

    cout << s << '\n';

    return 0;
}
