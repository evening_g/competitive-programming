#include <bits/stdc++.h>
using namespace std;
#define LIM 11
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        string game;
        cin >> game;
        short a = 0, b = 0;
        for(short i = 0; i < (short) game.size(); i += 2) {
                char p=game[i], s=game[i+1];
                if (p == 'A') {
                        a += (s - '0');
                        if (a >= LIM && a >= b + 2) {
                                cout << "A\n";
                                break;
                        }
                } else {
                        b += (s - '0');
                        if (b >= LIM && b >= a + 2) {
                                cout << "B\n";
                                break;
                        }
                }
        }
        return 0;
}
