// graphing this thing in geogebra is very pretty

#include <bits/stdc++.h>
using namespace std;

double p, a, b, c, d;
int n;

double f(double k) {
        return p * ((double) sin(a * k + b) + (double) cos(c * k + d) + 2.0);
}

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        cin >> p >> a >> b >> c >> d >> n;

        vector<double> prices(n);
        for (int i = 0; i < n; i++)
                prices[i] = f((double) (i+1));

        double pivot = prices[0];
        double maxdec = 0;
        double diff = 0;

        for (int k = 1; k < n; k++) {
                if (prices[k] > pivot) {
                        pivot = prices[k];      // save the new pivot
                        maxdec = max(maxdec, diff);      // max the diff
                        diff = 0;
                } else {
                        //  increase the diff
                        //  the lowest point is not always the last one
                        diff = max(pivot - prices[k], diff);    
                                                                
                }
        }

        maxdec = max(maxdec, diff);

        cout << fixed << setprecision(7) << maxdec << '\n';

        return 0;
}
