#include <bits/stdc++.h>
using namespace std;
using vpi = vector<pair<int, int>>;
#define all(v) v.begin(), v.end()
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        int n;
        cin >> n;
        vpi v(n-1);
        for (int i = 2; i <= n; i++) {
                cin >> v[i-2].first;
                v[i-2].second = i;
        }
        sort(all(v));
        cout << "1 ";
        for (auto &[i, j] : v)
                cout << j << ' ';
        cout << '\n';
        return 0;
}
