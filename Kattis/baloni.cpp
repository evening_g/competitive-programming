#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = 1000000000;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int n;
        cin >> n;
        vi v(n);
        readv(v);
        map<int, int> arrows;

        for (const int &i : v) {
                if (arrows.count(i) && arrows[i] > 0) {
                        arrows[i] --;
                }
                arrows[i-1] ++;
        }

        int total = 0;
        for (auto &i : arrows) total += i.second;

        cout << total << '\n';

        return 0;
}
