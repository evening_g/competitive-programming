#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int n, s, t, ti, d;
    
    while (cin >> n) {
        if (n == -1) break;
        ti = d = 0;
        while (n--) {
            cin >> s >> t;
            d += s * (t - ti);
            ti = t;
        }
        cout << d << " miles\n";
    }

    return 0;
}
