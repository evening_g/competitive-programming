#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int t;
    cin >> t;

    while (t--) {
        int prev, curr, tot=0;
        cin >> prev;
        for (;;) {
            cin >> curr;

            if (!curr)
                break;

            tot += max(0, curr - prev*2);
            prev = curr;
        }

        cout << tot << '\n';
    }

    return 0;
}
