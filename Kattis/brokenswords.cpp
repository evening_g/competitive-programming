#include <bits/stdc++.h>

using namespace std;

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int n;
        cin >> n;
        string s;
        int h = 0, v = 0;
        while (n--) {
                cin >> s;
                v += 2 - ((s[0] - '0') + (s[1] - '0'));
                h += 2 - ((s[2] - '0') + (s[3] - '0'));
        }

        int num_swords = min(h, v) / 2;
        h -= (num_swords * 2);
        v -= (num_swords * 2);

        cout << num_swords << ' ' << v << ' ' << h << '\n';

        return 0;
}
