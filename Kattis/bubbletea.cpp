// ad hoc
// edge case

#include <bits/stdc++.h>
using namespace std;
using vi = vector<int>;
const int INF = 1000000000;
#define readv(v) for (auto &i : v) cin >> i

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        int n, m, x, money;
        cin >> n;
        vi tea(n);
        readv(tea);
        cin >> m;
        vi top(m);
        readv(top);

        int minprice = INF;

        for (int i = 0; i < n; i++) {
                cin >> m;
                while (m--) {
                        cin >> x;
                        x--;

                        minprice = min(minprice, tea[i]+top[x]);
                }
        }

        cin >> money;

        int total = max(0, money / minprice - 1);

        cout << total << '\n';

        return 0;
}
