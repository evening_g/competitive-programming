#include <bits/stdc++.h>
#define loop for(;;)
using namespace std;
using ll = long long;
int main() {
    int a, b, d;
    cin >> a >> b;
    bool swapped = false;
    
    // s is numerically smallest
    if (a > b) {
        swap(a, b);
        swapped = true;
    }

    // from `s` you can either go right or left
    int l = a + 360 - b;
    int r = b - a;

    if (l <= r)
        // going left is smaller
        // should have - but if (a, b) were swapped, then the sign is eliminated
        // also here we catch where they're opposite.
        if (swapped || l == r)
            d = l;
        else
            d = -l;
    else
        // going right is smaller
        // should have - only if they were swapped.
        if (!swapped)
            d = r;
        else
            d = -r;

    cout << d << '\n';

    return 0;
}
