#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    int n, y=0, m, d;
    cin >> n >> m;
    while (n--) {
        cin >> d;
        if (d > m) y++;
        else {
            printf("It hadn't snowed this early in %d years!\n", y);
            return 0;
        }
    }
    cout << "It had never snowed this early!\n";

    return 0;
}
