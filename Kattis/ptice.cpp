#include <bits/stdc++.h>
using namespace std;

using vi = vector<int>;

int solve(const string &ans, const string &guess) {
        int eqs = 0;

        int ansptr = 0;
        for (const char &c : ans) {
                if (c == guess[ansptr])
                        eqs ++;
                ansptr = (ansptr + 1) % guess.size();
        }

        return eqs;
}

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        
        vector<string> guesses = {"", "ABC", "BABC", "CCAABB"};
        vi scores(4, 0);
        vector<string> names = {"", "Adrian", "Bruno", "Goran"};

        int n;
        string ans;
        cin >> n >> ans;

        int best = 0;
        for (int i = 1; i <= 3; i++) {
                scores[i] = solve(ans, guesses[i]);
                if (scores[best] < scores[i]) {
                        best = i;
                }
        }

        cout << scores[best] << '\n';
        for (int i = 1; i <= 3; i++)
                if (scores[i] == scores[best])
                        cout << names[i] << '\n';
        return 0;
}
