/**
 * status: AC
 * source: https://open.kattis.com/problems/rings2
 * keywords: implicit-graph multiroot bfs
*/

#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;
using mll = vector<vector<ll>>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int N, M;

struct Point {
    int i;
    int j;

    Point(int _i, int _j) {
        i = _i;
        j = _j;
    }

    bool is_inside() {
        return i >= 0 and i < N and j >= 0 and j < M;
    }

    vector<Point> adj() {
        vector<Point> adjs = vector<Point>();

        int di[] = {0, 0, 1, -1};
        int dj[] = {1, -1, 0, 0};

        for (int k = 0; k < 4; k++) {
            adjs.push_back({i+di[k], j+dj[k]});
        }

        return adjs;
    }
};

queue<Point> roots;
int num_of_rings = 0;

void find_roots(mi &depth, const vector<string> grid) {
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++) {
            if (grid[i][j] == '.') continue;

            for (Point point : Point(i, j).adj()) {
                if (!point.is_inside() or grid[point.i][point.j] == '.') {
                    roots.push({i, j});
                    break;
                }
            }
            
        }  
}

void solve(mi &depth, const vector<string> &grid) {
    int attempts = N+1;

    while (attempts--)
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M; j++)
            {
                if (grid[i][j] != 'T') continue;

                int ring = INF;

                for (auto adj : Point(i, j).adj()) {
                    if (!adj.is_inside()) {
                        ring = min(ring, 0);
                    } else {
                        ring = min(ring, depth[adj.i][adj.j]);
                    }
                }

                depth[i][j] = ring + 1;
                num_of_rings = max(num_of_rings, ring + 1);
            }   
        }
    }
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    cin >> N >> M;

    vector<string> grid(N);
    readv(grid);
    mi depth(N, vi(M, 0));

    find_roots(depth, grid);
    solve(depth, grid);

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
        {
            if (grid[i][j] == '.')
            {
                if (num_of_rings >= 10)
                {
                    cout << "...";
                }
                else
                {
                    cout << "..";
                }
            }
            else
            {
                if (num_of_rings >= 10)
                {
                    if (depth[i][j] >= 10) {
                        cout << '.' << depth[i][j];
                    } else {
                        cout << ".." << depth[i][j];
                    }
                }
                else
                {
                    cout << '.' << depth[i][j];
                }
            }
        }
        cout << '\n';
    }
    

    return 0;
}
