/**
 * tags: matrix
 */
#include <bits/stdc++.h>
using namespace std;
using vi = vector<int>;
using mi = vector<vector<int>>;
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '
int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);
        int H, W, N, M;
        cin >> H >> W >> N >> M;
        mi mtx(H, vi(W));
        mi ker(N, vi(M));
        mi res(H - N + 1, vi(W - M + 1, 0));
        for (vi &v : mtx) readv(v);
        for (int i = N - 1; i >= 0; i--)
                for (int j = M - 1; j >= 0; j--)
                        cin >> ker[i][j];
        for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                        for (int k = 0; k < H - N + 1; k++)
                                for (int l = 0; l < W - M + 1; l++)
                                        res[k][l] += ker[i][j] * mtx[i + k][j + l];
        for (vi &v : res) {
                printv(v);
                cout << '\n';
        }
        return 0;
}
