#include <bits/stdc++.h>
using namespace std;

using ll = long long;

int main() {
    ll n, p, r, f, y;
    cin >> n;
    while (n--) {
        y = 0;
        cin >> p >> r >> f;
        while (p <= f) {
            p *= r;
            y ++;
        }
        cout << y << '\n';
    }

    return 0;
}
