#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vl = vector<ll>;
using vii = vector<pair<int, int>>;
using ii = pair<int, int>;
using mi = vector<vector<int>>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (ll &i : v) cin >> i
#define printv(v) for (int &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n, x;
    cin >> n >> x;
    vl coins(n);
    readv(coins);
    sort(all(coins));

    vl A(x+1);

    for (int r = n-1; r >= 0; r--) {
        for (int c = 1; c <= x; c++) {
            if (c - coins[r] < 0) {
                A[c] = 0;
            } else if (c - coins[r] == 0) {
                A[c] = (A[c] + 1) % MOD;
            } else {
                int col = c - coins[r];
                A[c] = (A[c] + A[col])% MOD;
            }
        }
    }

    cout << A[x] << '\n';

    return 0;
}
