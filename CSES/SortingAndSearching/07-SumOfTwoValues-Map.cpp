/*
You are given an array of n integers, and your task is to find two values (at distinct positions) whose sum is x.

Input

The first input line has two integers n and x: the array size and the target sum.

The second line has n integers a1,a2,…,an: the array values.

Output

Print two integers: the positions of the values. If there are several solutions, you may print any of them. If there are no solutions, print IMPOSSIBLE.

Constraints
1≤n≤2⋅105
1≤x,ai≤109
Example

Input:
4 8
2 7 5 1

Output:
2 4
*/

#include <bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int n, x, ai;
    cin>>n>>x;

    map<int, vector<int>> values;

    for (int i = 0; i < n; i++)
    {
        int a;
        cin >> a;

        values[a].push_back(i+1);
    }

    for (auto &i : values) {
        if (values.count(x-i.first)) {
            int a = i.second[0];
            int b = values[x-i.first][0];

            if (a == b) { 
                if (values[x-i.first].size() > 1) {
                    b = values[x-i.first][1];
                } else {
                    continue;
                }
            }

            cout << a << ' ' << b << '\n';
            return 0;
        }
    }

    cout << "IMPOSSIBLE\n";

    return 0;
}