/*
You are given an array of n integers, and your task is to find two values (at distinct positions) whose sum is x.

Input

The first input line has two integers n and x: the array size and the target sum.

The second line has n integers a1,a2,…,an: the array values.

Output

Print two integers: the positions of the values. If there are several solutions, you may print any of them. If there are no solutions, print IMPOSSIBLE.

Constraints
1≤n≤2⋅105
1≤x,ai≤109
Example

Input:
4 8
2 7 5 1

Output:
2 4
*/

#include <bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int n, x, ai;
    cin>>n>>x;

    pair<int, int> a[n];

    for (int i = 0; i < n; i++) {
        cin>> ai;
        a[i].first = ai;
        a[i].second = i + 1;
    }

    sort(a, a + n);

    // This are the pointers
    int left = 0;
    int right = n -1;

    while (left < right) {
        if (a[left].first + a[right].first == x) {
            cout<<a[left].second<<" "<<a[right].second<<'\n';
            break;
        } else if (a[left].first + a[right].first < x) {
            left ++;
        } else if (a[left].first + a[right].first > x) {
            right --;
        }        
    }
    if (left == right) {
        cout<<"IMPOSSIBLE"<<'\n';
    }
    
    return 0;
}