#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vull = vector<ull>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using pll = pair<int, int>;
using mi = vector<vector<int>>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (int &i : v) cin >> i
#define printv(v) for (int &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

vull machines;

ull product_output(ll t) {
    ull total_output = 0;
    for(ull &m : machines) total_output += (t / m);
    return total_output;
}

ull binary_search(ull l, ull r, ull t) {
    while(l < r) {
        ull m = (l+r)/2;

        ull output = product_output(m);

        if(output >= t) {
            r = m;
        } else {
            l = m + 1;
        }
    }

    if(product_output(l) < t) l++;

    return l;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    ull x, t;

    cin >> x >> t;

    machines.resize(x);
    for(ull &m : machines) cin >> m;

    ull l = 0;
    ull r = (ull) 10e20;

    ull ans = binary_search(l, r, t);

    cout << ans << "\n";

    return 0;
}
