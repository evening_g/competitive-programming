/*
source: https://cses.fi/problemset/task/1621/
date: 2021-10-16
*/
 
#include<iostream>
#include<algorithm>

using namespace std;
 
int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
 
    int n, r = 0;
 
    cin>>n;
 
    int array[n];
 
    for (int i = 0; i < n; i++)
    {
        cin>>array[i];
    }
    
    sort(array, array+n);
 
    for (int i = 0; i < n - 1; i++)
    {
        if (array[i] != array[i+1]) r ++;
    }
    
    cout<<r+1<<'\n';
 
    return 0;
}