/*
source: https://cses.fi/problemset/task/1074/
date: 2021-10-16
*/

#include<iostream>
#include<algorithm>
#include<math.h>
 
using namespace std;
 
int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
 
    int n;
    long long median, r = 0;
    cin>>n;
 
    int p [n];
    median = n/2;
 
    for (int i = 0; i < n; i++)
    {
        cin>>p[i];
    }
    
    sort(p,p+n);
    median = p[median];
 
    for (int i = 0; i < n; i++) {
        r += abs(p[i] - median);
    }
 
    cout<<r<<'\n';
 
    return 0;
}