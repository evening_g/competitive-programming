#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vull = vector<ull>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using pll = pair<int, int>;
using mi = vector<vector<int>>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (int &i : v) cin >> i
#define printv(v) for (int &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n, m, k;
    cin >> n >> m >> k;

    vi a(n);
    vi b(m);

    readv(a);
    readv(b);

    sort(all(a));
    sort(all(b));

    int total = 0;
    int i = 0, j = 0;
    while (i < n and j < m) {
        int upper = b[j]+k;
        int lower = b[j]-k;

        if (a[i] <= upper and a[i] >= lower) {
            total ++;
            i ++;
            j ++;
            continue;
        }

        if (a[i] < lower) {
            i++;
            continue;
        }

        if (a[i] > upper) {
            j++;
            continue;
        }
    }

    cout << total << '\n';

    return 0;
}
