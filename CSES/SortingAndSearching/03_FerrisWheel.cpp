/*
source: https://cses.fi/problemset/task/1090/
date: 2022-03-12

by Bernardo Hernández Hernández
*/

#include<bits/stdc++.h>

#define all(x) begin(x), end(x)

using namespace std;

/* Se utiliza const para ahorrar recursos
 * El vector se pasa por referencia para evitar hacer una copia y ahorrar recursos
*/
bool is_possible(const vector<int> &children_weights, const int max_weight, int gondola_amount) {
    vector<int> gondola(gondola_amount, 0);

    int next_child = 0;

    for (int i = 0; i < gondola_amount; i++) {
        if (next_child >= children_weights.size()) {
            break;
        }

        if (gondola[i] + children_weights[next_child] <= max_weight) {            
            gondola[i] += children_weights[next_child];
            next_child ++;
        }
    }

    reverse(all(gondola));

    for (int i = 0; i < gondola_amount; i++) {
        if (next_child >= children_weights.size()) {
            break;
        }

        if (gondola[i] + children_weights[next_child] <= max_weight) {
            gondola[i] += children_weights[next_child];
            next_child ++;
        }
    }
    
    return next_child >= children_weights.size();
}

int binary_search(const vector<int> &children_weights, const int max_weight, int left, int right) {
    int middle;
    while (left < right) {
        middle = (left + right) / 2;

        if (is_possible(children_weights, max_weight, middle)) {
            right = middle;
        } else {
            left = middle + 1;
        }
    }

    return left;
}

int main() {
    unsigned int n, max_weight, answer;

    vector<int> children_weights;

    cin>>n;
    cin>>max_weight;

    children_weights.resize(n);

    for (int &i : children_weights)
        cin>>i;
    
    // Sorting the vector
    // Option 1
    sort(all(children_weights), greater<int>()); // greater hace que se ordene de mayor a menor

    // Option 2
    /*sort(all(children_weights));
    reverse(all(children_weights)); // invierte los elementos del vector
    */

    answer = binary_search(children_weights, max_weight, 1, 2*int(1e5));

    cout<<answer<<'\n';
}