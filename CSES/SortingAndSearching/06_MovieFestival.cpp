#include <bits/stdc++.h>
using namespace std;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (auto &i : v) cin >> i
#define printv(v) for (auto &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n;
    cin >> n;

    vpi v(n);

    readv(v).second >> i.first;

    sort(all(v));
    
    int current_end = 0;
    int total_movies = 0;

    for (int i = 0; i < n; i++)
    {
        if (v[i].second >= current_end) {
            total_movies ++;
            current_end = v[i].first;
        }
    }
    
    cout << total_movies << '\n';

    return 0;
}
