#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace std;

using namespace __gnu_pbds;

using ll = long long;
using ull = unsigned long long;

using vi = vector<int>;
using vll = vector<ll>;
using vpi = vector<pair<int, int>>;
using pi = pair<int, int>;
using mi = vector<vector<int>>;

using indexed_multiset = tree<int,null_type,less_equal<int>,rb_tree_tag,
                              tree_order_statistics_node_update>;

using indexed_set = tree<int,null_type,less<int>,rb_tree_tag,
                         tree_order_statistics_node_update>;

const ll MOD = 1000000007;
const int INF = INT_MAX;

#define all(v) v.begin(), v.end()
#define loop while(true)
#define readv(v) for (int &i : v) cin >> i
#define printv(v) for (int &i : v) cout << i << ' '

#define lsz(x) ~(x) & (x+1)
#define lso(x) (x) & -(x)

/**
 * Divides the array `v` in subarrays
 * with a sum of atmost `limit`
 * @param v
 * @param limit
 * @returns the number of subarrays. 0 if not possible
*/
int divide(const vi &v, ull limit) {
    int count_of_divisions = 0;

    ull current_sum = v[0];
    for (size_t i = 1; i < v.size(); i++) {
        if (current_sum > limit) {
            return INF;
        }

        if (current_sum+v[i] > limit) {
            count_of_divisions ++;
            current_sum = 0;
        }

        current_sum += v[i];
    }

    if (current_sum > limit) {
        return INF;
    } else if (current_sum > 0) {
        count_of_divisions ++;
    }
    
    return count_of_divisions;
}

/**
 * Binary searches the limit 
 * until the number of divisions equals k
*/
ull lower_bound(const vi &v, int k)
{
    ull lower = 0, upper = (ull) 1e15, mid;
    int num_of_divisions;

    while (lower < upper) {
        mid = (lower + upper) / 2;

        num_of_divisions = divide(v, mid);

        if (num_of_divisions > k) {
            // The number of divisions is too big
            // We need to make it smaller by allowing
            // a bigger sum

            lower = mid + 1;
        } else {
            upper = mid - 1;
        }
    }

    num_of_divisions = divide(v, lower);
    if (num_of_divisions > k) {
        lower ++;
    }

    return lower;
}

int main() {
    cin.tie(0)->sync_with_stdio(0);
    cin.exceptions(cin.failbit);

    int n, k;

    cin >> n >> k;
    vi v(n);
    readv(v);

    cout << lower_bound(v, k) << '\n';

    return 0;
}
