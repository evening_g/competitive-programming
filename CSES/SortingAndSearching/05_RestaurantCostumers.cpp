/*
source: https://cses.fi/problemset/task/1619
date: 2022-02-26
*/

#include<bits/stdc++.h>

#define all(x) begin(x), end(x)

using ll = long long;

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data

    int n;                          // Amount of clients
    int arrival_to_analyse = 0;     // Arrival index to analize
    int departure_to_analyze = 0;   // Departure index to analize
    int people_in_restaurant = 0;   // Min people in the restaurant
    int max_in_restaurant = 0;      // Max amount of people in the restaurant
    vector<int> arrivals;           // List of arrivals times
    vector<int> departures;         // List of departures times
    
    // Reading total of clients
    cin>>n;
    
    // Process

    // Resizing the vectors so they will have the enough space
    arrivals.resize(n);
    departures.resize(n);

    // Reading arrival and departure time of each client
    for (int i = 0; i < n; i++)
        cin>>arrivals[i]>>departures[i];
    
    // Sorting the arrivals and departures
    sort(all(arrivals));
    sort(all(departures));

    // Analizing all the arrivals and departures with a two pointer algorithm
    // arrival_to_analize = 0;
    // departure_to_analize = 0;
    while (arrival_to_analyse < n and departure_to_analyze < n)
    {
        // if the current arrival time is smaller than the current departure time
        if (arrivals[arrival_to_analyse] < departures[departure_to_analyze]) {
            // So there is one more person in the restaurant as one arrived but none has left
            people_in_restaurant ++;

            // And we analize the next arrival time
            arrival_to_analyse ++;

        // if the current arrival is bigger than the current departure time
        } else if (arrivals[arrival_to_analyse] > departures[departure_to_analyze]) {
            // So there is one less person in the restaurant as one leaved and no one arrived
            people_in_restaurant --;

            // Analize the next departure
            departure_to_analyze ++;
        }

        // We save the people in the restaurant in this row only if it is bigger than the previous bigger
        max_in_restaurant = max(max_in_restaurant, people_in_restaurant);
    }
    
    // I DO NOT KNOW WHAT THIS IS FOR
    // The problem is accepted without this part
    /*while (arrival_to_analyse < n) {
        people_in_restaurant --;
        departure_to_analyze ++;
        
        max_in_restaurant = max(max_in_restaurant, people_in_restaurant);
    }*/
    
    // Print result
    cout<<max_in_restaurant<<'\n';
    
    
    return EXIT_SUCCESS;
}