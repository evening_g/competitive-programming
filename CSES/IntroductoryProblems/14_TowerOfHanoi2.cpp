/*
source: https://cses.fi/problemset/task/2165
date: 2022-03-05

The steps to move all the n rings from the beggining to the end
are the same than moving n-1 rings from the beginning to the auxiliar stake
then moving the n-th ring to the end
and moving the n-1 rings from the auxiliar to the end
*/

#include<bits/stdc++.h>

using namespace std;

/**
 * @brief Returns the steps to solve a hanoi tower of 'size' rings
 * 
 * Returns a vector with the steps to move all the 'size' rings of a Hanoi Tower
 * from the stack 'begin' to the stack 'end', being 'aux' the remaining stack
 * 
 * In each pair:
 * The first element is the stack from where we are going to take the upper ring
 * and the second element is the stack where we are going to place this ring
 * 
 * @param size 
 * @param begin 
 * @param end 
 * @param aux 
 * @return vector<pair<int, int>> 
 */
vector<pair<int, int>> solve_hanoi(int size, int begin, int end, int aux) {
    pair<int, int> step;
    if (size == 1) {    // base case
        /* For a tower of size 1, we are going to take this single ring
        and move it to the final position
        */
        vector<pair<int, int>> steps;

        step.first = begin;
        step.second = end;
        steps.push_back(step);

        return steps;

        // cout<<begin<<" -> "<<end<<'\n';
    } else {
        /* For a tower of size n, n > 1 */
        
        // Move the tower of size n-1 to the auxiliar stack
        vector<pair<int, int>> steps = solve_hanoi(size - 1, begin, aux, end);

        // Move the n-th ring from begin to end
        step.first = begin;
        step.second = end;
        steps.push_back(step);

        // cout<<begin<<" -> "<<end<<'\n';

        // Move the tower located in auxiliar, to the end.
        vector<pair<int, int>> temp_steps = solve_hanoi(size - 1, aux, end, begin);

        steps.insert(steps.end(), temp_steps.begin(), temp_steps.end());

        return steps;
    }
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Declare variables
    int size;
    vector<pair<int, int>> steps;

    // Read data
    cin>>size;

    // Solve
    steps = solve_hanoi(size, 1, 3, 2);

    // Print number of steps
    cout<<steps.size()<<'\n';

    // Print steps
    for (auto &step : steps)
        cout<<step.first<<" "<<step.second<<'\n';
       
    return EXIT_SUCCESS;
}