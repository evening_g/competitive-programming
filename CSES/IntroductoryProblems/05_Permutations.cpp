/*
Source:
https://cses.fi/problemset/task/1070
date: 2022-06-08
*/

#include<bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int n;

    cin>>n;

    // Process

    if (n ==2 or n == 3) {
        cout<<"NO SOLUTION\n";
        return 0;
    }

    // Print all the even numbers in [1, n]
    for (int i = 2; i <= n; i+=2)
        cout<<i<<' ';
    
    // Print all the odd numbers
    for (int i = 1; i <= n; i+=2)
        cout<<i<<' ';
    
    cout<<'\n';

    return 0;
}