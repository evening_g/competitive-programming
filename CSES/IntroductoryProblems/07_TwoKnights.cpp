/**
 * CSES task 1072
 * Two Knights
 * 2024-01-26
 *
 * AC
 * */

#include<iostream>

using ll = long long;

using namespace std;

ll solve(ll n) {
    return n*n*(n*n-1)/2-4*(n-1)*(n-2);
}

int main() {
    int n;
    cin >> n;

    int results [] = {-1, 0, 6, 28, 96};

    for (int i = 1; i <= n; i++) {
        if (i <= 4) {
            cout << results[i] << '\n';
        } else {
            cout << solve(i) << '\n';
        }
    }

    return 0;
}
