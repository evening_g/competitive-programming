# by Bernardo Hernandez Hernandez

def solve_hanoi(size, source, destination, auxiliary):
    if size == 1:
        solution = [[source, destination]]

        return solution
        
    # print(f"Mover el disco {size} desde {source} a {destination}")
    solution = solve_hanoi(size-1, source, auxiliary, destination)
    solution.append([source, destination])
    solution.extend(solve_hanoi(size-1, auxiliary, destination, source))

    return solution


def main():
    n = int(input())
    answer = solve_hanoi(n, 1, 3, 2)
    print(len(answer))

    for movement in answer:
        print(movement[0], movement[1])


if __name__ == "__main__":
    main()
