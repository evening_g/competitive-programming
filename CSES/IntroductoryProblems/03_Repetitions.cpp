/*
source: https://cses.fi/problemset/task/1069
date: 2022-03-05
*/

#include<bits/stdc++.h>

#define all(x) begin(x), end(x)

using ll = long long;

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);   

    // Define variables
    char current, past;
    int longest_repetition = 1, current_repetition = 1;

    // read data
    cin>>past;
    while (cin>>current) {
        if (past == current) {
            current_repetition ++;
        } else {
            if (current_repetition > longest_repetition) {
                longest_repetition = current_repetition;
            }
            current_repetition = 1;            
        }
        past = current;
    }

    if (current_repetition > longest_repetition) {
        longest_repetition = current_repetition;
    }
    
    cout<<longest_repetition<<'\n';
       
    return EXIT_SUCCESS;
}