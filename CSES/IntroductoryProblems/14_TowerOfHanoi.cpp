/*
source: https://cses.fi/problemset/task/2165
date: 2022-03-05

The steps to move all the n rings from the beggining to the end
are the same than moving n-1 rings from the beginning to the auxiliar stake
then moving the n-th ring to the end
and moving the n-1 rings from the auxiliar to the end

This program only prints the steps
*/

#include<bits/stdc++.h>

using namespace std;

void solve_hanoi(int size, int begin, int end, int aux) {
    pair<int, int> step;
    if (size == 1) {    // base case
        cout<<begin<<" -> "<<end<<'\n';
    } else {
        solve_hanoi(size - 1, begin, aux, end);
        
        cout<<begin<<" -> "<<end<<'\n';

        solve_hanoi(size - 1, aux, end, begin);
    }
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int size;

    cin>>size;

    solve_hanoi(size, 1, 3, 2);
       
    return EXIT_SUCCESS;
}