/*
source: https://cses.fi/problemset/task/1094
date: 2022-02-26

DOESN'T WORK

TO DO:
Find another solution
Find if there is a math formula to solve this in linear time
*/

#include<bits/stdc++.h>

#define fori(i,b,n) for (int i = b; i < n; i++)

#define ll long long

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // DECLARE VARIABLES
    int n;
    ll moves = 0;

    // READ DATA
    cin>>n;

    ll arr[n];

    fori(i, 0, n) {
        cin>>arr[i];
    }

    // PROCESS

    fori(i, 1, n) {        
        if (arr[i-1] > arr[i]) {
            moves += arr[i-1] - arr[i];
            arr[i] = arr[i-1];
        }
    }

    cout<<moves<<'\n';
       
    return EXIT_SUCCESS;
}