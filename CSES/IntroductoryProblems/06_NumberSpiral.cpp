/**
 * https://cses.fi/problemset/task/1071
 * 2023-03-13
 *
 * Number Spiral
 *
 * A number spiral is an infinite grid that grows this way
 *
 *  1  2  9 10 25
 *  4  3  8 11 24
 *  5  6  7 12 23
 * 16 15 14 13 22
 * 17 18 19 20 21
 *
 * INPUT
 * t = number of tests
 * y x = position
 *
 * layer = max(x, y)
 *
 * if layer is odd, then the layer ends at (layer, 1) (the top) with layer^2
 * then
 *      n = layer^2
 *
 *      n -= (y - 1)
 *      n -= (layer - x)
 *
 * if layer is even, then the layer ends at (1, layer) with layer^2
 * then
 *      n = layer^2
 *
 *      n -= (x - 1)
 *      n -= (layer - y)
 * * */
 
#include <bits/stdc++.h>
 
using namespace std;
 
using u64 = unsigned long long;
 
u64 number_spiral(u64 x, u64 y) {
    u64 layer = max(x, y);
 
    u64 number = layer * layer;
    
    if (layer % 2) {    // Odd
        number -= (layer + y - x - 1);
    } else {            // Even
        number -= (layer + x - y - 1);
    }
 
    return number;
}
 
int main() {
    u64 t, x, y;
 
    cin >> t;
 
    while (t--) {
        cin >> y >> x;
 
        cout << number_spiral(x, y) << '\n';
    }
 
    return 0;
}
