/*
source: https://cses.fi/problemset/task/1083/
date: 2021-10-15
*/

#include<iostream>
 
using namespace std;
 
int main()
{
    int n, x;
    int sum_of_num = 0;
    cin>>n;
    int sum = n;
 
    for(int i=1; i<n; i++) {
        cin>>x;
        sum_of_num += x;
        sum += i;
    }
 
    cout<<sum-sum_of_num<<endl;
}