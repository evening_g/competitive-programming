/*
source: https://cses.fi/problemset/task/1068
date: 2022-02-26
*/

#include<bits/stdc++.h>
using ll = long long;
// It's the same than
// typedef long long ll but it is old

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    ll n;

    cin>>n;
    
    // Process
    while (n != 1)
    {
        cout<<n<<' ';
        if (n % 2 == 0) {
            n /= 2;
        } else
        {
            n = n * 3 + 1;
        }
    }
    
    
    // Print result
    cout<<"1\n";
    
    return EXIT_SUCCESS;
}