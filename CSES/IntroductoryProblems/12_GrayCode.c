#include <stdio.h>

#define u32 unsigned int
#define u64 unsigned long long

int main()
{
    u32 n;
    scanf("%d", &n);

    for (u64 i = 0; i < 1 << n; i++)
    {
        printf("%0*llb\n", n, (i ^ i>>1));
    }

    return 0;
}

