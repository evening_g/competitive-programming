/*
source: https://cses.fi/problemset/task/1192/
date: 2022-04-30
*/

#include<bits/stdc++.h>

#define all(x) begin(x), end(x)

using ll = long long;

using namespace std;

/**
 * Returns if the index is valid or not
 * */
bool valid_index (
    const vector<vector<char>> &floor_map,
    int i,
    int j
) {
    return (
        i >= 0
        and i < floor_map.size()
        and j >= 0
        and j < floor_map.front().size()
    );
}

/**
 * Deep first search
 * 
 * bool cpp 8 bit
 * char cpp 8 bit
 * We can use char instead of bool
 * Vector bool aren't vectors and are unneficient
 * */
void dfs (
    const vector<vector<char>> &floor_map,
    vector<vector<char>> &visited,
    int i,
    int j
) {
    if (not valid_index(floor_map, i, j) or visited[i][j])
        return;

    visited[i][j] = true;

    if (floor_map[i][j] == '#') {
        return;
    }

    int dx[] = {0, 1, 0, -1};
    int dy[] = {1, 0, -1, 0};

    for (int k = 0; k < 4; k++) {
        dfs(floor_map, visited, i + dy[k], j + dx[k]);
    }
    
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);  

    // variables
    int n, m, room_count = 0;

    // read map size
    cin>>n>>m;

    vector<vector<char>> floor_map(n, vector<char> (m));
    vector<vector<char>> visited(n, vector<char> (m, false));

    // read the map
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin>>floor_map[i][j];
        }        
    }
    
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (
                not visited[i][j]
                and floor_map[i][j] == '.'
            ) {
                room_count ++;
                dfs(floor_map, visited, i, j);
            }
        }
    }

    cout<<room_count<<'\n';
       
    return EXIT_SUCCESS;
}