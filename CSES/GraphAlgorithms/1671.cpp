
// keywords: graph dijsktra

#include <bits/stdc++.h>

using namespace std;

using vi = vector<int>;
using vpi = vector<pair<int, int>>;
using mi = vector<vector<int>>;

using ll = long long;
using pi = pair<ll, ll>;

const ll INF = 100000000000000;

vector<vector<pi>> adj;

void dijkstra(int s, vector<ll> &d) {
        
        ll n = adj.size();
        d.assign(n, INF);

        d[s] = 0;

        priority_queue<pi> pq;
        pq.push({0, s});

        while(!pq.empty()) {
                ll w = - pq.top().first;
                ll v = pq.top().second;
                pq.pop();

                if(w != d[v]) {
                        continue;
                }

                for (auto e : adj[v]) {
                        ll to = e.first;
                        ll len = e.second;

                        if(d[v] + len < d[to]) {
                                d[to] = d[v] + len;
                                pq.push({-d[to], to});
                        }
                }
        }

}

int main() {
        cin.tie(0)->sync_with_stdio(0);
        cin.exceptions(cin.failbit);

        ll n, m;
        cin >> n >> m;

        adj.resize(n + 1);

        for(int i = 0; i < m; ++i) {
                ll u, v;
                ll w;
                cin >> u >> v >> w;
                adj[u].push_back({v, w});
        }

        vector<ll> dist(n);

        dijkstra(1, dist);

        for(int i = 1; i <= n; ++i) {
                cout << dist[i] << " ";
        }
        cout << "\n";

        return 0;
}
