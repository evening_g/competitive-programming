#include<bits/stdc++.h>

using namespace std;

bool sortbysec(const pair<string,int> &a, const pair<string,int> &b){
    return(a.second < b.second);
}

struct persona {
    string name;

    unordered_map <string, int> lang;

    bool disponible = 1;
};

struct project{
    string name;
    int days, score, best_date, roles;

    unordered_map <string, int> req;
};

int main() {

    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
    //freopen("./input_data/input.txt", "r", stdin);

    bool found;
    int contributors, nProjects;

    cin>>contributors>>nProjects;
    vector<persona> equipo (contributors);
    vector<project> proyectos (nProjects);

    for(int i=0; i<contributors; ++i){
        int nSkills;
        cin>>equipo[i].name>>nSkills;
        for(int j=0; j<nSkills; ++j){
            string langName;
            int skillLvl;
            cin>>langName>>skillLvl;
            equipo[i].lang.insert({langName, skillLvl});
        }
    }

    for(int i=0; i<nProjects; ++i){
        cin>>proyectos[i].name>>proyectos[i].days>>proyectos[i].score>>proyectos[i].best_date>>proyectos[i].roles;
        for(int j=0; j<proyectos[i].roles; ++j){
            string langName;
            int skillLvl;
            cin>>langName>>skillLvl;
            proyectos[i].req.insert({langName, skillLvl});
        }
    }

    cout<<nProjects<<'\n';
    for(int i=0; i<nProjects; ++i){
        cout<<proyectos[i].name<<"\n";
        //for(int j=0; j<proyectos[i].roles; ++j){
        for(pair<string,int> l : proyectos[i].req){
            vector<pair<string,int>> posibles;
            found = false;// Added after submit
            for(auto &p : equipo){
                //cout<<l.first<<" ";
                if(p.lang.find(l.first) != p.lang.end() && p.disponible){
                    // Added after submit
                    if (l.second <= p.lang[l.first]) {
                        posibles.push_back({p.name, p.lang[l.first]});
                        p.disponible=0;
                        found = true;
                    }                        
                }
            }
            if (found) {
                sort(posibles.begin(),posibles.end(),sortbysec);
                string selected = posibles[0].first;
                cout<<selected<<" ";
            }                
        }
        for(auto &p : equipo){ p.disponible=1; }

        if (found)
            cout<<"\n";
        //}
    }

    return 0;
}