#include <bits/stdc++.h>
using namespace std;

struct wall{
    int x1;
    int y1;
    int x2;
    int y2;
    int Qi;
    bool active;
};

int bin_search_Q(const vector<wall> &v, int n, int x){
    int low=0, high=n-1;
    while(low<=high){
        int mid = (low+high) / 2;
        if(v[mid].Qi == x){
            return mid;
        } else if(v[mid].Qi > x){
            high = mid - 1;
        } else {
            low = mid + 1;
        }
    }
    return -1;
}

bool is_inside(const wall &w, int x, int y){
    if(w.x2 > x && w.y2 > y && w.x1 < x && w.y1 < y){
        return true;
    }

    return false;
}

int main(){
    ios::sync_with_stdio(0); 
    cin.tie(0);

    int Q;
    cin >> Q;

    vector<wall> walls;

    for(int i=0; i<Q; ++i){
        int op;
        cin >> op;
        switch(op){
            case 1:
            {
                wall w;
                cin >> w.x1 >> w.y1 >> w.x2 >> w.y2;
                w.active = true;
                w.Qi = i+1;
                walls.push_back(w);
            }
            break;
            
            case 2:
            {
                int Q_to_delete;
                cin >> Q_to_delete;
                int Q_to_delete_i = bin_search_Q(walls, walls.size(), Q_to_delete);
                walls[Q_to_delete_i].active = false;
            }
            break;

            case 3:
            {
                bool path_exists = true;
                int xb, yb, xp, yp;
                cin >> xb >> yb >> xp >> yp;
                for(auto &w : walls){
                    if(w.active){
                        bool b_inside = is_inside(w, xb, yb);
                        bool p_inside = is_inside(w, xp, yp);
                        path_exists =  b_inside == p_inside;
                    }
                    if(!path_exists) break;
                }
                char ans = path_exists? 'Y' : 'N';
                cout << ans;
            }
            
        }
    }
    
    return 0;
}
