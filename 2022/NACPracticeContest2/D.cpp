/*
source: https://open.kattis.com/contests/nac22practice2/problems/dreamer
date: 2022-04-30
*/

#include<bits/stdc++.h>

using namespace std;

short to_num(const char *date, short l, short r) {
    short num = 0;

    short p = 1;
    for (int i = r; i >= l; i--) {
        num += (date[i] - '0') * p;
        p *= 10;
    }
    
    return num;
}

bool is_valid_date(const char *date) {
    short year = to_num(date, 4, 7);
    short month = to_num(date, 2, 3);
    short day = to_num(date, 0, 1);
    
    bool is_leap_year = false;
    if (
        year % 400 == 0
        or (
            year % 4 == 0 
            and year % 100 != 0
        )        
    ) {
        is_leap_year = true;
    }

    short day_limits [12] = {31, short(is_leap_year ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    
    if (
        year >= 2000
        and month >= 1
        and month <= 12
    ) {
        if (day >= 1 and day <= day_limits[month-1]) {
            return true;
        }
    }

    return false;
}

/* Returns
1 if a > b
0 if a = b
-1 if a < b
*/
int date_compare(const char *date_a, const char *date_b) {
    short year_a = to_num(date_a, 4, 7);
    short month_a = to_num(date_a, 2, 3);
    short day_a = to_num(date_a, 0, 1);

    short year_b = to_num(date_b, 4, 7);
    short month_b = to_num(date_b, 2, 3);
    short day_b = to_num(date_b, 0, 1);

    if (year_a > year_b) {
        return 1;
    } else if (year_a < year_b) {
        return -1;
    } else {    // years are equal
        if (month_a > month_b) {
            return 1;
        } else if (month_a < month_b) {
            return -1;
        } else {    // months are equal
            if (day_a > day_b) {
                return 1;
            } else if (day_a < day_b) {
                return -1;
            } else {    // days are equal
                return 0;
            }
        }
    }
}

void copy_date(const char *source, char *destination) {
    for (int i = 0; i < 8; i++) {
        destination[i] = source[i];
    }
}

void solve() {
    // read date
    char date [8] = {'\0'};
    char smallest[8] = {'\0'};
    bool initialized = false;
    unordered_set<string> valid_dates;

    for (char &c : date) {
        cin>>c;
    }

    // create all possible permutations
    // permutations = 8! = 40320
    for (int i = 0; i < 40320; i++) {
        if (is_valid_date(date)) {
            string current_date = (date);

            int prev_size = valid_dates.size();
            
            valid_dates.insert(current_date);

            if (prev_size < valid_dates.size()) {
                cout<<"Inserted valid date: "<< current_date <<"\n";
            }

            if (initialized) {
                // checks if this is smaller than the previous
                if (date_compare(date, smallest) < 0) {
                    copy_date(date, smallest);
                }
            } else {
                // initializes the smallest with this date                
                copy_date(date, smallest);
                initialized = true;
            }
        }

        // generates next permutation
        next_permutation(date, date+8);
    }

    cout<<"Valid dates: "<<valid_dates.size()<<"\nSmallest: ";

    if (initialized) {
        for (int i = 0; i < 8; i++) {
            cout<<smallest[i];
            if (i == 1 or i == 3) {
                cout<<' ';
            }
        }
    }
    cout<<'\n';
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // read cases
    int t;
    cin>>t;

    // for each test
    while (t--) {
        solve();
    }
       
    return EXIT_SUCCESS;
}
