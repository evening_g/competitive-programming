from itertools import permutations

def is_valid_date(date):
    year = int(date[4:8])
    month = int(date[2:4])
    day = int(date[0:2])

    # print(f"year = {year}, month = {month}, day = {day}")

    is_leap_year = False
    if year % 400 == 0 or (year % 4 == 0 and year % 100 != 0):
        is_leap_year = True

    day_limits = [31, 29 if is_leap_year else 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    if year >= 2000 and month >= 1 and month <= 12:
        if day >= 1 and day <= day_limits[month-1]:
            return True


def date_compare(date_a, date_b):
    year_a = int(date_a[4:8])
    month_a = int(date_a[2:4])
    day_a = int(date_a[0:2])

    year_b = int(date_b[4:8])
    month_b = int(date_b[2:4])
    day_b = int(date_b[0:2])

    if year_a > year_b:
        return 1
    elif year_a < year_b:
        return -1
    else:
        if month_a > month_b:
            return 1
        elif month_a < month_b:
            return -1
        else:
            if day_a > day_b:
                return 1
            elif day_a < day_b:
                return -1
            else:
                return 0        
    

def solve():
    date = input().replace(" ", "")
    smallest = ""
    valid_dates = {""}

    chars = (c for c in date)

    dates = permutations(chars)

    for date_p in dates:
        date = ""
        for c in date_p:
            date += c

        if is_valid_date(date):
            # print(date)
            valid_dates.add(date)

            if smallest == "":
                smallest = date
            elif date_compare(date, smallest) < 0:
                # print("Smallest", smallest[0:2], smallest[2:4], smallest[4:8])
                smallest = date

    print(len(valid_dates)-1, end="")
    if len(valid_dates) == 1:
        print()
    else:
        print("", smallest[0:2], smallest[2:4], smallest[4:8])



def main():
    t = int(input())
    
    for _ in range(t):
        solve()


if __name__ == "__main__":
    main()
