#include <bits/stdc++.h>

#define all(x) x.begin(), x.end()

using namespace std;

using ll = long long;

bool is_valid(int i, int j, int N, int M){
    return i >= 0 && i < N && j >= 0 && j < M;
}

int main()
{
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    int N, M;
    cin >> N >> M;

    int r1, c1, r2, c2;
    cin >> r1 >> c1 >> r2 >> c2;

    vector<vector<int>> qdr(N, vector<int> (M));

    for(int i=0; i<N; ++i){
        for(int j=0; j<M; ++j){
            cin >> qdr[i][j];
        }
    }

    vector<vector<int>> dist(N, vector<int> (M, INT_MAX));
    vector<vector<char>> processed(N, vector<char> (M, false));
    priority_queue<pair<int,pair<int,int>>> q;

    dist[r1-1][c1-1] = 1;
    q.push({1, {r1-1, c1-1}});
    while(!q.empty()){
        int vr = q.top().second.first;
        int vc = q.top().second.second;

        q.pop();

        if(processed[vr][vc]) continue;
        processed[vr][vc] = true;
        stamps.insert(qdr[vr][vc]);

        int dr[] = {-1, 0, 1, 0};
        int dc[] = {0, 1, 0, -1};
        for(int i=0; i<4; ++i){
            int wr = vr + dr[i]; 
            int wc = vc + dc[i];
            if(is_valid(wr, wc, N, M)){
                int d = dist[vr][vc];
                if(stamps.find(qdr[wr][wc]) == end(stamps)){
                    d++;
                }
                if(d < dist[wr][wc]){
                    dist[wr][wc] = d;
                    q.push({-dist[wr][wc], {wr, wc}});
                }
            }
        }
    }

    cout << '\n';
    for(int i=0; i<N; ++i){
        for(int j=0; j<M; ++j){
            cout << dist[i][j] << ' ';
        }
        cout << '\n';
    }

    cout << dist[r2-1][c2-1] << '\n';

    return 0;
}
