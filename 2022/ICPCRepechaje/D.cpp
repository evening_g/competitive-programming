#include <bits/stdc++.h> 

#define all(x) x.begin(), x.end()

using namespace std;

using ll = long long;

const ll INF = 10e15;

int main()
{
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    vector<ll> nums;
    map<ll, int> s;

    int m, op, x, i;

    cin >> m;

    while (m--) {

        cin >> op;

        switch (op)
        {
        case 1:
            cin >> x;
            nums.push_back(x);
            s[x]++;

            break;

        case 2:
            cin >> i;
            if(s[nums[i-1]] > 0){
                s[nums[i-1]]--;
            }

            break;

        case 3:
            cin >> i >> x;
            if(s[nums[i-1]] > 0){
                s[nums[i-1]]--;
                s[nums[i-1]+x]++;
            }

            break;

        case 4:
            cin >> i;
            ll min = 0;

            for (auto &p : s) {
                if(p.first >= nums[i-1]){
                    break;
                }
                min += p.second;
            }

            cout << min << '\n';

            break;
        }
    }

    return 0;
}
