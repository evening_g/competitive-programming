#include <bits/stdc++.h>

#define all(x) x.begin(), x.end()

using namespace std;

using ll = long long;

const ll INF = 10e15;

int main()
{
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    vector<ll> nums;

    int m, op, x, i;

    cin >> m;

    while (m--) {

        cin >> op;

        switch (op)
        {
        case 1:
            cin >> x;
            nums.push_back(x);

            break;

        case 2:
            cin >> i;
            nums[i-1] = INT64_MAX;

            break;

        case 3:
            cin >> i >> x;
            nums[i-1] += x;

            break;

        case 4:
            cin >> i;
            ll min = 0;

            for (const ll &num : nums) {
                if (num < nums[i-1]) min++;
            }

            cout << min << '\n';

            break;
        }
    }

    return 0;
}
