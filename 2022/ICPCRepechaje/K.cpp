#include <bits/stdc++.h>

#define all(x) x.begin(), x.end()

using namespace std;

using ll = long long;

char c2num(char c)
{
    if (c >= 'a' and c <= 'c')
        return '2';

    if (c >= 'd' and c <= 'f')
        return '3';

    if (c >= 'g' and c <= 'i')
        return '4';

    if (c >= 'j' and c <= 'l')
        return '5';

    if (c >= 'm' and c <= 'o')
        return '6';

    if (c >= 'p' and c <= 's')
        return '7';

    if (c >= 't' and c <= 'v')
        return '8';

    return '9';
}

int main()
{
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    map<string, int> v;
    int ids, n;

    cin >> ids >> n;

    while (ids--) {
        string id;
        cin >> id;
        for(char &c: id){
            c = c2num(c);
        }
        v[id]++;
    }

    while (n--) {
        string num;
        cin >> num;
        if(v.find(num) != v.end()){
            cout << v[num] << '\n';
        }else{
            cout << '0' << '\n';
        }
    }

    return 0;
}
