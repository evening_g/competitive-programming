#include <bits/stdc++.h>

#define all(x) x.begin(), x.end()

using namespace std;

using ll = long long;

int main()
{
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    ll n, m;

    cin >> n >> m;

    vector<ll> qs(n);

    for (ll &i : qs) {
        cin >> i;
    } 

    sort(all(qs));

    vector<ll> sums(n);
    sums[0] = qs[0];
    for(ll i=1; i<n; i++){
        sums[i] = sums[i-1] + qs[i];
    }

    while (m--) {
        ll s, sum = 0;
        cin >> s;

        auto it = lower_bound(all(qs), s);
        int index = it - qs.begin();

        sum += ((n-index)*s);
        if(index > 0){
            sum += sums[index-1];
        }

        cout << sum << '\n';    
    }

    return 0;
}
