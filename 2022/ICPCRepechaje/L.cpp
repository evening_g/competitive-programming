// status: WA
// keywords: graph
// link: https://codeforces.com/gym/104120/problem/L

#include <bits/stdc++.h>

#define all(x) x.begin(), x.end()

using namespace std;

using ll = long long;

set<int> explored;

bool dfs(int u, int &n, int x, const vector<vector<int>> &adj) {
    if (u == x) return true;

    explored.insert(u);

    bool result = false;

    for (int v : adj[u]) {
        if (explored.find(v) == explored.end()) {
            bool found = dfs(v, n, x, adj);
            if (!found) {
                n += 2;
            } else {
                n += 1;
                result = true;
            }
        }            
    }

    return result;
}

int main()
{
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    int n, x, u, v;

    cin >> n >> x;

    vector<vector<int>> adj(n, vector<int>(0));

    for (int i = 0; i < n - 1; i++)
    {
        cin >> u >> v;

        u --;
        v --;

        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    int path = 0;

    dfs(0, path, x, adj);
    
    cout << path << '\n';

    return 0;
}
