#include <bits/stdc++.h>

#define all(x) x.begin(), x.end()

using namespace std;

using ll = long long;

int main()
{
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    int steps;

    cin >> steps;

    int minutes = ceil(3000.0 / (float) steps);

    if (minutes > 15)
        minutes = 15;

    cout << minutes << '\n';

    return 0;
}
