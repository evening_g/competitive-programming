/**
 * source: https://codeforces.com/gym/104120/problem/B
 * status: WA
 * keywords: implicit-graph
 * */

#include <bits/stdc++.h>

#define all(x) x.begin(), x.end()

using namespace std;

using ll = long long;

bool is_valid(int i, int j, int N, int M){
    return i >= 0 && i < N && j >= 0 && j < M;
}

int dfs(vector<vector<int>> &qdr, int r, int c, int r2, int c2, int N, int M, set<int> &tickets, vector<vector<char>> &visited){

    if(visited[c][r]){
        return 0;
    }
    
    bool past = tickets.find(qdr[r][c]) != tickets.end();
    visited[r][c] = true;

    if(r == r2 && c == c2){
        if(!past){
            return 0;
        }
        return 1;
    }

    if(!past){
        tickets.insert(qdr[r][c]);
    }

    int u = INT_MAX;
    if(is_valid(r-1, c, N, M)){
        u = dfs(qdr, r-1, c, r2, c2, N, M, tickets, visited);
    }

    int ri = INT_MAX;
    if(is_valid(r, c+1, N, M)){
        ri = dfs(qdr, r, c+1, r2, c2, N, M, tickets, visited);
    }

    int d = INT_MAX;
    if(is_valid(r+1, c, N, M)){
        d = dfs(qdr, r+1, c, r2, c2, N, M, tickets, visited);
    }

    int l = INT_MAX;
    if(is_valid(r, c-1, N, M)){
        l = dfs(qdr, r, c-1, r2, c2, N, M, tickets, visited);
    }

    int mini = INT_MAX;
    mini = min(mini, u);
    mini = min(mini, l);
    mini = min(mini, d);
    mini = min(mini, ri);

    if(!past){
        tickets.erase(qdr[r][c]);
        return mini + 1;
    }

    return mini;
}

int main()
{
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    int N, M;
    cin >> N >> M;

    int r1, c1, r2, c2;
    cin >> r1 >> c1 >> r2 >> c2;

    vector<vector<int>> qdr(N, vector<int> (M));
    vector<vector<char>> visited(N, vector<char> (M, false));

    for(int i=0; i<N; ++i){
        for(int j=0; j<M; ++j){
            cin >> qdr[i][j];
        }
    }

    set<int> tickets;

    cout << dfs(qdr, r1-1, c1-1, r2-1, c2-1, N, M, tickets, visited) << '\n';

    return 0;
}
