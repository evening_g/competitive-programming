#include<bits/stdc++.h>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int n, m;

    cin >> n >> m;

    // Print result
    cout << n / m + (n%m != 0) << '\n';


    return 0;
}