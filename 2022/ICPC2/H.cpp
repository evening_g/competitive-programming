#include<bits/stdc++.h>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int n, x;

    cin >> n >> x;

    unordered_set<int> total;
    vector<int> funny(n);

    for (int &i : funny) 
        cin >> i;

    for (int &i : funny) {
        for (int j = i; j <= x; j+=i)
        {
            total.insert(j);
        }
        
    }

    // Print result
    cout << total.size() << '\n';


    return 0;
}