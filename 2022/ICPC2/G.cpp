#include<bits/stdc++.h>

using namespace std;

using ll = long long;
using ull = unsigned long long;

struct train {
    int pos;
    ll time;
};

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int N;

    cin >> N;

    int lim = 2*N-1;
    vector<int> path (lim);

    for(int i = 1; i < lim; i += 2){
        cin >> path[i];
    }

    for(int i = 0; i < lim; i += 2){
        cin >> path[i];
    }

    train l = {0,0}, r = {lim-1, 0};
    int total_time = 0;

    while(l.pos <= r.pos) {
        if(l.pos == r.pos && l.pos % 2 == 0) {
            total_time = min(abs(l.time + path[l.pos] - r.time),
                             abs(r.time + path[r.pos] - l.time));
            break;
        }

        if (l.time + path[l.pos] <= r.time + path[r.pos]) {
            l.time += path[l.pos];
            l.pos ++;
        } else {
            r.time += path[r.pos];
            r.pos --;
        }
    }

    cout << total_time << '\n';

    return 0;
}