#include<bits/stdc++.h>

using namespace std;

using ll = long long;
using ull = unsigned long long;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    // Define variables and read data
    int N, M;

    cin >> N >> M;

    map<int, set<int>> student_requested;
    map<int, set<int>> student_granted;
    set<int> students;

    

    for(int i = 0; i < N; i++) {
    
        int a, b;
        cin >> a >> b;
        students.insert(a);

        for(int j = 0; j < b; ++j) {
            int c;
            cin >> c;
            student_requested[a].insert(c);
        }
    }

    for(int i = 0; i < M; i++) {
    
        int a, b;
        cin >> a >> b;
        students.insert(a);

        for(int j = 0; j < b; ++j) {
            int c;
            cin >> c;
            student_granted[a].insert(c);
        }
    }


    int errors = 0, extra_c = 0, missing_c = 0;
    for(int s : students){

        set<int> missing;
        set_difference(student_requested[s].begin(), student_requested[s].end(), student_granted[s].begin(), student_granted[s].end(), inserter(missing, missing.end()));
        set<int> extra;
        set_difference(student_granted[s].begin(), student_granted[s].end(), student_requested[s].begin(), student_requested[s].end(), inserter(extra, extra.end()));
        
        set<int> all;
        all.insert(missing.begin(), missing.end());
        all.insert(extra.begin(), extra.end());

        if(all.size() == 0){
            continue;
        }

        errors++;
        extra_c += extra.size();
        missing_c += missing.size();

        cout << s;
        for(int x: all) {
            if(missing.count(x)) {
                cout << " +";
            } else {
                cout << " -";
            }
            cout << x;
        }
        cout << '\n';
    }

    if(errors == 0) {
        cout << "GREAT WORK! NO MISTAKES FOUND!\n";
    } else {
        cout << "MISTAKES IN " << errors << " STUDENTS: " << extra_c << " NOT REQUESTED, " << missing_c << " MISSED\n";
    }

    return 0;
}