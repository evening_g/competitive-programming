#include<bits/stdc++.h>

using namespace std;

struct building {
    int s;
    int w;
    int h;
};

int partition(struct building *a, int l, int r) {
    int p = l;

    for (int i = l; i <= r; i++) {
        if (a[i].s <= a[r].s) {
            swap(a[i], a[p]);
            p ++;
        }
    }
    
    return p - 1;
}

void quicksort(struct building *a, int l, int r) {
    if (l < r) {
        int p = partition(a, l, r);
        quicksort(a, l, p-1);
        quicksort(a, p+1, r);
    }
}

void swap(struct building *a, struct building *b) {
    struct building c = *a;
    *a = *b;
    *b = c;
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int pos [] = {0, 0};

    int d = 100;
    int b, s, w, h;

    cin>>b;
    struct building buildings [b];

    for (int i = 0; i < b; i++)
        cin>>buildings[i].s>>buildings[i].w>>buildings[i].h;
    

    quicksort(buildings, 0, b - 1);

    for (int i = 0; i < b; i++) {
        // If the buildings are together
        if (pos[0] == buildings[i].s) {
            // Calculate the difference of height
            d += abs(buildings[i].h - pos[1]);
        } else {
            // Gets down of the last building climbs this one
            d += buildings[i].h + pos[1];
        }

        pos[0] = buildings[i].s + buildings[i].w;
        pos[1] = buildings[i].h;
    }

    // Get down of the last building
    d += pos[1];

    cout<<d<<"\n";
       
    return 0;
}