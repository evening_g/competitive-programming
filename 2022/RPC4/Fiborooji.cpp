#include<bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    short a, b, x, y, z;
    int i = 2;
    cin>>a>>b;

    x = a;
    y = b;

    do
    {
        z = x + y;
        z %= 10;
        x = y;
        y = z;
        i ++;
    } while (x != a or y != b);

    cout<<i<<'\n'; 
       
    return 0;
}