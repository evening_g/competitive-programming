#include<bits/stdc++.h>

#define all(x) begin(x), end(x)

using ll = long long;

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);

    int practices, students, brownies, groups, group_size;
    cin>>practices;

    for (int i = 1; i <= practices; i++) {    
        cin>>students>>brownies;

        cout<<"Practice #"<<i<<": "<<students<<" "<<brownies<<"\n";

        cin>>groups;
        while (groups--) {
            cin>>group_size;

            while (group_size >= brownies) {
                brownies *= 2;
            }

            brownies -= group_size;

            cout<<group_size<<" "<<brownies<<"\n";
        }        
    }
    
       
    return 0;
}