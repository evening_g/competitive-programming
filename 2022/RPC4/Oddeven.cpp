#include <bits/stdc++.h>
using namespace std;

int main(){
    ios::sync_with_stdio(0); 
    cin.tie(0);

    string s;
    cin >> s;

    map<char,int> letters;
    for(char &c : s){
        letters[c]++;
    }

    int odd = 0, even = 0;
    for(auto p: letters){
        if(p.second % 2){
            odd++;
        } else {
            even++;
        }

        if(odd != 0 && even != 0){
            cout << "1/0\n";
            return 0;
        }
    }

    if(even){
        cout << "0\n";
    } else {
        cout << "1\n";
    }

    return 0;
}