from itertools import permutations

def is_valid(data):
    return (
        data[0] == data[1] + data[2] + data[3]
        and data[4] == 3 * data[1] + data[2]
        )



def main():
    data = map(int, input().split())

    scores = permutations(data)

    for score in scores:
        if is_valid(score):
            print(f"{score[0]} {score[1]} {score[2]} {score[3]} {score[4]}")
            break


if __name__ == "__main__":
    main()
