#include <bits/stdc++.h>

#define all(x) x.begin(), x.end()

using namespace std;

using ll = long long;

int main()
{
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    int n, J = 0, M = 0;
    
    vector<int> usables(14, 4);
    cin >> n;

    int m1, m2, j1, j2;

    cin >> j1 >> j2;
    J += j1 > 10 ? 10 : j1;
    J += j2 > 10 ? 10 : j2;
    usables[j1]--; usables[j2]--; 

    cin >> m1 >> m2;
    M += m1 > 10 ? 10 : m1;
    M += m2 > 10 ? 10 : m2;
    usables[m1]--; usables[m2]--; 

    for(int i=0; i<n; ++i){
        int x;
        cin >> x;
        J += x > 10 ? 10 : x;
        M += x > 10 ? 10 : x;
        usables[x]--;
    }

    for(int i=1; i<14; ++i){
        if(usables[i] > 0){
            int currM = M; 
            currM += i > 10 ? 10 : i;
            int currJ = J; 
            currJ += i > 10 ? 10 : i;
            if(currM == 23 || (currJ > 23 && currM < 23)){
                cout << i << '\n';
                return 0;
            }
        }
    }

    cout << -1 << '\n';

    return 0;
}