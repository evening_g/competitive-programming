#include <bits/stdc++.h>

#define all(x) x.begin(), x.end()

using namespace std;

using ll = long long;

int bsearch(int l, int r, int target) {
    int m = (l+r)/2;
    if(m == target){
        return 0;
    } else if (target > m) {
        return 1 + bsearch(m, r, target);
    } else {
        return 1 + bsearch(l, m, target);
    }
}

int main()
{
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    int n, x, y;
    cin >> n >> x >> y;

    int total_x = bsearch(0, pow(2, n), x);
    int total_y = bsearch(0, pow(2, n), y);

    cout << max(total_x, total_y) << '\n';

    return 0;
}