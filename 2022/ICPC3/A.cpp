#include <bits/stdc++.h>

#define all(x) x.begin(), x.end()

using namespace std;

using ll = long long;

int main()
{
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    int m, total = 0, count = 0;
    string str;

    cin >> m >> str;


    for (char c : str) {
        if (c  == 'a')
        {
            count ++;
        } else {
            if (count >= 2) {
                total += count;
            }
            count = 0;
        }
    }

    if (count >= 2) {
        total += count;
    }

    cout << total << '\n';

    return 0;
}