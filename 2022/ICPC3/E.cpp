#include <bits/stdc++.h>

#define all(x) x.begin(), x.end()

using namespace std;

using ll = long long;

int main()
{
    cin.tie(nullptr);
    cin.sync_with_stdio(false);

    int n, x;
    cin >> n;

    map<int,int> prev;
    for(int i=0; i<n; ++i){
        cin >> x;

        if (prev.find(x+1) != prev.end() && prev[x+1] > 0) // padre existe
        {
            prev[x + 1] --; // quitamos el padre
        }

        prev[x] ++;     // aumenta hijo
    }
    
    int arrows = 0;
    for(auto &p : prev){
        arrows += p.second;
    }

    cout <<  arrows << '\n';
    return 0;
}