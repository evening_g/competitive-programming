#include <bits/stdc++.h>
using namespace std;

#define MAX 50001

int main(){
    ios::sync_with_stdio(0); 
    cin.tie(0);
    
    int k, p;
    cin >> k >> p;

    vector<int> presentations(p);
    for(int i=0; i<p; ++i){
        cin>>presentations[i];
    }
    
    int value[MAX];
    value[0] = 0;
    for (int x = 1; x <= MAX; x++) {
        value[x] = MAX+1;
        for (auto &p : presentations) {
            if (x-p >= 0) {
                value[x] = min(value[x], value[x-p]+1);
            }
        }
    }

    for(int i=0; i<k; ++i){
        int dinner;
        cin >> dinner;

        int ans = 0;
        if(value[dinner] == MAX+1){
            int next = dinner+1, prev = dinner-1;
            while(next < MAX && value[next] == MAX+1) next++;
            while(prev > 0 && value[prev] == MAX+1) prev--;
            //cout << "next: " << next << " - prev: " << prev << '\n';

            if(prev == 0){
                ans = next-dinner;
            } else if (next == MAX){
                ans = dinner-prev;
            } else {
                ans = dinner-prev < next-dinner ? dinner-prev : next-dinner;
            }
        }

        cout << ans << '\n';
    }
   
    return 0;
}
