/*
date: 2022-05-14
*/

#include<iostream>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);   

    int perimeter;
    cin>>perimeter;

    perimeter /= 2;

    int x = perimeter / 2;
    int area = perimeter * x - x * x;

    cout<<area<<'\n';
       
    return 0;
}