/*
date: 2022-05-14
*/

#include<bits/stdc++.h>

using namespace std;

int solve(int n, int *sizes, int start, int end) {
    for (size_t i = start; i < end; i++) {
        if (n == 0) {
            return 0;
        }

        if (n > sizes[i]) {
            n %= sizes[i];
        } else {
            n = sizes[i] - n;
        }
    }

    return n;
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);   

    // VARIABLES
    int k, p, b, n, best;

    // READ
    cin>>k>>p;
    int sizes [p];

    // read bags' sizes
    for (size_t i = 0; i < p; i++) {
        cin>>sizes[i];
    }
    
    sort(sizes, sizes+p, std::greater<int>());

    // solve
    while (k--) {
        cin>>n;

        best = 5e4 + 1;

        for (size_t i = 0; i < p; i++) {
            best = min(best, solve(n, sizes, i, p));
        }

        cout<<best<<"\n";
    }

    return 0;
}