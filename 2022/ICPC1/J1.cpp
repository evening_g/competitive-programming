/*
date: 2022-05-14
*/

#include<bits/stdc++.h>

using namespace std;

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);   

    int n, m, k, i, unsold;
    bool bought;
    
    cin>>n>>m;

    bool sold [m] = {false};
    unsold = m;

    while (n--) {
        cin>>k;
        bought = false; // this person hasn't bought

        while (k--) {
            cin>>i;

            // sells this company if it has not been sold
            // and this person has not bought anything
            if (!sold[i-1] and !bought) {
                sold[i-1] = true;
                unsold --;
                bought = true;
            }
        }
    }

    cout<<unsold<<'\n';
       
    return 0;
}