/*
date: 2022-05-14
*/

#include<bits/stdc++.h>

using namespace std;

using ll = long long;

ll fact(ll n) {
    ll result;
    for (int i = result = 1; i <= n; result*=i++);
    return result;    
}

int main() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);   

    vector<vector<int>> data;
    vector<int> candidates;

    int n, m = 0, k, i, unsold;
    
    cin>>n>>m;

    // READ DATA
    while (n--) {
        cin>>k;
        candidates.clear();

        while (k--) {
            cin>>i;
            candidates.push_back(i-1);
        }

        data.push_back(candidates);
    }

    // Combine data
    int prev_unsold = m;
    int limit = fact(n);
    
    sort(data.begin(), data.end());

    for (int i = 0; i < limit; i++) {
        bool sold [m] = {false};
        unsold = m;

        for (auto &person : data) {
            for (int &companie : person) {
                if (!sold[companie]) {
                    sold[companie] = true;
                    unsold --;
                    break;
                }
            }
        }

        if (unsold == 0) {
            cout<<"0\n";
            return 0;
        }

        if (unsold < prev_unsold) {
            prev_unsold = unsold;
        }

        next_permutation(data.begin(), data.end());
    }

    cout<<prev_unsold<<'\n';

    return 0;
}
